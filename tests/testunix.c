/*
 * GNetwork: tests/testunix.c
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnetwork/gnetwork.h>
#include <string.h>

#include <glib/gi18n.h>


#define FILENAME  "testunix.socket"


static GMainLoop *mainloop = NULL;

static GNetworkUnixServer * server = NULL;
static GNetworkUnixConnection * client = NULL;
static GNetworkUnixConnection * server_cxn = NULL;

#define COMMAND1  "<?xml encoding=\"UTF-8\"?><request>some-request</request>\n"
#define REPLY1    "<?xml encoding=\"UTF-8\"?><reply>some-reply</reply>\n"


static void
client_cxn_error_cb (GNetworkConnection * cxn, GError * error, gpointer user_data)
{
  g_print ("Client Connection: Error:\n\tDomain\t= %s\n\tCode\t= %d\n\tMessage\t= %s\n",
	   g_quark_to_string (error->domain), error->code, error->message);
}


static void
client_cxn_received_cb (GNetworkConnection * cxn, gconstpointer data, gulong length,
			gpointer user_data)
{
  g_print ("Client Connection: Received: %lu bytes\n\"%s\"\n", length, (gchar *) data);

  if (strcmp (data, REPLY1) == 0)
    {
      gnetwork_connection_close (cxn);
    }
}


static void
client_cxn_sent_cb (GNetworkConnection * cxn, gconstpointer data, gulong length,
		    gpointer user_data)
{
  g_print ("Client Connection: Sent: %lu bytes\n\"%s\"\n", length, (gchar *) data);
}


static void
client_cxn_notify_unix_status_cb (GObject *cxn, GParamSpec *pspec, gpointer user_data)
{
  GNetworkUnixConnectionStatus status;

  g_object_get (cxn, "unix-status", &status, NULL);

  switch (status)
    {
    case GNETWORK_UNIX_CONNECTION_CLOSING:
      g_print ("Client Connection: Shutting down...\n");
      break;
    case GNETWORK_UNIX_CONNECTION_CLOSED:
      g_print ("Client Connection: Shut down to \"" FILENAME "\".\n");
      g_object_unref (client);

      if (server != NULL)
	gnetwork_server_close (GNETWORK_SERVER (server));
      break;
    case GNETWORK_UNIX_CONNECTION_OPENING:
      g_print ("Client Connection: Opening...\n");
      break;
    case GNETWORK_UNIX_CONNECTION_AUTHENTICATING:
      g_print ("Client Connection: Authenticating credentials...\n");
      break;
    case GNETWORK_UNIX_CONNECTION_OPEN:
      g_print ("Client connection: Open to \"" FILENAME "\".\n");
      gnetwork_connection_send (GNETWORK_CONNECTION (client), COMMAND1, -1);
      break;
    }
}


static void
server_notify_status_cb (GObject * object, GParamSpec *pspec, gpointer user_data)
{
  GNetworkServerStatus status;

  g_object_get (object, "status", &status, NULL);

  switch (status)
    {
    case GNETWORK_SERVER_CLOSING:
      g_print ("Server: Shutting down...\n");
      break;
    case GNETWORK_SERVER_CLOSED:
      g_print ("Server: Shut down.\n");
      g_object_unref (server);
      g_print ("Quitting %s in 10 seconds... ", g_get_application_name ());
      g_timeout_add (10000, (GSourceFunc) g_main_loop_quit, mainloop);
      break;
    case GNETWORK_SERVER_OPENING:
      g_print ("Server: Opening...\n");
      break;
    case GNETWORK_SERVER_OPEN:
      g_print ("Server: Open, creating client connection... ");
      client = g_object_new (GNETWORK_TYPE_UNIX_CONNECTION, "filename", FILENAME, NULL);
      g_object_add_weak_pointer (G_OBJECT (client), (gpointer *) &client);
      g_signal_connect_object (client, "received", G_CALLBACK (client_cxn_received_cb), NULL, 0);
      g_signal_connect_object (client, "sent", G_CALLBACK (client_cxn_sent_cb), NULL, 0);
      g_signal_connect_object (client, "error", G_CALLBACK (client_cxn_error_cb), NULL, 0);
      g_signal_connect_object (client, "notify::unix-status",
			G_CALLBACK (client_cxn_notify_unix_status_cb), NULL, 0);
      g_print ("Done (%p).\n", client);
      gnetwork_connection_open (GNETWORK_CONNECTION (client));
      break;
    }
}

static void
server_cxn_error_cb (GNetworkConnection * cxn, GError * error, gpointer user_data)
{
  g_print ("Server Connection: Error:\n\tDomain\t= %s\n\tCode\t= %d\n\tMessage\t= %s\n",
	   g_quark_to_string (error->domain), error->code, error->message);
}


static void
server_cxn_received_cb (GNetworkConnection * cxn, gconstpointer data, gulong length,
			gpointer user_data)
{
  g_print ("Server Connection: Received: %lu bytes\n\"%s\"\n", length, (gchar *) data);

  if (strcmp (data, COMMAND1) == 0)
    gnetwork_connection_send (cxn, REPLY1, -1);
  else
    gnetwork_connection_close (cxn);

  g_message ("Sent reply.");
}


static void
server_cxn_sent_cb (GNetworkConnection * cxn, gconstpointer data, gulong length,
		    gpointer user_data)
{
  g_print ("Server Connection: Sent: %lu bytes\n\"%s\"\n", length, (gchar *) data);
}


static void
server_cxn_notify_unix_status_cb (GNetworkTcpConnection *cxn, GParamSpec *pspec, gpointer user_data)
{
  GNetworkUnixConnectionStatus status;

  g_object_get (cxn, "unix-status", &status, NULL);

  switch (status)
    {
    case GNETWORK_UNIX_CONNECTION_CLOSING:
      g_print ("Server Connection: Shutting down...\n");
      break;
    case GNETWORK_UNIX_CONNECTION_CLOSED:
      g_print ("Server Connection: Shut down\n");
      break;
    case GNETWORK_UNIX_CONNECTION_OPENING:
      g_print ("Server Connection: Opening...\n");
      break;
    case GNETWORK_UNIX_CONNECTION_AUTHENTICATING:
      g_print ("Server Connection: Authenticating unix credentials...\n");
      break;
    case GNETWORK_UNIX_CONNECTION_OPEN:
      /* We're connected. */
      g_print ("Server connection: Open.\n");
      break;
    }
}


static void
server_incoming_cb (GNetworkServer * svr, GNetworkConnection * svr_cxn, gpointer user_data)
{
  gchar *filename;

  server_cxn = g_object_ref (svr_cxn);
  g_object_add_weak_pointer (G_OBJECT (svr_cxn), (gpointer *) &server_cxn);
  g_object_get (svr_cxn, "filename", &filename, NULL);
  g_print ("Server: Incoming connection:\n\tFilename:\t%s\n", filename);
  g_free (filename);

  g_signal_connect_object (svr_cxn, "received", G_CALLBACK (server_cxn_received_cb), svr, 0);
  g_signal_connect_object (svr_cxn, "sent", G_CALLBACK (server_cxn_sent_cb), svr, 0);
  g_signal_connect_object (svr_cxn, "error", G_CALLBACK (server_cxn_error_cb), svr, 0);
  g_signal_connect_object (svr_cxn, "notify::unix-status",
			   G_CALLBACK (server_cxn_notify_unix_status_cb), svr, 0);
}


static void
server_error_cb (GNetworkServer * svr, GError * error, gpointer user_data)
{
  g_print ("Server: Error:\n\tDomain:\t%s\n\tCode:\t%d\n\tMessage:\t%s\n",
	   g_quark_to_string (error->domain), error->code, error->message);
}


static gboolean
start_server (gpointer user_data)
{
  g_print ("Server: Creating Server (%s)...", FILENAME);
  server = g_object_new (GNETWORK_TYPE_UNIX_SERVER, "filename", FILENAME,
			 "max-connections", 1, NULL);
  g_object_add_weak_pointer (G_OBJECT (server), (gpointer *) &server);
  g_signal_connect_object (server, "notify::status", G_CALLBACK (server_notify_status_cb), NULL, 0);
  g_signal_connect_object (server, "new-connection", G_CALLBACK (server_incoming_cb), NULL, 0);
  g_signal_connect_object (server, "error", G_CALLBACK (server_error_cb), NULL, 0);
  g_print (" Done.\n");
  gnetwork_server_open (GNETWORK_SERVER (server));

  return FALSE;
}


int
main (int argc, gchar * argv[])
{
  if (!g_thread_supported ())
    g_thread_init (NULL);

  g_set_application_name ("GNetwork Unix Server/Connection Test");

  g_type_init ();

  mainloop = g_main_loop_new (g_main_context_default (), FALSE);

  g_idle_add (start_server, NULL);
  g_main_loop_run (mainloop);
  g_main_loop_unref (mainloop);
  mainloop = NULL;
  g_print ("done.\nclient = %p, server = %p, server_cxn = %p\n", client, server, server_cxn);

  return 0;
}
