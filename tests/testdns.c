/* 
 * GNetwork Library: tests/testdns.c
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnetwork/gnetwork.h>

static GMainLoop *loop;


static void
dns_cb (const GSList * list, const GError * error, gpointer data)
{
  static guint n_lookups = 0;

  if (error != NULL)
    {
      g_print ("Error finding \"%s\":\n  Domain\t= %s\n  Code\t\t= %d\n  Message\t= %s\n",
	       (gchar *) data, g_quark_to_string (error->domain), error->code, error->message);
    }
  else
    {
      guint i;

      g_print ("DNS Return for \"%s\":\n", (gchar *) data);
      for (i = 1; list != NULL; list = list->next, i++)
	{
	  gchar *str;

	  g_print ("  Hostname   %u\t= %s\n", i, gnetwork_dns_entry_get_hostname (list->data));
	  str = gnetwork_ip_address_to_string (gnetwork_dns_entry_get_ip_address (list->data));
	  g_print ("  IP Address %u\t= %s\n", i, str);
	  g_free (str);
	}
    }
  g_print ("\n");

  n_lookups++;

  if (n_lookups > 5)
    g_main_loop_quit (loop);
}


static gboolean
do_lookups (gpointer data)
{
  g_print ("Finding www.google.com...\n");
  gnetwork_dns_get ("www.google.com", dns_cb, "www.google.com", NULL);
  g_print ("Finding ignore-your.tv...\n");
  gnetwork_dns_get ("ignore-your.tv", dns_cb, "ignore-your.tv", NULL);
  g_print ("Finding irc.gnome.org...\n");
  gnetwork_dns_get ("irc.gnome.org", dns_cb, "irc.gnome.org", NULL);
  g_print ("Finding nonexistant...\n");
  gnetwork_dns_get ("nonexistant", dns_cb, "nonexistant", NULL);
  g_print ("Finding ::1...\n");
  gnetwork_dns_get ("::1", dns_cb, "::1", NULL);
  g_print ("Finding 127.0.0.1...\n");
  gnetwork_dns_get ("127.0.0.1", dns_cb, "127.0.0.1", NULL);

  return FALSE;
}


int
main (gint argc, gchar * argv[])
{
  g_type_init ();

  g_idle_add (do_lookups, NULL);

  loop = g_main_loop_new (g_main_context_default (), FALSE);
  g_main_loop_run (loop);

  return 0;
}
