/*
 * GNetwork: tests/testudp.c
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnetwork/gnetwork.h>
#include <string.h>

#include <glib/gi18n.h>


#define HOST	"::1"
#define PORT	((guint) ('g' + 'n' + 'e' + 't' + 'w' + 'o' + 'r' + 'k' + 'u' + 'd' + 'p' + 1024))


static GMainLoop *mainloop = NULL;

static GNetworkUdpDatagram *server = NULL;
static GNetworkUdpDatagram *client = NULL;

#define COMMAND1  "Test message via UDP.\n"
#define COMMAND2  "Another test message via UDP.\n"

#define REPLY1    "Test message received, getting http://www.gnome.org/...\n"
#define REPLY2	  "Another test message received...\n"


static void
client_error_cb (GNetworkConnection * cxn, GValue * info, GError * error, gpointer user_data)
{
  g_print ("Client: Error:\n\tDomain\t= %s\n\tCode\t= %d\n\tMessage\t= %s\n",
	   g_quark_to_string (error->domain), error->code, error->message);
}


static void
client_received_cb (GNetworkDatagram * cln, GValue * info, gconstpointer data, gulong length,
		    gpointer user_data)
{
  g_print ("Client Connection: Received: %lu bytes\n\"%s\"\n", length, (gchar *) data);

  if (strcmp (data, REPLY1) == 0)
    gnetwork_datagram_send (cln, info, COMMAND2, -1);
  else if (strcmp (data, REPLY2) == 0)
    gnetwork_datagram_close (cln);
}


static void
client_sent_cb (GNetworkDatagram * cln, GValue * info, gconstpointer data, gulong length,
		gpointer user_data)
{
  g_print ("Client: Sent: %lu bytes\n\"%s\"\n", length, (gchar *) data);
}


static void
client_notify_status_cb (GObject * cxn, GParamSpec * pspec, gpointer user_data)
{
  GNetworkDatagramStatus status;

  g_object_get (cxn, "status", &status, NULL);

  switch (status)
    {
    case GNETWORK_DATAGRAM_CLOSING:
      g_print ("Client: Shutting down...\n");
      break;
    case GNETWORK_DATAGRAM_CLOSED:
      g_print ("Client: Shut down\n");
      g_object_unref (client);
      gnetwork_datagram_close (GNETWORK_DATAGRAM (server));
      break;
    case GNETWORK_DATAGRAM_OPENING:
      g_print ("Client: Opening...\n");
      break;
    case GNETWORK_DATAGRAM_OPEN:
      g_print ("Client: Open.\n");
      gnetwork_udp_datagram_send_to (client, HOST, PORT, COMMAND1, -1);
      break;
    }
}


static void
server_notify_status_cb (GObject * object, GParamSpec * pspec, gpointer user_data)
{
  GNetworkServerStatus status;

  g_object_get (object, "status", &status, NULL);

  switch (status)
    {
    case GNETWORK_DATAGRAM_CLOSING:
      g_print ("Server: Shutting down...\n");
      break;
    case GNETWORK_DATAGRAM_CLOSED:
      g_print ("Server: Shut down, quitting %s... ", g_get_application_name ());
      g_object_unref (server);
      g_main_loop_quit (mainloop);
      break;
    case GNETWORK_DATAGRAM_OPENING:
      g_print ("Server: Opening...\n");
      break;
    case GNETWORK_DATAGRAM_OPEN:
      g_print ("Server: Open, creating client... ");
      client = g_object_new (GNETWORK_TYPE_UDP_DATAGRAM, "interface", "lo", NULL, 0);
      g_signal_connect_object (client, "received", G_CALLBACK (client_received_cb), NULL, 0);
      g_signal_connect_object (client, "sent", G_CALLBACK (client_sent_cb), NULL, 0);
      g_signal_connect_object (client, "error", G_CALLBACK (client_error_cb), NULL, 0);
      g_signal_connect_object (client, "notify::status", G_CALLBACK (client_notify_status_cb), NULL, 0);
      g_print ("Done.\n");
      gnetwork_datagram_open (GNETWORK_DATAGRAM (client));
      break;
    }
}


static void
server_received_cb (GNetworkDatagram * svr, GValue * info, gconstpointer data, gulong length,
		    gpointer user_data)
{
  g_print ("Server: Received: %lu bytes\n\"%s\"\n", length, (gchar *) data);

  if (strcmp (data, COMMAND1) == 0)
    gnetwork_datagram_send (svr, info, REPLY1, -1);
  else if (strcmp (data, COMMAND2) == 0)
    gnetwork_datagram_send (svr, info, REPLY2, -1);
}


static void
server_sent_cb (GNetworkDatagram * svr, GValue * info, gconstpointer data, gulong length,
		gpointer user_data)
{
  g_print ("Server: Sent: %lu bytes\n\"%s\"\n", length, (gchar *) data);
}


static void
server_error_cb (GNetworkDatagram * svr, const GValue * info, GError * error, gpointer user_data)
{
  g_print ("Server: Error:\n\tDomain:\t%s\n\tCode:\t%d\n\tMessage:\t%s\n",
	   g_quark_to_string (error->domain), error->code, error->message);
}


static gboolean
start_server (gpointer user_data)
{
  g_print ("Server: Creating Server (%s:%u)...", HOST, PORT);
  server = g_object_new (GNETWORK_TYPE_UDP_DATAGRAM, "port", PORT, "interface", "lo", NULL, 0);
  g_signal_connect_object (server, "notify::status", G_CALLBACK (server_notify_status_cb), NULL, 0);
  g_signal_connect_object (server, "received", G_CALLBACK (server_received_cb), NULL, 0);
  g_signal_connect_object (server, "sent", G_CALLBACK (server_sent_cb), NULL, 0);
  g_signal_connect_object (server, "error", G_CALLBACK (server_error_cb), NULL, 0);
  g_print (" Done.\n");
  gnetwork_datagram_open (GNETWORK_DATAGRAM (server));
  return FALSE;
}


int
main (int argc, gchar * argv[])
{
  if (!g_thread_supported ())
    g_thread_init (NULL);

  g_set_application_name ("GNetwork UDP Datagram Test");

  g_type_init ();

  mainloop = g_main_loop_new (g_main_context_default (), FALSE);

  g_idle_add (start_server, NULL);
  g_main_loop_run (mainloop);
  g_print ("Done.\n");

  return 0;
}
