/* 
 * GNetwork Library: tests/testinterfaces.c
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnetwork/gnetwork.h>
#include <string.h>


static gchar *
protocols_to_string (GNetworkProtocols protocols)
{
  gchar *retval, *tmp;

  retval = NULL;
  if (protocols & GNETWORK_PROTOCOL_PACKET)
    {
      retval = g_strdup ("Ethernet");
    }

  if (protocols & GNETWORK_PROTOCOL_IPv4)
    {
      tmp = retval;
      retval = g_strconcat ((tmp != NULL ? "IPv4 " : "IPv4"), tmp, NULL);
      g_free (tmp);
    }

  if (protocols & GNETWORK_PROTOCOL_IPv6)
    {
      tmp = retval;
      retval = g_strconcat ((tmp != NULL ? "IPv6 " : "IPv6"), tmp, NULL);
      g_free (tmp);
    }

  return retval;
}


static gchar *
flags_to_string (GNetworkInterfaceFlags flags)
{
  gchar *ptr;
  gchar data[1024] = { 0 };

  ptr = data;
  if (flags & GNETWORK_INTERFACE_IS_UP)
    {
      memcpy (ptr, "Up ", 3);
      ptr += 3;
    }

  if (flags & GNETWORK_INTERFACE_IS_RUNNING)
    {
      memcpy (ptr, "Running ", 8);
      ptr += 8;
    }

  if (flags & GNETWORK_INTERFACE_IS_DEBUGGING)
    {
      memcpy (ptr, "Debugging ", 10);
      ptr += 10;
    }

  if (flags & GNETWORK_INTERFACE_IS_LOOPBACK)
    {
      memcpy (ptr, "Loopback ", 9);
      ptr += 9;
    }

  if (flags & GNETWORK_INTERFACE_IS_POINT_TO_POINT)
    {
      memcpy (ptr, "Point-to-point ", 15);
      ptr += 15;
    }

  if (flags & GNETWORK_INTERFACE_IS_LOAD_MASTER)
    {
      memcpy (ptr, "Load-master ", 12);
      ptr += 12;
    }

  if (flags & GNETWORK_INTERFACE_IS_LOAD_SLAVE)
    {
      memcpy (ptr, "Load-slave ", 11);
      ptr += 11;
    }

  if (flags & GNETWORK_INTERFACE_NO_TRAILERS)
    {
      memcpy (ptr, "No-trailers ", 12);
      ptr += 12;
    }

  if (flags & GNETWORK_INTERFACE_NO_ARP)
    {
      memcpy (ptr, "No-ARP ", 7);
      ptr += 7;
    }

  if (flags & GNETWORK_INTERFACE_RECV_ALL_PACKETS)
    {
      memcpy (ptr, "All-packets ", 12);
      ptr += 12;
    }

  if (flags & GNETWORK_INTERFACE_RECV_ALL_MULTICAST)
    {
      memcpy (ptr, "All-multicast ", 14);
      ptr += 14;
    }

  if (flags & GNETWORK_INTERFACE_CAN_BROADCAST)
    {
      memcpy (ptr, "Broadcast ", 10);
      ptr += 10;
    }

  if (flags & GNETWORK_INTERFACE_CAN_MULTICAST)
    {
      memcpy (ptr, "Multicast ", 10);
      ptr += 10;
    }

  if (flags & GNETWORK_INTERFACE_CAN_SET_MEDIA)
    {
      memcpy (ptr, "Media-select ", 13);
      ptr += 13;
    }

  if (flags & GNETWORK_INTERFACE_AUTOSELECTED_MEDIA)
    {
      memcpy (ptr, "Media-autoselect ", 17);
      ptr += 17;
    }

  return g_strdup (data);
}


int
main (int argc, char *argv[])
{
  GSList *interfaces;
  guint i;

  g_type_init ();

  g_print ("Configured Interfaces:\n");
  for (interfaces = gnetwork_interface_get_all_interfaces (), i = 0; interfaces != NULL;
       interfaces = g_slist_remove_link (interfaces, interfaces), i++)
    {
      gchar *str = NULL;
      G_CONST_RETURN GSList *multicasts;

      g_print ("\nInterface %u:\n", i);
      str = gnetwork_interface_info_get_name (interfaces->data);
      g_print ("  Name\t\t\t= %s\n", str);
      g_free (str);

      str = protocols_to_string (gnetwork_interface_info_get_protocols (interfaces->data));
      g_print ("  Protocols\t\t= %s\n", (str != NULL ? str : "None"));
      g_free (str);

      str = flags_to_string (gnetwork_interface_info_get_flags (interfaces->data));
      g_print ("  Flags\t\t\t= %s\n", str);
      g_free (str);

      /* HW */
      g_print ("  Hardware Address\t= %s\n",
	       (gchar *) gnetwork_interface_info_get_address (interfaces->data,
							      GNETWORK_PROTOCOL_PACKET));

      g_print ("  Hardware Broadcast\t= %s\n",
	       (gchar *) gnetwork_interface_info_get_broadcast_address (interfaces->data,
									GNETWORK_PROTOCOL_PACKET));

      /* IPv4 */
      str = gnetwork_ip_address_to_string (gnetwork_interface_info_get_address (interfaces->data,
										GNETWORK_PROTOCOL_IPv4));
      g_print ("  IPv4 Address\t\t= %s\n", str);
      g_free (str);

      str = gnetwork_ip_address_to_string (gnetwork_interface_info_get_broadcast_address (interfaces->data,
										GNETWORK_PROTOCOL_IPv4));
      g_print ("  IPv4 Broadcast\t= %s\n", str);
      g_free (str);

      str = gnetwork_ip_address_to_string (gnetwork_interface_info_get_netmask (interfaces->data,
										GNETWORK_PROTOCOL_IPv4));
      g_print ("  IPv4 Netmask\t\t= %s\n", str);
      g_free (str);

      multicasts = gnetwork_interface_info_get_multicasts (interfaces->data,
							   GNETWORK_PROTOCOL_IPv4);
      if (multicasts != NULL)
	{
	  g_print ("  IPv4 Multicasts\t=");
	  for (; multicasts != NULL; multicasts = multicasts->next)
	    {
	      str = gnetwork_ip_address_to_string (multicasts->data);
	      g_print (" %s", str);
	      g_free (str);
	    }
	  g_print ("\n");
	}

      /* IPv6 */
      str = gnetwork_ip_address_to_string (gnetwork_interface_info_get_address (interfaces->data,
										GNETWORK_PROTOCOL_IPv6));
      g_print ("  IPv6 Address\t\t= %s\n", str);
      g_free (str);

      str = gnetwork_ip_address_to_string (gnetwork_interface_info_get_netmask (interfaces->data,
										GNETWORK_PROTOCOL_IPv6));
      g_print ("  IPv6 Netmask\t\t= %s\n", str);
      g_free (str);

      multicasts = gnetwork_interface_info_get_multicasts (interfaces->data,
							   GNETWORK_PROTOCOL_IPv6);
      if (multicasts != NULL)
	{
	  g_print ("  IPv6 Multicasts\t= ");
	  for (; multicasts != NULL; multicasts = multicasts->next)
	    {
	      str = gnetwork_ip_address_to_string (multicasts->data);
	      g_print ("%s%s", str, (multicasts->next != NULL ? ", " : "\n"));
	      g_free (str);
	    }
	}

      gnetwork_interface_info_unref (interfaces->data);
    }

  return 0;
}
