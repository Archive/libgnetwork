/*
 * GNetwork Library: tests/gnetwork-demo.c
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gtk/gtk.h>
#include <libgnetwork/gnetwork.h>
#include <glib/gi18n.h>


typedef struct {
  gchar *address;
  guint port;

  GtkWidget *tab_label;
  GtkWidget *page_box;
  GtkTextBuffer *buffer;
  GtkWidget *entry;
  GtkWidget *send_btn;

  GMainLoop *loop;
  GMainContext *context;
  GNetworkTcpConnection *cxn;
}
ConnectionPage;


static GtkWidget *addr_entry = NULL;
static GtkWidget *spinbtn = NULL;
static GtkWidget *notebook = NULL;
static GtkTextTagTable *table = NULL;

static GSList *pages = NULL;


static void
connection_page_free (ConnectionPage * page)
{
  gchar *tmp;

  g_object_unref (page->cxn);

  g_free (page->address);
  page->address = NULL;

  g_main_loop_unref (page->loop);
  page->loop = NULL;

  tmp = g_strdup_printf ("(%s)", gtk_label_get_text (GTK_LABEL (page->tab_label)));
  gtk_label_set_text (GTK_LABEL (page->tab_label), tmp);
  g_free (tmp);
  gtk_widget_set_sensitive (page->tab_label, FALSE);
}


static void
received_cb (GNetworkConnection * connection, gconstpointer data, gulong length, ConnectionPage * page)
{
  GtkTextIter iter;

  GDK_THREADS_ENTER ();

  gtk_text_buffer_get_end_iter (page->buffer, &iter);
  gtk_text_buffer_insert_with_tags_by_name (page->buffer, &iter, data, length, "received", NULL);
  
  GDK_THREADS_LEAVE ();
}


static void
sent_cb (GNetworkConnection * connection, gconstpointer data, gulong length, ConnectionPage * page)
{
  GtkTextIter iter;

  GDK_THREADS_ENTER ();

  gtk_text_buffer_get_end_iter (page->buffer, &iter);
  gtk_text_buffer_insert_with_tags_by_name (page->buffer, &iter, data, length, "sent", NULL);
  
  GDK_THREADS_LEAVE ();
}


static void
notify_tcp_status_cb (GNetworkTcpConnection * connection, GParamSpec * pspec, ConnectionPage * page)
{
  GNetworkTcpConnectionStatus status;
  GtkTextIter iter;
  const gchar *str;
  gboolean sensitive = FALSE;

  g_object_get (connection, "tcp-status", &status, NULL);

  switch (status)
    {
    case GNETWORK_TCP_CONNECTION_CLOSING:
      str = _("Closing Connection...\n");
      break;
    case GNETWORK_TCP_CONNECTION_CLOSED:
      str = _("Connection Closed.\n");
      g_main_loop_quit (page->loop);
      break;
    case GNETWORK_TCP_CONNECTION_LOOKUP:
      str = _("Finding Host...\n");
      break;
    case GNETWORK_TCP_CONNECTION_OPENING:
      str = _("Opening Connection...\n");
      break;
    case GNETWORK_TCP_CONNECTION_PROXYING:
      str = _("Traversing Proxy Server...\n");
      break;
    case GNETWORK_TCP_CONNECTION_AUTHENTICATING:
      str = _("Authenticating SSL...\n");
      break;
    case GNETWORK_TCP_CONNECTION_OPEN:
      str = _("Connection Open...\n");
      sensitive = TRUE;
      break;
    }

  GDK_THREADS_ENTER ();

  gtk_widget_set_sensitive (page->entry, sensitive);
  gtk_widget_set_sensitive (page->send_btn, sensitive);

  gtk_text_buffer_get_end_iter (page->buffer, &iter);
  gtk_text_buffer_insert_with_tags_by_name (page->buffer, &iter, str, -1, "status", NULL);
  
  GDK_THREADS_LEAVE ();
}


static gpointer
connection_thread (ConnectionPage *page)
{
  GMainLoop *loop;

  g_signal_connect (page->cxn, "received", G_CALLBACK (received_cb), page);
  g_signal_connect (page->cxn, "sent", G_CALLBACK (sent_cb), page);
  g_signal_connect (page->cxn, "notify::tcp-status", G_CALLBACK (notify_tcp_status_cb), page);

  page->loop = g_main_loop_new (page->context, FALSE);

  gnetwork_connection_open (GNETWORK_CONNECTION (page->cxn));

  g_main_loop_run (page->loop);

  GDK_THREADS_ENTER ();
  connection_page_free (page);
  GDK_THREADS_LEAVE ();

  return NULL;
}


static void
page_entry_activate_cb (GtkEntry * entry, ConnectionPage * page)
{
  gnetwork_connection_send (GNETWORK_CONNECTION (page->cxn), gtk_entry_get_text (entry), -1);

  gtk_entry_set_text (entry, "");
}


static void
page_send_btn_clicked_cb (GtkButton * button, ConnectionPage * page)
{
  page_entry_activate_cb (GTK_ENTRY (page->entry), page);
}


static void
connect_clicked_cb (GtkButton * button, gpointer data)
{
  GtkWidget *tab_label, *hbox, *scrwin, *textview;
  ConnectionPage *page = NULL;
  GMainContext *context;
  GSList *list;

  GDK_THREADS_ENTER ();

  for (list = pages; list != NULL; list = list->next)
    {
      if (((ConnectionPage *) (list->data))->address == NULL)
	page = list->data;
    }

  if (page == NULL)
    {
      page = g_new0 (ConnectionPage, 1);

      page->address = g_strdup (gtk_entry_get_text (GTK_ENTRY (addr_entry)));
      page->port = (guint) gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (spinbtn));

      pages = g_slist_prepend (pages, page);

      page->tab_label = gtk_label_new (NULL);
      gtk_widget_show (page->tab_label);

      page->page_box = gtk_vbox_new (FALSE, 6);
      gtk_container_set_border_width (GTK_CONTAINER (page->page_box), 9);
      gtk_widget_show (page->page_box);

      scrwin = gtk_scrolled_window_new (NULL, NULL);
      gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
				      GTK_POLICY_AUTOMATIC);
      gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
      gtk_container_add (GTK_CONTAINER (page->page_box), scrwin);
      gtk_widget_show (scrwin);

      textview = gtk_text_view_new ();
      gtk_text_view_set_editable (GTK_TEXT_VIEW (textview), FALSE);
      gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (textview), GTK_WRAP_WORD_CHAR);
      gtk_container_add (GTK_CONTAINER (scrwin), textview);
      gtk_widget_show (textview);

      page->buffer = gtk_text_buffer_new (table);
      gtk_text_view_set_buffer (GTK_TEXT_VIEW (textview), page->buffer);

      hbox = gtk_hbox_new (FALSE, 6);
      gtk_box_pack_start (GTK_BOX (page->page_box), hbox, FALSE, FALSE, 0);
      gtk_widget_show (hbox);

      page->entry = gtk_entry_new ();
      g_signal_connect (page->entry, "activate", G_CALLBACK (page_entry_activate_cb), page);
      gtk_container_add (GTK_CONTAINER (hbox), page->entry);
      gtk_widget_set_sensitive (page->entry, FALSE);
      gtk_widget_show (page->entry);

      page->send_btn = gtk_button_new_with_label (_("Send"));
      g_signal_connect (page->send_btn, "clicked", G_CALLBACK (page_send_btn_clicked_cb), page);
      gtk_box_pack_start (GTK_BOX (hbox), page->send_btn, FALSE, FALSE, 0);
      gtk_widget_set_sensitive (page->send_btn, FALSE);
      gtk_widget_show (page->send_btn);

      page->context = g_main_context_new ();
      g_object_get (addr_entry, "text", &(page->address), NULL);
      gtk_entry_set_text (GTK_ENTRY (addr_entry), "");
      page->port = (guint) gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (spinbtn));

      gtk_widget_set_sensitive (page->entry, FALSE);
      gtk_widget_set_sensitive (page->send_btn, FALSE);
    }
  else
    {
      page->address = g_strdup (gtk_entry_get_text (GTK_ENTRY (addr_entry)));
      page->port = (guint) gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (spinbtn));

      gtk_widget_set_sensitive (page->tab_label, TRUE);
    }

  gtk_label_set_text (GTK_LABEL (page->tab_label), page->address);

  page->cxn = g_object_new (GNETWORK_TYPE_TCP_CONNECTION,
			    "context", page->context,
			    "address", page->address,
			    "port", page->port,
			    "ssl-enabled", FALSE, NULL);

  g_assert (gnetwork_thread_new ((GThreadFunc) connection_thread, page, NULL, page->context, NULL));

  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page->page_box, page->tab_label);

  GDK_THREADS_LEAVE ();
}


static void
response_cb (GtkDialog * dialog, gint response, gpointer user_data)
{
  gtk_main_quit ();
}


int
main (gint argc, gchar * argv[])
{
  GtkWidget *window, *label, *vbox, *hbox, *buffer, *entry, *button;
  GtkCellRenderer *cell;
  GtkTextTag *tag;
  guint i;

  gchar *tag_data[3][2] = {
    {"status", "#666666"},
    {"received", "#006600"},
    {"sent", "#aa0000"}
  };

  gtk_init (&argc, &argv);

  table = gtk_text_tag_table_new ();

  for (i = 0; i < G_N_ELEMENTS (tag_data); i++)
    {
      GtkTextTag *tag = g_object_new (GTK_TYPE_TEXT_TAG,
				      "name", tag_data[i][0], "foreground", tag_data[i][1], NULL);

      gtk_text_tag_table_add (table, tag);
    }

  window = gtk_dialog_new_with_buttons (_("GNetwork Library Demonstration"), NULL,
					GTK_DIALOG_NO_SEPARATOR, GTK_STOCK_CLOSE, GTK_RESPONSE_OK,
					NULL);
  g_signal_connect (window, "response", G_CALLBACK (response_cb), NULL);
  gtk_window_set_default_size (GTK_WINDOW (window), 550, 450);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  vbox = gtk_vbox_new (FALSE, 12);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (window)->vbox), vbox);
  gtk_widget_show (vbox);

  hbox = gtk_hbox_new (FALSE, 6);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show (hbox);

  label = gtk_label_new (_("Address:"));
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  addr_entry = gtk_entry_new ();
  gtk_container_add (GTK_CONTAINER (hbox), addr_entry);
  gtk_widget_show (addr_entry);

  label = gtk_label_new (_("Port:"));
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
  gtk_widget_show (label);

  spinbtn = gtk_spin_button_new (gtk_adjustment_new (80.0, 0.0, 65535.0, 1.0, 10.0, 10.0), 1.0, 0);
  gtk_box_pack_start (GTK_BOX (hbox), spinbtn, FALSE, FALSE, 0);
  gtk_widget_show (spinbtn);

  button = gtk_button_new_with_mnemonic (_("_Connect"));
  g_signal_connect (button, "clicked", G_CALLBACK (connect_clicked_cb), NULL);
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
  gtk_widget_show (button);

  notebook = gtk_notebook_new ();
  gtk_container_add (GTK_CONTAINER (vbox), notebook);
  gtk_widget_show (notebook);

  gtk_window_present (GTK_WINDOW (window));

  gtk_main ();

  return 0;
}
