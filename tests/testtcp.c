/*
 * GNetwork: tests/testtcp.c
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnetwork/gnetwork.h>
#include <string.h>

#include <glib/gi18n.h>


#define WEB_ADDRESS	"www.gnome.org"
#define WEB_PORT	80
#define HOST_ADDRESS	"127.0.0.1"
#define HOST_PORT	((guint) ('g' + 'n' + 'e' + 't' + 'w' + 'o' + 'r' + 'k' + 1024))


static GMainLoop *mainloop = NULL;

static GNetworkTcpServer * server = NULL;
static GNetworkTcpConnection * client = NULL;
static GNetworkTcpConnection * server_cxn = NULL;

#define COMMAND1  "GET /index.html HTTP/1.1\r\nHost: www.gnome.org\r\n\r\n"

#define REPLY1    "HTTP 500 Cannot get index.html if we're not a webserver, buddy :-).\r\n\r\n"


static void
client_cxn_error_cb (GNetworkConnection * cxn, GError * error, gpointer user_data)
{
  g_print ("Client Connection: Error:\n\tDomain\t= %s\n\tCode\t= %d\n\tMessage\t= %s\n",
	   g_quark_to_string (error->domain), error->code, error->message);
}


static void
client_cxn_received_cb (GNetworkConnection * cxn, gconstpointer data, gulong length,
			gpointer user_data)
{
  g_print ("Client Connection: Received: %lu bytes\n\"%s\"\n", length, (gchar *) data);

  if (strcmp (data, REPLY1) == 0)
    {
      gnetwork_connection_close (cxn);
    }
}


static void
client_cxn_sent_cb (GNetworkConnection * cxn, gconstpointer data, gulong length,
		    gpointer user_data)
{
  g_print ("Client Connection: Sent: %lu bytes\n\"%s\"\n", length, (gchar *) data);
}


static void
client_cxn_notify_tcp_status_cb (GObject *cxn, GParamSpec *pspec, gpointer user_data)
{
  GNetworkTcpConnectionStatus status;

  g_object_get (cxn, "tcp-status", &status, NULL);

  switch (status)
    {
    case GNETWORK_TCP_CONNECTION_CLOSING:
      g_print ("Client Connection: Shutting down...\n");
      break;
    case GNETWORK_TCP_CONNECTION_CLOSED:
      {
	gchar *address;

	g_object_get (client, "address", &address, NULL);

	g_print ("Client Connection: Shut down to \"%s\"\n", address);

	if (server != NULL)
	  gnetwork_server_close (GNETWORK_SERVER (server));

	if (strcmp (address, WEB_ADDRESS) == 0)
	  {
	    g_object_unref (client);
	    g_print ("Quitting %s in 10 seconds... ", g_get_application_name ());
	    g_timeout_add (10000, (GSourceFunc) g_main_loop_quit, mainloop);
	  }
	g_free (address);
      }
      break;
    case GNETWORK_TCP_CONNECTION_LOOKUP:
      g_print ("Client Connection: Finding host...\n");
      break;
    case GNETWORK_TCP_CONNECTION_OPENING:
      g_print ("Client Connection: Opening...\n");
      break;
    case GNETWORK_TCP_CONNECTION_PROXYING:
      g_print ("Client Connection: Proxying...\n");
      break;
    case GNETWORK_TCP_CONNECTION_AUTHENTICATING:
      g_print ("Client Connection: Authenticating SSL...\n");
      break;
    case GNETWORK_TCP_CONNECTION_OPEN:
      {
	GNetworkIpAddress *ipaddr;
	gchar *str;

	ipaddr = NULL;
	g_object_get (cxn, "local-address", &ipaddr, NULL);
	str = gnetwork_ip_address_to_string (ipaddr);
	g_free (ipaddr);
	g_print ("Client connection: Open at %s.\n", str);
	g_free (str);
	gnetwork_connection_send (GNETWORK_CONNECTION (client), COMMAND1, -1);
      }
      break;
    }
}


static void
server_notify_status_cb (GObject * object, GParamSpec *pspec, gpointer user_data)
{
  GNetworkServerStatus status;

  g_object_get (object, "status", &status, NULL);

  switch (status)
    {
    case GNETWORK_SERVER_CLOSING:
      g_print ("Server: Shutting down...\n");
      break;
    case GNETWORK_SERVER_CLOSED:
      g_print ("Server: Shut down.\n");
      g_object_set (client, "address", WEB_ADDRESS, "port", WEB_PORT,
		    "proxy-type", GNETWORK_TCP_PROXY_HTTP, NULL);
      gnetwork_connection_open (GNETWORK_CONNECTION (client));
      g_object_unref (server);
      break;
    case GNETWORK_SERVER_OPENING:
      g_print ("Server: Opening...\n");
      break;
    case GNETWORK_SERVER_OPEN:
      g_print ("Server: Open, creating client connection... ");
      client = g_object_new (GNETWORK_TYPE_TCP_CONNECTION, "address", HOST_ADDRESS,
			     "port", HOST_PORT, "proxy-type", GNETWORK_TCP_PROXY_NONE, NULL);
      g_object_add_weak_pointer (G_OBJECT (client), (gpointer *) &client);
      g_signal_connect (client, "received", G_CALLBACK (client_cxn_received_cb), NULL);
      g_signal_connect (client, "sent", G_CALLBACK (client_cxn_sent_cb), NULL);
      g_signal_connect (client, "error", G_CALLBACK (client_cxn_error_cb), NULL);
      g_signal_connect (client, "notify::tcp-status", G_CALLBACK (client_cxn_notify_tcp_status_cb),
			NULL);
      g_print ("Done (%p).\n", client);
      gnetwork_connection_open (GNETWORK_CONNECTION (client));
      break;
    }
}

static void
server_cxn_error_cb (GNetworkConnection * cxn, GError * error, gpointer user_data)
{
  g_print ("Server Connection: Error:\n\tDomain\t= %s\n\tCode\t= %d\n\tMessage\t= %s\n",
	   g_quark_to_string (error->domain), error->code, error->message);
}


static void
server_cxn_received_cb (GNetworkConnection * cxn, gconstpointer data, gulong length,
			gpointer user_data)
{
  g_print ("Server Connection: Received: %lu bytes\n\"%s\"\n", length, (gchar *) data);

  if (strcmp (data, COMMAND1) == 0)
    gnetwork_connection_send (cxn, REPLY1, -1);
  else
    gnetwork_connection_close (cxn);
}


static void
server_cxn_sent_cb (GNetworkConnection * cxn, gconstpointer data, gulong length,
		    gpointer user_data)
{
  g_print ("Server Connection: Sent: %lu bytes\n\"%s\"\n", length, (gchar *) data);
}


static void
server_cxn_notify_tcp_status_cb (GNetworkTcpConnection *cxn, GParamSpec *pspec, gpointer user_data)
{
  GNetworkTcpConnectionStatus status;

  g_object_get (cxn, "tcp-status", &status, NULL);

  switch (status)
    {
    case GNETWORK_TCP_CONNECTION_CLOSING:
      g_print ("Server Connection: Shutting down...\n");
      break;
    case GNETWORK_TCP_CONNECTION_CLOSED:
      g_print ("Server Connection: Shut down\n");
      break;
    case GNETWORK_TCP_CONNECTION_LOOKUP:
      g_print ("Server Connection: Finding host...\n");
      break;
    case GNETWORK_TCP_CONNECTION_OPENING:
      g_print ("Server Connection: Opening...\n");
      break;
    case GNETWORK_TCP_CONNECTION_PROXYING:
      g_print ("Server Connection: Proxying...\n");
      break;
    case GNETWORK_TCP_CONNECTION_AUTHENTICATING:
      g_print ("Server Connection: Authenticating SSL...\n");
      break;
    case GNETWORK_TCP_CONNECTION_OPEN:
      /* We're connected. */
      g_print ("Server connection: Open.\n");
      break;
    }
}


static void
server_incoming_cb (GNetworkServer * svr, GNetworkConnection * svr_cxn, gpointer user_data)
{
  gchar *addr;
  guint port;

  server_cxn = GNETWORK_TCP_CONNECTION (svr_cxn);
  g_object_add_weak_pointer (G_OBJECT (svr_cxn), (gpointer *) &server_cxn);
  g_object_get (svr_cxn, "address", &addr, "port", &port, NULL);
  g_print ("Server: Incoming connection:\n\tAddress:\t%s\n\tPort:\t\t%u\n", addr, port);
  g_free (addr);

  g_signal_connect_object (svr_cxn, "received", G_CALLBACK (server_cxn_received_cb), svr, 0);
  g_signal_connect_object (svr_cxn, "sent", G_CALLBACK (server_cxn_sent_cb), svr, 0);
  g_signal_connect_object (svr_cxn, "error", G_CALLBACK (server_cxn_error_cb), svr, 0);
  g_signal_connect_object (svr_cxn, "notify::tcp-status",
			   G_CALLBACK (server_cxn_notify_tcp_status_cb), svr, 0);
}


static void
server_error_cb (GNetworkServer * svr, GError * error, gpointer user_data)
{
  g_print ("Server: Error:\n\tDomain:\t%s\n\tCode:\t%d\n\tMessage:\t%s\n",
	   g_quark_to_string (error->domain), error->code, error->message);
}


static gboolean
start_server (gpointer user_data)
{
  g_print ("Server: Creating Server (%s:%u)...", HOST_ADDRESS, HOST_PORT);
  server = g_object_new (GNETWORK_TYPE_TCP_SERVER, "port", HOST_PORT, "interface", HOST_ADDRESS,
			 "max-connections", 1, NULL);
  g_object_add_weak_pointer (G_OBJECT (server), (gpointer *) &server);
  g_signal_connect (server, "notify::status", G_CALLBACK (server_notify_status_cb), NULL);
  g_signal_connect (server, "new-connection", G_CALLBACK (server_incoming_cb), NULL);
  g_signal_connect (server, "error", G_CALLBACK (server_error_cb), NULL);
  g_print (" Done.\n");
  gnetwork_server_open (GNETWORK_SERVER (server));

  return FALSE;
}


int
main (int argc, gchar * argv[])
{
  if (!g_thread_supported ())
    g_thread_init (NULL);

  g_set_application_name ("GNetwork TCP Server/Connection Test");

  g_type_init ();

  mainloop = g_main_loop_new (g_main_context_default (), FALSE);

  g_idle_add (start_server, NULL);
  g_main_loop_run (mainloop);
  g_main_loop_unref (mainloop);
  mainloop = NULL;
  g_print ("done.\nclient = %p, server = %p, server_cxn = %p\n", client, server, server_cxn);

  return 0;
}
