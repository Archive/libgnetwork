<?xml version="1.0"?>
<gconfschemafile>
	<schemalist>
		<!-- HTTP Proxy -->
		<schema>
			<applyto>/system/http_proxy/use_http_proxy</applyto>
			<key>/schemas/system/http_proxy/use_http_proxy</key>
			<owner>gnome-vfs</owner>
			<type>bool</type>
			<default>false</default>
			<locale name="C">
				<short>Use a proxy when accessing HTTP</short>
				<long>Enables proxy settings when accessing HTTP over the
				internet.</long>
			</locale>
		</schema>
		<schema>
			<applyto>/system/http_proxy/host</applyto>
			<key>/schemas/system/http_proxy/host</key>
			<owner>gnome-vfs</owner>
			<type>string</type>
			<default/>
			<locale name="C">
				<short>HTTP Proxy Location</short>
				<long>The location of the proxy server to use for HTTP
				connections.</long>
			</locale>
		</schema>
		<schema>
			<applyto>/system/http_proxy/port</applyto>
			<key>/schemas/system/http_proxy/port</key>
			<owner>gnome-vfs</owner>
			<type>int</type>
			<default>0</default>
			<locale name="C">
				<short>HTTP Proxy Port</short>
				<long>The port on the proxy server defined by
				/system/http_proxy/host.</long>
			</locale>
		</schema>
		<schema>
			<applyto>/system/http_proxy/use_authentication</applyto>
			<key>/schemas/system/http_proxy/use_authentication</key>
			<owner>gnome-vfs</owner>
			<type>bool</type>
			<default>false</default>
			<locale name="C">
				<short>Authenticate HTTP Proxy Connections</short>
				<long>If true, then connections to the proxy server require
				authentication.  The username/password defined by
				/system/http_proxy/authentication_user and
				/system/http_proxy/authentication_password.</long>
			</locale>
		</schema>
		<schema>
			<applyto>/system/http_proxy/authentication_user</applyto>
			<key>/schemas/system/http_proxy/authentication_user</key>
			<owner>gnome-vfs</owner>
			<type>string</type>
			<default/>
			<locale name="C">
				<short>HTTP Proxy User Name</short>
				<long>The username to use when authenticating with the HTTP
				proxy server.</long>
			</locale>
		</schema>
		<schema>
			<applyto>/system/http_proxy/authentication_password</applyto>
			<key>/schemas/system/http_proxy/authentication_password</key>
			<owner>gnome-vfs</owner>
			<type>string</type>
			<default/>
			<locale name="C">
				<short>http proxy password</short>
				<long>The password to use when authenticating with the HTTP
				proxy server.</long>
			</locale>
		</schema>
		<!-- FTP Proxy -->
		<schema>
			<applyto>/system/proxy/ftp_host</applyto>
			<key>/schemas/system/proxy/ftp_host</key>
			<owner>gnome-vfs</owner>
			<type>string</type>
			<default/>
			<locale name="C">
				<short>Proxy host name</short>
				<long>The location of the FTP proxy server.</long>
			</locale>
		</schema>
		<schema>
			<applyto>/system/proxy/ftp_port</applyto>
			<key>/schemas/system/proxy/ftp_port</key>
			<owner>libgtcpsocket</owner>
			<type>int</type>
			<default>0</default>
			<locale name="C">
				<short>FTP Proxy Port</short>
				<long>The port on the proxy server defined by
				/system/proxy/ftp_host.</long>
			</locale>
		</schema>
		<!-- SSL Proxy -->
		<schema>
			<applyto>/system/proxy/secure_host</applyto>
			<key>/schemas/system/proxy/secure_host</key>
			<owner>gnome-vfs</owner>
			<type>string</type>
			<default/>
			<locale name="C">
				<short>SSL Proxy Host Name</short>
				<long>The location of the proxy server which handles encrypted
				connections.</long>
			</locale>
		</schema>
		<schema>
			<applyto>/system/proxy/secure_port</applyto>
			<key>/schemas/system/proxy/secure_port</key>
			<owner>gnome-vfs</owner>
			<type>int</type>
			<default>0</default>
			<locale name="C">
				<short>SSL Proxy Port</short>
				<long>The port on the proxy server defined by
				/system/proxy/ssl_host.</long>
			</locale>
		</schema>
		<!-- SOCKS4/5 Proxy -->
		<schema>
			<applyto>/system/proxy/socks_host</applyto>
			<key>/schemas/system/proxy/socks_host</key>
			<owner>gnome-vfs</owner>
			<type>string</type>
			<default/>
			<locale name="C">
				<short>SOCKS Proxy Host Name</short>
				<long>The location of the proxy server which handles non-HTTP
				and non-SSL connections.</long>
			</locale>
		</schema>
		<schema>
			<applyto>/system/proxy/socks_port</applyto>
			<key>/schemas/system/proxy/socks_port</key>
			<owner>gnome-vfs</owner>
			<type>int</type>
			<default>0</default>
			<locale name="C">
				<short>Proxy port</short>
				<long>The port on the proxy server defined by
				/system/proxy/socks_host.</long>
			</locale>
		</schema>
		<schema>
			<applyto>/system/proxy/socks_version</applyto>
			<key>/schemas/system/proxy/socks_version</key>
			<owner>libgnetwork</owner>
			<type>int</type>
			<default>5</default>
			<locale name="C">
				<short>SOCKS Version</short>
				<long>The SOCKS proxy version used by
				/system/proxy/socks_host.</long>
			</locale>
		</schema>
	</schemalist>
</gconfschemafile>
