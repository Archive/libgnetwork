/* 
 * GNetwork Library: libgnetwork/gnetwork-tcp-proxy.h
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0/GObject-based TCP/IP networking sockets wrapper.

Notes on editing:
	Tab size: 4
*/


#ifndef __GNETWORK_TCP_PROXY_H__
#define __GNETWORK_TCP_PROXY_H__ 1

#include <glib.h>

G_BEGIN_DECLS


#define GNETWORK_TCP_PROXY_ERROR	(gnetwork_tcp_proxy_error_get_quark ())


typedef enum			/* < prefix=GNETWORK_TCP_PROXY > */
{
  GNETWORK_TCP_PROXY_HTTP,
  GNETWORK_TCP_PROXY_HTTPS,
  GNETWORK_TCP_PROXY_FTP,
  GNETWORK_TCP_PROXY_SOCKS,

  GNETWORK_TCP_PROXY_NONE
}
GNetworkTcpProxyType;


typedef enum			/* < prefix=GNETWORK_TCP_PROXY_ERROR > */
{
  GNETWORK_TCP_PROXY_ERROR_UNKNOWN,

  GNETWORK_TCP_PROXY_ERROR_CONNECTION_REFUSED,
  GNETWORK_TCP_PROXY_ERROR_TIMEOUT,
  GNETWORK_TCP_PROXY_ERROR_NETWORK_UNREACHABLE,
  GNETWORK_TCP_PROXY_ERROR_FIREWALL,

  GNETWORK_TCP_PROXY_ERROR_ABORTED,
  GNETWORK_TCP_PROXY_ERROR_AUTHENTICATION_FAILED,

  GNETWORK_TCP_PROXY_ERROR_SERVER_FAILED
}
GNetworkTcpProxyError;


gboolean gnetwork_tcp_proxy_get_use_proxy (GNetworkTcpProxyType type, const gchar * address);

GQuark gnetwork_tcp_proxy_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* !__GNETWORK_TCP_PROXY_H__ */
