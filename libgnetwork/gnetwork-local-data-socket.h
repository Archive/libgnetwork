/* 
 * GNetwork Library: libgnetwork/gnetwork-local-data-socket.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_LOCAL_DATA_SOCKET_H__
#define __GNETWORK_LOCAL_DATA_SOCKET_H__ 1

#include "gnetwork-data-socket.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_LOCAL_DATA_SOCKET \
  (gnetwork_local_data_socket_get_type ())
#define GNETWORK_LOCAL_DATA_SOCKET(object) \
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GNETWORK_TYPE_LOCAL_DATA_SOCKET, GNetworkLocalDataSocket))
#define GNETWORK_LOCAL_DATA_SOCKET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_LOCAL_DATA_SOCKET, GNetworkLocalDataSocketClass))
#define GNETWORK_IS_LOCAL_DATA_SOCKET(object) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GNETWORK_TYPE_LOCAL_DATA_SOCKET))
#define GNETWORK_IS_LOCAL_DATA_SOCKET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_LOCAL_DATA_SOCKET))
#define GNETWORK_LOCAL_DATA_SOCKET_GET_CLASS(object) \
  (G_TYPE_INSTANCE_GET_CLASS ((object), GNETWORK_TYPE_LOCAL_DATA_SOCKET, GNetworkLocalDataSocketClass))

#define GNETWORK_TYPE_LOCAL_SOCKET_CREDENTIALS \
  (gnetwork_local_socket_credentials_get_type ())
#define GNETWORK_IS_LOCAL_SOCKET_CREDENTIALS(cred) \
  ((cred) != NULL && G_TYPE_FROM_CLASS (cred) == GNETWORK_TYPE_LOCAL_SOCKET_CREDENTIALS)
#define GNETWORK_LOCAL_SOCKET_CREDENTIALS(cred) \
  ((GNetworkLocalSocketCredentials *) (cred))


typedef enum /* <prefix=GNETWORK_LOCAL_DATA_SOCKET> */
{
  GNETWORK_LOCAL_DATA_SOCKET_STATE_AUTHENTICATING = GNETWORK_SOCKET_STATE_LAST,
  GNETWORK_LOCAL_DATA_SOCKET_STATE_OPEN,

  GNETWORK_LOCAL_DATA_SOCKET_STATE_LAST
}
GNetworkLocalDataSocketState;


typedef enum /* <flags,prefix=GNETWORK_LOCAL_DATA_SOCKET_AUTH> */
{
  GNETWORK_LOCAL_DATA_SOCKET_AUTH_NONE = 0,
  GNETWORK_LOCAL_DATA_SOCKET_AUTH_SAME_GROUP = 1 << 0,
  GNETWORK_LOCAL_DATA_SOCKET_AUTH_SAME_USER = 1 << 1,
  GNETWORK_LOCAL_DATA_SOCKET_AUTH_SAME_PROCESS = 1 << 2
}
GNetworkLocalDataSocketAuthFlags;


typedef enum /* <flags,prefix=GNETWORK_LOCAL_DATA_SOCKET_ERROR> */
{
  GNETWORK_LOCAL_DATA_SOCKET_ERROR_NONE = 0,
  GNETWORK_LOCAL_DATA_SOCKET_ERROR_NO_CREDENTIALS = 1 << 0,
  GNETWORK_LOCAL_DATA_SOCKET_ERROR_DIFFERENT_USER = 1 << 1,
  GNETWORK_LOCAL_DATA_SOCKET_ERROR_DIFFERENT_GROUP = 1 << 2,
  GNETWORK_LOCAL_DATA_SOCKET_ERROR_DIFFERENT_PROCESS = 1 << 3
}
GNetworkLocalDataSocketErrorFlags;


typedef struct _GNetworkLocalDataSocket GNetworkLocalDataSocket;
typedef struct _GNetworkLocalDataSocketClass GNetworkLocalDataSocketClass;
typedef struct _GNetworkLocalDataSocketPrivate GNetworkLocalDataSocketPrivate;
  
typedef struct _GNetworkLocalSocketCredentials GNetworkLocalSocketCredentials;


struct _GNetworkLocalDataSocket
{
  /* < private > */
  GNetworkDataSocket parent;

  GNetworkLocalDataSocketPrivate *_priv;
};

struct _GNetworkLocalDataSocketClass
{
  /* <private> */
  GNetworkDataSocketClass parent_class;

  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_local_data_socket_get_type (void) G_GNUC_CONST;


GType gnetwork_local_socket_credentials_get_type (void) G_GNUC_CONST;

GNetworkLocalSocketCredentials * gnetwork_local_socket_credentials_new (void);
guint gnetwork_local_socket_credentials_get_pid (const GNetworkLocalSocketCredentials * cred);
guint gnetwork_local_socket_credentials_get_uid (const GNetworkLocalSocketCredentials * cred);
guint gnetwork_local_socket_credentials_get_gid (const GNetworkLocalSocketCredentials * cred);

gpointer gnetwork_local_socket_credentials_dup (gpointer cred);
void gnetwork_local_socket_credentials_free (gpointer cred);


GQuark gnetwork_local_data_socket_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* !__GNETWORK_LOCAL_DATA_SOCKET_H__ */
