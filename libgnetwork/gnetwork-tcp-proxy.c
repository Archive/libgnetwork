/* 
 * GNetwork Library: libgnetwork/gnetwork-tcp-proxy.c
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */


#include "gnetwork-tcp-proxy.h"

#include "gnetwork-utils.h"
#include "gnetwork-tcp-proxy-private.h"
#include "gnetwork-threads.h"
#include "gnetwork-type-builtins.h"
#include "proxy-keys.h"

#include "gnetwork-connection.h"

#include <gconf/gconf-client.h>

#include <glib/gi18n.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


typedef enum
{
  PROXY_MODE_NONE,
  PROXY_MODE_MANUAL,
  PROXY_MODE_AUTO
}
ProxyMode;


G_LOCK_DEFINE_STATIC (client);

static GConfClient *client = NULL;
static guint num_clients = 0;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static inline const gchar *
get_host_key_for_proxy_type (GNetworkTcpProxyType type)
{
  switch (type)
    {
    case GNETWORK_TCP_PROXY_HTTP:
      return HTTP_PROXY_HOST_KEY;
    case GNETWORK_TCP_PROXY_HTTPS:
      return HTTPS_PROXY_HOST_KEY;
    case GNETWORK_TCP_PROXY_FTP:
      return FTP_PROXY_HOST_KEY;
    case GNETWORK_TCP_PROXY_SOCKS:
      return SOCKS_PROXY_HOST_KEY;
    case GNETWORK_TCP_PROXY_NONE:
    default:
      return NULL;
    }
}

static inline const gchar *
get_port_key_for_proxy_type (GNetworkTcpProxyType type)
{
  switch (type)
    {
    case GNETWORK_TCP_PROXY_HTTP:
      return HTTP_PROXY_PORT_KEY;
    case GNETWORK_TCP_PROXY_HTTPS:
      return HTTPS_PROXY_PORT_KEY;
    case GNETWORK_TCP_PROXY_FTP:
      return FTP_PROXY_PORT_KEY;
    case GNETWORK_TCP_PROXY_SOCKS:
      return SOCKS_PROXY_PORT_KEY;
    case GNETWORK_TCP_PROXY_NONE:
    default:
      return NULL;
    }
}


static gboolean
proxies_enabled (void)
{
  gboolean retval;
  gint value;
  gchar *str = gconf_client_get_string (client, PROXY_MODE_KEY, NULL);
  static const GConfEnumStringPair mode_lookup_table[] = {
    {PROXY_MODE_NONE, "none"},
    {PROXY_MODE_MANUAL, "manual"},
    {PROXY_MODE_AUTO, "auto"},
    {0, NULL}
  };

  if (gconf_string_to_enum ((GConfEnumStringPair *) mode_lookup_table, str, &value))
    {
      switch (value)
	{
	case PROXY_MODE_MANUAL:
	case PROXY_MODE_AUTO:
	  retval = TRUE;
	  break;

	case PROXY_MODE_NONE:
	default:
	  retval = FALSE;
	  break;
	}
    }
  else
    {
      retval = FALSE;
    }

  g_free (str);

  return retval;
}


/* ************************************************************************** *
 *  Proxy Traversal Internals                                                 *
 * ************************************************************************** */

#define GNETWORK_IO_CHANNEL_PROXY(ptr)	((GNetworkIOChannelProxy *) (ptr))
#define G_IO_CHANNEL(ptr)		((GIOChannel *) (ptr))


typedef enum
{
  PROXY_STAGE_REQUESTING,
  PROXY_STAGE_CONNECTING,
  PROXY_STAGE_CONNECTED
}
ProxyStage;


typedef struct
{
  GIOChannel channel;

  GIOChannel *parent;

  GNetworkDnsEntry *destination;

  GNetworkIOCallback func;
  gpointer data;
  GDestroyNotify notify;

  guint16 port;

  GNetworkTcpProxyType type:3;
  ProxyStage stage:3;
  gboolean is_udp:1;
}
GNetworkIOChannelProxy;


/* ***************** *
 *  Shared GIOFuncs  *
 * ***************** */

static void
io_channel_proxy_free (GIOChannel * channel)
{
  if (channel != NULL)
    {
      GNetworkIOChannelProxy *proxy = GNETWORK_IO_CHANNEL_PROXY (channel);

      gnetwork_dns_entry_free (proxy->destination);
      g_io_channel_unref (proxy->parent);

      if (proxy->notify != NULL)
	(*(proxy->notify)) (proxy->data);

      g_free (proxy);
    }
}


static GIOStatus
io_channel_proxy_close (GIOChannel * channel, GError ** error)
{
  return g_io_channel_shutdown (GNETWORK_IO_CHANNEL_PROXY (channel)->parent, FALSE, error);
}


static GSource *
io_channel_proxy_create_watch (GIOChannel * channel, GIOCondition condition)
{
  return g_io_create_watch (GNETWORK_IO_CHANNEL_PROXY (channel)->parent, condition);
}


static GIOStatus
io_channel_proxy_set_flags (GIOChannel * channel, GIOFlags flags, GError ** error)
{
  return g_io_channel_set_flags (GNETWORK_IO_CHANNEL_PROXY (channel)->parent, flags, error);
}


static GIOFlags
io_channel_proxy_get_flags (GIOChannel * channel)
{
  return g_io_channel_get_flags (GNETWORK_IO_CHANNEL_PROXY (channel)->parent);
}


static GIOStatus
io_channel_proxy_read (GIOChannel * channel, gchar * buffer, gsize length, gsize * bytes_read,
		       GError ** error)
{
  return g_io_channel_read_chars (GNETWORK_IO_CHANNEL_PROXY (channel)->parent, buffer, length,
				  bytes_read, error);
}


static GIOStatus
io_channel_proxy_write (GIOChannel * channel, const gchar * buffer, gsize length,
			gsize * bytes_written, GError ** error)
{
  return g_io_channel_write_chars (GNETWORK_IO_CHANNEL_PROXY (channel)->parent, buffer, length,
				   bytes_written, error);
}



/* ********************** *
 *  HTTP Proxy Traversal  *
 * ********************** */

/* See: RFC 2068, 2817 */

typedef enum
{
  HTTP_REPLY_INVALID = 0,
  HTTP_REPLY_OK = 200,
  HTTP_REPLY_AUTH_REQUIRED = 407
}
HttpReply;


static void
http_reply_handler (GNetworkIOChannelProxy * proxy, GIOCondition cond, gpointer user_data)
{
  GError *error = NULL;

  switch (cond)
    {
    case G_IO_IN:
    case G_IO_PRI:
      {
	gchar data[30];
	gsize bytes_done;
	GIOStatus status;
	guint reply_code;

	do
	  {
	    status = g_io_channel_read_chars (G_IO_CHANNEL (proxy), data, sizeof (data),
					      &bytes_done, &error);
	  }
	while (status == G_IO_STATUS_AGAIN);

	switch (status)
	  {
	    /* The data was received OK, check the reply.. */
	  case G_IO_STATUS_NORMAL:
	    reply_code = 0;
	    sscanf (data, "HTTP/%*f %u %*s", &reply_code);

	    switch (reply_code)
	      {
		/* Success, don't set error */
	      case HTTP_REPLY_OK:
		break;

		/* The server is requesting some HTTP authorization not handled by the user/pass
		   stuff. */
	      case HTTP_REPLY_AUTH_REQUIRED:
		error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
					     GNETWORK_TCP_PROXY_ERROR_AUTHENTICATION_FAILED, NULL);

		error->message =
		  _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_AUTHENTICATION_FAILED,
						GNETWORK_TCP_PROXY_HTTP, proxy->destination);
		break;

	      case HTTP_REPLY_INVALID:
	      default:
		error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
					     GNETWORK_TCP_PROXY_ERROR_SERVER_FAILED, NULL);
		error->message =
		  _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_SERVER_FAILED,
						GNETWORK_TCP_PROXY_HTTP, proxy->destination);
		break;
	      }
	    break;

	    /* The error is already set. */
	  case G_IO_STATUS_ERROR:
	    break;

	    /* The connection died */
	  case G_IO_STATUS_EOF:
	    error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, GNETWORK_TCP_PROXY_ERROR_ABORTED,
					 NULL);
	    error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
							   GNETWORK_TCP_PROXY_HTTP,
							   proxy->destination);
	    break;

	    /* It should be impossible to get here */
	  default:
	    g_assert_not_reached ();
	    break;
	  }
      }
      break;

      /* The connection was closed on us. */
    case G_IO_HUP:
      error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, GNETWORK_TCP_PROXY_ERROR_ABORTED,
				   NULL);
      error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
						     GNETWORK_TCP_PROXY_HTTP, proxy->destination);
      break;

    default:
      break;
    }

  (*(proxy->func)) (G_IO_CHANNEL (proxy), error, proxy->data);

  if (error != NULL)
    g_error_free (error);
}


static inline void
traverse_http_proxy (GNetworkIOChannelProxy * proxy)
{
  guchar *data;
  gsize data_size, bytes_done, tmp;
  GIOStatus status;
  GError *error = NULL;
  G_CONST_RETURN GNetworkIpAddress *ip_address;
  gchar *address;

  ip_address = gnetwork_dns_entry_get_ip_address (proxy->destination);
  if (ip_address != NULL)
    address = gnetwork_ip_address_to_string (ip_address);
  else
    address = g_strdup (gnetwork_dns_entry_get_hostname (proxy->destination));

  if (gconf_client_get_bool (client, HTTP_PROXY_USE_AUTH_KEY, NULL))
    {
      gchar *username, *passwd;

      username = gconf_client_get_string (client, HTTP_PROXY_AUTH_USER_KEY, NULL);
      passwd = gconf_client_get_string (client, HTTP_PROXY_AUTH_PASS_KEY, NULL);

      if (username != NULL && passwd == NULL)
	{
	  data = g_strdup_printf ("CONNECT: %s:%u HTTP/1.1\r\n"
				  "Host: %s:%u\r\n"
				  "Proxy-Authentication: userid=%s\r\n"
				  "\r\n", address, proxy->port, address, proxy->port, username);
	}
      else if (username == NULL && passwd != NULL)
	{
	  data = g_strdup_printf ("CONNECT: %s:%u HTTP/1.1\r\n"
				  "Host: %s:%u\r\n"
				  "Proxy-Authentication: password=%s\r\n"
				  "\r\n", address, proxy->port, address, proxy->port, passwd);
	}
      else
	{
	  data = g_strdup_printf ("CONNECT: %s:%u HTTP/1.1\r\n"
				  "Host: %s:%u\r\n"
				  "Proxy-Authentication: user-pass=%s:%s\r\n"
				  "\r\n", address, proxy->port, address, proxy->port,
				  username, passwd);
	}

      g_free (username);
      g_free (passwd);
    }
  else
    {
      data = g_strdup_printf ("CONNECT: %s:%u\r\n"
			      "Host: %s:%u\r\n\r\n", address, proxy->port, address, proxy->port);
    }
  g_free (address);
  data_size = strlen (data);

  /* Connection request */
  bytes_done = 0;
  do
    {
      tmp = 0;
      status = g_io_channel_write_chars (G_IO_CHANNEL (proxy), data + bytes_done,
					 data_size - bytes_done, &tmp, &error);

      if (status == G_IO_STATUS_NORMAL)
	bytes_done += tmp;
    }
  while ((status == G_IO_STATUS_NORMAL && bytes_done < data_size) || status == G_IO_STATUS_AGAIN);

  g_free (data);

  switch (status)
    {
    case G_IO_STATUS_NORMAL:
      gnetwork_thread_io_add_watch (G_IO_CHANNEL (proxy),
				    (G_IO_IN | G_IO_PRI | G_IO_HUP | G_IO_ERR),
				    (GIOFunc) http_reply_handler, NULL);
      break;
    case G_IO_STATUS_EOF:
      error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, GNETWORK_TCP_PROXY_ERROR_ABORTED,
				   NULL);
      error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
						     GNETWORK_TCP_PROXY_HTTP, proxy->destination);
      break;

    case G_IO_STATUS_ERROR:
      break;
    default:
      g_assert_not_reached ();
      break;
    }

  (*(proxy->func)) (G_IO_CHANNEL (proxy), error, proxy->data);
  g_io_channel_unref (G_IO_CHANNEL (proxy));

  if (error != NULL)
    g_error_free (error);
}


/* *********************** *
 *  HTTPS Proxy Traversal  *
 * *********************** */

static inline void
traverse_https_proxy (GNetworkIOChannelProxy * proxy)
{
  g_warning ("%s (%s): FIXME: HTTPS Proxy support not yet implemented.",
	     G_STRLOC, __PRETTY_FUNCTION__);
}


/* ********************* *
 *  FTP Proxy Traversal  *
 * ********************* */

static inline void
traverse_ftp_proxy (GNetworkIOChannelProxy * proxy)
{
  g_warning ("%s (%s): FIXME: FTP Proxy support not yet implemented.",
	     G_STRLOC, __PRETTY_FUNCTION__);
}


/* ************************* *
 *  SOCKSv4 Proxy Traversal  *
 * ************************* */

/* See also: http://www.socks.nec.com/protocol/socks4.protocol
 * http://www.socks.nec.com/protocol/socks4a.protocol */

#define SOCKS4_DATA(ptr)	((Socks4Data *) (ptr))
#define SOCKS4A_ADDRESS		(0x000000FF)


typedef enum
{
  SOCKS4_COMMAND_LOGIN = 1,
  SOCKS4_COMMAND_BIND = 2
}
Socks4Command;

typedef enum
{
  SOCKS4_REPLY_OK = 90,
  SOCKS4_REPLY_ERROR_FAILED = 91,
  SOCKS4_REPLY_ERROR_IDENT_NOT_RUNNING = 92,
  SOCKS4_REPLY_ERROR_USER_IDENT_MISMATCH = 93
}
Socks4Reply;


typedef struct
{
  guint8 version;
  guint8 command;

  guint16 port;
  guint32 address;
}
Socks4Data;


static gboolean
socks4_reply_handler (GNetworkIOChannelProxy * proxy, GIOCondition cond, gpointer user_data)
{
  GIOStatus status;
  Socks4Data data;
  gsize bytes_done;
  GError *error = NULL;

  switch (cond)
    {
    case G_IO_IN:
    case G_IO_PRI:
      bytes_done = 0;
      do
	{
	  gint tmp = 0;

	  status = g_io_channel_read_chars (G_IO_CHANNEL (proxy), (gchar *) & data,
					    sizeof (Socks4Data), &tmp, &error);

	  if (status == G_IO_STATUS_NORMAL)
	    bytes_done += tmp;
	}
      while ((status == G_IO_STATUS_NORMAL && bytes_done < sizeof (Socks4Data)) ||
	     status == G_IO_STATUS_AGAIN);

      /* If the recv failed */
      switch (status)
	{
	case G_IO_STATUS_NORMAL:
	  /* Figure out what happened with the connection (the command field is reused
	     for reply status codes when it's incoming) */
	  switch (data.command)
	    {
	      /* Success */
	    case SOCKS4_REPLY_OK:
	      break;

	      /* This is just the generic "couldn't connect" error. SOCKSv4 doesn't
	       * provide detailed errors */
	    case SOCKS4_REPLY_ERROR_FAILED:
	      {
		gchar *host, *destination;

		host = gconf_client_get_string (client, SOCKS_PROXY_HOST_KEY, NULL);
		destination = g_strdup (gnetwork_dns_entry_get_hostname (proxy->destination));
		if (destination == NULL)
		  {
		    destination =
		      gnetwork_ip_address_to_string (gnetwork_dns_entry_get_ip_address
						     (proxy->destination));
		  }

		error = g_error_new (GNETWORK_CONNECTION_ERROR,
				     GNETWORK_CONNECTION_ERROR_REFUSED,
				     _("The proxy service at %s could not connect to %s."), host,
				     destination);
		g_free (host);
		g_free (destination);
	      }
	      break;

	    case SOCKS4_REPLY_ERROR_IDENT_NOT_RUNNING:
	    case SOCKS4_REPLY_ERROR_USER_IDENT_MISMATCH:
	      error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
					   GNETWORK_TCP_PROXY_ERROR_AUTHENTICATION_FAILED, NULL);
	      error->message =
		_gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_AUTHENTICATION_FAILED,
					      GNETWORK_TCP_PROXY_SOCKS, proxy->destination);
	      break;

	      /* Anything else is the proxy server crapping itself */
	    default:
	      error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
					   GNETWORK_TCP_PROXY_ERROR_SERVER_FAILED, NULL);
	      error->message =
		_gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_SERVER_FAILED,
					      GNETWORK_TCP_PROXY_SOCKS, proxy->destination);
	      break;
	    }
	  break;

	case G_IO_STATUS_ERROR:
	  break;

	case G_IO_STATUS_EOF:
	  if (proxy->func != NULL)
	    {
	      error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
					   GNETWORK_TCP_PROXY_ERROR_ABORTED, NULL);
	      error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
							     GNETWORK_TCP_PROXY_SOCKS,
							     proxy->destination);
	    }
	  break;

	default:
	  g_assert_not_reached ();
	  break;
	}
      break;

    case G_IO_HUP:
      if (proxy->func != NULL)
	{
	  error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, GNETWORK_TCP_PROXY_ERROR_ABORTED,
				       NULL);
	  error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
							 GNETWORK_TCP_PROXY_SOCKS,
							 proxy->destination);
	}
      break;

    default:
      if (proxy->func != NULL)
	{
	  error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, GNETWORK_TCP_PROXY_ERROR_UNKNOWN,
				       NULL);
	  error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_UNKNOWN,
							 GNETWORK_TCP_PROXY_SOCKS,
							 proxy->destination);
	}
      break;
    }

  (*(proxy->func)) (G_IO_CHANNEL (proxy), error, proxy->data);

  if (error != NULL)
    g_error_free (error);

  return FALSE;
}


static inline void
traverse_socks4_proxy (GNetworkIOChannelProxy * proxy)
{
  const gchar *username;
  guchar *data;
  gsize data_size, bytes_done;
  gsize username_size;
  GIOStatus status;
  GError *error = NULL;
  G_CONST_RETURN GNetworkIpAddress *ip_address;

  /* Create the socks4 request. */
  username = g_get_user_name ();
  username_size = strlen (username);
  data = NULL;

  /* We don't handle passing DNS lookups to the proxy server yet, but we do know for sure that
     Socks4 doesn't handle IPv6, so try a socks4a lookup for the string-ified proxy->address. */
  ip_address = gnetwork_dns_entry_get_ip_address (proxy->destination);
  if (ip_address != NULL && gnetwork_ip_address_is_ipv4 (ip_address))
    {
      SOCKS4_DATA (data)->address = GNETWORK_IP_ADDRESS32 (ip_address, 3);
      data = (guchar *) g_malloc (sizeof (Socks4Data) + username_size + 1);
      strcpy (data + sizeof (Socks4Data), username);
      data_size = sizeof (Socks4Data) + username_size;
    }
  else
    {
      gchar *str;
      gsize str_size;

      if (ip_address == NULL)
	str = g_strdup (gnetwork_dns_entry_get_hostname (proxy->destination));
      else
	str = gnetwork_ip_address_to_string (ip_address);

      str_size = strlen (str);

      SOCKS4_DATA (data)->address = SOCKS4A_ADDRESS;
      data = (guchar *) g_malloc (sizeof (Socks4Data) + username_size + str_size + 2);
      strcpy (data + sizeof (Socks4Data), username);
      strcpy (data + sizeof (Socks4Data) + username_size, str);
      data_size = sizeof (Socks4Data) + username_size + str_size;

      g_free (str);
    }

  SOCKS4_DATA (data)->version = 4;
  SOCKS4_DATA (data)->command = SOCKS4_COMMAND_LOGIN;
  SOCKS4_DATA (data)->port = g_htons (proxy->port);

  /* Connection request */
  bytes_done = 0;
  do
    {
      gint tmp = 0;
      status = g_io_channel_write_chars (G_IO_CHANNEL (proxy), data + bytes_done,
					 data_size - bytes_done, &tmp, &error);

      if (status == G_IO_STATUS_NORMAL)
	bytes_done += tmp;
    }
  while ((status == G_IO_STATUS_NORMAL && bytes_done < data_size) || status == G_IO_STATUS_AGAIN);

  g_free (data);

  switch (status)
    {
    case G_IO_STATUS_NORMAL:
      gnetwork_thread_io_add_watch (G_IO_CHANNEL (proxy),
				    (G_IO_IN | G_IO_PRI | G_IO_HUP | G_IO_ERR),
				    (GIOFunc) socks4_reply_handler, NULL);
      return;
      break;

    case G_IO_STATUS_ERROR:
      break;

    case G_IO_STATUS_EOF:
      error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, GNETWORK_TCP_PROXY_ERROR_ABORTED,
				   NULL);
      error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
						     GNETWORK_TCP_PROXY_HTTP, proxy->destination);
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  (*(proxy->func)) (G_IO_CHANNEL (proxy), error, proxy->data);
  g_io_channel_unref (G_IO_CHANNEL (proxy));

  if (error != NULL)
    g_error_free (error);
}


/* ************************* *
 *  SOCKSv5 Proxy Traversal  *
 * ************************* */

/* See also: RFC 1928, 1929, 1961 */

#define SOCKS5_NEGOTIATION_DATA(ptr)		((Socks5NegotiationData *) (ptr))
#define SOCKS5_NEGOTIATION_REPLY_DATA(ptr)	((Socks5NegotiationReplyData *) (ptr))
#define SOCKS5_COMMON_DATA(ptr)			((Socks5CommonData *) (ptr))
#define SOCKS5_IP4_DATA(ptr)			((Socks5Ip4Data *) (ptr))
#define SOCKS5_DOMAIN_DATA(ptr)			((Socks5DomainData *) (ptr))
#define SOCKS5_IP6_DATA(ptr)			((Socks5Ip6Data *) (ptr))


typedef enum
{
  SOCKS5_AUTH_METHOD_NONE = 0,
  SOCKS5_AUTH_METHOD_GSSAPI = 1,
  SOCKS5_AUTH_METHOD_USER_PASS = 2,
  SOCKS5_AUTH_METHOD_FAILED = 0xFF
}
Socks5AuthMethod;

typedef struct
{
  guint8 version;
  guint8 num_methods;
  Socks5AuthMethod method:8;
}
Socks5NegotiationData;

typedef struct
{
  guint8 version;
  Socks5AuthMethod method:8;
}
Socks5NegotiationReplyData;


typedef enum
{
  SOCKS5_ADDRESS_IP4 = 1,
  SOCKS5_ADDRESS_DOMAINNAME = 3,
  SOCKS5_ADDRESS_IP6 = 4
}
Socks5AddressType;

typedef enum
{
  SOCKS5_REPLY_OK = 0,
  SOCKS5_REPLY_ERROR_SERVER_FAILED = 1,
  SOCKS5_REPLY_ERROR_NOT_ALLOWED = 2,
  SOCKS5_REPLY_ERROR_NETWORK_UNREACHABLE = 3,
  SOCKS5_REPLY_ERROR_HOST_UNREACHABLE = 4,
  SOCKS5_REPLY_ERROR_CONNECTION_REFUSED = 5,
  SOCKS5_REPLY_ERROR_TTL_EXPIRED = 6,
  SOCKS5_REPLY_ERROR_REQUEST_NOT_SUPPORTED = 7,
  SOCKS5_REPLY_ERROR_ADDRESS_TYPE_NOT_SUPPORTED = 8
}
Socks5Reply;

typedef enum
{
  SOCKS5_COMMAND_CONNECT = 1,
  SOCKS5_COMMAND_BIND = 2,
  SOCKS5_COMMAND_UDP_ASSOCIATE = 1
}
Socks5Command;

typedef struct
{
  guint8 version;
  guint8 command;
  guint8 reserved;
  guint8 address_type;
}
Socks5CommonData;

typedef struct
{
  Socks5CommonData common;

  guint8 address_length;
  /* max address_length + sizeof (guint16) */
  gchar address[258];
}
Socks5DomainData;

typedef struct
{
  Socks5CommonData common;

  GNetworkIpAddress address;
  guint16 port;
}
Socks5Ip6Data;

typedef struct
{
  Socks5CommonData common;

  guint32 address;
  guint16 port;
}
Socks5Ip4Data;

typedef union
{
  Socks5CommonData common;
  Socks5DomainData domain;
  Socks5Ip6Data ip6;
  Socks5Ip4Data ip4;
}
Socks5Data;


static inline gboolean
socks5_send_request (GNetworkIOChannelProxy * proxy, GError ** error)
{
  GIOStatus status;
  Socks5Data data;
  gsize bytes_done, data_size;
  G_CONST_RETURN GNetworkIpAddress *ip_address;

  data.common.version = 5;
  data.common.command = SOCKS5_COMMAND_CONNECT;
  data.common.reserved = 0;

  ip_address = gnetwork_dns_entry_get_ip_address (proxy->destination);
  /* We need a hostname lookup */
  if (ip_address == NULL)
    {
      guint16 port;
      gsize hostname_size;
      G_CONST_RETURN gchar *hostname;

      hostname = gnetwork_dns_entry_get_hostname (proxy->destination);
      hostname_size = strlen (hostname);

      data.common.address_type = SOCKS5_ADDRESS_DOMAINNAME;
      data.domain.address_length = hostname_size;
      strncpy (data.domain.address, hostname, hostname_size);
      port = g_htons (proxy->port);
      memcpy ((G_UCHAR (&data) + sizeof (Socks5CommonData) + 1 + hostname_size), &port, 2);

      data_size = sizeof (Socks5CommonData) + 1 + hostname_size + 2;
    }
  /* We need an IPv4 connection */
  else if (gnetwork_ip_address_is_ipv4 (ip_address))
    {
      data.common.address_type = SOCKS5_ADDRESS_IP4;
      memcpy (&(data.ip4.address), &(GNETWORK_IP_ADDRESS32 (ip_address, 3)), sizeof (guint32));
      data.ip4.port = g_htons (proxy->port);

      data_size = sizeof (Socks5Ip4Data);
    }
  /* We need an IPv6 connection */
  else
    {
      data.common.address_type = SOCKS5_ADDRESS_IP4;
      memcpy (&(data.ip6.address), ip_address, sizeof (GNetworkIpAddress));
      data.ip6.port = g_htons (proxy->port);

      data_size = sizeof (Socks5Ip6Data);
    }

  proxy->stage = PROXY_STAGE_CONNECTING;

  bytes_done = 0;
  do
    {
      gsize tmp = 0;

      status = g_io_channel_write_chars (G_IO_CHANNEL (proxy), (gchar *) & data,
					 data_size, &tmp, error);

      if (status == G_IO_STATUS_NORMAL)
	bytes_done += tmp;
    }
  while ((status == G_IO_STATUS_NORMAL && bytes_done < data_size) || status == G_IO_STATUS_AGAIN);

  switch (status)
    {
    case G_IO_STATUS_NORMAL:
      return TRUE;
      break;

    case G_IO_STATUS_ERROR:
      break;

    case G_IO_STATUS_EOF:
      *error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, GNETWORK_TCP_PROXY_ERROR_ABORTED,
				    NULL);
      (*error)->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
							GNETWORK_TCP_PROXY_SOCKS,
							proxy->destination);
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  return FALSE;
}


static gboolean
socks5_reply_handler (GNetworkIOChannelProxy * proxy, GIOCondition cond, gpointer user_data)
{
  GError *error = NULL;

  switch (cond)
    {
    case G_IO_IN:
    case G_IO_PRI:
      {
	GIOStatus status;
	gsize bytes_done = 0;

	if (proxy->stage == PROXY_STAGE_REQUESTING)
	  {
	    Socks5NegotiationReplyData reply_data;

	    do
	      {
		gsize tmp = 0;

		status = g_io_channel_read_chars (G_IO_CHANNEL (proxy), (gchar *) & reply_data,
						  sizeof (Socks5NegotiationReplyData), &tmp,
						  &error);

		if (status == G_IO_STATUS_NORMAL)
		  bytes_done += tmp;
	      }
	    while ((status == G_IO_STATUS_NORMAL &&
		    bytes_done < sizeof (Socks5NegotiationReplyData)) ||
		   status == G_IO_STATUS_AGAIN);

	    switch (status)
	      {
	      case G_IO_STATUS_NORMAL:
		switch (reply_data.method)
		  {
		  case SOCKS5_AUTH_METHOD_FAILED:
		    error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
						 GNETWORK_TCP_PROXY_ERROR_ABORTED, NULL);
		    error->message =
		      _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
						    GNETWORK_TCP_PROXY_SOCKS, proxy->destination);
		    break;

		  default:
		    if (socks5_send_request (proxy, &error))
		      return TRUE;
		    break;
		  }
		break;

	      case G_IO_STATUS_ERROR:
		break;

	      case G_IO_STATUS_EOF:
		error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
					     GNETWORK_TCP_PROXY_ERROR_ABORTED, NULL);
		error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
							       GNETWORK_TCP_PROXY_SOCKS,
							       proxy->destination);
		break;

	      default:
		g_assert_not_reached ();
		break;
	      }
	  }
	else if (proxy->stage == PROXY_STAGE_CONNECTING)
	  {
	    Socks5Data data;

	    do
	      {
		gsize tmp = 0;

		status = g_io_channel_read_chars (G_IO_CHANNEL (proxy), (gchar *) & data,
						  sizeof (Socks5CommonData) - bytes_done, &tmp,
						  &error);

		if (status == G_IO_STATUS_NORMAL)
		  bytes_done += tmp;
	      }
	    while ((status == G_IO_STATUS_NORMAL && bytes_done < sizeof (Socks5CommonData)) ||
		   status == G_IO_STATUS_AGAIN);

	    /* Figure out how much more we've got to read to clear the incoming buffer */
	    switch (data.common.address_type)
	      {
	      case SOCKS5_ADDRESS_DOMAINNAME:
		{
		  bytes_done = 0;
		  do
		    {
		      gsize tmp = 0;
		      status = g_io_channel_read_chars (G_IO_CHANNEL (proxy),
							(gchar *) (&data) +
							sizeof (Socks5CommonData) + bytes_done, 1,
							&tmp, &error);

		      if (status == G_IO_STATUS_NORMAL)
			bytes_done += tmp;
		    }
		  while ((status == G_IO_STATUS_NORMAL && bytes_done < 1) ||
			 status == G_IO_STATUS_AGAIN);

		  if (status == G_IO_STATUS_NORMAL)
		    {
		      bytes_done = 0;
		      do
			{
			  gsize tmp = 0;
			  status = g_io_channel_read_chars (G_IO_CHANNEL (proxy),
							    (gchar *) (&data) +
							    sizeof (Socks5CommonData) + 1 +
							    bytes_done,
							    data.domain.address_length + 2 -
							    bytes_done, &tmp, &error);

			  if (status == G_IO_STATUS_NORMAL)
			    bytes_done += tmp;
			}
		      while ((status == G_IO_STATUS_NORMAL && bytes_done < 1) ||
			     status == G_IO_STATUS_AGAIN);
		    }
		}
		break;
	      case SOCKS5_ADDRESS_IP6:
		bytes_done = 0;
		do
		  {
		    gsize tmp = 0;
		    status = g_io_channel_read_chars (G_IO_CHANNEL (proxy),
						      (gchar *) (&data) +
						      sizeof (Socks5CommonData) + bytes_done,
						      sizeof (Socks5Ip6Data) -
						      sizeof (Socks5CommonData) - bytes_done, &tmp,
						      &error);

		    if (status == G_IO_STATUS_NORMAL)
		      bytes_done += tmp;
		  }
		while ((status == G_IO_STATUS_NORMAL && bytes_done < 1) ||
		       status == G_IO_STATUS_AGAIN);
		break;
	      case SOCKS5_ADDRESS_IP4:
		bytes_done = 0;
		do
		  {
		    gsize tmp = 0;
		    status = g_io_channel_read_chars (G_IO_CHANNEL (proxy),
						      (gchar *) (&data) +
						      sizeof (Socks5CommonData) + bytes_done,
						      sizeof (Socks5Ip4Data) -
						      sizeof (Socks5CommonData) - bytes_done, &tmp,
						      &error);

		    if (status == G_IO_STATUS_NORMAL)
		      bytes_done += tmp;
		  }
		while ((status == G_IO_STATUS_NORMAL && bytes_done < 1) ||
		       status == G_IO_STATUS_AGAIN);
		break;
	      }

	    switch (status)
	      {
	      case G_IO_STATUS_NORMAL:
		switch (data.common.command)
		  {
		    /* Success */
		  case SOCKS5_REPLY_OK:
		    proxy->stage = PROXY_STAGE_CONNECTED;
		    break;

		    /* Standard errno-esque problems connecting to the destination from the proxy
		       server. */
		  case SOCKS5_REPLY_ERROR_NETWORK_UNREACHABLE:
		  case SOCKS5_REPLY_ERROR_HOST_UNREACHABLE:
		    error = g_error_new_literal (GNETWORK_CONNECTION_ERROR,
						 GNETWORK_CONNECTION_ERROR_UNREACHABLE,
						 gnetwork_connection_strerror
						 (GNETWORK_CONNECTION_ERROR_UNREACHABLE));
		    break;

		    /* Connection refused */
		  case SOCKS5_REPLY_ERROR_CONNECTION_REFUSED:
		    error = g_error_new_literal (GNETWORK_CONNECTION_ERROR,
						 GNETWORK_CONNECTION_ERROR_REFUSED,
						 gnetwork_connection_strerror
						 (GNETWORK_CONNECTION_ERROR_REFUSED));
		    break;

		    /* TTL expired (Timeout) */
		  case SOCKS5_REPLY_ERROR_TTL_EXPIRED:
		    error = g_error_new_literal (GNETWORK_CONNECTION_ERROR,
						 GNETWORK_CONNECTION_ERROR_TIMEOUT,
						 gnetwork_connection_strerror
						 (GNETWORK_CONNECTION_ERROR_TIMEOUT));
		    break;

		    /* Proxy ruleset prevents connection (firewall) */
		  case SOCKS5_REPLY_ERROR_NOT_ALLOWED:
		    error = g_error_new_literal (GNETWORK_CONNECTION_ERROR,
						 GNETWORK_CONNECTION_ERROR_PERMISSIONS,
						 gnetwork_connection_strerror
						 (GNETWORK_CONNECTION_ERROR_PERMISSIONS));
		    break;

		    /* Proxy-server related problems */
		  case SOCKS5_REPLY_ERROR_REQUEST_NOT_SUPPORTED:
		  case SOCKS5_REPLY_ERROR_ADDRESS_TYPE_NOT_SUPPORTED:
		    error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
						 GNETWORK_TCP_PROXY_ERROR_AUTHENTICATION_FAILED,
						 NULL);
		    error->message =
		      _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_AUTHENTICATION_FAILED,
						    GNETWORK_TCP_PROXY_SOCKS, proxy->destination);
		    break;

		  case SOCKS5_REPLY_ERROR_SERVER_FAILED:
		  default:
		    error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
						 GNETWORK_TCP_PROXY_ERROR_SERVER_FAILED, NULL);
		    error->message =
		      _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_SERVER_FAILED,
						    GNETWORK_TCP_PROXY_SOCKS, proxy->destination);
		    break;
		  }
		break;

	      case G_IO_STATUS_ERROR:
		break;

	      case G_IO_STATUS_EOF:
		error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR,
					     GNETWORK_TCP_PROXY_ERROR_ABORTED, NULL);
		error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
							       GNETWORK_TCP_PROXY_SOCKS,
							       proxy->destination);
		break;

	      default:
		g_assert_not_reached ();
		break;
	      }
	  }
      }
      break;

    case G_IO_ERR:
      break;

    case G_IO_HUP:
      error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, GNETWORK_TCP_PROXY_ERROR_ABORTED,
				   NULL);
      error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
						     GNETWORK_TCP_PROXY_SOCKS, proxy->destination);
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  (*(proxy->func)) (G_IO_CHANNEL (proxy), error, proxy->data);

  if (error != NULL)
    g_error_free (error);

  return FALSE;
}


static inline void
traverse_socks5_proxy (GNetworkIOChannelProxy * proxy)
{
  Socks5NegotiationData data;
  gsize bytes_done;
  GIOStatus status;
  GError *error = NULL;

  data.version = 5;
  data.num_methods = 1;
  data.method = SOCKS5_AUTH_METHOD_NONE;

  proxy->stage = PROXY_STAGE_REQUESTING;

  /* Send the negotiation data */
  bytes_done = 0;
  do
    {
      gint tmp = 0;

      status = g_io_channel_write_chars (G_IO_CHANNEL (proxy), (gchar *) (&data) + bytes_done,
					 sizeof (Socks5NegotiationData) - bytes_done, &tmp, &error);

      if (status == G_IO_STATUS_NORMAL)
	bytes_done += tmp;
    }
  while ((status == G_IO_STATUS_NORMAL && bytes_done < sizeof (Socks5NegotiationData)) ||
	 status == G_IO_STATUS_AGAIN);

  switch (status)
    {
    case G_IO_STATUS_NORMAL:
      gnetwork_thread_io_add_watch (G_IO_CHANNEL (proxy),
				    (G_IO_IN | G_IO_PRI | G_IO_HUP | G_IO_ERR),
				    (GIOFunc) socks5_reply_handler, NULL);
      return;
      break;
    case G_IO_STATUS_ERROR:
      break;
    case G_IO_STATUS_EOF:
      error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, GNETWORK_TCP_PROXY_ERROR_ABORTED,
				   NULL);
      error->message = _gnetwork_tcp_proxy_strerror (GNETWORK_TCP_PROXY_ERROR_ABORTED,
						     GNETWORK_TCP_PROXY_SOCKS, proxy->destination);
      break;
    default:
      g_assert_not_reached ();
      break;
    }

  (*(proxy->func)) (G_IO_CHANNEL (proxy), error, proxy->data);
  g_io_channel_unref (G_IO_CHANNEL (proxy));

  g_error_free (error);
}


/* ************************************************************************** *
 *  Library-Public API                                                        *
 * ************************************************************************** */

void
_gnetwork_io_channel_proxy_new (GIOChannel * parent, GNetworkTcpProxyType type,
				const GNetworkDnsEntry * destination, guint16 port,
				GNetworkIOCallback func, gpointer data, GDestroyNotify notify)
{
  GNetworkIOChannelProxy *channel;
  static const GIOFuncs channel_funcs = {
    io_channel_proxy_read,
    io_channel_proxy_write,
    NULL,
    io_channel_proxy_close,
    io_channel_proxy_create_watch,
    io_channel_proxy_free,
    io_channel_proxy_set_flags,
    io_channel_proxy_get_flags,
  };

  g_return_if_fail (parent != NULL);
  g_return_if_fail (_gnetwork_enum_value_is_valid (GNETWORK_TYPE_TCP_PROXY_TYPE, type));
  g_return_if_fail (destination != NULL);
  g_return_if_fail (func != NULL);

  channel = g_new0 (GNetworkIOChannelProxy, 1);
  channel->type = type;

  g_io_channel_ref (parent);
  channel->parent = parent;

  channel->destination = gnetwork_dns_entry_dup (destination);
  channel->port = port;
  channel->func = func;
  channel->data = data;
  channel->notify = notify;

  G_IO_CHANNEL (channel)->funcs = (GIOFuncs *) (&channel_funcs);
  g_io_channel_init (G_IO_CHANNEL (channel));

  io_channel_proxy_get_flags (G_IO_CHANNEL (channel));
  g_io_channel_set_encoding (G_IO_CHANNEL (channel), NULL, NULL);
  g_io_channel_set_buffered (G_IO_CHANNEL (channel), FALSE);

  /* And so it gets icky. */
  switch (type)
    {
    case GNETWORK_TCP_PROXY_HTTP:
      traverse_http_proxy (channel);
      break;
    case GNETWORK_TCP_PROXY_HTTPS:
      traverse_https_proxy (channel);
      break;
    case GNETWORK_TCP_PROXY_FTP:
      traverse_ftp_proxy (channel);
      break;
    case GNETWORK_TCP_PROXY_SOCKS:
      if (gconf_client_get_int (client, SOCKS_PROXY_VERSION_KEY, NULL) == 4)
	traverse_socks4_proxy (channel);
      else
	traverse_socks5_proxy (channel);
      break;

    default:
      g_assert_not_reached ();
      break;
    }
}


void
_gnetwork_tcp_proxy_initialize (void)
{
  G_LOCK (client);

  num_clients++;

  if (client == NULL)
    {
      client = gconf_client_get_default ();

      gconf_client_add_dir (client, HTTP_PROXY_DIR, GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
      gconf_client_add_dir (client, PROXY_DIR, GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);
    }
  G_UNLOCK (client);
}


void
_gnetwork_tcp_proxy_shutdown (void)
{
  G_LOCK (client);

  num_clients--;

  if (num_clients == 0)
    {
      gconf_client_remove_dir (client, HTTP_PROXY_DIR, NULL);
      gconf_client_remove_dir (client, PROXY_DIR, NULL);

      g_object_unref (client);
      client = NULL;
    }

  G_UNLOCK (client);
}


/**
 * _gnetwork_tcp_proxy_get_host:
 * @type: the proxy type to examine.
 * 
 * Retrieves the hostname of the proxy host used for @type connections. The
 * returned string should be freed with g_free() when no longer needed.
 * 
 * Returns: a newly allocated string for the hostname used in @type connections.
 * 
 * Since: 1.0
 **/
gchar *
_gnetwork_tcp_proxy_get_host (GNetworkTcpProxyType type)
{
  gchar *retval;

  g_return_val_if_fail (_gnetwork_enum_value_is_valid (GNETWORK_TYPE_TCP_PROXY_TYPE, type), NULL);

  _gnetwork_tcp_proxy_initialize ();
  if (proxies_enabled ())
    {
      retval = gconf_client_get_string (client, get_host_key_for_proxy_type (type), NULL);
    }
  else
    {
      retval = NULL;
    }
  _gnetwork_tcp_proxy_shutdown ();

  return retval;
}


/**
 * _gnetwork_tcp_proxy_get_port:
 * @type: the proxy type to examine.
 * 
 * Retrieves the port number of the proxy host used for @type connections.
 * 
 * Returns: the proxy port number for @type
 * 
 * Since: 1.0
 **/
guint
_gnetwork_tcp_proxy_get_port (GNetworkTcpProxyType type)
{
  guint retval;

  g_return_val_if_fail (_gnetwork_enum_value_is_valid (GNETWORK_TYPE_TCP_PROXY_TYPE, type), 0);

  _gnetwork_tcp_proxy_initialize ();
  if (proxies_enabled ())
    {
      retval = gconf_client_get_int (client, get_port_key_for_proxy_type (type), NULL);
    }
  else
    {
      retval = 0;
    }
  _gnetwork_tcp_proxy_shutdown ();

  return retval;
}


/**
 * _gnetwork_tcp_proxy_error_from_errno:
 * @en: the error number.
 * 
 * Retrieves the #GNetworkTcpProxyError which corresponds to the errno @en.
 * 
 * Returns: the error value for @en.
 * 
 * Since: 1.0
 **/
GNetworkTcpProxyError
_gnetwork_tcp_proxy_error_from_errno (gint en)
{
  switch (en)
    {
    case ECONNREFUSED:
      return GNETWORK_TCP_PROXY_ERROR_CONNECTION_REFUSED;
      break;
    case ETIMEDOUT:
      return GNETWORK_TCP_PROXY_ERROR_TIMEOUT;
      break;
    case ENETUNREACH:
      return GNETWORK_TCP_PROXY_ERROR_NETWORK_UNREACHABLE;
      break;
    case EACCES:
    case EPERM:
      return GNETWORK_TCP_PROXY_ERROR_FIREWALL;
      break;
    default:
      break;
    }

  return GNETWORK_TCP_PROXY_ERROR_UNKNOWN;
}


/**
 * _gnetwork_tcp_proxy_strerror:
 * @error: the error code.
 * @type: the type of proxy experiencing this error.
 * @destination: the intended destination.
 * 
 * Retrieves the error message string which corresponds to the error code and
 * type of proxy.
 * 
 * Returns: the error message for @error/@type/@destination.
 * 
 * Since: 1.0
 **/
gchar *
_gnetwork_tcp_proxy_strerror (GNetworkTcpProxyError error, GNetworkTcpProxyType type,
			      const GNetworkDnsEntry * destination)
{
  const gchar *format;
  gchar *host, *dest, *retval;

  g_return_val_if_fail (_gnetwork_enum_value_is_valid (error, GNETWORK_TYPE_TCP_PROXY_ERROR), NULL);
  g_return_val_if_fail (_gnetwork_enum_value_is_valid (type, GNETWORK_TYPE_TCP_PROXY_TYPE), NULL);
  g_return_val_if_fail (destination != NULL, NULL);

  _gnetwork_tcp_proxy_initialize ();

  switch (error)
    {
    case GNETWORK_TCP_PROXY_ERROR_UNKNOWN:
      format = _("The connection to %s could not be completed because the GNetwork library has a "
		 "bug in it.");
      break;

      /* Errors connecting *to* the proxy server. */
    case GNETWORK_TCP_PROXY_ERROR_CONNECTION_REFUSED:
      format = _("The connection to %s could not be completed because the proxy service at %s is "
		 "not running.");
      break;
    case GNETWORK_TCP_PROXY_ERROR_TIMEOUT:
      format = _("The connection to %s could not be completed because the proxy service at %s did "
		 "not respond to our requests for a connection.");
      break;
    case GNETWORK_TCP_PROXY_ERROR_NETWORK_UNREACHABLE:
      format = _("The connection to %s could not be completed because the proxy service at %s "
		 "could not be reached. Your network connection may be down or misconfigured.");
      break;
    case GNETWORK_TCP_PROXY_ERROR_FIREWALL:
      format = _("The connection to %s could not be completed because the proxy service at %s "
		 "is blocked by a firewall.");
      break;

      /* Errors connecting *through* the proxy server */
    case GNETWORK_TCP_PROXY_ERROR_ABORTED:
      format = _("The connection to %s could not be completed because the proxy service at %s "
		 "stopped the connection attempt.");
      break;
    case GNETWORK_TCP_PROXY_ERROR_AUTHENTICATION_FAILED:
      switch (type)
	{
	case GNETWORK_TCP_PROXY_HTTP:
	  format = _("The connection to %s could not be completed because the proxy service at %s "
		     "could not verify our user name and password.");
	  break;
	case GNETWORK_TCP_PROXY_HTTPS:
	  return NULL;
	  break;
	case GNETWORK_TCP_PROXY_FTP:
	  return NULL;
	  break;
	case GNETWORK_TCP_PROXY_SOCKS:
	  if (gconf_client_get_int (client, SOCKS_PROXY_VERSION_KEY, NULL) == 4)
	    {
	      format = _("The connection to %s could not be completed because the proxy service at "
			 "%s could not verify our user name. The Identity Service on this computer "
			 "is not running or is misconfigured.");
	    }
	  else
	    {
	      format = _("The connection to %s could not be completed because the proxy service at "
			 "%s does not allow the requested type of connection.");
	    }
	  break;
	default:
	  g_assert_not_reached ();
	  format = NULL;
	  break;
	}
      break;
    case GNETWORK_TCP_PROXY_ERROR_SERVER_FAILED:
      format = _("The connection to %s could not be completed because the proxy service at %s is "
		 "throwing a tantrum right now.");
      break;

    default:
      g_assert_not_reached ();
      format = NULL;
      break;
    }

  dest = g_strdup (gnetwork_dns_entry_get_hostname (destination));
  if (dest == NULL)
    dest = gnetwork_ip_address_to_string (gnetwork_dns_entry_get_ip_address (destination));

  host = gconf_client_get_string (client, HTTP_PROXY_HOST_KEY, NULL);
  retval = g_strdup_printf (format, dest, host);
  g_free (host);
  g_free (dest);

  _gnetwork_tcp_proxy_shutdown ();

  return retval;
}


/* ************************************************************************** *
 *  PUBLIC API                                                                *
 * ************************************************************************** */

/**
 * gnetwork_tcp_proxy_get_use_proxy:
 * @type: the type of proxy for this connection.
 * @address: the remote address to check the proxy for.
 *
 * This function checks @type and @address, and returns TRUE if a proxy
 * will be used, or %FALSE, if one won't. This is useful if you need to check to
 * see if you can create a #GNetworkServer without problems.
 *
 * Returns: a #gboolean if a proxy will be used for this connection.
 *
 * Since: 1.0
 **/
gboolean
gnetwork_tcp_proxy_get_use_proxy (GNetworkTcpProxyType type, const gchar * address)
{
  gboolean retval;

  g_return_val_if_fail (address != NULL, FALSE);
  g_return_val_if_fail (address[0] != '\0', FALSE);
  g_return_val_if_fail (_gnetwork_enum_value_is_valid (GNETWORK_TYPE_TCP_PROXY_TYPE, type), FALSE);

  if (type == GNETWORK_TCP_PROXY_NONE)
    return FALSE;

  _gnetwork_tcp_proxy_initialize ();

  if (proxies_enabled ())
    {
      GSList *exceptions;

      /* Checks to see if the host in @address is covered by an exception rule */
      for (exceptions = gconf_client_get_list (client, PROXY_EXCEPTIONS_KEY, GCONF_VALUE_LIST,
					       NULL), retval = TRUE;
	   exceptions != NULL && retval == TRUE;
	   exceptions = g_slist_remove_link (exceptions, exceptions),
	   retval = g_pattern_match_simple (exceptions->data, address));

      /* If it's not covered by a rule, check that the proxy host is != NULL */
      if (retval == FALSE)
	{
	  gchar *tmp = gconf_client_get_string (client, get_host_key_for_proxy_type (type), NULL);

	  retval = (tmp != NULL);
	  g_free (tmp);
	}
    }
  else
    {
      retval = FALSE;
    }

  _gnetwork_tcp_proxy_shutdown ();

  return retval;
}


G_LOCK_DEFINE_STATIC (quark);

GQuark
gnetwork_tcp_proxy_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (quark);
  if (quark == 0)
    {
      quark = g_quark_from_static_string ("gnetwork-tcp-proxy-error");
    }
  G_UNLOCK (quark);

  return quark;
}
