/*
 * GNetwork Library: libgnetwork/gnetwork-server.c
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */


#include "gnetwork-server.h"

#include "gnetwork-errors.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"

#include <glib/gi18n.h>


enum
{
  NEW_CONNECTION,
  ERROR,
  LAST_SIGNAL
};


typedef struct _EnumString
{
  const guint value;
  const gchar *const str;
}
EnumString;


static gint signals[LAST_SIGNAL] = { 0 };


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_server_base_init (gpointer g_iface)
{
  static gboolean initialized = FALSE;

  if (!initialized)
    {
      signals[NEW_CONNECTION] =
	g_signal_new ("new-connection",
		      GNETWORK_TYPE_SERVER,
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (GNetworkServerIface, new_connection),
		      NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
		      G_TYPE_NONE, 1, GNETWORK_TYPE_CONNECTION);
      signals[ERROR] =
	g_signal_new ("error",
		      GNETWORK_TYPE_SERVER,
		      (G_SIGNAL_RUN_FIRST | G_SIGNAL_DETAILED),
		      G_STRUCT_OFFSET (GNetworkServerIface, error),
		      NULL, NULL, g_cclosure_marshal_VOID__BOXED, G_TYPE_NONE, 1, G_TYPE_ERROR);

      g_object_interface_install_property (g_iface,
					   g_param_spec_enum ("status",
							      _("Server Status"),
							      _("The status of this server."),
							      GNETWORK_TYPE_SERVER_STATUS,
							      GNETWORK_SERVER_CLOSED,
							      G_PARAM_READABLE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_uint64 ("bytes-received",
								_("Bytes Received"),
								_("The number of bytes received "
								  "through this server."),
								0, (guint64) - 1, 0,
								G_PARAM_READABLE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_uint64 ("bytes-sent",
								_("Bytes Sent"),
								_("The number of bytes sent "
								  "through this server."),
								0, (guint64) (- 1), 0,
								G_PARAM_READABLE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_uint ("max-connections",
							      _("Maximum Incoming Connections"),
							      _("The maximum number of incoming "
								"connections to allow, or %0, if "
								"all connections should be "
								"allowed."), 0, G_MAXUINT, 0,
							      G_PARAM_READWRITE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_boolean ("close-children",
								 _("Close Children"),
								 _("Whether or not to close "
								   "currently open connections when "
								   "the server is closed."), TRUE,
								 G_PARAM_READWRITE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_value_array
					   ("connections", _("Connections"),
					    _("A value array of the currently open connections."),
					    g_param_spec_object ("connection",
								 _("An Open Connection"),
								 _("A single currently-open "
								   "connection."),
								 GNETWORK_TYPE_CONNECTION,
								 G_PARAM_READABLE),
					    G_PARAM_READABLE));

      initialized = TRUE;
    }
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_server_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      static const GTypeInfo info = {
	sizeof (GNetworkServerIface),	/* class_size */
	gnetwork_server_base_init,	/* base_init */
	NULL,			/* base_finalize */
	NULL,
	NULL,			/* class_finalize */
	NULL,			/* class_data */
	0,
	0,			/* n_preallocs */
	NULL
      };

      type = g_type_register_static (G_TYPE_INTERFACE, "GNetworkServer", &info, 0);

      g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
    }

  return type;
}


/**
 * gnetwork_server_open:
 * @server: the server to open.
 *
 * Starts the server process for @server.
 *
 * Since: 1.0
 **/
void
gnetwork_server_open (GNetworkServer * server)
{
  GNetworkServerIface *iface;

  g_return_if_fail (GNETWORK_IS_SERVER (server));

  iface = GNETWORK_SERVER_GET_IFACE (server);

  g_return_if_fail (iface->open != NULL);

  (*iface->open) (server);
}


/**
 * gnetwork_server_close:
 * @server: the server to close.
 *
 * Closes the @server in question.
 *
 * Since: 1.0
 **/
void
gnetwork_server_close (GNetworkServer * server)
{
  GNetworkServerIface *iface;

  g_return_if_fail (GNETWORK_IS_SERVER (server));

  iface = GNETWORK_SERVER_GET_IFACE (server);

  g_return_if_fail (iface->close != NULL);

  (*iface->close) (server);
}


/**
 * gnetwork_server_set_create_func:
 * @server: the server to modify.
 * @func: the function which will create new connections in response to client requests.
 * @data: the user data to pass to @func, or %NULL.
 * @notify: a function which will be called when @data is no longer needed, or %NULL.
 *
 * Sets the function which will be used by @server to create new connections as
 * needed to @func. The @notify function will be called with @data as it's
 * argument when this function is called again, or when @server is destroyed.
 * 
 * Implementations should provide a default create function, so if @func is %NULL,
 * the object should reset itself to call the default creation function.
 *
 * Since: 1.0
 **/
void
gnetwork_server_set_create_func (GNetworkServer * server, GNetworkServerCreateFunc func,
				 gpointer data, GDestroyNotify notify)
{
  GNetworkServerIface *iface;

  g_return_if_fail (GNETWORK_IS_SERVER (server));
  g_return_if_fail (func != NULL || (func == NULL && data == NULL && notify == NULL));

  iface = GNETWORK_SERVER_GET_IFACE (server);

  g_return_if_fail (iface->set_create_func != NULL);

  (*iface->set_create_func) (server, func, data, notify);
}


/**
 * gnetwork_server_new_connection:
 * @server: the server to use.
 * @connection: the new connection object.
 *
 * Emits the "new-connection" signal for @server, using @connection.
 *
 * Since: 1.0
 **/
void
gnetwork_server_new_connection (GNetworkServer * server, GNetworkConnection * connection)
{
  g_return_if_fail (GNETWORK_IS_SERVER (server));
  g_return_if_fail (GNETWORK_IS_CONNECTION (connection));

  g_signal_emit (server, signals[NEW_CONNECTION], 0, connection);
}


/**
 * gnetwork_server_error:
 * @server: the server to use.
 * @error: the error structure.
 *
 * Emits the "error" signal for @server, using @error.
 *
 * Since: 1.0
 **/
void
gnetwork_server_error (GNetworkServer * server, const GError * error)
{
  g_return_if_fail (GNETWORK_IS_SERVER (server));
  g_return_if_fail (error != NULL);

  g_signal_emit (server, signals[ERROR], error->domain, error);
}


/**
 * gnetwork_server_strerror:
 * @error: the server error code to use.
 * 
 * Retrieves an error message string for @error. The returned value should not
 * be modified or freed.
 *
 * Returns: the error message for @error.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_server_strerror (GNetworkServerError error)
{
  const gchar *str;

  g_return_val_if_fail (_gnetwork_enum_value_is_valid (GNETWORK_TYPE_SERVER_ERROR, error), NULL);

  switch (error)
    {
    case GNETWORK_SERVER_ERROR_INTERNAL:
      str = _("You cannot start a new service because an error occurred inside the GNetwork "
	      "library.");
      break;
    case GNETWORK_SERVER_ERROR_TOO_MANY_CONNECTIONS:
      str = _("You cannot start a new service because the maximum number of incoming connections "
	      "has been reached.");
      break;
    case GNETWORK_SERVER_ERROR_PERMISSIONS:
      str = _("You cannot start a new service because you do not have permission to create one.");
      break;
    case GNETWORK_SERVER_ERROR_NO_MEMORY:
      str = _("You cannot start a new service because your computer is out of memory for "
	      "networking purposes.");
      break;
    case GNETWORK_SERVER_ERROR_TOO_MANY_PROCESSES:
      str = _("You cannot start a new service because there are too many applications open.");
      break;
    case GNETWORK_SERVER_ERROR_ALREADY_EXISTS:
      str = _("You cannot start a new service because there is service being used already.");
    default:
      g_assert_not_reached ();
      str = NULL;
    }

  return str;
}


G_LOCK_DEFINE_STATIC (quark);

GQuark
gnetwork_server_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (quark);

  if (quark == 0)
    {
      quark = g_quark_from_static_string ("gnetwork-server-error");
    }

  G_UNLOCK (quark);

  return quark;
}
