/* 
 * GNetwork Library: libgnetwork/gnetwork-utils.c
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */


#include "gnetwork-utils.h"

#include <sys/types.h>
#include <errno.h>

#include <net/if.h>
#ifdef __linux__
#include <netpacket/packet.h>
#include <netinet/ether.h>
#else
#include <net/ethernet.h>
#endif
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>


/**
 * _gnetwork_slist_to_value_array:
 * @list: the list of items.
 * @item_type: the registered type of the items in @list.
 *
 * Copies the items of type @item_type from @list to a #GValueArray. The
 * returned data should be freed with g_value_array_free().
 * 
 * Returns: a new value array containing the items in @list.
 * 
 * Since: 1.0
 **/
GValueArray *
_gnetwork_slist_to_value_array (GSList * list, GType item_type)
{
  GValueArray *array;
  GValue value = { 0 };

  g_return_val_if_fail (item_type != G_TYPE_INVALID, NULL);
  g_return_val_if_fail (item_type != G_TYPE_NONE, NULL);

  array = g_value_array_new (g_slist_length (list));

  for (; list != NULL; list = list->next)
    {
      switch (G_TYPE_FUNDAMENTAL (item_type))
	{
	case G_TYPE_POINTER:
	  g_value_set_pointer (&value, list->data);
	  break;

	case G_TYPE_INTERFACE:
	  {
	    GType *prereqs;
	    guint n_prereqs, i;
	    gboolean is_object;

	    /* Search the prerequisites, if one is G_TYPE_OBJECT, then we use g_value_set_object,
	     * otherwise, g_value_set_pointer. */
	    prereqs = g_type_interface_prerequisites (item_type, &n_prereqs);

	    for (i = 0, is_object = FALSE; i < n_prereqs && !is_object;
		 i++, is_object = G_TYPE_FUNDAMENTAL (prereqs[i] == G_TYPE_OBJECT));

	    g_free (prereqs);

	    if (is_object)
	      g_value_set_object (&value, list->data);
	    else
	      g_value_set_pointer (&value, list->data);
	  }
	  break;

	case G_TYPE_STRING:
	  g_value_set_string (&value, (const gchar *) list->data);
	  break;

	case G_TYPE_BOXED:
	  g_value_set_boxed (&value, list->data);
	  break;

	case G_TYPE_OBJECT:
	  g_value_set_object (&value, list->data);
	  break;

	case G_TYPE_CHAR:
	  g_value_set_char (&value, (gchar) GPOINTER_TO_INT (list->data));
	  break;
	case G_TYPE_UCHAR:
	  g_value_set_uchar (&value, (guchar) GPOINTER_TO_UINT (list->data));
	  break;

	case G_TYPE_INT:
	  g_value_set_int (&value, GPOINTER_TO_INT (list->data));
	  break;
	case G_TYPE_UINT:
	  g_value_set_int (&value, GPOINTER_TO_UINT (list->data));
	  break;

	case G_TYPE_ENUM:
	  g_value_set_enum (&value, GPOINTER_TO_INT (list->data));
	  break;
	case G_TYPE_FLAGS:
	  g_value_set_flags (&value, GPOINTER_TO_UINT (list->data));
	  break;

	case G_TYPE_PARAM:
	  g_value_set_param (&value, G_PARAM_SPEC (list->data));
	  break;

	case G_TYPE_LONG:
	  g_value_set_long (&value, *((glong *) (list->data)));
	  break;
	case G_TYPE_ULONG:
	  g_value_set_ulong (&value, *((gulong *) (list->data)));
	  break;

	case G_TYPE_FLOAT:
	  g_value_set_float (&value, *((gfloat *) (list->data)));
	  break;
	case G_TYPE_DOUBLE:
	  g_value_set_double (&value, *((gdouble *) (list->data)));
	  break;

	case G_TYPE_INT64:
	  g_value_set_int64 (&value, *((gint64 *) (list->data)));
	  break;
	case G_TYPE_UINT64:
	  g_value_set_uint64 (&value, *((guint64 *) (list->data)));
	  break;

	default:
	  g_assert_not_reached ();
	  break;
	}

      g_value_array_append (array, &value);

      g_value_reset (&value);
    }

  return array;
}


static gboolean
remove_all (gpointer key, gpointer value, gpointer data)
{
  return TRUE;
}


/**
 * _gnetwork_hash_table_clear:
 * 
 * Removes all the items in a hash table without destroying it.
 * 
 * Since: 1.0
 **/
void
_gnetwork_hash_table_clear (GHashTable * table)
{
  g_hash_table_foreach_remove (table, remove_all, NULL);
}


/**
 * _gnetwork_slist_from_hash_table:
 * 
 * A GHFunc which puts value in the "(GSList **) data" list.
 * 
 * Since: 1.0
 **/
void
_gnetwork_slist_from_hash_table (gpointer key, gpointer value, gpointer data)
{
  *((GSList **) data) = g_slist_prepend (*((GSList **) data), value);
}


/**
 * _gnetwork_flags_value_is_valid:
 * @flags_type: the registered #GFlags type.
 * @value: the value to check.
 * 
 * Checks to see if @value is a valid bitmask for @flags_type.
 * 
 * Returns: %TRUE or %FALSE, depending @value.
 * 
 * Since: 1.0
 **/
gboolean
_gnetwork_flags_value_is_valid (GType flags_type, guint value)
{
  GFlagsClass *flags_class;
  gboolean retval;

  g_return_val_if_fail (G_TYPE_IS_FLAGS (flags_type), FALSE);

  flags_class = g_type_class_ref (flags_type);
  retval = ((value & ~(flags_class->mask)) == 0);
  g_type_class_unref (flags_class);

  return retval;
}


/**
 * _gnetwork_enum_value_is_valid:
 * @enum_type: the registered #GEnum type.
 * @value: the value to check.
 * 
 * Checks to see if @value is a valid enumeration for @enum_type.
 * 
 * Returns: %TRUE or %FALSE, depending @value.
 * 
 * Since: 1.0
 **/
gboolean
_gnetwork_enum_value_is_valid (GType enum_type, gint value)
{
  GEnumClass *enum_class;
  gboolean retval;

  g_return_val_if_fail (G_TYPE_IS_ENUM (enum_type), FALSE);

  enum_class = g_type_class_ref (enum_type);
  retval = (g_enum_get_value (enum_class, value) != NULL);
  g_type_class_unref (enum_class);

  return retval;
}


gchar *
_gnetwork_sockaddr_get_address (const struct sockaddr * sock)
{
  if (sock != NULL)
    {
      gchar address[INET_ADDRESS_SIZE + 1] = { 0 };

      switch (sock->sa_family)
	{
	case AF_INET:
	  inet_ntop (sock->sa_family,
		     &(((struct sockaddr_in *) sock)->sin_addr), address, INET_ADDRESS_SIZE);
	  break;
	case AF_INET6:
	  inet_ntop (sock->sa_family,
		     &(((struct sockaddr_in6 *) sock)->sin6_addr), address, INET_ADDRESS_SIZE);
	  break;
#ifdef AF_PACKET
	case AF_PACKET:
	  switch (((struct sockaddr_ll *) sock)->sll_halen)
	    {
	    case 6:
	      g_snprintf (address, INET_ADDRESS_SIZE,
			  "%02x:%02x:%02x:%02x:%02x:%02x",
			  ((struct sockaddr_ll *) sock)->
			  sll_addr[0],
			  ((struct sockaddr_ll *) sock)->
			  sll_addr[1],
			  ((struct sockaddr_ll *) sock)->
			  sll_addr[2],
			  ((struct sockaddr_ll *) sock)->
			  sll_addr[3],
			  ((struct sockaddr_ll *) sock)->
			  sll_addr[4], ((struct sockaddr_ll *) sock)->sll_addr[5]);
	      break;
	    default:
	      break;
	    }
	  break;
#endif
	}

      return (address[0] != '\0' ? g_strdup (address) : NULL);
    }

  return NULL;
}


guint16
_gnetwork_sockaddr_get_port (const struct sockaddr * sock)
{
  if (sock == NULL)
    return 0;

  switch (sock->sa_family)
    {
    case AF_INET:
      return g_ntohs (((struct sockaddr_in *) sock)->sin_port);
    case AF_INET6:
      return g_ntohs (((struct sockaddr_in *) sock)->sin_port);
    default:
      break;
    }

  g_return_val_if_reached (0);
}


GNetworkProtocols
_gnetwork_get_socket_protocol (gpointer sockfd)
{
  struct sockaddr_storage sa;
  socklen_t sa_size;
  GNetworkProtocols retval;

  memset (&sa, 0, sizeof (sa));
  sa_size = sizeof (sa);
  retval = GNETWORK_PROTOCOL_NONE;

  if (getsockname (GPOINTER_TO_INT (sockfd), (struct sockaddr *) &sa, &sa_size) >= 0)
    {
      switch (sa.ss_family)
	{
	case AF_INET:
	  retval = GNETWORK_PROTOCOL_IPv4;
	  break;
	case AF_INET6:
	  retval = GNETWORK_PROTOCOL_IPv6;
	  break;
#ifdef AF_PACKET
	case AF_PACKET:
	  retval = GNETWORK_PROTOCOL_PACKET;
	  break;
#endif
	default:
	  break;
	}
    }

  return retval;
}


gboolean
_gnetwork_boolean_accumulator (GSignalInvocationHint * ihint, GValue * return_accu,
			       const GValue * handler_return, gpointer dummy)
{
  gboolean continue_emission;
  gboolean signal_handled;

  signal_handled = g_value_get_boolean (handler_return);
  g_value_set_boolean (return_accu, signal_handled);
  continue_emission = !signal_handled;

  return continue_emission;
}
