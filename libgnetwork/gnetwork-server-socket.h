/* 
 * GNetwork Library: libgnetwork/gnetwork-server-socket.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_SERVER_SOCKET_H__
#define __GNETWORK_SERVER_SOCKET_H__ 1

#include "gnetwork-data-socket.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_SERVER_SOCKET \
  (gnetwork_server_socket_get_type ())
#define GNETWORK_SERVER_SOCKET(object) \
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GNETWORK_TYPE_SERVER_SOCKET, GNetworkServerSocket))
#define GNETWORK_SERVER_SOCKET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_SERVER_SOCKET, GNetworkServerSocketClass))
#define GNETWORK_IS_SERVER_SOCKET(object) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GNETWORK_TYPE_SERVER_SOCKET))
#define GNETWORK_IS_SERVER_SOCKET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_SERVER_SOCKET))
#define GNETWORK_SERVER_SOCKET_GET_CLASS(object) \
  (G_TYPE_INSTANCE_GET_CLASS ((object), GNETWORK_TYPE_SERVER_SOCKET, GNetworkServerSocketClass))


typedef struct _GNetworkServerSocket GNetworkServerSocket;
typedef struct _GNetworkServerSocketClass GNetworkServerSocketClass;
typedef struct _GNetworkServerSocketPrivate GNetworkServerSocketPrivate;


struct _GNetworkServerSocket
{
  /* < private > */
  GNetworkSocket parent;

  GNetworkServerSocketPrivate *_priv;
};

struct _GNetworkServerSocketClass
{
  /* <private> */
  GNetworkSocketClass parent_class;

  /* <public> */
  /* Methods */
  GNetworkDataSocket *(*create_socket)  (GNetworkServerSocket * server,
					 GNetworkParams * params);

  /* Signals */
  void		      (*new_socket)     (GNetworkServerSocket * server,
				         GNetworkDataSocket * socket);

  /* <private> */
  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_server_socket_get_type (void) G_GNUC_CONST;

GNetworkDataSocket *gnetwork_server_socket_create_socket (GNetworkServerSocket * server,
						          GNetworkParams * params);
void gnetwork_server_socket_new_socket (GNetworkServerSocket * server, GNetworkDataSocket * socket);


G_END_DECLS

#endif /* !__GNETWORK_SERVER_SOCKET_H__ */
