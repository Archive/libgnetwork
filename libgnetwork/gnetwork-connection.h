/*
 * GNetwork Library: libgnetwork/gnetwork-connection.h
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_CONNECTION_H__
#define __GNETWORK_CONNECTION_H__

#include "gnetwork-macros.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_CONNECTION		(gnetwork_connection_get_type ())
#define GNETWORK_CONNECTION(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNETWORK_TYPE_CONNECTION, GNetworkConnection))
#define GNETWORK_IS_CONNECTION(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNETWORK_TYPE_CONNECTION))
#define GNETWORK_CONNECTION_GET_IFACE(obj)	(G_TYPE_INSTANCE_GET_INTERFACE ((obj), GNETWORK_TYPE_CONNECTION, GNetworkConnectionIface))
#define GNETWORK_CONNECTION_CALL_PARENT(obj,method,args) \
  (GNETWORK_INTERFACE_CALL_PARENT (GNETWORK_CONNECTION_GET_IFACE, GNetworkConnectionIface, obj, method, args))

#define GNETWORK_CONNECTION_ERROR		(gnetwork_connection_error_get_quark ())


typedef enum /* <prefix=GNETWORK_CONNECTION> */
{
  GNETWORK_CONNECTION_INVALID,

  GNETWORK_CONNECTION_CLIENT,
  GNETWORK_CONNECTION_SERVER
}
GNetworkConnectionType;


typedef enum /* <prefix=GNETWORK_CONNECTION> */
{
  GNETWORK_CONNECTION_CLOSING,
  GNETWORK_CONNECTION_CLOSED,
  GNETWORK_CONNECTION_OPENING,
  GNETWORK_CONNECTION_OPEN
}
GNetworkConnectionStatus;


typedef enum /* <prefix=GNETWORK_CONNECTION_ERROR> */
{
  GNETWORK_CONNECTION_ERROR_INTERNAL,

  GNETWORK_CONNECTION_ERROR_REFUSED,
  GNETWORK_CONNECTION_ERROR_TIMEOUT,
  GNETWORK_CONNECTION_ERROR_UNREACHABLE,
  GNETWORK_CONNECTION_ERROR_PERMISSIONS
}
GNetworkConnectionError;


typedef struct _GNetworkConnection GNetworkConnection;
typedef struct _GNetworkConnectionIface GNetworkConnectionIface;


typedef void (*GNetworkConnectionFunc) (GNetworkConnection * connection);
typedef void (*GNetworkConnectionSendFunc) (GNetworkConnection * connection,
					    gconstpointer data,
					    gsize length);


struct _GNetworkConnectionIface
{
  /* < private > */
  GTypeInterface g_iface;

  /* < public > */
  /* Signals */
  void (*received)           (GNetworkConnection * connection,
			      gconstpointer data,
			      gulong length);
  void (*sent)		     (GNetworkConnection * connection,
			      gconstpointer data,
			      gulong length);
  void (*error)		     (GNetworkConnection * connection,
			      GError * error);

  /* Methods */
  GNetworkConnectionFunc      open;
  GNetworkConnectionFunc      close;
  GNetworkConnectionSendFunc  send;

  /* < private > */
  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_connection_get_type (void) G_GNUC_CONST;

/* Methods */
void gnetwork_connection_open (GNetworkConnection * connection);
void gnetwork_connection_close (GNetworkConnection * connection);
void gnetwork_connection_send (GNetworkConnection * connection, gconstpointer data, glong length);

/* Signals */
void gnetwork_connection_received (GNetworkConnection * connection, gconstpointer data,
				   gulong length);
void gnetwork_connection_sent (GNetworkConnection * connection, gconstpointer data, gulong length);
void gnetwork_connection_error (GNetworkConnection * connection, const GError * error);

/* Error Reporting */
GQuark gnetwork_connection_error_get_quark (void) G_GNUC_CONST;
G_CONST_RETURN gchar *gnetwork_connection_strerror (GNetworkConnectionError error);


G_END_DECLS

#endif /* __GNETWORK_CONNECTION_H__ */
