/*
 * GNetwork Library: libgnetwork/gnetwork-data-socket.c
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-data-socket.h"

#include "gnetwork-buffer.h"
#include "gnetwork-type-builtins.h"

#include <glib/gi18n.h>

#define GNETWORK_DATA_SOCKET_GET_PRIVATE(obj)	(GNETWORK_DATA_SOCKET (obj)->_priv)

/* 1504 */
#define DEFAULT_BUFFER_SIZE	0x5E0


/**
 * GNetworkDataSocket:
 * 
 * The structure for data socket object instances. This structures contains no
 * public members.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkDataSocketClass:
 * @send: the vtable entry for the "send" method.
 * @sent: the C closure location for the "sent" signal.
 * @received: the C closure location for the "received" signal.
 * 
 * The structure for the data socket object class. All signals and methods are
 * parent-relative, but may be %NULL.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkDataSocketDirection:
 * @GNETWORK_DATA_SOCKET_UNKNOWN: the socket direction is unset.
 * @GNETWORK_DATA_SOCKET_INCOMING: a "server"-style socket.
 * @GNETWORK_DATA_SOCKET_OUTGOING: a "client"-style socket.
 * 
 * An enumeration used to indicate whether this socket is a "server"-style
 * socket, used for incoming connections, or whether it's a "client"-style
 * socket, used for outgoing connections.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkDataSocketError:
 * @GNETWORK_DATA_SOCKET_ERROR_REFUSED: the server refused to allow our connection, or is not available.
 * @GNETWORK_DATA_SOCKET_ERROR_TIMED_OUT: the connection took too long to complete (the host may be down).
 * @GNETWORK_DATA_SOCKET_ERROR_UNREACHABLE: the server could not be reached.
 * @GNETWORK_DATA_SOCKET_ERROR_HOST_CLOSED: the remote socket closed the connection.
 * @GNETWORK_DATA_SOCKET_ERROR_SEND_FAILED: the send operation failed.
 * @GNETWORK_DATA_SOCKET_ERROR_RECEIVE_FAILED: the socket could not read the incoming data.
 * 
 * An enumeration of errors common to data sockets.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_DATA_SOCKET:
 * @object: the pointer to cast.
 * 
 * Casts the #GNetworkDataSocket derived pointer at @object into a
 * (GNetworkDataSocket*) pointer. Depending on the current debugging level, this
 * function may invoke certain runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_DATA_SOCKET:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GNetworkDataSocket (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_DATA_SOCKET_CLASS:
 * @klass: the pointer to cast.
 * 
 * Casts a the #GNetworkDataSocketClass derieved pointer at @klass into a
 * (GNetworkDataSocketClass*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_DATA_SOCKET_CLASS:
 * @klass: the pointer to check.
 * 
 * Checks whether @klass is a #GNetworkDataSocketClass (or derived) class.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_DATA_SOCKET_GET_CLASS:
 * @object: the pointer to check.
 * 
 * Retrieves a pointer to the #GNetworkDataSocketClass for the
 * #GNetworkDataSocket (or derived) instance @object.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_TYPE_DATA_SOCKET:
 * 
 * The registered #GType of #GNetworkDataSocket.
 * 
 * Since: 1.0
 **/


/* ************** *
 *  Enumerations  *
 * ************** */
 
enum
{
  PROP_0,

  BUFFER_SIZE,
  DIRECTION
};

enum
{
  SENT,
  RECEIVED,

  LAST_SIGNAL
};


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkDataSocketPrivate
{
  guint buffer_size;
  GNetworkDataSocketDirection direction:2;
};


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer gnetwork_data_socket_parent_class = NULL;
static guint gnetwork_data_socket_signals[LAST_SIGNAL] = { 0 };


/* ************************** *
 *  GNetworkSocket Functions  *
 * ************************** */

static void
gnetwork_data_socket_real_sent (GNetworkDataSocket * socket, GNetworkParams * data)
{
  GNetworkBuffer *buffer = NULL;

  gnetwork_params_get (data, "raw-data", &buffer, NULL);

  if (buffer != NULL && gnetwork_buffer_get_length (buffer) > 0)
    {
      gnetwork_socket_add_to_bytes_sent (GNETWORK_SOCKET (socket),
					 gnetwork_buffer_get_length (buffer));
    }

  gnetwork_buffer_unref (buffer);
}


static void
gnetwork_data_socket_real_received (GNetworkDataSocket * socket, GNetworkParams * data)
{
  GNetworkBuffer *buffer = NULL;

  gnetwork_params_get (data, "raw-data", &buffer, NULL);

  if (buffer != NULL && gnetwork_buffer_get_length (buffer) > 0)
    {
      gnetwork_socket_add_to_bytes_received (GNETWORK_SOCKET (socket),
					     gnetwork_buffer_get_length (buffer));
    }

  gnetwork_buffer_unref (buffer);
}


/* ************************** *
 *  GNetworkSocket Functions  *
 * ************************** */

static void
gnetwork_data_socket_open (GNetworkSocket * socket)
{
  GNetworkDataSocketPrivate *priv = GNETWORK_DATA_SOCKET_GET_PRIVATE (socket);

  if (priv->direction == GNETWORK_DATA_SOCKET_UNKNOWN)
    {
      g_warning ("The \"direction\" property must be set on `%s' objects before they can be "
		 "opened.", G_OBJECT_TYPE_NAME (socket));
    }

  if (GNETWORK_SOCKET_CLASS (gnetwork_data_socket_parent_class)->open != NULL)
    (*GNETWORK_SOCKET_CLASS (gnetwork_data_socket_parent_class)->open) (socket);
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_data_socket_set_property (GObject * object, guint property, const GValue * value,
			      GParamSpec * pspec)
{
  GNetworkDataSocketPrivate *priv = GNETWORK_DATA_SOCKET_GET_PRIVATE (object);

  switch (property)
    {
    case BUFFER_SIZE:
      priv->buffer_size = g_value_get_uint (value);
      break;
    case DIRECTION:
      priv->direction = g_value_get_enum (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_data_socket_get_property (GObject * object, guint property, GValue * value,
				   GParamSpec * pspec)
{
  GNetworkDataSocketPrivate *priv = GNETWORK_DATA_SOCKET_GET_PRIVATE (object);

  switch (property)
    {
    case BUFFER_SIZE:
      g_value_set_uint (value, priv->buffer_size);
      break;
    case DIRECTION:
      g_value_set_enum (value, priv->direction);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_data_socket_finalize (GObject * object)
{
  GNetworkDataSocketPrivate *priv = GNETWORK_DATA_SOCKET_GET_PRIVATE (object);

  g_free (priv);

  if (G_OBJECT_CLASS (gnetwork_data_socket_parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (gnetwork_data_socket_parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_data_socket_class_init (GNetworkDataSocketClass * class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GNetworkObjectClass *netobject_class = GNETWORK_OBJECT_CLASS (class);
  GNetworkSocketClass *socket_class = GNETWORK_SOCKET_CLASS (class);

  gnetwork_data_socket_parent_class = g_type_class_peek_parent (class);

  gobject_class->set_property = gnetwork_data_socket_set_property;
  gobject_class->get_property = gnetwork_data_socket_get_property;
  gobject_class->finalize = gnetwork_data_socket_finalize;

  socket_class->open = gnetwork_data_socket_open;

  class->sent = gnetwork_data_socket_real_sent;
  class->received = gnetwork_data_socket_real_received;

  class->send = NULL;

/**
 * GNetworkDataSocket::sent:
 * @socket: the object which emitted this signal.
 * @params: parameters specific to this signal emission.
 * 
 * This signal is emitted by socket objects after data has been sent.
 * 
 * Since: 1.0
 **/
  gnetwork_data_socket_signals[SENT] =
    g_signal_new ("sent",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GNetworkDataSocketClass, sent),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__BOXED,
		  G_TYPE_NONE, 1, GNETWORK_TYPE_PARAMS);

/**
 * GNetworkDataSocket::received:
 * @socket: the object which emitted this signal.
 * @params: parameters specific to this signal emission.
 * 
 * This signal is emitted by socket objects when new data has been received.
 * 
 * Since: 1.0
 **/
  gnetwork_data_socket_signals[RECEIVED] =
    g_signal_new ("received",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GNetworkDataSocketClass, received),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__BOXED,
		  G_TYPE_NONE, 1, GNETWORK_TYPE_PARAMS);

  /* Properties */
  g_object_class_install_property (gobject_class, DIRECTION,
				   g_param_spec_enum ("direction", _("Data Socket Direction"),
						      _("The direction of this socket."),
						      GNETWORK_TYPE_DATA_SOCKET_DIRECTION,
						      GNETWORK_DATA_SOCKET_UNKNOWN,
						      G_PARAM_READWRITE));
  g_object_class_install_property (gobject_class, BUFFER_SIZE,
				   g_param_spec_uint ("buffer-size", _("Buffer Size"),
						      _("The buffer size to use."),
						      0, G_MAXUINT, DEFAULT_BUFFER_SIZE,
						      G_PARAM_READWRITE));

  /* Signal/Method Parameters */
  gnetwork_object_class_install_param (netobject_class,
				       g_param_spec_boxed ("raw-data", _("Raw Data"),
							   _("The raw data in question."),
							   GNETWORK_TYPE_BUFFER,
							   G_PARAM_READWRITE));

  g_type_class_add_private (class, sizeof (GNetworkDataSocketPrivate));  
}


static void
gnetwork_data_socket_instance_init (GNetworkDataSocket * socket)
{
  socket->_priv = G_TYPE_INSTANCE_GET_PRIVATE (socket, GNETWORK_TYPE_DATA_SOCKET,
					       GNetworkDataSocketPrivate);

  socket->_priv->buffer_size = DEFAULT_BUFFER_SIZE;
  socket->_priv->direction = GNETWORK_DATA_SOCKET_UNKNOWN;
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_data_socket_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      static const GTypeInfo info = {
	sizeof (GNetworkDataSocketClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_data_socket_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkDataSocket),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_data_socket_instance_init,
	NULL			/* value table */
      };

      type = g_type_register_static (GNETWORK_TYPE_SOCKET, "GNetworkDataSocket", &info,
				     G_TYPE_FLAG_ABSTRACT);
    }

  return type;
}


/**
 * gnetwork_data_socket_send:
 * @socket: the socket object to use.
 * @params: the parameters to use while sending.
 * 
 * This function calls the "send" method for @socket to send the data in
 * @params.
 * 
 * Since: 1.0
 **/
void
gnetwork_data_socket_send (GNetworkDataSocket * socket, GNetworkParams * params)
{
  GNetworkDataSocketClass *class;

  g_return_if_fail (GNETWORK_IS_DATA_SOCKET (socket));
  g_return_if_fail (GNETWORK_IS_PARAMS (params));

  class = GNETWORK_DATA_SOCKET_GET_CLASS (socket);

  if (class->send != NULL)
    {
      (*class->send) (socket, params);
    }
  else
    {
      g_warning ("%s: The socket type `%s' does not support the send() method.", G_STRLOC,
		 G_OBJECT_TYPE_NAME (socket));
    }
}


/**
 * gnetwork_data_socket_sent:
 * @socket: the socket object.
 * @params: the parameters used.
 * 
 * Emits the "sent" signal for @socket. Applications typically will not use this
 * function, it exists for subclasses of the socket object.
 * 
 * Since: 1.0
 **/
void
gnetwork_data_socket_sent (GNetworkDataSocket * socket, GNetworkParams * params)
{
  g_return_if_fail (GNETWORK_IS_DATA_SOCKET (socket));
  g_return_if_fail (GNETWORK_IS_PARAMS (params));

  g_signal_emit (socket, gnetwork_data_socket_signals[SENT], 0, params);
}


/**
 * gnetwork_data_socket_received:
 * @socket: the socket object.
 * @params: the parameters used.
 * 
 * Emits the "received" signal for @socket. Applications typically will not use
 * this function, it exists for subclasses of the socket object.
 * 
 * Since: 1.0
 **/
void
gnetwork_data_socket_received (GNetworkDataSocket * socket, GNetworkParams * params)
{
  g_return_if_fail (GNETWORK_IS_DATA_SOCKET (socket));
  g_return_if_fail (GNETWORK_IS_PARAMS (params));

  g_signal_emit (socket, gnetwork_data_socket_signals[RECEIVED], 0, params);
}


GQuark
gnetwork_data_socket_error_get_quark (void)
{
  static GQuark quark = 0;

  if (G_UNLIKELY (quark == 0))
    quark = g_quark_from_static_string ("gnetwork-data-socket-error");
  
  return quark;
}
