/*
 * GNetwork Library: libgnetwork/gnetwork-dns.c
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-dns.h"

#include "gnetwork-ip-address.h"
#include "gnetwork-threads.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"

#include <time.h>
#include <netdb.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <glib/gi18n.h>


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkDnsEntry
{
  GTypeClass g_class;

  gchar *hostname;
  GNetworkIpAddress *ip_address;
};

typedef struct _EnumString
{
  const guint value;
  const gchar *const str;
}
EnumString;


/* ****************** *
 *  Global Variables  *
 * ****************** */

G_LOCK_DEFINE_STATIC (lookups);

static GSList *lookups = NULL;


/* ************* *
 *  Lookup Data  *
 * ************* */

struct _GNetworkDnsLookup
{
  GMainContext *context;
  GStaticMutex *mutex;

  gchar *address;

  GNetworkDnsCallbackFunc callback;
  gpointer data;
  GDestroyNotify notify;

  gboolean running_cb:1;
};


static void
gnetwork_dns_lookup_free (GNetworkDnsLookup * lookup)
{
  if (lookup == NULL)
    return;

  g_free (lookup->address);

  if (lookup->notify != NULL && lookup->data != NULL)
    (*lookup->notify) (lookup->data);

  if (lookup->context != NULL)
    g_main_context_unref (lookup->context);

  g_free (lookup);
}


/* ******************* *
 *  Callback Handling  *
 * ******************* */

typedef struct
{
  GNetworkDnsLookup *lookup;
  GSList *list;
  GError *error;
}
GNetworkDnsCallbackData;


static gboolean
run_callback (GNetworkDnsCallbackData * data)
{
  (*data->lookup->callback) (data->list, data->error, data->lookup->data);

  return FALSE;
}


static void
gnetwork_dns_callback_data_free (GNetworkDnsCallbackData * data)
{
  gnetwork_dns_lookup_free (data->lookup);

  if (data->list != NULL)
    {
      g_slist_foreach (data->list, (GFunc) gnetwork_dns_entry_free, NULL);
      g_slist_free (data->list);
    }

  if (data->error != NULL)
    g_error_free (data->error);

  g_free (data);
}


/* ************ *
 *  DNS Lookup  *
 * ************ */

static gpointer
getaddrinfo_lookup_thread (GNetworkDnsLookup * lookup)
{
  struct addrinfo *list, *current;
  struct addrinfo hints;
  gint result;

  G_LOCK (lookups);
  lookups = g_slist_prepend (lookups, lookup);
  G_UNLOCK (lookups);

  memset (&hints, 0, sizeof (struct addrinfo));

  hints.ai_flags = AI_CANONNAME;
#ifdef AI_ADDRCONFIG
  hints.ai_flags |= AI_ADDRCONFIG;
#endif
#ifdef AI_V4MAPPED
  hints.ai_flags |= AI_V4MAPPED;
#endif
#ifdef AI_ALL
  hints.ai_flags |= AI_ALL;
#endif
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  list = NULL;
  result = getaddrinfo (lookup->address, NULL, &hints, &list);

  G_LOCK (lookups);
  if (g_slist_find (lookups, lookup))
    {
      GNetworkDnsCallbackData *cb_data;

      /* It's too late to cancel by now... */
      lookups = g_slist_remove (lookups, lookup);

      cb_data = g_new0 (GNetworkDnsCallbackData, 1);
      cb_data->lookup = lookup;
      cb_data->list = NULL;

      switch (result)
	{
	case 0:
	  for (current = list; current != NULL; current = current->ai_next)
	    {
	      GNetworkIpAddress ip_address; //, old;

	      if (current->ai_addr->sa_family == AF_INET || current->ai_addr->sa_family == AF_INET6)
		{
		  _gnetwork_ip_address_set_from_sockaddr (&ip_address, current->ai_addr);
		  cb_data->list =
		    g_slist_prepend (cb_data->list,
				     gnetwork_dns_entry_new (current->ai_canonname, &ip_address));
		}
	    }
	  break;
	case EAI_AGAIN:
	  cb_data->error =
	    g_error_new_literal (GNETWORK_DNS_ERROR, GNETWORK_DNS_ERROR_TRY_AGAIN,
				 gnetwork_dns_strerror (GNETWORK_DNS_ERROR_TRY_AGAIN));
	  break;
	case EAI_NONAME:
	  cb_data->error =
	    g_error_new_literal (GNETWORK_DNS_ERROR, GNETWORK_DNS_ERROR_NOT_FOUND,
				 gnetwork_dns_strerror (GNETWORK_DNS_ERROR_NOT_FOUND));
	  break;
	case EAI_FAIL:
	  cb_data->error =
	    g_error_new_literal (GNETWORK_DNS_ERROR, GNETWORK_DNS_ERROR_NO_RECOVERY,
				 gnetwork_dns_strerror (GNETWORK_DNS_ERROR_NO_RECOVERY));
	  break;
	default:
	  cb_data->error =
	    g_error_new_literal (GNETWORK_DNS_ERROR, GNETWORK_DNS_ERROR_INTERNAL,
				 gnetwork_dns_strerror (GNETWORK_DNS_ERROR_INTERNAL));
	  break;
	}

      /* gnetwork_dns_callback_data_free will unlock the lookups mutex. */
      gnetwork_thread_timeout_add_full (G_PRIORITY_DEFAULT, 0, (GSourceFunc) run_callback, cb_data,
					(GDestroyNotify) gnetwork_dns_callback_data_free);
    }
  else
    {
      gnetwork_dns_lookup_free (lookup);
    }
  G_UNLOCK (lookups);

  if (list != NULL)
    freeaddrinfo (list);

  return NULL;
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

/**
 * gnetwork_dns_get:
 * @address: the hostname or IP address to find.
 * @callback: the callback to be called when the lookup has completed.
 * @data: the user data to pass to @callback.
 * @notify: a function capable of freeing @user_data, or %NULL.
 *
 * This function performs an asynchronous DNS lookup (or reverse lookup) on the
 * hostname or IP address in @address. After @callback has been called, @notify
 * will be called with @data as it's argument. If there an error occurred trying
 * to create the DNS lookup thread, @error will be set, and
 * #GNETWORK_DNS_HANDLE_INVALID will be returned.
 *
 * Note: @callback may be called before this function returns if there was a
 * threading error.
 *
 * Returns: a #GNetworkDnsHandle used to cancel the lookup.
 *
 * Since: 1.0
 **/
GNetworkDnsHandle
gnetwork_dns_get (const gchar * address, GNetworkDnsCallbackFunc callback, gpointer data,
		  GDestroyNotify notify)
{
  GNetworkDnsLookup *lookup;
  GError *error;

  g_return_val_if_fail (address != NULL && address[0] != '\0' && strlen (address) < NI_MAXHOST,
			GNETWORK_DNS_HANDLE_INVALID);
  g_return_val_if_fail (callback != NULL, GNETWORK_DNS_HANDLE_INVALID);
  g_return_val_if_fail (data != NULL || (data == NULL && notify == NULL),
			GNETWORK_DNS_HANDLE_INVALID);

  lookup = g_new0 (GNetworkDnsLookup, 1);
  lookup->context = gnetwork_thread_get_context ();
  lookup->address = g_strdup (address);
  lookup->callback = callback;
  lookup->data = data;
  lookup->notify = notify;

  error = NULL;
  /* We pass the current thread's context so we can easily make the callback run in the same thread
     which started the lookup by calling this function. */
  if (!gnetwork_thread_new ((GThreadFunc) getaddrinfo_lookup_thread, lookup, NULL, lookup->context,
			    &error))
    {
      gnetwork_dns_lookup_free (lookup);
      lookup = NULL;

      (*callback) (NULL, error, data);

      if (error != NULL)
	g_error_free (error);
    }

  return lookup;
}


/**
 * gnetwork_dns_get_from_ip:
 * @ip_address: the IP address to find.
 * @callback: the callback to be called when the lookup has completed.
 * @data: the user data to pass to @callback.
 * @notify: a function capable of freeing @user_data, or %NULL.
 *
 * This function performs an asynchronous DNS reverse lookup on the IP address
 * in @ip_address. After @callback has been called, @notify will be called with
 * @data as it's argument. If an error occurred the callback will be called with
 * the error argument set.
 *
 * Note: @callback may be called before this function returns if there was a
 * threading error.
 *
 * Returns: a #GNetworkDnsHandle used to cancel the lookup.
 *
 * Since: 1.0
 **/
GNetworkDnsHandle
gnetwork_dns_get_from_ip (const GNetworkIpAddress * ip_address, GNetworkDnsCallbackFunc callback,
			  gpointer data, GDestroyNotify notify)
{
  GNetworkDnsLookup *lookup;
  gchar *address;

  g_return_val_if_fail (gnetwork_ip_address_is_address (ip_address), GNETWORK_DNS_HANDLE_INVALID);
  g_return_val_if_fail (callback != NULL, GNETWORK_DNS_HANDLE_INVALID);

  address = gnetwork_ip_address_to_string (ip_address);

  lookup = gnetwork_dns_get (address, callback, data, notify);

  g_free (address);

  return lookup;
}


/**
 * gnetwork_dns_cancel:
 * @handle: a #GNetworkDnsHandle for a running lookup.
 *
 * This function prevents an asynchronous DNS lookup (or reverse lookup)
 * from completing.
 * 
 * Note: This function will not actually stop a DNS lookup, only prevent the
 * callback associated with @handle from being called. (The lookup will still
 * finish.)
 *
 * Since: 1.0
 **/
void
gnetwork_dns_cancel (GNetworkDnsHandle handle)
{
  g_return_if_fail (handle != NULL);

  G_LOCK (lookups);
  lookups = g_slist_remove (lookups, handle);
  G_UNLOCK (lookups);
}


GType
gnetwork_dns_entry_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      type = g_boxed_type_register_static ("GNetworkDnsEntry",
					   (GBoxedCopyFunc) gnetwork_dns_entry_dup,
					   (GBoxedFreeFunc) gnetwork_dns_entry_free);
    }

  return type;
}


/**
 * gnetwork_dns_entry_new:
 * @hostname: the hostname to set, or %NULL.
 * @ip_address: the IP address to set, or %NULL.
 * 
 * Creates a new host-to-IP structure with the appropriate values from @hostname
 * and @ip_address. The returned value should be freed with
 * gnetwork_dns_entry_free() when no longer needed.
 * 
 * Returns: #GNetworkDnsEntry structure.
 * 
 * Since: 1.0
 **/
GNetworkDnsEntry *
gnetwork_dns_entry_new (const gchar * hostname, const GNetworkIpAddress * ip_address)
{
  GNetworkDnsEntry *entry;

  g_return_val_if_fail (hostname == NULL || (hostname[0] != '\0' && strlen (hostname) < NI_MAXHOST),
			NULL);
  g_return_val_if_fail (ip_address == NULL || gnetwork_ip_address_is_valid (ip_address), NULL);

  entry = g_new0 (GNetworkDnsEntry, 1);

  entry->g_class.g_type = GNETWORK_TYPE_DNS_ENTRY;
  entry->hostname = g_strdup (hostname);
  entry->ip_address = gnetwork_ip_address_dup (ip_address);

  return entry;
}


/**
 * gnetwork_dns_entry_get_hostname:
 * @entry: the DNS entry to examine.
 * 
 * Retrieves the hostname pointer from @entry. The returned value should not be
 * modified or freed.
 * 
 * Returns: the hostname of @entry.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_dns_entry_get_hostname (const GNetworkDnsEntry * entry)
{
  g_return_val_if_fail (GNETWORK_IS_DNS_ENTRY (entry), NULL);

  return entry->hostname;
}


/**
 * gnetwork_dns_entry_set_hostname:
 * @entry: the DNS entry to modify.
 * @hostname: the new hostname, or %NULL.
 * 
 * Modifies @entry to use the contents of @hostname for it's hostname.
 * 
 * Since: 1.0
 **/
void
gnetwork_dns_entry_set_hostname (GNetworkDnsEntry * entry, const gchar * hostname)
{
  g_return_if_fail (GNETWORK_IS_DNS_ENTRY (entry));
  g_return_if_fail (hostname == NULL || (hostname[0] != '\0' && strlen (hostname) < NI_MAXHOST));

  g_free (entry->hostname);
  entry->hostname = g_strdup (hostname);
}


/**
 * gnetwork_dns_entry_get_ip_address:
 * @entry: the DNS entry to examine.
 * 
 * Retrieves a pointer to the IP address of @entry. The returned data should not
 * be modified or freed.
 * 
 * Returns: the #GNetworkIpAddress of @entry.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN GNetworkIpAddress *
gnetwork_dns_entry_get_ip_address (const GNetworkDnsEntry * entry)
{
  g_return_val_if_fail (GNETWORK_IS_DNS_ENTRY (entry), NULL);

  return entry->ip_address;
}


/**
 * gnetwork_dns_entry_set_ip_address:
 * @entry: the DNS entry to modify.
 * @ip_address: the new hostname, or %NULL.
 * 
 * Modifies @entry to use the contents of @ip_address for it's IP address.
 * 
 * Since: 1.0
 **/
void
gnetwork_dns_entry_set_ip_address (GNetworkDnsEntry * entry, const GNetworkIpAddress * ip_address)
{
  g_return_if_fail (GNETWORK_IS_DNS_ENTRY (entry));
  g_return_if_fail (ip_address == NULL || gnetwork_ip_address_is_valid (ip_address));

  g_free (entry->ip_address);
  entry->ip_address = gnetwork_ip_address_dup (ip_address);
}


/**
 * gnetwork_dns_entry_dup:
 * @src: the DNS entry to duplicate.
 * 
 * Creates a copy of the data in @src. The returned data should be freed with
 * gnetwork_dns_entry_free() when no longer needed.
 * 
 * Returns: a copy of @src.
 * 
 * Since: 1.0
 **/
GNetworkDnsEntry *
gnetwork_dns_entry_dup (const GNetworkDnsEntry * src)
{
  g_return_val_if_fail (src == NULL || GNETWORK_IS_DNS_ENTRY (src), NULL);

  if (src == NULL)
    return NULL;

  return gnetwork_dns_entry_new (src->hostname, src->ip_address);
}


/**
 * gnetwork_dns_entry_free:
 * @entry: the DNS entry to destroy.
 * 
 * Frees the data used by @entry.
 * 
 * Since: 1.0
 **/
void
gnetwork_dns_entry_free (GNetworkDnsEntry * entry)
{
  g_return_if_fail (entry == NULL || GNETWORK_IS_DNS_ENTRY (entry));

  if (entry == NULL)
    return;

  g_free (entry->hostname);
  g_free (entry->ip_address);
  g_free (entry);
}


/**
 * gnetwork_dns_strerror:
 * @error: the DNS error code to use.
 * 
 * Retrieves a string message corresponding to @error. The returned data should
 * not be modified or freed.
 * 
 * Returns: a string message.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_dns_strerror (GNetworkDnsError error)
{
  static const EnumString msgs[] = {
    {GNETWORK_DNS_ERROR_NOT_FOUND,
     N_("The host could not be found. The name might be misspelled, or it may not exist.")},
    {GNETWORK_DNS_ERROR_NO_RECOVERY,
     N_("The DNS lookup server could not be contacted. The network may be down, or the DNS server "
	"may be broken.")},
    {GNETWORK_DNS_ERROR_TRY_AGAIN,
     N_("The DNS lookup server is too busy to respond right now, try connecting again in a few "
	"minutes.")},
    {GNETWORK_DNS_ERROR_INTERNAL,
     N_("There is a problem with your networking library, please verify that a thread "
	"implementation is installed, and GLib is configured to use it.")},
    {0, NULL}
  };
  guint i;

  g_return_val_if_fail (_gnetwork_enum_value_is_valid (GNETWORK_TYPE_DNS_ERROR, error), NULL);

  for (i = 0; i < G_N_ELEMENTS (msgs); i++)
    {
      if (error == msgs[i].value)
	{
	  return _(msgs[i].str);
	}
    }

  return NULL;
}


G_LOCK_DEFINE_STATIC (quark);

GQuark
gnetwork_dns_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (quark);

  if (quark == 0)
    {
      quark = g_quark_from_static_string ("gnetwork-dns-error");
    }

  G_UNLOCK (quark);

  return quark;
}
