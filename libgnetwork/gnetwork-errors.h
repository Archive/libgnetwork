/* 
 * GNetwork Library: libgnetwork/gnetwork-errors.h
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_ERRORS_H__
#define __GNETWORK_ERRORS_H__ 1

#include <glib-object.h>

G_BEGIN_DECLS


#ifndef G_TYPE_ERROR
# define G_TYPE_ERROR	(g_error_get_type ())
#endif /* !G_TYPE_ERROR */

#ifndef g_error_get_type
GType g_error_get_type (void) G_GNUC_CONST;
#endif /* !g_error_get_type */


G_END_DECLS

#endif /* !__GNETWORK_ERRORS_H__ */
