/* 
 * GNetwork Library: libgnetwork/gnetwork-threads.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_THREADS_H__
#define __GNETWORK_THREADS_H__

#include <glib.h>

G_BEGIN_DECLS


#define GNETWORK_IO_ANY	(G_IO_PRI | G_IO_IN | G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL)

/* Thread Creation */
gboolean gnetwork_thread_new (GThreadFunc func, gpointer data, GDestroyNotify notify,
			      GMainContext *context, GError ** error);

/* Threadsafe Mainloop Functions */
void gnetwork_thread_set_context (GMainContext * context);
GMainContext *gnetwork_thread_get_context (void);

#define gnetwork_thread_idle_add(func,data) \
  gnetwork_thread_idle_add_full (G_PRIORITY_DEFAULT_IDLE, func, data, NULL)

guint gnetwork_thread_idle_add_full (gint priority, GSourceFunc func, gpointer data,
				     GDestroyNotify notify);

#define gnetwork_thread_timeout_add(interval,func,data) \
  gnetwork_thread_timeout_add_full (G_PRIORITY_DEFAULT, interval, func, data, NULL)

guint gnetwork_thread_timeout_add_full (gint priority, guint interval, GSourceFunc func,
					gpointer data, GDestroyNotify notify);

#define gnetwork_thread_io_add_watch(channel,condition,func,data) \
  gnetwork_thread_io_add_watch_full (channel, G_PRIORITY_DEFAULT, condition, func, data, NULL)

guint gnetwork_thread_io_add_watch_full (GIOChannel * channel, gint priority,
					 GIOCondition condition, GIOFunc func, gpointer data,
					 GDestroyNotify notify);

gboolean gnetwork_thread_source_remove (guint id);


G_END_DECLS

#endif /* !__GNETWORK_THREADS_H__ */
