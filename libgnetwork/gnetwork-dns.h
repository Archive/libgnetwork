/*
 * GNetwork Library: libgnetwork/gnetwork-dns.h
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_DNS_H__
#define __GNETWORK_DNS_H__

#include "gnetwork-ip-address.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_DNS_ENTRY		(gnetwork_dns_entry_get_type ())
#define GNETWORK_DNS_ENTRY(entry)	((GNetworkDnsEntry *) (entry))
#define GNETWORK_IS_DNS_ENTRY(entry)	((entry) != NULL && ((GTypeClass *) (entry))->g_type == GNETWORK_TYPE_DNS_ENTRY)

#define GNETWORK_DNS_HANDLE_INVALID	NULL

#define GNETWORK_DNS_ERROR		(gnetwork_dns_error_get_quark ())


typedef enum /* <prefix=GNETWORK_DNS_ERROR> */
{
  GNETWORK_DNS_ERROR_INTERNAL,

  GNETWORK_DNS_ERROR_NOT_FOUND,
  GNETWORK_DNS_ERROR_NO_RECOVERY,
  GNETWORK_DNS_ERROR_TRY_AGAIN
}
GNetworkDnsError;

typedef struct _GNetworkDnsEntry GNetworkDnsEntry;
typedef struct _GNetworkDnsLookup GNetworkDnsLookup;
typedef GNetworkDnsLookup *GNetworkDnsHandle;

typedef void (*GNetworkDnsCallbackFunc) (const GSList * entries,
					 const GError * error,
					 gpointer user_data);


GNetworkDnsHandle gnetwork_dns_get (const gchar * address, GNetworkDnsCallbackFunc callback,
				    gpointer data, GDestroyNotify notify);
GNetworkDnsHandle gnetwork_dns_get_from_ip (const GNetworkIpAddress * ip_address,
					    GNetworkDnsCallbackFunc callback, gpointer data,
					    GDestroyNotify notify);
void gnetwork_dns_cancel (GNetworkDnsHandle handle);

GType gnetwork_dns_entry_get_type (void);
GNetworkDnsEntry *gnetwork_dns_entry_new (const gchar * hostname,
					  const GNetworkIpAddress * ip_address);

G_CONST_RETURN gchar *gnetwork_dns_entry_get_hostname (const GNetworkDnsEntry * entry);
void gnetwork_dns_entry_set_hostname (GNetworkDnsEntry * entry, const gchar * hostname);
G_CONST_RETURN GNetworkIpAddress *gnetwork_dns_entry_get_ip_address (const GNetworkDnsEntry * entry);
void gnetwork_dns_entry_set_ip_address (GNetworkDnsEntry * entry,
					const GNetworkIpAddress * ip_address);
GNetworkDnsEntry * gnetwork_dns_entry_dup (const GNetworkDnsEntry * src);
void gnetwork_dns_entry_free (GNetworkDnsEntry * entry);

G_CONST_RETURN gchar *gnetwork_dns_strerror (GNetworkDnsError error);
GQuark gnetwork_dns_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* __GNETWORK_Dns_H__ */
