/* 
 * GNetwork Library: libgnetwork/gnetwork-tcp-server.c
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */


#include "gnetwork-tcp-server.h"

#include "gnetwork-tcp-connection.h"
#include "gnetwork-dns.h"
#include "gnetwork-interfaces.h"
#include "gnetwork-threads.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"

#include <glib/gi18n.h>

#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>


#ifndef DEFAULT_BUFFER_SIZE
# define DEFAULT_BUFFER_SIZE 2048
#endif /* DEFAULT_BUFFER_SIZE */


enum
{
  PROP_0,

  /* User-settable */
  INTERFACE,
  INTERFACE_INFO,
  PORT,
  REVERSE_LOOKUPS,

  /* GNetworkServer Properties */
  SVR_STATUS,
  SVR_BYTES_SENT,
  SVR_BYTES_RECEIVED,
  SVR_BUFFER_SIZE,
  SVR_CLOSE_CHILDREN,
  SVR_MAX_CONNECTIONS,
  SVR_CONNECTIONS
};


struct _GNetworkTcpServerPrivate
{
  /* Object Properties */
  gchar *interface;
  GNetworkInterfaceInfo *interface_info;
  guint port;

  /* GNetworkServer Properties */
  GSList *connections;
  guint64 bytes_sent;
  guint64 bytes_received;
  guint max_connections;

  GNetworkServerCreateFunc create_func;
  gpointer data;
  GDestroyNotify notify;

  /* Object Data */
  GIOChannel *channel;
  gint sockfd;
  gint incoming_id;

  /* Property Bits */
  /* GNetworkServer */
  GNetworkServerStatus status:2;
  gboolean close_children:1;
  /* GNetworkTcpServer */
  gboolean reverse_lookups:1;
};


struct _GNetworkTcpServerCreationData
{
  GTypeClass g_class;

  gchar *address;
  guint16 port;
  gpointer socket;
};


/* ********************* *
 *  Classwide Variables  *
 * ********************* */

static gpointer parent_class = NULL;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static GNetworkConnection *
create_incoming (GNetworkTcpServer * server, const GValue * server_data, gpointer user_data,
		 GError ** error)
{
  const GNetworkTcpServerCreationData *data = g_value_get_boxed (server_data);

  return g_object_new (GNETWORK_TYPE_TCP_CONNECTION,
		       "connection-type", GNETWORK_CONNECTION_SERVER,
		       "address", data->address, "port", data->port,
		       "socket", GPOINTER_TO_INT (data->socket), NULL);
}


static void
dns_callback (const GSList *entries, const GError * error, GNetworkConnection * connection)
{
  if (entries != NULL && entries->data != NULL)
    {
      GNetworkConnectionStatus status;

      g_object_get (connection, "status", &status, NULL);

      if (status == GNETWORK_CONNECTION_OPEN)
	g_object_set (connection, "address", gnetwork_dns_entry_get_hostname (entries->data), NULL);
    }
}


/* ************************* *
 *  New Connection Handling  *
 * ************************* */

/* GNetworkConnection Callbacks */
static void
cxn_received_cb (GNetworkConnection * connection, gconstpointer data, gulong length,
		 GNetworkTcpServer * server)
{
  server->_priv->bytes_received += length;
  g_object_notify (G_OBJECT (server), "bytes-received");
}


static void
cxn_sent_cb (GNetworkConnection * connection, gconstpointer data, gulong length,
	     GNetworkTcpServer * server)
{
  server->_priv->bytes_sent += length;
  g_object_notify (G_OBJECT (server), "bytes-sent");
}


static void
cxn_notify_status_cb (GNetworkConnection * connection, GParamSpec * pspec,
		      GNetworkTcpServer * server)
{
  GNetworkConnectionStatus status;

  g_object_get (connection, "status", &status, NULL);

  if (status == GNETWORK_CONNECTION_CLOSED)
    {
      g_signal_handlers_disconnect_by_func (connection, cxn_notify_status_cb, server);
      g_signal_handlers_disconnect_by_func (connection, cxn_received_cb, server);
      g_signal_handlers_disconnect_by_func (connection, cxn_sent_cb, server);
      server->_priv->connections = g_slist_remove (server->_priv->connections, connection);
      g_object_unref (connection);
      g_object_notify (G_OBJECT (server), "connections");
    }
}


/* GIOFunc */
static gboolean
incoming_handler (GIOChannel * channel, GIOCondition cond, GNetworkTcpServer * server)
{
  if (server->_priv->status != GNETWORK_SERVER_OPEN)
    return FALSE;

  /* If there is a connection limit && we're already there */
  if (server->_priv->max_connections != 0 &&
      g_slist_length (server->_priv->connections) >= server->_priv->max_connections)
    return TRUE;

  if (cond & G_IO_IN || cond & G_IO_PRI)
    {
      GNetworkConnection *cxn;
      GError *error;
      gint cxn_fd;
      struct sockaddr *sa;
      socklen_t sa_size;
      GNetworkTcpServerCreationData creation_data;
      GValue value = { 0 };

      sa_size = MAX (sizeof (struct sockaddr_in), sizeof (struct sockaddr_in6));
      sa = g_malloc0 (sa_size);
      cxn_fd = accept (server->_priv->sockfd, sa, &sa_size);

      /* For whatever reason the incoming connection failed */
      if (cxn_fd < 0)
	{
	  g_free (sa);
	  return TRUE;
	}

      creation_data.address = _gnetwork_sockaddr_get_address (sa);
      creation_data.g_class.g_type = GNETWORK_TYPE_TCP_SERVER_CREATION_DATA;
      creation_data.port = _gnetwork_sockaddr_get_port (sa);
      creation_data.socket = GINT_TO_POINTER (cxn_fd);
      g_free (sa);

      g_value_init (&value, GNETWORK_TYPE_TCP_SERVER_CREATION_DATA);
      g_value_set_boxed (&value, &creation_data);

      error = NULL;
      cxn = (*server->_priv->create_func) (GNETWORK_SERVER (server), &value, server->_priv->data,
					   &error);
      g_value_unset (&value);

      if (cxn != NULL && GNETWORK_IS_TCP_CONNECTION (cxn))
	{
	  /* Add it to the list of connections we've got */
	  server->_priv->connections = g_slist_prepend (server->_priv->connections,
							g_object_ref (cxn));
	  if (server->_priv->reverse_lookups)
	    {
	      gnetwork_dns_get (creation_data.address, (GNetworkDnsCallbackFunc) dns_callback,
				g_object_ref (cxn), g_object_unref);
	    }

	  /* Connect signals & tell the world we've got a new incoming connection */
	  g_signal_connect_object (cxn, "received", G_CALLBACK (cxn_received_cb), server, 0);
	  g_signal_connect_object (cxn, "sent", G_CALLBACK (cxn_sent_cb), server, 0);
	  g_signal_connect_object (cxn, "notify::status", G_CALLBACK (cxn_notify_status_cb), server,
				   0);

	  gnetwork_server_new_connection (GNETWORK_SERVER (server), GNETWORK_CONNECTION (cxn));
	  g_object_notify (G_OBJECT (server), "connections");

	  /* Actually open the connection, we do it after we've connected the signals & told the
	     world about it, so no data is lost. */
	  gnetwork_connection_open (GNETWORK_CONNECTION (cxn));
	  g_object_unref (cxn);
	}
      else
	{
	  shutdown (cxn_fd, SHUT_RDWR);
	  close (cxn_fd);
	}
      g_free (creation_data.address);

      return TRUE;
    }

  return FALSE;
}


/* ******************************* *
 *  GNetworkServerIface Functions  *
 * ******************************* */

static void
gnetwork_tcp_server_open (GNetworkTcpServer * server)
{
  GError *error;
  gint result, value;
  struct sockaddr_storage sa = { 0 };

  g_return_if_fail (GNETWORK_IS_TCP_SERVER (server));

  g_object_freeze_notify (G_OBJECT (server));
  server->_priv->status = GNETWORK_SERVER_OPENING;
  g_object_notify (G_OBJECT (server), "status");

  server->_priv->bytes_sent = 0;
  g_object_notify (G_OBJECT (server), "bytes-sent");

  server->_priv->bytes_received = 0;
  g_object_notify (G_OBJECT (server), "bytes-received");
  g_object_thaw_notify (G_OBJECT (server));

  /* Create the socket */
  errno = 0;
  server->_priv->sockfd = socket (AF_INET6, SOCK_STREAM, 0);
  if (server->_priv->sockfd < 0 && errno == EAFNOSUPPORT)
    {
      struct sockaddr_in *sin = (struct sockaddr_in *) &sa;

      errno = 0;
      server->_priv->sockfd = socket (AF_INET, SOCK_STREAM, 0);
      sa.ss_family = AF_INET;
      if (server->_priv->interface_info != NULL)
	{
	  sin->sin_addr.s_addr =
	    GNETWORK_IP_ADDRESS32 (gnetwork_interface_info_get_address (server->_priv->interface_info,
									GNETWORK_PROTOCOL_IPv4), 3);
	}
      else
	{
	  sin->sin_addr.s_addr = INADDR_ANY;
	}

      sin->sin_port = g_htons (server->_priv->port);
    }
  else
    {
      struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *) &sa;

      sa.ss_family = AF_INET6;
      if (server->_priv->interface_info != NULL)
	{
	  sin6->sin6_addr =
	    *((struct in6_addr *) gnetwork_interface_info_get_address (server->_priv->interface_info,
								       GNETWORK_PROTOCOL_IPv6));
	}
      else
	{
	  sin6->sin6_addr = in6addr_any;
	}

      sin6->sin6_port = g_htons (server->_priv->port);
    }

  if (server->_priv->sockfd < 0)
    {
      switch (errno)
	{
	case EMFILE:
	  error =
	    g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_TOO_MANY_PROCESSES,
				 gnetwork_server_strerror
				 (GNETWORK_SERVER_ERROR_TOO_MANY_PROCESSES));
	  break;
	case EACCES:
	  error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_PERMISSIONS,
				       gnetwork_server_strerror
				       (GNETWORK_SERVER_ERROR_PERMISSIONS));
	  break;

	case ENFILE:
	case ENOBUFS:
	case ENOMEM:
	  error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_NO_MEMORY,
				       gnetwork_server_strerror (GNETWORK_SERVER_ERROR_NO_MEMORY));
	  break;

	default:
	  error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_INTERNAL,
				       gnetwork_server_strerror (GNETWORK_SERVER_ERROR_INTERNAL));
	  break;
	}

      gnetwork_server_error (GNETWORK_SERVER (server), error);
      g_error_free (error);

      if (server->_priv->status > GNETWORK_SERVER_CLOSED)
	{
	  server->_priv->status = GNETWORK_SERVER_CLOSED;
	  g_object_notify (G_OBJECT (server), "status");
	}
      return;
    }

  fcntl (server->_priv->sockfd, F_SETFL, O_NONBLOCK);

  value = TRUE;
  setsockopt (server->_priv->sockfd, SOL_SOCKET, SO_REUSEADDR, &value, sizeof (value));

  /* Bind the socket */
  errno = 0;
  result = bind (server->_priv->sockfd, (struct sockaddr *) &sa, sizeof (sa));
  if (result < 0)
    {
      switch (errno)
	{
	case EINVAL:
	  error = g_error_new (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_ALREADY_EXISTS,
			       _("The service could not be started because there is already "
				 "another service using port %d."), server->_priv->port);
	  break;
	case EACCES:
	  error = g_error_new (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_PERMISSIONS,
			       _("You do not have permission to offer a service on port %d. Ports "
				 "below 1024 can only be used as services by the root user."),
			       server->_priv->port);
	  break;

	default:
	  error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_INTERNAL,
				       gnetwork_server_strerror (GNETWORK_SERVER_ERROR_INTERNAL));
	  break;
	}

      gnetwork_server_error (GNETWORK_SERVER (server), error);
      g_error_free (error);

      if (server->_priv->status > GNETWORK_SERVER_CLOSED)
	{
	  shutdown (server->_priv->sockfd, SHUT_RDWR);
	  close (server->_priv->sockfd);
	  server->_priv->sockfd = -1;

	  server->_priv->status = GNETWORK_SERVER_CLOSED;
	  g_object_notify (G_OBJECT (server), "status");
	}
      return;
    }

  errno = 0;
  if (listen (server->_priv->sockfd, server->_priv->max_connections) < 0)
    {
      switch (errno)
	{
	case EADDRINUSE:
	  error = g_error_new (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_ALREADY_EXISTS,
			       _("The service could not be started because there is already "
				 "another service using port %d."), server->_priv->port);
	  break;
	default:
	  error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_INTERNAL,
				       gnetwork_server_strerror (GNETWORK_SERVER_ERROR_INTERNAL));
	  break;
	}

      gnetwork_server_error (GNETWORK_SERVER (server), error);
      g_error_free (error);

      if (server->_priv->status > GNETWORK_SERVER_CLOSED)
	{
	  shutdown (server->_priv->sockfd, SHUT_RDWR);
	  close (server->_priv->sockfd);
	  server->_priv->sockfd = -1;

	  server->_priv->status = GNETWORK_SERVER_CLOSED;
	  g_object_notify (G_OBJECT (server), "status");
	}
      return;
    }

  server->_priv->status = GNETWORK_SERVER_OPEN;
  g_object_notify (G_OBJECT (server), "status");

  server->_priv->channel = g_io_channel_unix_new (server->_priv->sockfd);
  server->_priv->incoming_id = gnetwork_thread_io_add_watch (server->_priv->channel,
							     GNETWORK_IO_ANY,
							     (GIOFunc) incoming_handler, server);
}


static void
gnetwork_tcp_server_close (GNetworkTcpServer * server)
{
  g_return_if_fail (GNETWORK_IS_TCP_SERVER (server));

  if (server->_priv->status <= GNETWORK_SERVER_CLOSED)
    return;

  g_object_freeze_notify (G_OBJECT (server));
  server->_priv->status = GNETWORK_SERVER_CLOSING;
  g_object_notify (G_OBJECT (server), "status");

  for (; server->_priv->connections != NULL;
       g_slist_remove_link (server->_priv->connections, server->_priv->connections))
    {
      if (server->_priv->close_children)
	{
	  gnetwork_connection_close (GNETWORK_CONNECTION (server->_priv->connections->data));
	}
      else
	{
	  g_signal_handlers_disconnect_by_func (server->_priv->connections->data,
						cxn_notify_status_cb, server);
	  g_signal_handlers_disconnect_by_func (server->_priv->connections->data,
						cxn_received_cb, server);
	  g_signal_handlers_disconnect_by_func (server->_priv->connections->data,
						cxn_sent_cb, server);
	  g_object_unref (server->_priv->connections->data);
	}
    }
  g_object_notify (G_OBJECT (server), "connections");
  g_object_thaw_notify (G_OBJECT (server));

  if (server->_priv->incoming_id != -1)
    {
      gnetwork_thread_source_remove (server->_priv->incoming_id);
      server->_priv->incoming_id = -1;
    }
  g_io_channel_shutdown (server->_priv->channel, FALSE, NULL);
  g_io_channel_unref (server->_priv->channel);
  server->_priv->channel = NULL;
  server->_priv->sockfd = -1;

  server->_priv->status = GNETWORK_SERVER_CLOSED;
  g_object_notify (G_OBJECT (server), "status");
}


static void
gnetwork_tcp_server_set_create_func (GNetworkTcpServer * server, GNetworkServerCreateFunc func,
				     gpointer data, GDestroyNotify notify)
{
  g_return_if_fail (GNETWORK_IS_TCP_SERVER (server));

  if (server->_priv->notify != NULL && server->_priv->data != NULL)
    (*server->_priv->notify) (server->_priv->data);

  if (func != NULL)
    {
      server->_priv->create_func = func;
      server->_priv->data = data;
      server->_priv->notify = notify;
    }
  else
    {
      server->_priv->create_func = (GNetworkServerCreateFunc) create_incoming;
    }

  server->_priv->data = data;
  server->_priv->notify = notify;
}


static void
gnetwork_tcp_server_server_iface_init (GNetworkServerIface * iface)
{
  iface->open = (GNetworkServerFunc) gnetwork_tcp_server_open;
  iface->close = (GNetworkServerFunc) gnetwork_tcp_server_close;

  iface->set_create_func = (GNetworkServerSetCreateFunc) gnetwork_tcp_server_set_create_func;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_tcp_server_set_property (GObject * object, guint property, const GValue * value,
				  GParamSpec * param_spec)
{
  GNetworkTcpServer *server = GNETWORK_TCP_SERVER (object);

  switch (property)
    {
    case INTERFACE:
      g_return_if_fail (server->_priv->status < GNETWORK_SERVER_OPENING);

      {
	GNetworkInterfaceInfo *info;
	const gchar *interface;

	interface = g_value_get_string (value);

	g_free (server->_priv->interface);

	if (interface != NULL)
	  {
	    server->_priv->interface = g_strdup (server->_priv->interface);
	    info = gnetwork_interface_get_info (interface);
	  }
	else
	  {
	    server->_priv->interface = NULL;
	    info = NULL;
	  }

	if (info != server->_priv->interface_info)
	  {
	    gnetwork_interface_info_unref (server->_priv->interface_info);
	    server->_priv->interface_info = gnetwork_interface_info_ref (info);

	    g_object_notify (object, "interface-info");
	  }

	gnetwork_interface_info_unref (info);
      }
      break;
    case INTERFACE_INFO:
      g_return_if_fail (server->_priv->status < GNETWORK_SERVER_OPENING);

      {
	GNetworkInterfaceInfo *info = g_value_dup_boxed (value);

	g_free (server->_priv->interface);
	gnetwork_interface_info_unref (server->_priv->interface_info);

	if (GNETWORK_IS_INTERFACE_INFO (info))
	  {
	    server->_priv->interface = g_strdup (gnetwork_interface_info_get_name (info));
	    server->_priv->interface_info = info;
	  }
	else
	  {
	    server->_priv->interface = NULL;
	    server->_priv->interface_info = NULL;
	  }

	g_object_notify (object, "interface");
      }
      break;
    case PORT:
      g_return_if_fail (server->_priv->status < GNETWORK_SERVER_OPENING);

      server->_priv->port = g_value_get_uint (value);
      break;
    case REVERSE_LOOKUPS:
      server->_priv->reverse_lookups = g_value_get_boolean (value);
      break;

    case SVR_MAX_CONNECTIONS:
      server->_priv->max_connections = g_value_get_uint (value);
      break;
    case SVR_CLOSE_CHILDREN:
      server->_priv->close_children = g_value_get_boolean (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_tcp_server_get_property (GObject * object, guint property, GValue * value,
				  GParamSpec * param_spec)
{
  GNetworkTcpServer *server = GNETWORK_TCP_SERVER (object);

  switch (property)
    {
    case INTERFACE:
      g_value_set_string (value, server->_priv->interface);
      break;
    case INTERFACE_INFO:
      g_value_set_boxed (value, server->_priv->interface_info);
      break;
    case PORT:
      g_value_set_uint (value, server->_priv->port);
      break;
    case REVERSE_LOOKUPS:
      g_value_set_boolean (value, server->_priv->reverse_lookups);
      break;

    case SVR_STATUS:
      g_value_set_enum (value, server->_priv->status);
      break;
    case SVR_MAX_CONNECTIONS:
      g_value_set_uint (value, server->_priv->max_connections);
      break;
    case SVR_BYTES_SENT:
      g_value_set_ulong (value, server->_priv->bytes_sent);
      break;
    case SVR_BYTES_RECEIVED:
      g_value_set_ulong (value, server->_priv->bytes_received);
      break;
    case SVR_CONNECTIONS:
      g_value_take_boxed (value, _gnetwork_slist_to_value_array (server->_priv->connections,
								 GNETWORK_TYPE_CONNECTION));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_tcp_server_dispose (GObject * object)
{
  GNetworkTcpServer *server = GNETWORK_TCP_SERVER (object);

  if (server->_priv->status > GNETWORK_SERVER_CLOSED)
    gnetwork_tcp_server_close (server);

  gnetwork_interface_info_unref (server->_priv->interface_info);

  if (server->_priv->notify != NULL && server->_priv->data != NULL)
    (*server->_priv->notify) (server->_priv->data);

  if (G_OBJECT_CLASS (parent_class)->dispose != NULL)
    (*G_OBJECT_CLASS (parent_class)->dispose) (object);
}


static void
gnetwork_tcp_server_finalize (GObject * object)
{
  GNetworkTcpServer *server = GNETWORK_TCP_SERVER (object);

  if (server->_priv->status > GNETWORK_SERVER_CLOSED)
    gnetwork_tcp_server_close (server);

  g_free (server->_priv->interface);
  g_free (server->_priv);

  if (G_OBJECT_CLASS (parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_tcp_server_class_init (GNetworkTcpServerClass * class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  parent_class = g_type_class_peek_parent (class);

  object_class->set_property = gnetwork_tcp_server_set_property;
  object_class->get_property = gnetwork_tcp_server_get_property;
  object_class->dispose = gnetwork_tcp_server_dispose;
  object_class->finalize = gnetwork_tcp_server_finalize;

  g_object_class_install_property (object_class, INTERFACE,
				   g_param_spec_string ("interface", _("Local Interface"),
							_("The name of the interface to allow "
							  "connections to (e.g. \"eth0\")."), NULL,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class, INTERFACE_INFO,
				   g_param_spec_boxed ("interface-info", _("Interface Information"),
						       _("The hostname or IP address of the "
							 "interface to allow connections to."),
						       GNETWORK_TYPE_INTERFACE_INFO,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class, PORT,
				   g_param_spec_uint ("port", _("Local Port"),
						      _("The port number to allow connections to."),
						      0, 65535, 0, G_PARAM_READWRITE));
  g_object_class_install_property (object_class, REVERSE_LOOKUPS,
				   g_param_spec_boolean ("reverse-lookups",
							 _("Performs Reverse DNS Lookups"),
							 _("Whether or not to perform reverse "
							   "lookups on incoming connections."),
							 FALSE, G_PARAM_READWRITE));

  g_object_class_override_property (object_class, SVR_STATUS, "status");
  g_object_class_override_property (object_class, SVR_BYTES_SENT, "bytes-sent");
  g_object_class_override_property (object_class, SVR_BYTES_RECEIVED, "bytes-received");
  g_object_class_override_property (object_class, SVR_MAX_CONNECTIONS, "max-connections");
  g_object_class_override_property (object_class, SVR_CLOSE_CHILDREN, "close-children");
  g_object_class_override_property (object_class, SVR_CONNECTIONS, "connections");
}


static void
gnetwork_tcp_server_instance_init (GNetworkTcpServer * server)
{
  server->_priv = g_new (GNetworkTcpServerPrivate, 1);

  server->_priv->status = GNETWORK_SERVER_CLOSED;
  server->_priv->interface = NULL;
  server->_priv->interface_info = NULL;
  server->_priv->connections = NULL;

  server->_priv->notify = NULL;
  server->_priv->data = NULL;

  server->_priv->bytes_received = 0;
  server->_priv->bytes_sent = 0;
  server->_priv->create_func = (GNetworkServerCreateFunc) create_incoming;
}


/* ************************************************************************** *
 *  Public API                                                                *
 **************************************************************************** */

GType
gnetwork_tcp_server_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      static const GTypeInfo info = {
	sizeof (GNetworkTcpServerClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_tcp_server_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkTcpServer),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_tcp_server_instance_init,
	NULL			/* value table */
      };
      static const GInterfaceInfo svr_info = {
	(GInterfaceInitFunc) gnetwork_tcp_server_server_iface_init, NULL, NULL
      };

      type = g_type_register_static (G_TYPE_OBJECT, "GNetworkTcpServer", &info, 0);

      g_type_add_interface_static (type, GNETWORK_TYPE_SERVER, &svr_info);
    }

  return type;
}


/**
 * gnetwork_tcp_server_new:
 * @interface: the local IP interface to use, or %NULL.
 * @port: the local port to allow connections through.
 *
 * Creates a new, unopened server object for incoming connections on @port on
 * @interface. The IP address in @interface should correspond to the interface
 * that the new server should listen on. If @interface is "127.0.0.1", then only
 * connections which come in via the loopback (lo) interface are allowed,
 * whereas if the IP address of eth0 is "192.168.0.10", and @interface is set to
 * "192.168.0.10", then only connections which come in over eth0 are accepted,
 * etc. This functions as a bare-bones firewall for this server, though if more
 * complicated firewalls are required (at this level), a custom
 * connection-creation function could do the trick.
 *
 * Returns: a new #GNetworkTcpServer object.
 *
 * Since: 1.0
 **/
GNetworkTcpServer *
gnetwork_tcp_server_new (const gchar * interface, guint port)
{
  GNetworkTcpServer *retval;

  g_return_val_if_fail (port <= 65535, NULL);
  g_return_val_if_fail (interface == NULL || interface[0] != '\0', NULL);

  if (gnetwork_str_is_ip_address (interface))
    {
      GNetworkInterfaceInfo *info = gnetwork_interface_get_info_by_address (interface);

      retval = g_object_new (GNETWORK_TYPE_TCP_SERVER, "interface-info", info, "port", port, NULL);
      gnetwork_interface_info_unref (info);
    }
  else
    {
      retval = g_object_new (GNETWORK_TYPE_TCP_SERVER, "interface", interface, "port", port, NULL);
    }

  return retval;
}


/* ************************************************************************** *
 *  GNetworkTcpServerCreationData                                             *
 * ************************************************************************** */

GType
gnetwork_tcp_server_creation_data_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      type = g_boxed_type_register_static ("GNetworkTcpServerCreationData",
					   (GBoxedCopyFunc) gnetwork_tcp_server_creation_data_dup,
					   (GBoxedFreeFunc) gnetwork_tcp_server_creation_data_free);
    }

  return type;
}


/**
 * gnetwork_tcp_server_creation_data_dup:
 * @src: the creation data to copy.
 * 
 * Creates a copy of the creation data in @src. The returned data should be
 * freed with gnetwork_tcp_server_creation_data_free() when no longer needed.
 * 
 * Returns: a newly allocated copy of @src.
 * 
 * Since: 1.0
 **/
GNetworkTcpServerCreationData *
gnetwork_tcp_server_creation_data_dup (const GNetworkTcpServerCreationData * src)
{
  GNetworkTcpServerCreationData *dest;

  g_return_val_if_fail (src == NULL || GNETWORK_IS_TCP_SERVER_CREATION_DATA (src), NULL);

  if (src == NULL)
    return NULL;

  dest = g_new0 (GNetworkTcpServerCreationData, 1);
  dest->g_class.g_type = GNETWORK_TYPE_TCP_SERVER_CREATION_DATA;
  dest->address = g_strdup (src->address);
  dest->port = src->port;
  dest->socket = src->socket;

  return dest;
}


/**
 * gnetwork_tcp_server_creation_data_free:
 * @data: the creation data to delete.
 * 
 * Frees the memory used by @data.
 * 
 * Since: 1.0
 **/
void
gnetwork_tcp_server_creation_data_free (GNetworkTcpServerCreationData * data)
{
  g_return_if_fail (data == NULL || GNETWORK_IS_TCP_SERVER_CREATION_DATA (data));

  if (data != NULL)
    {
      g_free (data->address);
      g_free (data);
    }
}


/**
 * gnetwork_tcp_server_creation_data_get_address:
 * @data: the connection creation data to examine.
 * 
 * Retrieves the string of the host IP address of the incoming connection in
 * @data.
 * 
 * Returns: the address of @data.
 **/
G_CONST_RETURN gchar *
gnetwork_tcp_server_creation_data_get_address (const GNetworkTcpServerCreationData * data)
{
  g_return_val_if_fail (GNETWORK_IS_TCP_SERVER_CREATION_DATA (data), NULL);

  return data->address;
}


/**
 * gnetwork_tcp_server_creation_data_get_port:
 * @data: the connection creation data to examine.
 * 
 * Retrieves the @port in @data. This value is 
 * 
 * Returns: the address of @data.
 **/
guint16
gnetwork_tcp_server_creation_data_get_port (const GNetworkTcpServerCreationData * data)
{
  g_return_val_if_fail (GNETWORK_IS_TCP_SERVER_CREATION_DATA (data), 0);

  return data->port;
}


/**
 * gnetwork_tcp_server_creation_data_get_socket:
 * @data: the connection creation data to examine.
 * 
 * Retrieves the socket data (a file descriptor on Unix) in @data.
 * 
 * Returns: the address of @data.
 **/
gconstpointer
gnetwork_tcp_server_creation_data_get_socket (const GNetworkTcpServerCreationData * data)
{
  g_return_val_if_fail (GNETWORK_IS_TCP_SERVER_CREATION_DATA (data), NULL);

  return data->socket;
}
