/* 
 * GNetwork Library: libgnetwork/gnetwork-ssl.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_SSL_H__
#define __GNETWORK_SSL_H__

#include <glib-object.h>

G_BEGIN_DECLS


/* ************************************************************************** *
 * Certificate Manipulation                                                   *
 * ************************************************************************** */

#define GNETWORK_TYPE_SSL_CERT	  (gnetwork_ssl_cert_get_type ())
#define GNETWORK_SSL_CERT(ptr)	  ((GNetworkSslCert *) (ptr))
#define GNETWORK_IS_SSL_CERT(ptr) (ptr != NULL && ((GTypeClass *) (ptr))->g_type == GNETWORK_TYPE_SSL_CERT)

#define GNETWORK_SSL_CERT_ERROR	(gnetwork_ssl_cert_error_get_quark ())


typedef enum /* <prefix=GNETWORK_SSL_CERT> */
{
  GNETWORK_SSL_CERT_INVALID,

  GNETWORK_SSL_CERT_X509
}
GNetworkSslCertType;

typedef enum /* <flags,prefix=GNETWORK_SSL_CERT_ERROR> */
{
  GNETWORK_SSL_CERT_ERROR_UNKNOWN = 0,

  GNETWORK_SSL_CERT_ERROR_NO_CERTIFICATE = (1 << 0),
  GNETWORK_SSL_CERT_ERROR_INVALID = (1 << 1),
  GNETWORK_SSL_CERT_ERROR_UNSIGNED = (1 << 2),
  GNETWORK_SSL_CERT_ERROR_NOT_TRUSTED = (1 << 3),
  GNETWORK_SSL_CERT_ERROR_REVOKED = (1 << 4),
  GNETWORK_SSL_CERT_ERROR_CORRUPTED = (1 << 5),
  GNETWORK_SSL_CERT_ERROR_UNSUPPORTED_TYPE = (1 << 6),
  GNETWORK_SSL_CERT_ERROR_EXPIRED = (1 << 7),
  GNETWORK_SSL_CERT_ERROR_NOT_ACTIVATED = (1 << 8),
  GNETWORK_SSL_CERT_ERROR_HOSTNAME_MISMATCH = (1 << 9),

  GNETWORK_SSL_CERT_ERROR_CANNOT_PARSE = (1 << 10)
}
GNetworkSslCertErrorFlags;


typedef struct _GNetworkSslCert GNetworkSslCert;


GType gnetwork_ssl_cert_get_type (void) G_GNUC_CONST;

GNetworkSslCert *gnetwork_ssl_cert_dup (const GNetworkSslCert * cert);
void gnetwork_ssl_cert_free (GNetworkSslCert * cert);


GSList *gnetwork_ssl_cert_list_fields (const GNetworkSslCert * cert);
gchar *gnetwork_ssl_cert_get_field (const GNetworkSslCert * cert, const gchar * id);

GSList *gnetwork_ssl_cert_list_owner_fields (const GNetworkSslCert * cert);
gchar *gnetowrk_ssl_cert_get_owner_field (const GNetworkSslCert * cert, const gchar * id);

GSList *gnetwork_ssl_cert_list_issuer_fields (const GNetworkSslCert * cert);
gchar *gnetowrk_ssl_cert_get_issuer_field (const GNetworkSslCert * cert, const gchar * id);

GNetworkSslCertType gnetwork_ssl_cert_get_cert_type (const GNetworkSslCert * cert);
GString *gnetwork_ssl_cert_get_raw (const GNetworkSslCert * cert);

glong gnetwork_ssl_cert_get_expiration (const GNetworkSslCert * cert);
glong gnetwork_ssl_cert_get_activation (const GNetworkSslCert * cert);

GQuark gnetwork_ssl_cert_error_get_quark (void) G_GNUC_CONST;


/* ************************************************************************** *
 *  Miscellaneous                                                             *
 * ************************************************************************** */

#define GNETWORK_SSL_ERROR	(gnetwork_ssl_error_get_quark ())


typedef enum /* <prefix=GNETWORK_SSL_AUTH> */
{
  GNETWORK_SSL_AUTH_INVALID,

  GNETWORK_SSL_AUTH_ANONYMOUS,
  GNETWORK_SSL_AUTH_CERTIFICATE
}
GNetworkSslAuthType;


typedef enum /* <prefix=GNETWORK_SSL_ERROR> */
{
  GNETWORK_SSL_ERROR_INTERNAL,

  GNETWORK_SSL_ERROR_TRY_AGAIN,
  GNETWORK_SSL_ERROR_INTERRUPTED,
  GNETWORK_SSL_ERROR_UNSUPPORTED_PROTOCOL,
  GNETWORK_SSL_ERROR_PROTOCOL_ALERT,
  GNETWORK_SSL_ERROR_HANDSHAKE_FAILED,
  GNETWORK_SSL_ERROR_AUTHENTICATION_FAILED
}
GNetworkSslError;

/* Error Domain */
GQuark gnetwork_ssl_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* !__GNETWORK_SSL_H__ */
