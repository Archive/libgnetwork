/* 
 * GNetwork Library: libgnetwork/gnetwork-unix-connection.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_UNIX_CONNECTION_H__
#define __GNETWORK_UNIX_CONNECTION_H__ 1

#include <glib-object.h>

G_BEGIN_DECLS


#define GNETWORK_TYPE_UNIX_CONNECTION		  (gnetwork_unix_connection_get_type ())
#define GNETWORK_UNIX_CONNECTION(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNETWORK_TYPE_UNIX_CONNECTION, GNetworkUnixConnection))
#define GNETWORK_UNIX_CONNECTION_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_UNIX_CONNECTION, GNetworkUnixConnectionClass))
#define GNETWORK_IS_UNIX_CONNECTION(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNETWORK_TYPE_UNIX_CONNECTION))
#define GNETWORK_IS_UNIX_CONNECTION_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_UNIX_CONNECTION))
#define GNETWORK_UNIX_CONNECTION_GET_CLASS(obj)	  (G_TYPE_INSTANCE_GET_CLASS ((obj), GNETWORK_TYPE_UNIX_CONNECTION, GNetworkUnixConnectionClass))


typedef enum /* <prefix=GNETWORK_UNIX_CONNECTION> */
{
  GNETWORK_UNIX_CONNECTION_CLOSING,
  GNETWORK_UNIX_CONNECTION_CLOSED,
  GNETWORK_UNIX_CONNECTION_OPENING,
  GNETWORK_UNIX_CONNECTION_AUTHENTICATING,
  GNETWORK_UNIX_CONNECTION_OPEN
}
GNetworkUnixConnectionStatus;


typedef struct _GNetworkUnixConnection GNetworkUnixConnection;
typedef struct _GNetworkUnixConnectionClass GNetworkUnixConnectionClass;
typedef struct _GNetworkUnixConnectionPrivate GNetworkUnixConnectionPrivate;


struct _GNetworkUnixConnection
{
  /* < private > */
  GObject parent;

  GNetworkUnixConnectionPrivate *_priv;
};

struct _GNetworkUnixConnectionClass
{
  /* <private> */
  GObjectClass parent_class;

  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_unix_connection_get_type (void) G_GNUC_CONST;


G_END_DECLS

#endif /* __GNETWORK_UNIX_CONNECTION_H__ */
