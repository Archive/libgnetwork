/* 
 * GNetwork Library: libgnetwork/gnetwork-macros.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_MACROS_H__
#define __GNETWORK_MACROS_H__

#include <glib-object.h>

G_BEGIN_DECLS


#define GNETWORK_INTERFACE_CALL_PARENT_WITH_DEFAULT(get_iface_macro,iface_ctype,instance,method,args,def) (\
  (get_iface_macro (instance) != NULL && \
   g_type_interface_peek_parent (get_iface_macro (instance)) != NULL && \
   ((iface_ctype*)g_type_interface_peek_parent (get_iface_macro (instance)))->method != NULL) ? \
  ((iface_ctype*)g_type_interface_peek_parent (get_iface_macro (instance)))->method args : \
  (void)def \
)


#define GNETWORK_INTERFACE_CALL_PARENT(get_iface_macro,iface_ctype,instance,method,args) \
  GNETWORK_INTERFACE_CALL_PARENT_WITH_DEFAULT(get_iface_macro,iface_ctype,instance,method,args,0)


G_END_DECLS

#endif /* __GNETWORK_MACROS_H__ */
