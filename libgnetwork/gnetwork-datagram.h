/*
 * GNetwork: libgnetwork/gnetwork-datagram.h
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GNETWORK_DATAGRAM_H__
#define __GNETWORK_DATAGRAM_H__ 1

#include "gnetwork-macros.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_DATAGRAM 		  (gnetwork_datagram_get_type ())
#define GNETWORK_DATAGRAM(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNETWORK_TYPE_DATAGRAM, GNetworkDatagram))
#define GNETWORK_IS_DATAGRAM(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNETWORK_TYPE_DATAGRAM))
#define GNETWORK_DATAGRAM_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), GNETWORK_TYPE_DATAGRAM, GNetworkDatagramIface))
#define GNETWORK_DATAGRAM_CALL_PARENT(obj,method,args) \
  (GNETWORK_INTERFACE_CALL_PARENT (GNETWORK_DATAGRAM_GET_IFACE, GNetworkDatagramIface, obj, method, args))

#define GNETWORK_DATAGRAM_ERROR		  (gnetwork_datagram_error_get_quark ())


typedef enum /* <prefix=GNETWORK_DATAGRAM> */
{
  GNETWORK_DATAGRAM_CLOSING,
  GNETWORK_DATAGRAM_CLOSED,
  GNETWORK_DATAGRAM_OPENING,
  GNETWORK_DATAGRAM_OPEN
}
GNetworkDatagramStatus;


typedef enum /* <prefix=GNETWORK_DATAGRAM_ERROR> */
{
  GNETWORK_DATAGRAM_ERROR_INTERNAL,

  GNETWORK_DATAGRAM_ERROR_UNREACHABLE,
  GNETWORK_DATAGRAM_ERROR_PERMISSIONS,
  GNETWORK_DATAGRAM_ERROR_ALREADY_EXISTS
}
GNetworkDatagramError;


typedef struct _GNetworkDatagram GNetworkDatagram;
typedef struct _GNetworkDatagramIface GNetworkDatagramIface;


typedef void (*GNetworkDatagramFunc) (GNetworkDatagram * datagram);
typedef void (*GNetworkDatagramSendFunc) (GNetworkDatagram * datagram,
                                          const GValue * destination,
					  gconstpointer data,
					  glong length);


struct _GNetworkDatagramIface
{
  /* < private > */
  GTypeInterface g_iface;

  /* < public > */
  /* Signals */
  void (*received)         (GNetworkDatagram * datagram,
			    GValue *source,
			    gconstpointer data,
			    gulong length);
  void (*sent)		   (GNetworkDatagram * datagram,
			    GValue *destination,
			    gconstpointer data,
			    gulong length);
  void (*error)		   (GNetworkDatagram * datagram,
			    GValue *info,
			    GError * error);

  /* Methods */
  GNetworkDatagramFunc      open;
  GNetworkDatagramFunc      close;
  GNetworkDatagramSendFunc  send;

  /* < private > */
  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_datagram_get_type (void) G_GNUC_CONST;

/* Methods */
void gnetwork_datagram_open (GNetworkDatagram * datagram);
void gnetwork_datagram_close (GNetworkDatagram * datagram);
void gnetwork_datagram_send (GNetworkDatagram * datagram, const GValue * destination,
			     gconstpointer data, glong length);

/* Signals */
void gnetwork_datagram_received (GNetworkDatagram * datagram,  const GValue * source,
				 gconstpointer data, gulong length);
void gnetwork_datagram_sent (GNetworkDatagram * datagram,  const GValue * destination,
			     gconstpointer data, gulong length);
void gnetwork_datagram_error (GNetworkDatagram * datagram, const GValue * info,
			      const GError * error);

/* Error Reporting */
GQuark gnetwork_datagram_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* __GNETWORK_DATAGRAM_H__ */
