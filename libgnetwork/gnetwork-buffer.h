/* 
 * GNetwork Library: libgnetwork/gnetwork-buffer.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_BUFFER_H__
#define __GNETWORK_BUFFER_H__ 1

#include <glib-object.h>

G_BEGIN_DECLS


#define GNETWORK_TYPE_BUFFER \
  (gnetwork_buffer_get_type ())
#define GNETWORK_BUFFER(buf) \
  ((GNetworkBuffer *) (buf))
#define GNETWORK_IS_BUFFER(buf) \
  ((buf) != NULL && G_TYPE_FROM_CLASS (buf) == GNETWORK_TYPE_BUFFER)


typedef struct _GNetworkBuffer GNetworkBuffer;


GType gnetwork_buffer_get_type (void) G_GNUC_CONST;

GNetworkBuffer *gnetwork_buffer_new (void);
GNetworkBuffer *gnetwork_buffer_new_with_data (const guint8 * data, gssize length);

G_CONST_RETURN guint8 *gnetwork_buffer_get_data (GNetworkBuffer * buffer);
void gnetwork_buffer_set_data (GNetworkBuffer * buffer, const guint8 * data, gssize length);

void gnetwork_buffer_take_data (GNetworkBuffer * buffer, guint8 * data, gssize length);

gsize gnetwork_buffer_get_length (GNetworkBuffer * buffer);

GNetworkBuffer *gnetwork_buffer_ref (GNetworkBuffer * buffer);
void gnetwork_buffer_unref (GNetworkBuffer * buffer);


G_END_DECLS

#endif /* !__GNETWORK_BUFFER_H__ */
