/*
 * GNetwork Library: libgnetwork/gnetwork-unix-server.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_UNIX_SERVER_H__
#define __GNETWORK_UNIX_SERVER_H__ 1

#include "gnetwork-server.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_UNIX_SERVER		(gnetwork_unix_server_get_type ())
#define GNETWORK_UNIX_SERVER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNETWORK_TYPE_UNIX_SERVER, GNetworkUnixServer))
#define GNETWORK_UNIX_SERVER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_UNIX_SERVER, GNetworkUnixServerClass))
#define GNETWORK_IS_UNIX_SERVER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNETWORK_TYPE_UNIX_SERVER))
#define GNETWORK_IS_UNIX_SERVER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_UNIX_SERVER))
#define GNETWORK_UNIX_SERVER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GNETWORK_TYPE_UNIX_SERVER, GNetworkUnixServerClass))


typedef struct _GNetworkUnixServer GNetworkUnixServer;
typedef struct _GNetworkUnixServerClass GNetworkUnixServerClass;
typedef struct _GNetworkUnixServerPrivate GNetworkUnixServerPrivate;


struct _GNetworkUnixServer
{
  /*<private> */
  GObject parent;

  GNetworkUnixServerPrivate *_priv;
};


struct _GNetworkUnixServerClass
{
  /* < private > */
  GObjectClass parent_class;

  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_unix_server_get_type (void);


GNetworkUnixServer *gnetwork_unix_server_new (const gchar * filename);


#define GNETWORK_TYPE_UNIX_SERVER_CREATION_DATA	     (gnetwork_unix_server_creation_data_get_type ())
#define GNETWORK_UNIX_SERVER_CREATION_DATA(boxed)    ((GNetworkUnixServerCreationData *) (boxed))
#define GNETWORK_IS_UNIX_SERVER_CREATION_DATA(boxed) ((boxed) != NULL && ((GTypeClass *) (boxed))->g_type == GNETWORK_TYPE_UNIX_SERVER_CREATION_DATA)

typedef struct _GNetworkUnixServerCreationData GNetworkUnixServerCreationData;

GType gnetwork_unix_server_creation_data_get_type (void);

GNetworkUnixServerCreationData *gnetwork_unix_server_creation_data_dup (const GNetworkUnixServerCreationData * src);
void gnetwork_unix_server_creation_data_free (GNetworkUnixServerCreationData * data);
G_CONST_RETURN gchar *gnetwork_unix_server_creation_data_get_filename (const GNetworkUnixServerCreationData * data);
gconstpointer gnetwork_unix_server_creation_data_get_socket (const GNetworkUnixServerCreationData * data);


G_END_DECLS

#endif /* __GNETWORK_UNIX_SERVER_H__ */
