/* 
 * GNetwork Library: libgnetwork/gnetwork-tcp-connection.h
 *
 * Copyright (C) 2001-2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_TCP_CONNECTION_H__
#define __GNETWORK_TCP_CONNECTION_H__ 1

#include <glib-object.h>

#include "gnetwork-ssl.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_TCP_CONNECTION		(gnetwork_tcp_connection_get_type ())
#define GNETWORK_TCP_CONNECTION(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNETWORK_TYPE_TCP_CONNECTION, GNetworkTcpConnection))
#define GNETWORK_TCP_CONNECTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_TCP_CONNECTION, GNetworkTcpConnectionClass))
#define GNETWORK_IS_TCP_CONNECTION(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNETWORK_TYPE_TCP_CONNECTION))
#define GNETWORK_IS_TCP_CONNECTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_TCP_CONNECTION))
#define GNETWORK_TCP_CONNECTION_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GNETWORK_TYPE_TCP_CONNECTION, GNetworkTcpConnectionClass))


typedef enum /* <prefix=GNETWORK_TCP_CONNECTION_STATUS> */
{
  GNETWORK_TCP_CONNECTION_CLOSING,
  GNETWORK_TCP_CONNECTION_CLOSED,
  GNETWORK_TCP_CONNECTION_LOOKUP,
  GNETWORK_TCP_CONNECTION_OPENING,
  GNETWORK_TCP_CONNECTION_PROXYING,
  GNETWORK_TCP_CONNECTION_AUTHENTICATING,
  GNETWORK_TCP_CONNECTION_OPEN
}
GNetworkTcpConnectionStatus;


typedef struct _GNetworkTcpConnection GNetworkTcpConnection;
typedef struct _GNetworkTcpConnectionClass GNetworkTcpConnectionClass;
typedef struct _GNetworkTcpConnectionPrivate GNetworkTcpConnectionPrivate;


struct _GNetworkTcpConnection
{
  /* < private > */
  GObject parent;

  GNetworkTcpConnectionPrivate *_priv;
};

struct _GNetworkTcpConnectionClass
{
  /* <private> */
  GObjectClass parent_class;

  /* <public> */
  void (*certificate_error)  (GNetworkTcpConnection * connection,
			      GNetworkSslCert * certificate,
			      GNetworkSslCertErrorFlags errors);

  /* <private> */
  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_tcp_connection_get_type (void) G_GNUC_CONST;


G_END_DECLS

#endif /* __GNETWORK_TCP_CONNECTION_H__ */
