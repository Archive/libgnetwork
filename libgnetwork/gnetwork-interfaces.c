/* 
 * GNetwork Library: libgnetwork/gnetwork-interfaces.c
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-interfaces.h"

#include "gnetwork-ip-address.h"

#include "gnetwork-utils.h"
#include "gnetwork-type-builtins.h"

#include <glib/gi18n.h>

#include <sys/types.h>

#include <unistd.h>

/* Interfaces & Addresses */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>

#ifdef __linux__
# include <netpacket/packet.h>
# include <netinet/ether.h>
#else
# include <net/ethernet.h>
#endif

#include <ifaddrs.h>
#include <arpa/inet.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


/**
 * GNetworkInterface:
 * 
 * The object instance structure for local interface objects. This structure
 * contains no public members and should only be accessed using the functions
 * below.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkInterfaceClass:
 * 
 * The object class structure for local interface objects. This structure
 * contains no public members.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkInterfaceType:
 * @GNETWORK_INTERFACE_UNKNOWN: an unknown type of interface.
 * @GNETWORK_INTERFACE_LOOPBACK: a local-only interface.
 * @GNETWORK_INTERFACE_ETHERNET: an ethernet card.
 * @GNETWORK_INTERFACE_DIALUP: a PPP dialup link.
 * @GNETWORK_INTERFACE_SERIAL: a serial-cable.
 * @GNETWORK_INTERFACE_PARALLEL: a parallel cable.
 * @GNETWORK_INTERFACE_IRDA_LAN: an infra-red lan.
 * @GNETWORK_INTERFACE_WAVE_LAN: a WaveLAN radio link.
 * @GNETWORK_INTERFACE_SHAPER: a traffic-shaping interface.
 * @GNETWORK_INTERFACE_IPV6_IN_IPV4: a tunnel to allow IPv6 over IPv4.
 * @GNETWORK_INTERFACE_IP_IN_IP_TUNNEL: a generic IP tunnel.
 * 
 * An enumeration describing known types of interfaces.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkInterfaceUpdateMode:
 * @GNETWORK_INTERFACE_UPDATE_NEVER: never update this interface.
 * @GNETWORK_INTERFACE_UPDATE_IDLE: update whenever nothing more important is running.
 * @GNETWORK_INTERFACE_UPDATE_TIMEOUT: update every @n microseconds (1/1000 of a second).
 * 
 * An enumeration describing how often a #GNetworkInterface object will update
 * itself.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkInterfaceFlags:
 * @GNETWORK_INTERFACE_NONE: no flags are set.
 * @GNETWORK_INTERFACE_IS_UP: the interface is up.
 * @GNETWORK_INTERFACE_IS_RUNNING: the interface is running (has resources allocated to it).
 * @GNETWORK_INTERFACE_IS_DEBUGGING: the interface is in debug mode.
 * @GNETWORK_INTERFACE_IS_LOOPBACK: the interface is a local-only interface.
 * @GNETWORK_INTERFACE_IS_POINT_TO_POINT: the interface is a one-to-one connection.
 * @GNETWORK_INTERFACE_IS_LOAD_MASTER: the interface is the master of a load-balancing network.
 * @GNETWORK_INTERFACE_IS_LOAD_SLAVE: the interface is a slave in a load-balancing network.
 * @GNETWORK_INTERFACE_CAN_BROADCAST: the interface supports broadcasting.
 * @GNETWORK_INTERFACE_CAN_MULTICAST: the interface supports multicasting.
 * @GNETWORK_INTERFACE_NO_TRAILERS: the interface does not support specifying the size of data being used (only useful in very low-level socket programming).
 * @GNETWORK_INTERFACE_NO_ARP: the interface does not support the Address Resolution Protocol.
 * @GNETWORK_INTERFACE_CAN_SET_MEDIA: the interface media type (speed) can be selected.
 * @GNETWORK_INTERFACE_ALTERNATE_LINK: 
 * @GNETWORK_INTERFACE_AUTOSELECTED_MEDIA: the interface media type (speed) is being automatically selected.
 * @GNETWORK_INTERFACE_RECV_ALL_PACKETS: the interface will inform listeners about all incoming packets.
 * @GNETWORK_INTERFACE_RECV_ALL_MULTICAST: the interface will inform listeners about all incoming multicast packets.
 * 
 * A bitwise enumeration of flags which can be set on an interface. These
 * correspond rougly to flags defined by the operating system in
 * &lt;<filename>net/if.h</filename>&gt;.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_INTERFACE:
 * @object: the pointer to cast.
 * 
 * Casts the #GNetworkInterface derived pointer at @object into a
 * (GNetworkInterface*) pointer. Depending on the current debugging level, this
 * function may invoke certain runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_INTERFACE:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GNetworkInterface (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_INTERFACE_CLASS:
 * @klass: the pointer to cast.
 * 
 * Casts a the #GNetworkInterfaceClass derieved pointer at @klass into a
 * (GNetworkSocketClass*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_INTERFACE_CLASS:
 * @klass: the pointer to check.
 * 
 * Checks whether @klass is a #GNetworkInterfaceClass (or derived) class.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_INTERFACE_GET_CLASS:
 * @object: the pointer to check.
 * 
 * Retrieves a pointer to the #GNetworkInterfaceClass for the #GNetworkInterface
 * (or derived) instance @object.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_TYPE_INTERFACE:
 * 
 * The registered #GType of #GNetworkInterface.
 * 
 * Since: 1.0
 **/



/* **************** *
 *  Private Macros  *
 * **************** */

#define GNETWORK_INTERFACE_GET_PRIVATE(obj)	(GNETWORK_INTERFACE (obj)->_priv)


/* ************** *
 *  Enumerations  *
 * ************** */

enum
{
  PROP_0,

  UPDATE_MODE,
  UPDATE_TIME,

  INDEX,
  NAME,
  TYPE,
  FLAGS,
  PROTOCOLS,

  BYTES_RECEIVED,
  PACKETS_RECEIVED,
  ERRORS_RECEIVED,
  BYTES_SENT,
  PACKETS_SENT,
  ERRORS_SENT,
  COLLISIONS,

  IP4_ADDRESS,
  IP4_NETMASK,
  IP4_BROADCAST,
  IP4_DESTINATION,
  IP4_MULTICASTS,

  IP6_ADDRESS,
  IP6_NETMASK,
  IP6_DESTINATION,
  IP6_MULTICASTS,
  
  HW_ADDRESS,
  HW_BROADCAST,
  HW_DESTINATION
};


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkInterfacePrivate
{
#ifdef __linux__
  guint update_id;
  guint notify_id;

  guint64 bytes_received;
  guint64 packets_received;
  guint64 errors_received;

  guint64 bytes_sent;
  guint64 packets_sent;
  guint64 errors_sent;

  guint64 collisions;
#endif /* __linux__ */

  gint index_;
  gchar *name;

  GNetworkIpAddress ip4_address;
  GNetworkIpAddress ip4_netmask;
  GNetworkIpAddress ip4_broadcast_or_destination;
  GSList *ip4_multicasts;

  GNetworkIpAddress ip6_address;
  GNetworkIpAddress ip6_netmask;
  GNetworkIpAddress ip6_destination;
  GSList *ip6_multicasts;

  gchar *hw_address;
  gchar *hw_broadcast_or_destination;

  gint update_time;

  GNetworkInterfaceUpdateMode update_mode:2;
  GNetworkInterfaceType type:5;
  GNetworkInterfaceFlags flags:16;
  GNetworkProtocols protocols:3;
};


typedef struct
{
  union {
    gchar *str;
    GNetworkIpAddress *ip_address;
  } key;
  gboolean is_str:1;
}
GNetworkInterfaceKey;


typedef struct 
{
  const gchar * const name;
  const GNetworkInterfaceType type;
}
GNetworkInterfaceNameTypeMap;


/* ****************** *
 *  Global Variables  *
 * ****************** */

G_LOCK_DEFINE_STATIC (cached_interfaces);
static GHashTable *cached_interfaces = NULL;

static gpointer gnetwork_interface_parent_class = NULL;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

#ifdef __linux__

static gboolean
update_counters (GNetworkObject * object, gpointer data)
{
  GNetworkInterfacePrivate *priv;
  GIOChannel *channel;
  GIOStatus status;
  gboolean found;

  priv = GNETWORK_INTERFACE_GET_PRIVATE (object);

  if (priv->name == NULL)
    {
      priv->update_id = 0;
      return FALSE;
    }

  status = G_IO_STATUS_NORMAL;
  found = FALSE;

  channel = g_io_channel_new_file ("/proc/net/dev", "r", NULL);
  g_io_channel_set_close_on_unref (channel, TRUE);

  while (!found && status == G_IO_STATUS_NORMAL)
    {
      gchar *line;
      gsize length;

      line = NULL;
      length = 0;

      status = g_io_channel_read_line (channel, &line, &length, NULL, NULL);

      if (status == G_IO_STATUS_NORMAL)
	{
	  gchar *name, *ifdata;

	  name = NULL;
	  ifdata = NULL;

	  line = g_strdelimit (line, ":", ' ');
	  if (sscanf (line, "%as %a[^\n]\n", &name, &ifdata) == 2)
	    {
	      GObject *gobject = G_OBJECT (object);
	      name = g_strstrip (name);

	      if (strcmp (name, priv->name) == 0)
		{
		  /* FORMAT (VALUE = read, value = ignored):
		   * BYTES_RECEIVED
		   * PACKETS_RECEIVED
		   * ERRORS_RECEIVED
		   * dropped_recv
		   * fifo_recv
		   * compressed_recv
		   * multicast_recv
		   * BYTES_SENT
		   * PACKETS_SENT
		   * ERRORS_SENT
		   * dropped_sent
		   * fifo_sent
		   * COLLISIONS
		   * compressed_sent */
		  sscanf (ifdata, "%"G_GUINT64_FORMAT" "
				  "%"G_GUINT64_FORMAT" "
				  "%"G_GUINT64_FORMAT" "
				  "%*s "
				  "%*s "
				  "%*s "
				  "%*s "
				  "%"G_GUINT64_FORMAT" "
				  "%"G_GUINT64_FORMAT" "
				  "%"G_GUINT64_FORMAT" "
				  "%*s "
				  "%*s "
				  "%"G_GUINT64_FORMAT" ",
			  &(priv->bytes_received),
			  &(priv->packets_received),
			  &(priv->errors_received),
			  &(priv->bytes_sent),
			  &(priv->packets_sent),
			  &(priv->errors_sent),
			  &(priv->collisions));

		  g_object_freeze_notify (gobject);
		  g_object_notify (gobject, "bytes-received");
		  g_object_notify (gobject, "packets-received");
		  g_object_notify (gobject, "errors-received");
		  g_object_notify (gobject, "bytes-sent");
		  g_object_notify (gobject, "packets-sent");
		  g_object_notify (gobject, "errors-sent");
		  g_object_notify (gobject, "collisions");
		  g_object_thaw_notify (gobject);
		}
	    }
	  else
	    {
	      status = G_IO_STATUS_ERROR;
	    }

	  free (name);
	  free (ifdata);
	}
    }

  g_io_channel_unref (channel);

  return found;
}


static void
reset_updates (GNetworkObject * object)
{
  GNetworkInterfacePrivate *priv = GNETWORK_INTERFACE_GET_PRIVATE (object);

  if (priv->update_id != 0)
    gnetwork_object_remove_source (object, priv->update_id);

  switch (priv->update_mode)
    {
    case GNETWORK_INTERFACE_UPDATE_NEVER:
      priv->update_id = 0;
      break;
    case GNETWORK_INTERFACE_UPDATE_IDLE:
      priv->update_id = gnetwork_object_add_idle (object, update_counters, NULL);
      break;
    case GNETWORK_INTERFACE_UPDATE_TIMEOUT:
      priv->update_id = gnetwork_object_add_timeout (object, priv->update_time, update_counters,
						     NULL);
      break;
    default:
      g_assert_not_reached ();
      break;
    }
}

#endif /* __linux__ */


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_interface_set_property (GObject * object, guint property, const GValue * value,
				 GParamSpec * pspec)
{
  GNetworkInterfacePrivate *priv = GNETWORK_INTERFACE_GET_PRIVATE (object);

  switch (property)
    {
    case UPDATE_MODE:
      priv->update_mode = g_value_get_enum (value);
      break;
    case UPDATE_TIME:
      priv->update_time = g_value_get_uint (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      return;
      break;
    }

  /* We do this here because all the (current) properties need this done. */
#ifdef __linux__
  reset_updates (GNETWORK_OBJECT (object));
#endif /* __linux__ */
}


static void
gnetwork_interface_get_property (GObject * object, guint property, GValue * value,
				 GParamSpec * pspec)
{
  GNetworkInterfacePrivate *priv = GNETWORK_INTERFACE_GET_PRIVATE (object);

  switch (property)
    {
    case UPDATE_MODE:
      g_value_set_enum (value, priv->update_mode);
      break;
    case UPDATE_TIME:
      g_value_set_uint (value, priv->update_time);
      break;

    case INDEX:
      g_value_set_int (value, priv->index_);
      break;
    case NAME:
      g_value_set_string (value, priv->name);
      break;
    case TYPE:
      g_value_set_enum (value, priv->type);
      break;
    case FLAGS:
      g_value_set_flags (value, priv->flags);
      break;
    case PROTOCOLS:
      g_value_set_flags (value, priv->protocols);
      break;

    case IP4_ADDRESS:
      if (gnetwork_ip_address_is_valid (&priv->ip4_address))
	g_value_set_boxed (value, &priv->ip4_address);
      else
	g_value_set_boxed (value, NULL);
      break;
    case IP4_NETMASK:
      if (gnetwork_ip_address_is_valid (&priv->ip4_netmask))
	g_value_set_boxed (value, &priv->ip4_netmask);
      else
	g_value_set_boxed (value, NULL);
      break;
    case IP4_BROADCAST:
      if ((priv->flags & GNETWORK_INTERFACE_CAN_BROADCAST) &&
	  gnetwork_ip_address_is_valid (&priv->ip4_broadcast_or_destination))
	g_value_set_boxed (value, &priv->ip4_broadcast_or_destination);
      else
	g_value_set_boxed (value, NULL);
      break;
    case IP4_DESTINATION:
      if ((priv->flags & GNETWORK_INTERFACE_IS_POINT_TO_POINT) &&
	  gnetwork_ip_address_is_valid (&priv->ip4_broadcast_or_destination))
	g_value_set_boxed (value, &priv->ip4_broadcast_or_destination);
      else
	g_value_set_boxed (value, NULL);
      break;

    case IP6_ADDRESS:
      if (gnetwork_ip_address_is_valid (&priv->ip6_address))
	g_value_set_boxed (value, &priv->ip6_address);
      else
	g_value_set_boxed (value, NULL);
      break;
    case IP6_NETMASK:
      if (gnetwork_ip_address_is_valid (&priv->ip6_netmask))
	g_value_set_boxed (value, &priv->ip6_netmask);
      else
	g_value_set_boxed (value, NULL);
      break;
    case IP6_DESTINATION:
      if ((priv->flags & GNETWORK_INTERFACE_IS_POINT_TO_POINT) &&
	  gnetwork_ip_address_is_valid (&priv->ip6_destination))
	g_value_set_boxed (value, &priv->ip6_destination);
      else
	g_value_set_boxed (value, NULL);
      break;

    case HW_ADDRESS:
      g_value_set_string (value, priv->hw_address);
      break;
    case HW_BROADCAST:
      if (priv->flags & GNETWORK_INTERFACE_CAN_BROADCAST)
	g_value_set_string (value, priv->hw_broadcast_or_destination);
      else
	g_value_set_string (value, NULL);
      break;
    case HW_DESTINATION:
      if (priv->flags & GNETWORK_INTERFACE_IS_POINT_TO_POINT)
	g_value_set_string (value, priv->hw_broadcast_or_destination);
      else
	g_value_set_string (value, NULL);
      break;

    /* FIXME: Could other OSes properly handle this stuff. */
#ifdef __linux__
    case BYTES_RECEIVED:
      g_value_set_uint64 (value, priv->bytes_received);
      break;
    case PACKETS_RECEIVED:
      g_value_set_uint64 (value, priv->bytes_received);
      break;
    case ERRORS_RECEIVED:
      g_value_set_uint64 (value, priv->errors_received);
      break;
    case BYTES_SENT:
      g_value_set_uint64 (value, priv->bytes_sent);
      break;
    case PACKETS_SENT:
      g_value_set_uint64 (value, priv->bytes_sent);
      break;
    case ERRORS_SENT:
      g_value_set_uint64 (value, priv->errors_sent);
      break;
    case COLLISIONS:
      g_value_set_uint64 (value, priv->collisions);
      break;
#else
    case BYTES_RECEIVED:
    case PACKETS_RECEIVED:
    case ERRORS_RECEIVED:
    case BYTES_SENT:
    case PACKETS_SENT:
    case ERRORS_SENT:
    case COLLISIONS:
      g_value_set_uint64 (value, 0);
      break;
#endif

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_interface_finalize (GObject * object)
{
  GNetworkInterfacePrivate * priv = GNETWORK_INTERFACE_GET_PRIVATE (object);

  g_free (priv->name);

  while (priv->ip4_multicasts != NULL)
    {
      g_free (priv->ip4_multicasts->data);
      priv->ip4_multicasts = g_slist_remove_link (priv->ip4_multicasts, priv->ip4_multicasts);
    }

  while (priv->ip6_multicasts != NULL)
    {
      g_free (priv->ip6_multicasts->data);
      priv->ip6_multicasts = g_slist_remove_link (priv->ip6_multicasts, priv->ip6_multicasts);
    }

  g_free (priv->hw_address);
  g_free (priv->hw_broadcast_or_destination);

  g_free (priv);

  if (G_OBJECT_CLASS (gnetwork_interface_parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (gnetwork_interface_parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_interface_class_init (GNetworkInterfaceClass * class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  if (cached_interfaces == NULL)
    cached_interfaces = g_hash_table_new (g_str_hash, g_str_equal);

  gnetwork_interface_parent_class = g_type_class_peek_parent (class);

  object_class->set_property = gnetwork_interface_set_property;
  object_class->get_property = gnetwork_interface_get_property;
  object_class->finalize = gnetwork_interface_finalize;

  g_object_class_install_property (object_class, UPDATE_MODE,
				   g_param_spec_enum ("update-mode", _("Interface Update Mode"),
						      _("If/How to update this interface's data "
							"counters."),
						      GNETWORK_TYPE_INTERFACE_UPDATE_MODE,
						      GNETWORK_INTERFACE_UPDATE_NEVER,
						      G_PARAM_READWRITE));
  g_object_class_install_property (object_class, UPDATE_TIME,
				   g_param_spec_uint ("update-time", _("Update Time"),
						     _("The amount of time (in milliseconds) "
						       "between updates if the update-mode is "
						       "TIMEOUT."), 0, G_MAXUINT, 0,
						     G_PARAM_READWRITE));

  g_object_class_install_property (object_class, INDEX,
				   g_param_spec_int ("index", _("Interface Index"),
						     _("The index of this interface."),
						     -1, G_MAXINT, -1,
						     G_PARAM_READABLE));
  g_object_class_install_property (object_class, NAME,
				   g_param_spec_string ("name", _("Interface Name"),
							_("The name of this interface."), NULL,
							G_PARAM_READABLE));
  g_object_class_install_property (object_class, TYPE,
				   g_param_spec_enum ("type", _("Interface Type"),
						      _("The type of this interface."),
						      GNETWORK_TYPE_INTERFACE_TYPE,
						      GNETWORK_INTERFACE_UNKNOWN,
						      G_PARAM_READABLE));
  g_object_class_install_property (object_class, FLAGS,
				   g_param_spec_flags ("flags", _("Interface Flags"),
						      _("Bitwise flags describing various features "
							"of this interface."),
						      GNETWORK_TYPE_INTERFACE_FLAGS,
						      GNETWORK_INTERFACE_NONE,
						      G_PARAM_READABLE));
  g_object_class_install_property (object_class, PROTOCOLS,
				   g_param_spec_flags ("protocols", _("Interface Protocols"),
						      _("Bitwise flags describing protocols "
							"supported on this interface."),
						      GNETWORK_TYPE_PROTOCOLS,
						      GNETWORK_PROTOCOL_NONE, G_PARAM_READABLE));

  g_object_class_install_property (object_class, BYTES_RECEIVED,
				   g_param_spec_uint64 ("bytes-received", _("Bytes Received"),
							_("The number of bytes received by this "
							  "interface."), 0, G_MAXUINT64, 0,
							G_PARAM_READABLE));
  g_object_class_install_property (object_class, PACKETS_RECEIVED,
				   g_param_spec_uint64 ("packets-received", _("Packets Received"),
							_("The number of packets received by this "
							  "interface."), 0, G_MAXUINT64, 0,
							G_PARAM_READABLE));
  g_object_class_install_property (object_class, ERRORS_RECEIVED,
				   g_param_spec_uint64 ("errors-received", _("Errors Received"),
							_("The number of errors received by this "
							  "interface."), 0, G_MAXUINT64, 0,
							G_PARAM_READABLE));

  g_object_class_install_property (object_class, BYTES_SENT,
				   g_param_spec_uint64 ("bytes-sent", _("Bytes Sent"),
							_("The number of bytes sent through this "
							  "interface."), 0, G_MAXUINT64, 0,
							G_PARAM_READABLE));
  g_object_class_install_property (object_class, PACKETS_SENT,
				   g_param_spec_uint64 ("packets-sent", _("Packets Sent"),
							_("The number of packets sent through this "
							  "interface."), 0, G_MAXUINT64, 0,
							G_PARAM_READABLE));
  g_object_class_install_property (object_class, ERRORS_SENT,
				   g_param_spec_uint64 ("errors-sent", _("Errors Sent"),
							_("The number of errors sent through this "
							  "interface."), 0, G_MAXUINT64, 0,
							G_PARAM_READABLE));

  g_object_class_install_property (object_class, COLLISIONS,
				   g_param_spec_uint64 ("collisions", _("Collisions"),
							_("The number of packet collisions which "
							  "have occurred on this interface."),
							0, G_MAXUINT64, 0, G_PARAM_READABLE));

  g_object_class_install_property (object_class, IP4_ADDRESS,
				   g_param_spec_boxed ("ip4-address", _("IPv4 Address"),
						       _("The IPv4 address of this interface."),
						       GNETWORK_TYPE_IP_ADDRESS,
						       G_PARAM_READABLE));
  g_object_class_install_property (object_class, IP4_NETMASK,
				   g_param_spec_boxed ("ip4-netmask", _("IPv4 Netmask"),
						       _("The IPv4 netmask of this interface."),
						       GNETWORK_TYPE_IP_ADDRESS,
						       G_PARAM_READABLE));
  g_object_class_install_property (object_class, IP4_DESTINATION,
				   g_param_spec_boxed ("ip4-destination", _("IPv4 Destination"),
						       _("The IPv4 address of this interface's "
							 "destination."),
						       GNETWORK_TYPE_IP_ADDRESS,
						       G_PARAM_READABLE));
  g_object_class_install_property (object_class, IP4_BROADCAST,
				   g_param_spec_boxed ("ip4-broadcast", _("IPv4 Broadcast"),
						       _("The IPv4 broadcast address of this "
							 "interface."),
						       GNETWORK_TYPE_IP_ADDRESS,
						       G_PARAM_READABLE));

  g_object_class_install_property (object_class, IP6_ADDRESS,
				   g_param_spec_boxed ("ip6-address", _("IPv6 Address"),
						       _("The IPv6 address of this interface."),
						       GNETWORK_TYPE_IP_ADDRESS,
						       G_PARAM_READABLE));
  g_object_class_install_property (object_class, IP6_NETMASK,
				   g_param_spec_boxed ("ip6-netmask", _("IPv6 Netmask"),
						       _("The IPv6 netmask of this interface."),
						       GNETWORK_TYPE_IP_ADDRESS,
						       G_PARAM_READABLE));
  g_object_class_install_property (object_class, IP6_DESTINATION,
				   g_param_spec_boxed ("ip6-destination", _("IPv6 Destination"),
						       _("The IPv6 address of this interface's "
							 "destination."),
						       GNETWORK_TYPE_IP_ADDRESS,
						       G_PARAM_READABLE));
						       
  g_object_class_install_property (object_class, HW_ADDRESS,
				   g_param_spec_string ("hardware-address", _("Hardware Address"),
							_("The hardware address of this interface."),
							NULL, G_PARAM_READABLE));
  g_object_class_install_property (object_class, HW_BROADCAST,
				   g_param_spec_string ("hardware-broadcast",
							_("Hardware Broadcast Address"),
							_("The hardware broadcast address of this "
							  "interface."), NULL, G_PARAM_READABLE));
  g_object_class_install_property (object_class, HW_DESTINATION,
				   g_param_spec_string ("hardware-destination",
							_("Hardware Destination Address"),
							_("The hardware address of this "
							  "interface's destination."), NULL,
							G_PARAM_READABLE));

  g_type_class_add_private (class, sizeof (GNetworkInterfacePrivate));
}


static void
gnetwork_interface_instance_init (GNetworkInterface * interface)
{
  GNetworkInterfacePrivate *priv = G_TYPE_INSTANCE_GET_PRIVATE (interface, GNETWORK_TYPE_INTERFACE,
								GNetworkInterfacePrivate);

  interface->_priv = priv;

  priv->name = NULL;
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_interface_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      static const GTypeInfo info = {
	sizeof (GNetworkInterfaceClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_interface_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkInterface),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_interface_instance_init,
	NULL			/* value table */
      };

      type = g_type_register_static (GNETWORK_TYPE_OBJECT, "GNetworkInterface", &info, 0);
    }

  return type;
}

/**
 * gnetwork_interface_new:
 * 
 * Creates a new, empty interface object. This object should then be connected
 * to a local interface with gnetwork_interface_load_from_name() or
 * gnetwork_interface_load_from_address().
 * 
 * Applications typically will not need to use this function. They should use
 * gnetwork_interface_get_for_name() and gnetwork_interface_get_for_address()
 * instead to retrieve references to already-existing interface objects.
 * 
 * Returns: the new interface object.
 * 
 * Since: 1.0
 **/
GNetworkInterface *
gnetwork_interface_new (void)
{
  return g_object_new (GNETWORK_TYPE_INTERFACE, NULL);
}


static void
load_common (GNetworkInterfacePrivate *priv, struct ifaddrs *data)
{
  priv->name = g_strdup (data->ifa_name);
  priv->ip4_multicasts = NULL;
  priv->ip6_multicasts = NULL;

  priv->flags = GNETWORK_INTERFACE_NONE;

  /* Flags */
  if (data->ifa_flags & IFF_UP)
    priv->flags |= GNETWORK_INTERFACE_IS_UP;

  if (data->ifa_flags & IFF_RUNNING)
    priv->flags |= GNETWORK_INTERFACE_IS_RUNNING;

  if (data->ifa_flags & IFF_DEBUG)
    priv->flags |= GNETWORK_INTERFACE_IS_DEBUGGING;

  if (data->ifa_flags & IFF_LOOPBACK)
    priv->flags |= GNETWORK_INTERFACE_IS_LOOPBACK;

  if (data->ifa_flags & IFF_POINTOPOINT)
    priv->flags |= GNETWORK_INTERFACE_IS_POINT_TO_POINT;

#ifdef IFF_MASTER
  if (data->ifa_flags & IFF_MASTER)
    priv->flags |= GNETWORK_INTERFACE_IS_LOAD_MASTER;
#endif /* IFF_MASTER */

#ifdef IFF_SLAVE
  if (data->ifa_flags & IFF_SLAVE)
    priv->flags |= GNETWORK_INTERFACE_IS_LOAD_SLAVE;
#endif /* IFF_SLAVE */

  if (data->ifa_flags & IFF_BROADCAST)
    priv->flags |= GNETWORK_INTERFACE_CAN_BROADCAST;

  if (data->ifa_flags & IFF_MULTICAST)
    priv->flags |= GNETWORK_INTERFACE_CAN_MULTICAST;

#ifdef IFF_NOTRAILERS
  if (data->ifa_flags & IFF_NOTRAILERS)
    priv->flags |= GNETWORK_INTERFACE_NO_TRAILERS;
#endif /* IFF_NOTRAILERS */

  if (data->ifa_flags & IFF_NOARP)
    priv->flags |= GNETWORK_INTERFACE_NO_ARP;

#ifdef IFF_PORTSEL
  if (data->ifa_flags & IFF_PORTSEL)
    priv->flags |= GNETWORK_INTERFACE_CAN_SET_MEDIA;
#endif /* IFF_PORTSEL */

#ifdef IFF_ALTPHYS
  if (data->ifa_flags & IFF_ALTPHYS)
    priv->flags |= GNETWORK_INTERFACE_ALTERNATE_LINK;
#endif /* IFF_ALTPHYS */

#ifdef IFF_AUTOMEDIA
  if (data->ifa_flags & IFF_AUTOMEDIA)
    priv->flags |= GNETWORK_INTERFACE_AUTOSELECTED_MEDIA;
#endif /* IFF_AUTOMEDIA */

  if (data->ifa_flags & IFF_PROMISC)
    priv->flags |= GNETWORK_INTERFACE_RECV_ALL_PACKETS;

  if (data->ifa_flags & IFF_ALLMULTI)
    priv->flags |= GNETWORK_INTERFACE_RECV_ALL_MULTICAST;
}


static void
load_protocol (GNetworkInterfacePrivate *priv, struct ifaddrs *data)
{
  GNetworkIpAddress addr = GNETWORK_IP_ADDRESS_INIT;

  switch (data->ifa_addr->sa_family)
    {
    case AF_INET:
      priv->protocols |= GNETWORK_PROTOCOL_IPv4;

      _gnetwork_ip_address_set_from_sockaddr (&addr, data->ifa_addr);

      if (gnetwork_ip_address_is_multicast (&addr))
	{
	  priv->ip4_multicasts = g_slist_prepend (priv->ip4_multicasts,
						  gnetwork_ip_address_dup (&addr));
	}
      else
	{
	  memcpy (&(priv->ip4_address), &addr, sizeof (GNetworkIpAddress));
	}

      _gnetwork_ip_address_set_from_sockaddr (&(priv->ip4_address), data->ifa_addr);
      _gnetwork_ip_address_set_from_sockaddr (&(priv->ip4_netmask), data->ifa_netmask);
      _gnetwork_ip_address_set_from_sockaddr (&(priv->ip4_broadcast_or_destination),
					      data->ifa_dstaddr);
      break;
    case AF_INET6:
      priv->protocols |= GNETWORK_PROTOCOL_IPv6;

      _gnetwork_ip_address_set_from_sockaddr (&addr, data->ifa_addr);

      if (gnetwork_ip_address_is_multicast (&addr))
	{
	  priv->ip6_multicasts = g_slist_prepend (priv->ip6_multicasts,
						  gnetwork_ip_address_dup (&addr));
	}
      else
	{
	  memcpy (&(priv->ip6_address), &addr, sizeof (GNetworkIpAddress));
	}

      _gnetwork_ip_address_set_from_sockaddr (&(priv->ip6_netmask), data->ifa_netmask);
      _gnetwork_ip_address_set_from_sockaddr (&(priv->ip6_destination), data->ifa_dstaddr);
      break;
#ifdef AF_PACKET
    case AF_PACKET:
      priv->protocols |= GNETWORK_PROTOCOL_PACKET;

      priv->index_ = ((struct sockaddr_ll *) (data->ifa_addr))->sll_ifindex;

      priv->hw_address = _gnetwork_sockaddr_get_address (data->ifa_addr);
      priv->hw_broadcast_or_destination = _gnetwork_sockaddr_get_address (data->ifa_dstaddr);
      break;
#endif
    }
}


/**
 * gnetwork_interface_get_for_name (const gchar * address)
 * @name: the name of the interface to retrieve.
 * 
 * Retrieves a reference to the cached interface object for @name.
 * 
 * Returns: a reference to the interface.
 * 
 * Since: 1.0
 **/
GNetworkInterface *
gnetwork_interface_get_for_name (const gchar * name)
{
  GNetworkInterface *interface;
  struct ifaddrs *addrs, *current;
  gint if_ret;

  g_return_val_if_fail (name != NULL && name[0] != '\0', NULL);
  
  if_ret = getifaddrs (&addrs);
  if (if_ret < 0)
    {
      g_warning ("Could not retrieve the list of local interfaces: %s", g_strerror (if_ret));
      return NULL;
    }

  interface = NULL;

  G_LOCK (cached_interfaces);

  for (current = addrs; current != NULL; current = current->ifa_next)
    {
      if (strcmp (current->ifa_name, name) == 0)
	{
	  interface = g_hash_table_lookup (cached_interfaces, name);

	  if (interface == NULL)
	    {
	      interface = g_object_new (GNETWORK_TYPE_INTERFACE, NULL);
	      load_common (interface->_priv, current);

	      g_hash_table_insert (cached_interfaces, interface->_priv->name, interface);
	    }

	  load_protocol (interface->_priv, current);
	}
    }

  G_UNLOCK (cached_interfaces);

  freeifaddrs (addrs);

  return interface;
}


/**
 * gnetwork_interface_get_for_address (const gchar * address)
 * @address: the address of the interface to retrieve.
 * 
 * Retrieves a reference to the cached interface object for @address.
 * 
 * Returns: a reference to the interface.
 * 
 * Since: 1.0
 **/
GNetworkInterface *
gnetwork_interface_get_for_address (const gchar * address)
{
//  GNetworkInterface *interface;

  g_return_val_if_fail (gnetwork_str_is_ip_address (address), NULL);

  return NULL;
}


/**
 * gnetwork_interface_load_from_name:
 * @interface: the interface object to use.
 * @name: the name of the interface to load.
 * 
 * Loads the interface data for @name into @interface. This function should be
 * called after creating an empty interface object to connect the object to the
 * system's configuration for a local interface.
 * 
 * Since: 1.0
 **/
void
gnetwork_interface_load_from_name (GNetworkInterface * interface, const gchar * name)
{
  g_return_if_fail (GNETWORK_IS_INTERFACE (interface));
  g_return_if_fail (name != NULL && name[0] != '\0');

  /* FIXME: load the interface. */
}


/**
 * gnetwork_interface_load_from_address:
 * @interface: the interface object to use.
 * @address: the address of the interface to load.
 * 
 * Loads the interface data for @address into @interface. This function should
 * be called after creating an empty interface object to connect the object to
 * the system's configuration for a local interface.
 * 
 * Since: 1.0
 **/
void
gnetwork_interface_load_from_address (GNetworkInterface * interface, const gchar * address)
{
  g_return_if_fail (GNETWORK_IS_INTERFACE (interface));
  g_return_if_fail (address != NULL && address[0] != '\0');

  /* FIXME: load the interface. */
}


/**
 * gnetwork_interface_get_all_interfaces:
 * 
 * Retrieves a list of #GNetworkInterface objects representing all the local
 * interfaces for this host. The returned list and list data should be freed
 * using the following code:
 * 
 * <informalexample><programlisting>g_slist_foreach (list, (GFunc) g_object_unref, NULL);
 * g_slist_free (list);
 * </programlisting></informalexample>
 * 
 * Returns: a list of local interfaces.
 * 
 * Since: 1.0
 **/
GSList *
gnetwork_interface_get_all_interfaces (void)
{
  GHashTable *table;
  struct ifaddrs *interfaces,
    *current;
  GSList *retval,
    *list;
  GNetworkInterface *interface;
  guint i;

  interfaces = NULL;

  if (getifaddrs (&interfaces) < 0)
    return NULL;

  G_LOCK (cached_interfaces);

  table = g_hash_table_new (g_str_hash, g_str_equal);

  retval = NULL;
  g_hash_table_foreach (cached_interfaces, _gnetwork_slist_from_hash_table, &retval);
  
  for (list = retval; list != NULL; list = list->next)
    {
      interface = list->data;
      g_hash_table_insert (table, interface->_priv->name, interface);
    }

  for (i = 0, current = interfaces; current != NULL; current = current->ifa_next, i++)
    {
      interface = g_hash_table_lookup (table, current->ifa_name);

      if (g_slist_find (list, interface) == NULL && strncmp (current->ifa_name, "sit", 3) != 0)
	{
	  if (interface == NULL)
	    {
	      interface = g_object_new (G_TYPE_INTERFACE, NULL);
	      load_common (interface->_priv, current);
	      g_hash_table_insert (table, interface->_priv->name, interface);
	      g_hash_table_insert (cached_interfaces, interface->_priv->name, interface);
	    }

	  load_protocol (interface->_priv, current);
	}
    }

  g_slist_free (retval);
  retval = NULL;

  freeifaddrs (interfaces);

  g_hash_table_foreach (table, _gnetwork_slist_from_hash_table, &retval);
  g_hash_table_destroy (table);

  G_UNLOCK (cached_interfaces);

  return g_slist_sort (retval, gnetwork_interface_collate);
}


/**
 * gnetwork_interface_get_ip4_multicasts:
 * @interface: the interface object to examine.
 * 
 * Retrieves a list of #GNetworkIpAddress structures representing the IPv4
 * multicast groups this interface is a member of. The returned value should
 * not be modified or freed.
 * 
 * Returns: a list of #GNetworkIpAddress structures.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN GSList *
gnetwork_interface_get_ip4_multicasts (GNetworkInterface * interface)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE (interface), NULL);

  return interface->_priv->ip4_multicasts;
}


/**
 * gnetwork_interface_get_ip6_multicasts:
 * @interface: the interface object to examine.
 * 
 * Retrieves a list of #GNetworkIpAddress structures representing the IPv6
 * multicast groups this interface is a member of. The returned value should
 * not be modified or freed.
 * 
 * Returns: a list of #GNetworkIpAddress structures.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN GSList *
gnetwork_interface_get_ip6_multicasts (GNetworkInterface * interface)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE (interface), NULL);

  return interface->_priv->ip6_multicasts;
}


/**
 * gnetwork_interface_collate:
 * @interface1: an interface object.
 * @interface2: an interface object.
 * 
 * Determines which interface of the arguments is "greater" (should be sorted
 * before) than the other, using the name.
 * 
 * Returns: %-1 if @interface1 should be sorted first, %1 if @interface2 should be sorted first, or %0 if they are equal.
 * 
 * Since: 1.0
 **/
gint
gnetwork_interface_collate (gconstpointer interface1, gconstpointer interface2)
{
  gint retval;

  g_return_val_if_fail (interface1 == NULL || GNETWORK_IS_INTERFACE (interface1), 0);
  g_return_val_if_fail (interface2 == NULL || GNETWORK_IS_INTERFACE (interface2), 0);

  if (interface1 == NULL && interface2 != NULL)
    retval = 1;
  else if (interface1 != NULL && interface2 == NULL)
    retval = -1;
  else if (interface1 == interface2)
    retval = 0;
  else
    {
      GNetworkInterfacePrivate *priv1, *priv2;

      priv1 = GNETWORK_INTERFACE_GET_PRIVATE (interface1);
      priv2 = GNETWORK_INTERFACE_GET_PRIVATE (interface2);

      if (priv1->name == NULL && priv2->name != NULL)
	retval = 1;
      else if (priv1->name != NULL && priv2->name == NULL)
	retval = -1;
      else if (priv1->name == priv2->name)
	retval = 0;
      else
	retval = g_utf8_collate (priv1->name, priv2->name);
    }

  return retval;
}


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkInterfaceInfo
{
  GTypeClass g_class;

  gint ref;

  guint index_;
  gchar *name;

  GNetworkIpAddress ip4_address;
  GNetworkIpAddress ip4_netmask;
  GNetworkIpAddress ip4_broadcast_or_destination;
  GSList *ip4_multicasts;

  GNetworkIpAddress ip6_address;
  GNetworkIpAddress ip6_netmask;
  GNetworkIpAddress ip6_destination;
  GSList *ip6_multicasts;

  gchar *hw_address;
  gchar *hw_broadcast_or_destination;

  GNetworkInterfaceFlags flags:16;
  GNetworkProtocols protocols:3;
};


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static void
append_iface_info_from_interface (GNetworkInterfaceInfo * info, struct ifaddrs *data)
{
  GNetworkIpAddress addr = GNETWORK_IP_ADDRESS_INIT;

  switch (data->ifa_addr->sa_family)
    {
    case AF_INET:
      info->protocols |= GNETWORK_PROTOCOL_IPv4;

      _gnetwork_ip_address_set_from_sockaddr (&addr, data->ifa_addr);

      if (gnetwork_ip_address_is_multicast (&addr))
	{
	  info->ip4_multicasts = g_slist_prepend (info->ip4_multicasts,
						  gnetwork_ip_address_dup (&addr));
	}
      else
	{
	  memcpy (&(info->ip4_address), &addr, sizeof (GNetworkIpAddress));
	}

      _gnetwork_ip_address_set_from_sockaddr (&(info->ip4_address), data->ifa_addr);
      _gnetwork_ip_address_set_from_sockaddr (&(info->ip4_netmask), data->ifa_netmask);
      _gnetwork_ip_address_set_from_sockaddr (&(info->ip4_broadcast_or_destination),
					     data->ifa_dstaddr);
      break;
    case AF_INET6:
      info->protocols |= GNETWORK_PROTOCOL_IPv6;

      _gnetwork_ip_address_set_from_sockaddr (&addr, data->ifa_addr);

      if (gnetwork_ip_address_is_multicast (&addr))
	{
	  info->ip6_multicasts = g_slist_prepend (info->ip6_multicasts,
						  gnetwork_ip_address_dup (&addr));
	}
      else
	{
	  memcpy (&(info->ip6_address), &addr, sizeof (GNetworkIpAddress));
	}

      _gnetwork_ip_address_set_from_sockaddr (&(info->ip6_netmask), data->ifa_netmask);
      _gnetwork_ip_address_set_from_sockaddr (&(info->ip6_destination), data->ifa_dstaddr);
      break;
#ifdef AF_PACKET
    case AF_PACKET:
      info->protocols |= GNETWORK_PROTOCOL_PACKET;

      info->index_ = ((struct sockaddr_ll *) data->ifa_addr)->sll_ifindex;

      info->hw_address = _gnetwork_sockaddr_get_address (data->ifa_addr);
      info->hw_broadcast_or_destination = _gnetwork_sockaddr_get_address (data->ifa_dstaddr);
      break;
#endif
    }
}


static GNetworkInterfaceInfo *
create_info_from_interface (struct ifaddrs *data)
{
  GNetworkInterfaceInfo *info = g_new0 (GNetworkInterfaceInfo, 1);

  info->g_class.g_type = GNETWORK_TYPE_INTERFACE_INFO;
  info->ref = 1;

  info->name = g_strdup (data->ifa_name);
  info->ip4_multicasts = NULL;
  info->ip6_multicasts = NULL;

  info->flags = GNETWORK_INTERFACE_NONE;

  /* Flags */
  if (data->ifa_flags & IFF_UP)
    info->flags |= GNETWORK_INTERFACE_IS_UP;

  if (data->ifa_flags & IFF_RUNNING)
    info->flags |= GNETWORK_INTERFACE_IS_RUNNING;

  if (data->ifa_flags & IFF_DEBUG)
    info->flags |= GNETWORK_INTERFACE_IS_DEBUGGING;

  if (data->ifa_flags & IFF_LOOPBACK)
    info->flags |= GNETWORK_INTERFACE_IS_LOOPBACK;

  if (data->ifa_flags & IFF_POINTOPOINT)
    info->flags |= GNETWORK_INTERFACE_IS_POINT_TO_POINT;

#ifdef IFF_MASTER
  if (data->ifa_flags & IFF_MASTER)
    info->flags |= GNETWORK_INTERFACE_IS_LOAD_MASTER;
#endif /* IFF_MASTER */

#ifdef IFF_SLAVE
  if (data->ifa_flags & IFF_SLAVE)
    info->flags |= GNETWORK_INTERFACE_IS_LOAD_SLAVE;
#endif /* IFF_SLAVE */

  if (data->ifa_flags & IFF_BROADCAST)
    info->flags |= GNETWORK_INTERFACE_CAN_BROADCAST;

  if (data->ifa_flags & IFF_MULTICAST)
    info->flags |= GNETWORK_INTERFACE_CAN_MULTICAST;

#ifdef IFF_NOTRAILERS
  if (data->ifa_flags & IFF_NOTRAILERS)
    info->flags |= GNETWORK_INTERFACE_NO_TRAILERS;
#endif /* IFF_NOTRAILERS */

  if (data->ifa_flags & IFF_NOARP)
    info->flags |= GNETWORK_INTERFACE_NO_ARP;

#ifdef IFF_PORTSEL
  if (data->ifa_flags & IFF_PORTSEL)
    info->flags |= GNETWORK_INTERFACE_CAN_SET_MEDIA;
#endif /* IFF_PORTSEL */

#ifdef IFF_ALTPHYS
  if (data->ifa_flags & IFF_ALTPHYS)
    info->flags |= GNETWORK_INTERFACE_ALTERNATE_LINK;
#endif /* IFF_ALTPHYS */

#ifdef IFF_AUTOMEDIA
  if (data->ifa_flags & IFF_AUTOMEDIA)
    info->flags |= GNETWORK_INTERFACE_AUTOSELECTED_MEDIA;
#endif /* IFF_AUTOMEDIA */

  if (data->ifa_flags & IFF_PROMISC)
    info->flags |= GNETWORK_INTERFACE_RECV_ALL_PACKETS;

  if (data->ifa_flags & IFF_ALLMULTI)
    info->flags |= GNETWORK_INTERFACE_RECV_ALL_MULTICAST;

  return info;
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_interface_info_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      type = g_boxed_type_register_static ("GNetworkInterfaceInfo",
					   (GBoxedCopyFunc) gnetwork_interface_info_ref,
					   (GBoxedFreeFunc) gnetwork_interface_info_unref);
    }

  return type;
}


/**
 * gnetwork_interface_get_info:
 * @name: the name of the interface.
 * 
 * Retrieves the information for the interface referred to by @name. The @name
 * should be something like "%eth0", "%lo", etc.
 * 
 * Returns: a structure describing a local interface.
 * 
 * Since: 1.0
 **/
GNetworkInterfaceInfo *
gnetwork_interface_get_info (const gchar * name)
{
  struct ifaddrs *interfaces, *current;
  GNetworkInterfaceInfo *retval;

  g_return_val_if_fail (name != NULL, NULL);
  g_return_val_if_fail (strncmp (name, "sit", 3) != 0, NULL);

  interfaces = NULL;

  if (getifaddrs (&interfaces) < 0)
    return NULL;

  retval = NULL;

  for (current = interfaces; current != NULL; current = current->ifa_next)
    {
      if (g_ascii_strcasecmp (current->ifa_name, name) == 0)
	{
	  if (retval == NULL)
	    {
	      retval = create_info_from_interface (current);
	    }

	  append_iface_info_from_interface (retval, current);
	}
    }

  freeifaddrs (interfaces);

  return retval;
}


typedef struct
{
  gboolean is_ipaddr;
  gconstpointer addr;
  GNetworkInterfaceInfo *retval;
}
FindByAddrData;


static void
find_by_address (gpointer key, GNetworkInterfaceInfo *info, FindByAddrData *find_data)
{
  if (find_data->is_ipaddr)
    {
      if (gnetwork_ip_address_is_ipv4 (find_data->addr))
	{
	  if (gnetwork_ip_address_equal (&(info->ip4_address), find_data->addr) ||
	      gnetwork_ip_address_equal (&(info->ip4_broadcast_or_destination), find_data->addr) ||
	      gnetwork_ip_address_equal (&(info->ip4_netmask), find_data->addr))
	    {
	      find_data->retval = gnetwork_interface_info_ref (info);
	    }
	}
      else if (gnetwork_ip_address_equal (&(info->ip6_address), find_data->addr))
	{
	  find_data->retval = gnetwork_interface_info_ref (info);
	}
    }
  else if (g_ascii_strcasecmp (find_data->addr, info->hw_address) == 0)
    {
      find_data->retval = gnetwork_interface_info_ref (info);
    }
}


/**
 * gnetwork_interface_get_info_by_address:
 * @address: a valid address string.
 * 
 * Retrieves the #GNetworkInterfaceInfo which uses the address in @address.
 * 
 * Returns: a list of local interfaces.
 * 
 * Since: 1.0
 **/
GNetworkInterfaceInfo *
gnetwork_interface_get_info_by_address (const gchar * address)
{
  GNetworkIpAddress addr;
  GHashTable *table;
  struct ifaddrs *interfaces, *current;
  guint i;
  FindByAddrData find_data = {
    FALSE, NULL, NULL
  };

  interfaces = NULL;

  if (getifaddrs (&interfaces) < 0)
    return NULL;

  /* Create the interface information. */
  table = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
				 (GDestroyNotify) gnetwork_interface_info_unref);

  for (i = 0, current = interfaces; current != NULL; current = current->ifa_next, i++)
    {
      if (strncmp (current->ifa_name, "sit", 3) != 0)
	{
	  GNetworkInterfaceInfo *data = g_hash_table_lookup (table, current->ifa_name);

	  if (data == NULL)
	    {
	      data = create_info_from_interface (current);
	      g_hash_table_insert (table, data->name, data);
	    }

	  append_iface_info_from_interface (data, current);
	}
    }

  freeifaddrs (interfaces);

  find_data.is_ipaddr = gnetwork_ip_address_set_from_string (&addr, address);

  if (find_data.is_ipaddr)
    find_data.addr = &addr;
  else
    find_data.addr = address;

  g_hash_table_foreach (table, (GHFunc) find_by_address, &find_data);
  g_hash_table_destroy (table);

  return find_data.retval;
}


/**
 * gnetwork_interface_info_get_name:
 * @info: the interface information to examine.
 * 
 * Retrieves the configured name of the interface described by @info (e.g.
 * "eth0"). If @info is invalid, %NULL will be returned.
 * 
 * Returns: the name of @info, or %NULL.
 * 
 * Since: 1.0
 **/
gchar *
gnetwork_interface_info_get_name (const GNetworkInterfaceInfo * info)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE_INFO (info), NULL);

  return g_strdup (info->name);
}


/**
 * gnetwork_interface_info_get_protocols:
 * @info: the interface information to examine.
 * 
 * Retrieves the protocols used by the interface at @info. If @info is invalid,
 * or the interface does not support any known protcols, %GNETWORK_PROTOCOL_NONE
 * will be returned.
 * 
 * Returns: the protocols used by @info.
 * 
 * Since: 1.0
 **/
GNetworkProtocols
gnetwork_interface_info_get_protocols (const GNetworkInterfaceInfo * info)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE_INFO (info), GNETWORK_PROTOCOL_NONE);

  return info->protocols;
}


/**
 * gnetwork_interface_info_get_address:
 * @info: the interface information to examine.
 * @protocol: the protocol type to use.
 * 
 * Retrieves the configured @protocol address of the interface described by
 * @info (e.g. "127.0.0.1" for IPv4, "::1" for IPv6, or "00:00:00:00:00:00" for
 * hardware). If @info is invalid, @protocol contains more than one flag, or if
 * @info does not support @protocol, %NULL will be returned.
 * 
 * Returns: the @protocol address of @info, or %NULL.
 * 
 * Since: 1.0
 **/
gconstpointer
gnetwork_interface_info_get_address (const GNetworkInterfaceInfo * info, GNetworkProtocols protocol)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE_INFO (info), NULL);
  g_return_val_if_fail (_gnetwork_flags_value_is_valid (GNETWORK_TYPE_PROTOCOLS, protocol), NULL);

  switch (protocol)
    {
    case GNETWORK_PROTOCOL_PACKET:
      return info->hw_address;
    case GNETWORK_PROTOCOL_IPv4:
      return &(info->ip4_address);
    case GNETWORK_PROTOCOL_IPv6:
      return &(info->ip6_address);

    default:
      break;
    }

  g_return_val_if_reached (NULL);
}


/**
 * gnetwork_interface_info_get_broadcast_address:
 * @info: the interface information to examine.
 * @protocol: the protocol type to use.
 * 
 * Retrieves the broadcast address of the interface described by @info (e.g.
 * "127.0.0.255" for IPv4 or "00:00:00:00:00:00" for hardware). If @info is
 * invalid or is a "point-to-point" interface, or if @protocol does not support
 * broadcasting (like IPv6), %NULL will be returned.
 * 
 * Returns: the @protocol broadcast address of @info, or %NULL.
 * 
 * Since: 1.0
 **/
gconstpointer
gnetwork_interface_info_get_broadcast_address (const GNetworkInterfaceInfo * info,
					       GNetworkProtocols protocol)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE_INFO (info), NULL);
  g_return_val_if_fail (_gnetwork_flags_value_is_valid (GNETWORK_TYPE_PROTOCOLS, protocol), NULL);

  switch (protocol)
    {
    case GNETWORK_PROTOCOL_PACKET:
      return (!(info->flags & GNETWORK_INTERFACE_IS_POINT_TO_POINT) ?
	      info->hw_broadcast_or_destination : NULL);
    case GNETWORK_PROTOCOL_IPv4:
      return (!(info->flags & GNETWORK_INTERFACE_IS_POINT_TO_POINT) ?
	      &(info->ip4_broadcast_or_destination) : NULL);
    case GNETWORK_PROTOCOL_IPv6:
      return NULL;

    default:
      break;
    }

  g_return_val_if_reached (NULL);
}


/**
 * gnetwork_interface_info_get_destination:
 * @info: the interface information to examine.
 * @protocol: the protocol type to use.
 * 
 * Retrieves the destination address of the interface described by @info, (e.g.
 * "127.0.0.1" for IPv4, "::1" for IPv6, or "00:00:00:00:00:00" for hardware).
 * The returned data should not be modified or freed. If @info is invalid or
 * is not a "point-to-point" interface, %NULL will be returned.
 * 
 * See also: gnetwork_interface_info_get_flags().
 * 
 * Returns: the @protocol destination address of @info, or %NULL.
 * 
 * Since: 1.0
 **/
gconstpointer
gnetwork_interface_info_get_destination (const GNetworkInterfaceInfo * info,
					 GNetworkProtocols protocol)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE_INFO (info), NULL);
  g_return_val_if_fail (_gnetwork_flags_value_is_valid (GNETWORK_TYPE_PROTOCOLS, protocol), NULL);

  switch (protocol)
    {
    case GNETWORK_PROTOCOL_PACKET:
      return (info->flags & GNETWORK_INTERFACE_IS_POINT_TO_POINT ?
	      info->hw_broadcast_or_destination : NULL);
    case GNETWORK_PROTOCOL_IPv4:
      return (info->flags & GNETWORK_INTERFACE_IS_POINT_TO_POINT ?
	      &(info->ip4_broadcast_or_destination) : NULL);
    case GNETWORK_PROTOCOL_IPv6:
      return (info->flags & GNETWORK_INTERFACE_IS_POINT_TO_POINT ?
	      &(info->ip6_destination) : NULL);

    default:
      break;
    }

  g_return_val_if_reached (NULL);
}


/**
 * gnetwork_interface_info_get_netmask:
 * @info: the interface information to examine.
 * @protocol: the protocol to use.
 * 
 * Retrieves the @protocol network mask of the interface described by @info
 * (e.g. "255.255.255.255" for IPv4, or "ffff:ffff:ffff:ffff" for IPv6). If
 * @info is invalid, %NULL will be returned.
 * 
 * Returns: the network mask of @info, or %NULL.
 * 
 * Since: 1.0
 **/
gconstpointer
gnetwork_interface_info_get_netmask (const GNetworkInterfaceInfo * info, GNetworkProtocols protocol)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE_INFO (info), NULL);
  g_return_val_if_fail (_gnetwork_flags_value_is_valid (GNETWORK_TYPE_PROTOCOLS, protocol), NULL);

  switch (protocol)
    {
    case GNETWORK_PROTOCOL_IPv4:
      return &(info->ip4_netmask);
    case GNETWORK_PROTOCOL_IPv6:
      return &(info->ip6_netmask);
    case GNETWORK_PROTOCOL_PACKET:
      return NULL;

    default:
      break;
    }

  g_return_val_if_reached (NULL);
}


/**
 * gnetwork_interface_info_get_multicasts:
 * @info: the interface information to examine.
 * @protocol: the protocol to use.
 * 
 * Retrieves a list of current multicast IP addresses for @protocol from the
 * interface described by @info (e.g. "224.0.0.1" for IPv4, or "ff02::1" for
 * IPv6). If @info or @protocol is invalid, or @protocol does not support
 * multicasting (like the packet protocol), %NULL will be returned.
 * 
 * Returns: the multicast addresses of @info, or %NULL.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN GSList *
gnetwork_interface_info_get_multicasts (const GNetworkInterfaceInfo * info,
					GNetworkProtocols protocol)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE_INFO (info), NULL);
  g_return_val_if_fail (_gnetwork_flags_value_is_valid (GNETWORK_TYPE_PROTOCOLS, protocol), NULL);

  switch (protocol)
    {
    case GNETWORK_PROTOCOL_IPv4:
      return info->ip4_multicasts;
    case GNETWORK_PROTOCOL_IPv6:
      return info->ip6_multicasts;
    case GNETWORK_PROTOCOL_PACKET:
      return NULL;

    default:
      break;
    }

  g_return_val_if_reached (NULL);
}


/**
 * gnetwork_interface_info_get_flags:
 * @info: the interface information to examine.
 * 
 * Retrieves the flags set on interface described by @info. If @info is invalid
 * or no flags have been set, %GNETWORK_INTERFACE_NONE is returned.
 * 
 * Returns: the flags set on @interface.
 * 
 * Since: 1.0
 **/
GNetworkInterfaceFlags
gnetwork_interface_info_get_flags (const GNetworkInterfaceInfo * info)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE_INFO (info), GNETWORK_INTERFACE_NONE);

  return info->flags;
}


/**
 * gnetwork_interface_info_get_index:
 * @info: the interface information to examine.
 * 
 * Retrieves the index of the interface described by @info. If @info is invalid
 * or the index is unknown, %0 is returned.
 * 
 * Returns: the index of @interface.
 * 
 * Since: 1.0
 **/
guint
gnetwork_interface_info_get_index (const GNetworkInterfaceInfo * info)
{
  g_return_val_if_fail (GNETWORK_IS_INTERFACE_INFO (info), 0);

  return info->index_;
}


/**
 * gnetwork_interface_info_ref:
 * @info: the data to reference.
 * 
 * Creates a reference to the data in @info. When no longer needed, this
 * reference should be released with gnetwork_interface_info_unref().
 * 
 * Returns: a reference to @info, or %NULL.
 * 
 * Since: 1.0
 **/
GNetworkInterfaceInfo *
gnetwork_interface_info_ref (GNetworkInterfaceInfo * info)
{
  g_return_val_if_fail (info == NULL || GNETWORK_IS_INTERFACE_INFO (info), NULL);

  if (info != NULL && info->ref > 0)
    {
      info->ref++;
    }

  return info;
}


/**
 * gnetwork_interface_info_unref:
 * @info: the local interface reference to release.
 * 
 * Releases a reference to the data in @info. When all references have been
 * released, the data in @info will be destroyed.
 * 
 * Since: 1.0
 **/
void
gnetwork_interface_info_unref (GNetworkInterfaceInfo * info)
{
  g_return_if_fail (info == NULL || GNETWORK_IS_INTERFACE_INFO (info));

  if (info == NULL)
    return;

  info->ref--;

  if (info->ref == 0)
    {
      g_free (info->name);

      g_slist_foreach (info->ip4_multicasts, (GFunc) g_free, NULL);
      g_slist_free (info->ip4_multicasts);

      g_slist_foreach (info->ip6_multicasts, (GFunc) g_free, NULL);
      g_slist_free (info->ip4_multicasts);

      g_free (info->hw_address);
      g_free (info->hw_broadcast_or_destination);
      g_free (info);
    }
}


/**
 * gnetwork_interface_info_collate:
 * @info1: a structure describing a local interface.
 * @info2: a structure describing a local interface.
 * 
 * Determines which interface of the arguments is "greater" (should be sorted
 * before) than the other, using the name.
 * 
 * Returns: %-1 if @info1 should be sorted first, %1 if @info2 should be sorted first, or %0 if they are equal.
 * 
 * Since: 1.0
 **/
gint
gnetwork_interface_info_collate (const GNetworkInterfaceInfo * info1,
				 const GNetworkInterfaceInfo * info2)
{
  gint retval;

  g_return_val_if_fail (info1 == NULL || GNETWORK_IS_INTERFACE_INFO (info1), 0);
  g_return_val_if_fail (info2 == NULL || GNETWORK_IS_INTERFACE_INFO (info2), 0);

  if (info1 == NULL && info2 != NULL)
    retval = 1;
  else if (info1 != NULL && info2 == NULL)
    retval = -1;
  else if (info1 == info2)
    retval = 0;
  else if (info1->name == NULL && info2->name != NULL)
    retval = 1;
  else if (info1->name != NULL && info2->name == NULL)
    retval = -1;
  else if (info1->name == info2->name)
    retval = 0;
  else
    retval = g_utf8_collate (info1->name, info2->name);

  return retval;
}


GNetworkProtocols
gnetwork_str_to_protocol (const gchar * address)
{
  GNetworkProtocols retval;
  gpointer ptr;

  if (address == NULL || address[0] == '\0')
    return GNETWORK_PROTOCOL_NONE;

  ptr = g_malloc0 (MAX (sizeof (struct in_addr), sizeof (struct in6_addr)));
  if (inet_pton (AF_INET6, address, ptr) > -1)
    retval = GNETWORK_PROTOCOL_IPv6;
  if (inet_pton (AF_INET, address, ptr) > -1)
    retval = GNETWORK_PROTOCOL_IPv4;
  else if (ether_aton (address) != NULL)
    retval = GNETWORK_PROTOCOL_PACKET;
  else
    retval = GNETWORK_PROTOCOL_NONE;

  g_free (ptr);

  return retval;
}
