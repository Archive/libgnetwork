/* 
 * GNetwork Library: libgnetwork/gnetwork-ip-address.c
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-ip-address.h"
#include "gnetwork-utils.h"

#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>


GType
gnetwork_ip_address_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      type = g_boxed_type_register_static ("GNetworkIpAddress",
					   (GBoxedCopyFunc) gnetwork_ip_address_dup, g_free);
    }

  return type;
}


/**
 * gnetwork_ip_address_new:
 * @str: the string representation of an IP address.
 * 
 * Creates a new IP address based on the data in @str. If @str is %NULL, a
 * zeroed address structure will be returned. The returned data should be freed
 * with g_free() when no longer needed.
 * 
 * Returns: a new #GNetworkIpAddress structure for @str.
 * 
 * Since: 1.0
 **/
GNetworkIpAddress *
gnetwork_ip_address_new (const gchar * str)
{
  GNetworkIpAddress *retval;

  retval = g_new0 (GNetworkIpAddress, 1);

  gnetwork_ip_address_set_from_string (retval, str);

  return retval;
}


/**
 * gnetwork_ip_address_to_string:
 * @address: the address to examine.
 * 
 * Creates a string representation of the IP address in @address. The returned
 * value should be freed with g_free() when no longer needed.
 * 
 * Returns: the string representation of @address.
 * 
 * Since: 1.0
 **/
gchar *
gnetwork_ip_address_to_string (const GNetworkIpAddress * address)
{
  gchar *retval;

  g_return_val_if_fail (address != NULL, NULL);

  if (gnetwork_ip_address_is_ipv4 (address))
    {
      retval = g_strdup_printf ("%u.%u.%u.%u", gnetwork_ip_address_get8 (address, 12),
				gnetwork_ip_address_get8 (address, 13),
				gnetwork_ip_address_get8 (address, 14),
				gnetwork_ip_address_get8 (address, 15));
    }
  else if (!gnetwork_ip_address_is_valid (address))
    {
      retval = NULL;
    }
  else
    {
      gchar addr[INET_ADDRESS_SIZE] = { 0 };

      if (inet_ntop (AF_INET6, address, addr, INET_ADDRESS_SIZE) < 0)
	retval = NULL;
      else
	retval = g_strdup (addr);
    }

  return retval;
}


/**
 * gnetwork_ip_address_set_from_string:
 * @address: the address to modify.
 * @str: the new IP address in string format.
 * 
 * Overwrites the existing IP address in @address using the string data in @str.
 * If @str is not a valid IP address string, @address will be set to the invalid
 * address (all 0s), and the function will return %FALSE. @str may be in IPv4
 * ("127.0.0.1"), IPv6 ("::1"), or IPv4-compatable IPv6 format ("::127.0.0.1").
 * 
 * Returns: %TRUE or %FALSE, depending on whether @str was a valid IP address string.
 * 
 * Since: 1.0.
 **/
gboolean
gnetwork_ip_address_set_from_string (GNetworkIpAddress * address, const gchar * str)
{
  struct in_addr addr;

  g_return_val_if_fail (address != NULL, FALSE);
  g_return_val_if_fail (str == NULL || (str[0] != '\0' && strlen (str) <= INET6_ADDRSTRLEN), FALSE);

  memset (address, 0, sizeof (GNetworkIpAddress));

  if (str == NULL)
    return FALSE;

  memset (&addr, 0, sizeof (struct in_addr));

  /* If we've got an IPv4 string, copy it in appropriately. */
  if (inet_pton (AF_INET, str, &addr) >= 0)
    {
      gnetwork_ip_address_set16 (address, 5, 0xFFFF);
      GNETWORK_IP_ADDRESS32 (address, 3) = ((addr.s_addr) & 0xFFFFFFFF);
      return TRUE;
    }
  /* Otherwise, try and set the IPv6 string */
  
  return (inet_pton (AF_INET6, str, address) >= 0);
}


/**
 * gnetwork_str_is_ip_address:
 * @str: the character string to check, or %NULL.
 * 
 * Checks if @str contains a valid IP address string (in either IPv6 or IPv4
 * format). %NULL and empty strings are considered invalid addresses.
 * 
 * Returns: %TRUE or %FALSE.
 * 
 * Since: 1.0
 **/
gboolean
gnetwork_str_is_ip_address (const gchar * str)
{
  GNetworkIpAddress addr;

  return gnetwork_ip_address_set_from_string (&addr, str);
}


/**
 * _gnetwork_ip_address_set_from_string:
 * @address: the address to modify.
 * @str: the new IP address in string format.
 * 
 * Overwrites the existing IP address in @address using the string data in @str.
 * If @str is not a valid string, @address will be set to the invalid address
 * (all 0s). @str may be in IPv4 ("127.0.0.1"), IPv6 ("::1"), or IPv4-on-IPv6
 * format ("::ffff::127.0.0.1").
 * 
 * Since: 1.0.
 **/
void
_gnetwork_ip_address_set_from_sockaddr (GNetworkIpAddress * address, const struct sockaddr *sa)
{
  g_return_if_fail (address != NULL);
  g_return_if_fail (sa == NULL || sa->sa_family == AF_INET || sa->sa_family == AF_INET6);

  memset (address, 0, sizeof (GNetworkIpAddress));

  if (sa == NULL)
    return;

  switch (sa->sa_family)
    {
    case AF_INET:
      gnetwork_ip_address_set16 (address, 5, 0xFFFF);
      memcpy (&(GNETWORK_IP_ADDRESS32 (address, 3)),
	      &(((struct sockaddr_in *) sa)->sin_addr.s_addr), sizeof (struct in_addr));
      break;
    case AF_INET6:
      memcpy (address, &(((struct sockaddr_in6 *) sa)->sin6_addr), sizeof (GNetworkIpAddress));
      break;
    default:
      break;
    }
}


/**
 * _gnetwork_ip_address_to_sockaddr:
 * @ip_address: the IP address to use.
 * @port: the port number to use.
 * @sa_size: the location to store the size of the returned value, or %NULL.
 * 
 * Creates a new sockaddr_in or sockaddr_in6 (if IPv6 is available) based on
 * @ip_address and @port. It doesn't matter whether @ip_address is in IPv4
 * ("127.0.0.1") or IPv6 ("0000:0000:0000:0001") for the new addr to be created.
 * 
 * Returns: a new sockaddr structure.
 * 
 * Since: 1.0
 **/
struct sockaddr *
_gnetwork_ip_address_to_sockaddr (const GNetworkIpAddress * address, guint16 port, gint * sa_size)
{
  gint sockfd;
  gpointer retval = NULL;

  sockfd = socket (AF_INET6, SOCK_DGRAM, 0);

  if (sockfd < 0)
    {
      struct sockaddr_in *sa;

      close (sockfd);

      if (sa_size != NULL)
	*sa_size = sizeof (struct sockaddr_in);

      sa = g_new0 (struct sockaddr_in, 1);

      if (address != NULL && gnetwork_ip_address_is_ipv4 (address))
	{
	  memcpy (&(sa->sin_addr.s_addr), &(GNETWORK_IP_ADDRESS32 (address, 3)), sizeof (guint32));
	}
      else
	{
	  sa->sin_addr.s_addr = INADDR_ANY;
	}

      sa->sin_family = AF_INET;
      sa->sin_port = g_htons (port);

      retval = sa;
    }
  else
    {
      struct sockaddr_in6 *sa;

      close (sockfd);

      if (sa_size != NULL)
	*sa_size = sizeof (struct sockaddr_in6);

      sa = g_new0 (struct sockaddr_in6, 1);

      if (address != NULL && gnetwork_ip_address_is_valid (address))
	{
	  memcpy (&(sa->sin6_addr), address, sizeof (GNetworkIpAddress));
	}
      else
	{
	  sa->sin6_addr = in6addr_any;
	}

      sa->sin6_family = AF_INET6;
      sa->sin6_port = g_htons (port);

      retval = sa;
    }

  return retval;
}


/**
 * gnetwork_ip_address_dup:
 * @address: a pointer to the address to duplicate.
 * 
 * Creates a copy of the IP address in @address. The returned value should be
 * freed with g_free() when no longer needed.
 * 
 * Returns: a copy of @address.
 * 
 * Since: 1.0
 **/
GNetworkIpAddress *
gnetwork_ip_address_dup (const GNetworkIpAddress * address)
{
  return g_memdup (address, sizeof (GNetworkIpAddress));
}


/**
 * gnetwork_ip_address_collate:
 * @address1: a pointer to the first address to test.
 * @address2: a pointer to the second address to test.
 * 
 * Compares @address1 and @address2 for sorting.
 * 
 * Returns: %-1 if @address1 should be sorted first, %1 if @address2 should be sorted first, and %0 if both addresses are equal.
 * 
 * Since: 1.0
 **/
gint
gnetwork_ip_address_collate (const GNetworkIpAddress * address1, const GNetworkIpAddress * address2)
{
  if (address1 == NULL && address2 != NULL)
    {
      return 1;
    }
  else if (address1 != NULL && address2 == NULL)
    {
      return -1;
    }
  else
    {
      if (gnetwork_ip_address_get64 (address1, 0) < gnetwork_ip_address_get64 (address2, 0))
	return 1;
      else if (gnetwork_ip_address_get64 (address1, 0) > gnetwork_ip_address_get64 (address2, 0))
	return -1;
      else if (gnetwork_ip_address_get64 (address1, 1) < gnetwork_ip_address_get64 (address2, 1))
	return 1;
      else if (gnetwork_ip_address_get64 (address1, 1) > gnetwork_ip_address_get64 (address2, 1))
	return -1;
    }

  return 0;
}


/**
 * gnetwork_ip_address_hash:
 * @address: a pointer to the address to hash.
 * 
 * Computes a hash value for the IP address in @address.
 * 
 * Returns: the hash of @address.
 * 
 * Since: 1.0
 **/
guint
gnetwork_ip_address_hash (gconstpointer address)
{
  if (address == NULL)
    return 0;
#if (G_MAXUINT == 0xFFFFFFFF)	/* 32-bit arch */
  return gnetwork_ip_address_get32 (address, 3);
#elif (G_MAXUINT == G_MAXUINT64)	/* 64-bit arch */
  return gnetwork_ip_address_get64 (address, 1);
#else /* 128+-bit arch? */
  return *GNETWORK_IP_ADDRESS (address);
#endif /* That's all folks */
}


/**
 * gnetwork_ip_address_equal:
 * @address1: a pointer to the first address to test.
 * @address2: a pointer to the second address to test.
 * 
 * Tests the equality of @address1 and @address2. This is a #GEqualFunc, so it
 * can be used straight-away in a #GHashTable.
 * 
 * Returns: %TRUE of @address1 equals @address2, %FALSE if it does not.
 * 
 * Since: 1.0
 **/
gboolean
gnetwork_ip_address_equal (gconstpointer address1, gconstpointer address2)
{
  return ((address1 == address2) ||
	  (address1 != NULL && address2 != NULL &&
	   gnetwork_ip_address_get64 (address1, 0) == gnetwork_ip_address_get64 (address2, 0) &&
	   gnetwork_ip_address_get64 (address2, 1) == gnetwork_ip_address_get64 (address2, 1)));
}


#ifndef __GNUC__
void
gnetwork_ip_address_set_ipv4 (GNetworkIpAddress * address, guint32 ip)
{
  g_return_if_fail (address != NULL);

  memset (address, 0, 80);
  gnetwork_ip_address_set16 (address, 5, 0xFFFF);
  gnetwork_ip_address_set32 (address, 3, ip);
}
#endif /* !__GNUC__ */
