/* 
 * GNetwork Library: libgnetwork/gnetwork-socket.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_SOCKET_H__
#define __GNETWORK_SOCKET_H__

#include "gnetwork-object.h"
#include "gnetwork-params.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_SOCKET \
  (gnetwork_socket_get_type ())
#define GNETWORK_SOCKET(object)	\
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GNETWORK_TYPE_SOCKET, GNetworkSocket))
#define GNETWORK_SOCKET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_SOCKET, GNetworkSocketClass))
#define GNETWORK_IS_SOCKET(object)	\
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GNETWORK_TYPE_SOCKET))
#define GNETWORK_IS_SOCKET_CLASS(klass)	\
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_SOCKET))
#define GNETWORK_SOCKET_GET_CLASS(object) \
  (G_TYPE_INSTANCE_GET_CLASS ((object), GNETWORK_TYPE_SOCKET, GNetworkSocketClass))


#define GNETWORK_SOCKET_ERROR		(gnetwork_socket_error_get_quark ())

  
typedef enum /* <prefix=GNETWORK_SOCKET> */
{
  GNETWORK_SOCKET_STATE_FIRST   = -2,

  GNETWORK_SOCKET_STATE_CLOSING = -1,
  GNETWORK_SOCKET_STATE_CLOSED  = 0,
  GNETWORK_SOCKET_STATE_OPENING = 1,
  GNETWORK_SOCKET_STATE_OPEN    = 2,

  GNETWORK_SOCKET_STATE_LAST    = 3
}
GNetworkSocketState;


typedef enum /* <prefix=GNETWORK_SOCKET_ERROR> */
{
  GNETWORK_SOCKET_ERROR_INTERNAL,
  GNETWORK_SOCKET_ERROR_PERMISSIONS,
  GNETWORK_SOCKET_ERROR_ALREADY_EXISTS
}
GNetworkSocketError;


typedef struct _GNetworkSocket GNetworkSocket;
typedef struct _GNetworkSocketPrivate GNetworkSocketPrivate;
typedef struct _GNetworkSocketClass GNetworkSocketClass;


struct _GNetworkSocket
{
  GNetworkObject parent;

  GNetworkSocketPrivate *_priv;
};

struct _GNetworkSocketClass
{
  /* <private> */
  GNetworkObjectClass parent_class;

  /* <public> */
  /* Methods */
  void (*open)   (GNetworkSocket * socket);
  void (*close)  (GNetworkSocket * socket);

  /* <private> */
  void (*__gnetwork_padding1);
  void (*__gnetwork_padding2);
  void (*__gnetwork_padding3);
  void (*__gnetwork_padding4);

  /* <public> */
  /* Signals */
  gboolean  (*error)  (GNetworkSocket * socket,
		       GNetworkParams * params);

  /* <private> */
  void (*__gnetwork_padding5);
  void (*__gnetwork_padding6);
  void (*__gnetwork_padding7);
  void (*__gnetwork_padding8);
};


GType gnetwork_socket_get_type (void) G_GNUC_CONST;

void gnetwork_socket_open (GNetworkSocket * socket);
void gnetwork_socket_close (GNetworkSocket * socket);

void gnetwork_socket_add_to_bytes_sent (GNetworkSocket * socket, gulong length);
void gnetwork_socket_add_to_bytes_received (GNetworkSocket * socket, gulong length);
void gnetwork_socket_set_state (GNetworkSocket * socket, gint state);
gboolean gnetwork_socket_error (GNetworkSocket * socket, GNetworkParams * params);

GQuark gnetwork_socket_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* __GNETWORK_SOCKET_H__ */
