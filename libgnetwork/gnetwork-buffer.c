/*
 * GNetwork Library: libgnetwork/gnetwork-buffer.c
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */


#include "gnetwork-buffer.h"

#include <string.h>


#define TRASH_STACK_SIZE 5


/**
 * GNetworkBuffer:
 * 
 * An opaque structure for bytestreams. This structure contains no public
 * members, and should only be accessed using the functions below.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_BUFFER:
 * @buf: the pointer to cast.
 * 
 * Casts the pointer at @buf into a (GNetworkBuffer*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_BUFFER:
 * @buf: the pointer to check.
 * 
 * Checks whether @buf is a #GNetworkBuffer structure.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_TYPE_BUFFER:
 * 
 * The registered #GType of #GNetworkBuffer.
 * 
 * Since: 1.0
 **/


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkBuffer
{
  GTypeClass g_class;

  guint ref;

  guint8 *data;
  gsize length;
};


/* ****************** *
 *  Global Variables  *
 * ****************** */

G_LOCK_DEFINE_STATIC (buffer_stack);

static volatile GTrashStack *buffer_stack = NULL;


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_buffer_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      type = g_boxed_type_register_static ("GNetworkBuffer", (GBoxedCopyFunc) gnetwork_buffer_ref,
					   (GBoxedFreeFunc) gnetwork_buffer_unref);
    }

  return type;
}


/**
 * gnetwork_buffer_new:
 * 
 * Creates a new empy buffer structure. The returned reference must be deleted
 * with gnetwork_buffer_unref() when no longer needed.
 * 
 * Returns: a newly-allocated #GNetworkBuffer structure.
 * 
 * Since: 1.0
 **/
GNetworkBuffer *
gnetwork_buffer_new (void)
{
  GNetworkBuffer *retval;

  G_LOCK (buffer_stack);
  retval = g_trash_stack_pop ((GTrashStack **) &buffer_stack);
  G_UNLOCK (buffer_stack);

  if (retval == NULL)
    {
      retval = g_new (GNetworkBuffer, 1);
      retval->g_class.g_type = GNETWORK_TYPE_BUFFER;
    }

  retval->ref = 1;
  retval->data = NULL;
  retval->length = 0;
  
  return retval;
}


/**
 * gnetwork_buffer_new_with_data:
 * @data: the data to use.
 * @length: the length of @data in bytes.
 * 
 * Creates a new buffer structure copying the values in @data and @length. If
 * @length is less than %0, it is assumed that @data is terminated by a %0
 * value, and the real length will be automatically calculated. The returned
 * reference must be deleted with gnetwork_buffer_unref() when no longer needed.
 * 
 * Returns: a newly-allocated #GNetworkBuffer structure.
 * 
 * Since: 1.0
 **/
GNetworkBuffer *
gnetwork_buffer_new_with_data (const guint8 * data, gssize length)
{
  GNetworkBuffer *retval;

  G_LOCK (buffer_stack);
  retval = g_trash_stack_pop ((GTrashStack **) &buffer_stack);
  G_UNLOCK (buffer_stack);

  if (retval == NULL)
    {
      retval = g_new0 (GNetworkBuffer, 1);
      retval->g_class.g_type = GNETWORK_TYPE_BUFFER;
    }

  retval->ref = 1;

  retval = gnetwork_buffer_new ();

  if (data != NULL)
    {
      if (length < 0)
	for (length = 0; data[length] != '\0'; length++);

      retval->data = g_memdup (data, (guint) length);
      retval->length = (gsize) length;
    }
  else
    {
      retval->data = NULL;
      retval->length = 0;
    }
  
  return retval;
}


/**
 * gnetwork_buffer_get_data:
 * @buffer: the buffer to examine.
 * 
 * Retrieves a pointer to the data contained in @buffer. The returned data must
 * not be modified or freed.
 * 
 * Returns: a constant pointer to the data in @buffer.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN guint8 *
gnetwork_buffer_get_data (GNetworkBuffer * buffer)
{
  g_return_val_if_fail (GNETWORK_IS_BUFFER (buffer), NULL);

  return buffer->data;
}


/**
 * gnetwork_buffer_set_data:
 * @buffer: the buffer to modify.
 * @data: the new data.
 * @length: the length of @data.
 * 
 * Modifies the @data contained in @buffer. 
 * 
 * Since: 1.0
 **/
void
gnetwork_buffer_set_data (GNetworkBuffer * buffer, const guint8 * data, gssize length)
{
  g_return_if_fail (GNETWORK_IS_BUFFER (buffer));
  
  if (data != NULL)
    {
      if (length < 0)
	for (length = 0; data[length] != '\0'; length++);

      buffer->data = g_memdup (data, (guint) length);
      buffer->length = (gsize) length;
    }
  else
    {
      buffer->data = NULL;
      buffer->length = 0;
    }
}


/**
 * gnetwork_buffer_take_data:
 * @buffer: the buffer to modify.
 * @data: the new data.
 * @length: the length of @data.
 * 
 * Overwrites the values in @buffer using @data and @length. Note that @buffer
 * takes ownership of @data, so it should not be modified or freed after calling
 * this function.
 * 
 * Since: 1.0
 **/
void
gnetwork_buffer_take_data (GNetworkBuffer * buffer, guint8 * data, gssize length)
{
  g_return_if_fail (GNETWORK_IS_BUFFER (buffer));
  
  if (data != NULL)
    {
      if (length < 0)
	for (length = 0; data[length] != '\0'; length++);

      buffer->data = data;
      buffer->length = (gsize) length;
    }
  else
    {
      buffer->data = NULL;
      buffer->length = 0;
    }
}


/**
 * gnetwork_buffer_get_length:
 * @buffer: the buffer to examine.
 * 
 * Retrieves the length of the data contained in @buffer.
 * 
 * Returns: the length of the data in @buffer.
 * 
 * Since: 1.0
 **/
gsize
gnetwork_buffer_get_length (GNetworkBuffer * buffer)
{
  g_return_val_if_fail (GNETWORK_IS_BUFFER (buffer), 0);

  return buffer->length;
}


/**
 * gnetwork_buffer_ref:
 * @buffer: the buffer to reference.
 * 
 * Creates a new reference to the data in @buffer. This reference should be
 * deleted with gnetwork_buffer_unref() when no longer needed.
 * 
 * Returns: a new reference to @buffer.
 * 
 * Since: 1.0
 **/
GNetworkBuffer *
gnetwork_buffer_ref (GNetworkBuffer * buffer)
{
  g_return_val_if_fail (buffer == NULL || GNETWORK_IS_BUFFER (buffer), NULL);

  if (buffer != NULL)
    buffer->ref++;
  
  return buffer;
}


/**
 * gnetwork_buffer_unref:
 * @buffer: the buffer reference to delete.
 * 
 * Deletes a reference to the data in @buffer. When all references have been
 * deleted, the data will be freed.
 * 
 * Since: 1.0
 **/
void
gnetwork_buffer_unref (GNetworkBuffer * buffer)
{
  g_return_if_fail (buffer == NULL || GNETWORK_IS_BUFFER (buffer));

  buffer->ref--;

  if (buffer->ref == 0)
    {
      g_free (buffer->data);

      G_LOCK (buffer_stack);

      if (g_trash_stack_height ((GTrashStack **) &buffer_stack) > TRASH_STACK_SIZE)
	g_free (buffer);
      else
	g_trash_stack_push ((GTrashStack **) &buffer_stack, buffer);

      G_UNLOCK (buffer_stack);
    }
}
