/*
 * GNetwork Library: libgnetwork/gnetwork-tcp-connection.c
 *
 * Copyright (C) 2001-2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-tcp-connection.h"

#include "gnetwork-connection.h"
#include "gnetwork-ssl-private.h"

#include "gnetwork-type-builtins.h"

#include "marshal.h"
#include "gnetwork-tcp-proxy-private.h"
#include "gnetwork-dns.h"
#include "gnetwork-interfaces.h"
#include "gnetwork-ip-address.h"
#include "gnetwork-threads.h"
#include "gnetwork-utils.h"

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <glib/gi18n.h>


enum
{
  PROP_0,

  /* Object Properties */
  TCP_STATUS,
  PROXY_TYPE,
  ADDRESS,
  PORT,
  LOCAL_ADDRESS,
  LOCAL_PORT,
  IP_ADDRESS,

  /* SSL Properties */
  SSL_ENABLED,
  SSL_AUTH_TYPE,
  SSL_CA_FILE,
  SSL_CERT_FILE,
  SSL_KEY_FILE,

  /* Shortcut for Servers */
  SOCKET_FD,

  /* GNetworkConnection Properties */
  CXN_TYPE,
  CXN_STATUS,
  CXN_BYTES_RECEIVED,
  CXN_BYTES_SENT,
  CXN_BUFFER_SIZE
};

enum
{
  CERTIFICATE_ERROR,
  LAST_SIGNAL
};


struct _GNetworkTcpConnectionPrivate
{
  /* Properties */
  /* GNetworkTcpConnection */
  gchar *address;
  GNetworkIpAddress ip_address;
  guint16 port;

  GNetworkIpAddress local_address;
  guint16 local_port;

  /* SSL Properties */
  gchar *ca_file;
  gchar *cert_file;
  gchar *key_file;

  /* GNetworkConnectionIface */
  guint buffer_size;
  gulong bytes_received;
  gulong bytes_sent;

  /* DNS Lookups In Progress */
  GNetworkDnsHandle dns_handle;
  GNetworkDnsHandle proxy_dns_handle;

  /* Proxy DNS Return */
  GNetworkIpAddress proxy_ip_address;

  /* The socket itself :-) */
  GIOChannel *channel;
  gint sockfd;

  GSList *buffer;
  guint source_id;
  GIOCondition source_cond:6;

  /* Property Bits */
  /* GNetworkTcpConnection */
  GNetworkTcpConnectionStatus tcp_status:3;
  GNetworkTcpProxyType tcp_proxy_type:3;

  /* GNetworkSslIface */
  GNetworkSslAuthType auth_type:2;
  gboolean ssl_enabled:1;

  /* GNetworkConnectionIface */
  GNetworkConnectionType cxn_type:2;
  GNetworkConnectionStatus cxn_status:3;
};


typedef struct
{
  gpointer data;
  gulong length;
}
BufferItem;


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer parent_class = NULL;
static gint signals[LAST_SIGNAL] = { 0 };


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static GError *
get_connection_error_from_errno (gint en, const gchar * address)
{
  GError *error = NULL;

  switch (en)
    {
    case EINPROGRESS:
      g_assert_not_reached ();
      break;

    case ECONNREFUSED:
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_REFUSED,
			   _("The connection to %s could not be completed because the server "
			     "refused to allow it."), address);
      break;

    case ETIMEDOUT:
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_TIMEOUT,
			   _("The connection to %s took too long to complete. The server may be "
			     "down, your network connection may be down, or your network "
			     "connection may be improperly configured."), address);
      break;

    case ENETUNREACH:
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_UNREACHABLE,
			   _("The network that %s is on could not be reached. Your network "
			     "connection may be down or improperly configured."), address);
      break;

    case EACCES:
    case EPERM:
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_PERMISSIONS,
			   _("You cannot connect to %s, because your computer or firewall is "
			     "configured to prevent it."), address);
      break;

    default:
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
			   _("The connection to %s could not be completed because an  error "
			     "occurred inside the GNetwork library."), address);
      break;
    }

  return error;
}


/* ********************** *
 *  Connection Functions  *
 * ********************** */

static void
gnetwork_tcp_connection_close (GNetworkTcpConnection * connection)
{
  GNetworkTcpConnectionStatus status;
  GObject *object;

  g_return_if_fail (GNETWORK_IS_TCP_CONNECTION (connection));

  if (connection->_priv->tcp_status == GNETWORK_TCP_CONNECTION_CLOSING ||
      connection->_priv->tcp_status == GNETWORK_TCP_CONNECTION_CLOSED)
    return;

  object = G_OBJECT (connection);

  /* Save the old status */
  status = connection->_priv->tcp_status;

  connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_CLOSING;
  connection->_priv->cxn_status = GNETWORK_CONNECTION_CLOSING;

  g_object_freeze_notify (object);
  g_object_notify (object, "tcp-status");
  g_object_notify (object, "status");
  g_object_thaw_notify (object);

  if (connection->_priv->dns_handle != GNETWORK_DNS_HANDLE_INVALID)
    {
      gnetwork_dns_cancel (connection->_priv->dns_handle);
      connection->_priv->dns_handle = GNETWORK_DNS_HANDLE_INVALID;
    }

  if (connection->_priv->proxy_dns_handle != GNETWORK_DNS_HANDLE_INVALID)
    {
      gnetwork_dns_cancel (connection->_priv->proxy_dns_handle);
      connection->_priv->proxy_dns_handle = GNETWORK_DNS_HANDLE_INVALID;
    }

  if (connection->_priv->source_id != 0)
    {
      gnetwork_thread_source_remove (connection->_priv->source_id);
      connection->_priv->source_id = 0;
      connection->_priv->source_cond = 0;
    }

  if (connection->_priv->channel != NULL)
    {
      g_io_channel_shutdown (connection->_priv->channel, FALSE, NULL);
      g_io_channel_unref (connection->_priv->channel);
      connection->_priv->channel = NULL;
    }
  else if (connection->_priv->sockfd > 0)
    {
      shutdown (connection->_priv->sockfd, SHUT_RDWR);
      close (connection->_priv->sockfd);
    }

  connection->_priv->sockfd = -1;

  /* Clean out any buffered writes */
  while (connection->_priv->buffer != NULL)
    {
      BufferItem *item = connection->_priv->buffer->data;

      g_free (item->data);
      g_free (item);

      connection->_priv->buffer = g_slist_remove_link (connection->_priv->buffer,
						       connection->_priv->buffer);
    }

  connection->_priv->cxn_status = GNETWORK_CONNECTION_CLOSED;
  connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_CLOSED;

  g_object_freeze_notify (object);
  g_object_notify (object, "status");
  g_object_notify (object, "tcp-status");
  g_object_notify (object, "socket");
  g_object_thaw_notify (object);
}


static gboolean
io_channel_handler (GIOChannel * channel, GIOCondition cond, GNetworkTcpConnection * connection)
{
  gboolean retval;

  if (connection->_priv->tcp_status <= GNETWORK_TCP_CONNECTION_CLOSED)
    return FALSE;

  /* Error and EOF */
  if (cond & (G_IO_ERR | G_IO_HUP))
    {
      gnetwork_tcp_connection_close (connection);
      return FALSE;
    }

  retval = FALSE;

  /* Read */
  if (cond & (G_IO_IN | G_IO_PRI))
    {
      GIOStatus status;
      guchar buffer[connection->_priv->buffer_size + 1];
      gsize bytes_read;
      GError *error = NULL;

      memset (buffer, 0, G_N_ELEMENTS (buffer));

      status = g_io_channel_read_chars (channel, buffer, connection->_priv->buffer_size,
					&bytes_read, &error);

      switch (status)
	{
	case G_IO_STATUS_AGAIN:
	  retval = TRUE;
	  break;

	case G_IO_STATUS_NORMAL:
	  if (bytes_read > 0)
	    {
	      connection->_priv->bytes_received += bytes_read;
	      g_object_notify (G_OBJECT (connection), "bytes-received");

	      /* Make sure the read buffer is 0-terminated. */
	      buffer[bytes_read] = 0;
	      gnetwork_connection_received (GNETWORK_CONNECTION (connection), buffer, bytes_read);
	    }
	  retval = TRUE;
	  break;

	case G_IO_STATUS_ERROR:
	  gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
	  g_error_free (error);
	  /* Fall through to close the connection */

	case G_IO_STATUS_EOF:
	  if (connection->_priv->cxn_status == GNETWORK_CONNECTION_OPEN)
	    gnetwork_tcp_connection_close (connection);
	  break;

	default:
	  g_assert_not_reached ();
	  break;
	}
    }

  /* Write */
  if (cond & G_IO_OUT)
    {
      if (connection->_priv->buffer != NULL)
	{
	  GIOStatus status;
	  gsize bytes_sent;
	  BufferItem *item;
	  GError *error = NULL;

      g_message ("Writing data");

	  item = connection->_priv->buffer->data;

	  status = g_io_channel_write_chars (channel, item->data, item->length, &bytes_sent,
					     &error);

	  switch (status)
	    {
	      /* Stop immediately & come back later. */
	    case G_IO_STATUS_AGAIN:
	      return TRUE;
	      break;

	    case G_IO_STATUS_NORMAL:
	      if (bytes_sent > 0)
		{
		  connection->_priv->bytes_sent += bytes_sent;
		  g_object_notify (G_OBJECT (connection), "bytes-sent");

		  gnetwork_connection_sent (GNETWORK_CONNECTION (connection), item->data,
					    bytes_sent);

		  /* Partial write, put the unsent data back on the top of the queue. */
		  if (bytes_sent < item->length)
		    {
		      BufferItem *new_item = g_new0 (BufferItem, 1);

		      new_item->data = g_new (guchar, item->length - bytes_sent + 1);
		      memcpy (new_item->data, G_UCHAR (item->data) + bytes_sent, new_item->length);
		      new_item->length = item->length - bytes_sent;
		      connection->_priv->buffer->data = new_item;
		    }
		  else
		    {
		      connection->_priv->buffer = g_slist_remove_link (connection->_priv->buffer,
								       connection->_priv->buffer);
		    }
		}
	      retval = TRUE;
	      break;

	    case G_IO_STATUS_ERROR:
	      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
	      g_error_free (error);
	      /* Fall through to close the connection. */

	    case G_IO_STATUS_EOF:
	      if (connection->_priv->tcp_status == GNETWORK_TCP_CONNECTION_OPEN)
		gnetwork_tcp_connection_close (connection);
	      break;

	    default:
	      g_assert_not_reached ();
	      break;
	    }

	  /* Free the data */
	  g_free (item->data);
	  g_free (item);
	}

      /* We've got nothing else to send, so stop listening for G_IO_OUT. */
      if (connection->_priv->buffer == NULL)
	{
	  gnetwork_thread_source_remove (connection->_priv->source_id);
	  connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);
	  connection->_priv->source_id =
	    gnetwork_thread_io_add_watch (connection->_priv->channel,
					  connection->_priv->source_cond,
					  (GIOFunc) io_channel_handler, connection);
	  retval = FALSE;
	}
    }

  return retval;
}


#ifdef _USE_SSL

static void
open_ssl_connection (GNetworkTcpConnection * connection)
{
  GIOChannel *channel;
  GNetworkSslAuth *auth;
  GError *error;

  if (connection->_priv->tcp_status <= GNETWORK_TCP_CONNECTION_CLOSED)
    return;

  error = NULL;

  connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_AUTHENTICATING;
  g_object_notify (G_OBJECT (connection), "tcp-status");

  if (connection->_priv->tcp_status <= GNETWORK_TCP_CONNECTION_CLOSED)
    return;

  auth = _gnetwork_ssl_auth_new (connection->_priv->auth_type, connection->_priv->cxn_type);

  if (connection->_priv->auth_type == GNETWORK_SSL_AUTH_CERTIFICATE)
    {
      _gnetwork_ssl_auth_set_server_hostname (auth, connection->_priv->address);

      if (connection->_priv->ca_file != NULL)
	_gnetwork_ssl_auth_set_authority_file (auth, connection->_priv->ca_file);

      if (connection->_priv->cert_file != NULL && connection->_priv->key_file != NULL)
	_gnetwork_ssl_auth_set_certs_and_keys_files (auth, connection->_priv->cert_file,
						     connection->_priv->key_file);
    }

  channel = _gnetwork_io_channel_ssl_new (connection->_priv->channel, auth, &error);
  _gnetwork_ssl_auth_unref (auth);

  g_io_channel_unref (connection->_priv->channel);
  connection->_priv->channel = channel;

  if (error != NULL)
    {
      if (error->domain == GNETWORK_SSL_CERT_ERROR)
	{
	  g_signal_emit (connection, signals[CERTIFICATE_ERROR], 0,
			 GNETWORK_SSL_CERT (error->message), error->code);
	  gnetwork_ssl_cert_free (GNETWORK_SSL_CERT (error->message));
	  error->message = NULL;
	}
      else
	{
	  gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
	}
      g_error_free (error);

      if (connection->_priv->tcp_status <= GNETWORK_TCP_CONNECTION_CLOSED)
	return;
    }

  connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);
  connection->_priv->source_id =
    gnetwork_thread_io_add_watch (connection->_priv->channel, connection->_priv->source_cond,
				  (GIOFunc) io_channel_handler, connection);

  connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_OPEN;
  connection->_priv->cxn_status = GNETWORK_CONNECTION_OPEN;

  g_object_freeze_notify (G_OBJECT (connection));
  g_object_notify (G_OBJECT (connection), "tcp-status");
  g_object_notify (G_OBJECT (connection), "status");
  g_object_thaw_notify (G_OBJECT (connection));
}

#endif /* _USE_SSL */


static void
proxy_done_cb (GIOChannel * channel, const GError * error, GNetworkTcpConnection * connection)
{
  if (connection->_priv->tcp_status <= GNETWORK_TCP_CONNECTION_CLOSED)
    return;

  g_io_channel_unref (connection->_priv->channel);
  g_io_channel_ref (channel);
  connection->_priv->channel = channel;

  if (error != NULL)
    {
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);

      /* If we're still open, close anyway :-) */
      if (connection->_priv->tcp_status == GNETWORK_TCP_CONNECTION_OPEN)
	gnetwork_tcp_connection_close (connection);
      return;
    }

  /* We still need SSL. */
#ifdef _USE_SSL
  if (connection->_priv->ssl_enabled)
    {
      open_ssl_connection (connection);
    }
  else
#endif /* _USE_SSL */
    {
      connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);
      connection->_priv->source_id =
	gnetwork_thread_io_add_watch (connection->_priv->channel, connection->_priv->source_cond,
				      (GIOFunc) io_channel_handler, connection);

      connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_OPEN;
      connection->_priv->cxn_status = GNETWORK_CONNECTION_OPEN;

      g_object_freeze_notify (G_OBJECT (connection));
      g_object_notify (G_OBJECT (connection), "tcp-status");
      g_object_notify (G_OBJECT (connection), "status");
      g_object_thaw_notify (G_OBJECT (connection));
    }
}


static void
open_proxy_connection (GNetworkTcpConnection * connection)
{
  GNetworkDnsEntry *entry;

  connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_PROXYING;
  g_object_notify (G_OBJECT (connection), "tcp-status");

  entry = gnetwork_dns_entry_new (connection->_priv->address, &(connection->_priv->ip_address));
  _gnetwork_io_channel_proxy_new (connection->_priv->channel, connection->_priv->tcp_proxy_type,
				  entry, connection->_priv->port,
				  (GNetworkIOCallback) proxy_done_cb, g_object_ref (connection),
				  g_object_unref);
  gnetwork_dns_entry_free (entry);
}


static void
connect_suceeded (GNetworkTcpConnection * connection)
{
  struct sockaddr *sa;
  socklen_t sin_size;

  sin_size = MAX (sizeof (struct sockaddr_in), sizeof (struct sockaddr_in6));
  sa = g_malloc0 (sin_size);
  getsockname (connection->_priv->sockfd, sa, &sin_size);

  _gnetwork_ip_address_set_from_sockaddr (&(connection->_priv->local_address), sa);
  connection->_priv->local_port = _gnetwork_sockaddr_get_port (sa);
  g_free (sa);

  g_object_freeze_notify (G_OBJECT (connection));
  g_object_notify (G_OBJECT (connection), "local-address");
  g_object_notify (G_OBJECT (connection), "local-port");
  g_object_thaw_notify (G_OBJECT (connection));

  /* We need a proxy. */
  if (gnetwork_ip_address_is_address (&(connection->_priv->proxy_ip_address)))
    {
      open_proxy_connection (connection);
    }
#ifdef _USE_SSL
  else if (connection->_priv->ssl_enabled)
    {
      open_ssl_connection (connection);
    }
#endif /* _USE_SSL */
  /* We're good to go. */
  else
    {
      connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_OPEN;
      connection->_priv->cxn_status = GNETWORK_CONNECTION_OPEN;

      g_object_freeze_notify (G_OBJECT (connection));
      g_object_notify (G_OBJECT (connection), "tcp-status");
      g_object_notify (G_OBJECT (connection), "status");
      g_object_thaw_notify (G_OBJECT (connection));

      connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);
      connection->_priv->source_id =
	gnetwork_thread_io_add_watch (connection->_priv->channel, connection->_priv->source_cond,
				      (GIOFunc) io_channel_handler, connection);
    }
}


/* connect() Is Done */
static gboolean
connect_done_handler (GIOChannel * channel, GIOCondition cond, GNetworkTcpConnection * connection)
{
  GError *error;
  gint result, error_val, length;

  /* Remove this here so we don't get called for proxy/SSL stuff */
  gnetwork_thread_source_remove (connection->_priv->source_id);
  connection->_priv->source_id = 0;
  connection->_priv->source_cond = 0;

  errno = 0;
  error_val = 0;
  length = 0;
  result = getsockopt (connection->_priv->sockfd, SOL_SOCKET, SO_ERROR, &error_val, &length);

  /* Couldn't get socket options */
  if (result != 0)
    {
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
			   _("The connection to %s could not be completed because an error "
			     "occurred inside the GNetwork library."), connection->_priv->address);
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
	gnetwork_tcp_connection_close (connection);
    }
  else if (error_val != 0)
    {
      error = get_connection_error_from_errno (error_val, connection->_priv->address);
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
	gnetwork_tcp_connection_close (connection);
    }
  else
    {
      connect_suceeded (connection);
    }

  return FALSE;
}


/* **************************** *
 *  Basic Connection Functions  *
 * **************************** */

static void
open_client_connection (GNetworkTcpConnection * connection)
{
  GError *error;
  struct sockaddr *sa;
  gint flags, result;
  socklen_t sa_size;
  GObject *object = G_OBJECT (connection);

  connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_OPENING;
  connection->_priv->cxn_status = GNETWORK_CONNECTION_OPENING;

  g_object_freeze_notify (object);
  g_object_notify (object, "tcp-status");
  g_object_notify (object, "status");
  g_object_thaw_notify (object);

  /* Create the socket */
  errno = 0;
  connection->_priv->sockfd = socket (AF_INET6, SOCK_STREAM, 0);
  if (connection->_priv->sockfd < 0 && errno == EAFNOSUPPORT)
    {
      errno = 0;
      connection->_priv->sockfd = socket (AF_INET, SOCK_STREAM, 0);
    }
  g_object_notify (object, "socket");

  if (connection->_priv->sockfd < 0)
    {
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
			   _("The connection to %s could not be completed because an error "
			     "occurred inside the GNetwork library."), connection->_priv->address);
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
	gnetwork_tcp_connection_close (connection);
      return;
    }


  /* Retrieve the current socket flags */
  flags = fcntl (connection->_priv->sockfd, F_GETFL, 0);
  if (flags == -1)
    {
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
			   _("The connection to %s could not be completed because an error "
			     "occurred inside the GNetwork library."), connection->_priv->address);
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
	gnetwork_tcp_connection_close (connection);
      return;
    }

  /* Add the non-blocking flag */
  if (fcntl (connection->_priv->sockfd, F_SETFL, flags | O_NONBLOCK) == -1)
    {
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
			   _("The connection to %s could not be completed because an error "
			     "occurred inside the GNetwork library."), connection->_priv->address);
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
	gnetwork_tcp_connection_close (connection);
      return;
    }

  sa_size = 0;
  /* Connection Stuff */
  if (gnetwork_ip_address_is_address (&(connection->_priv->proxy_ip_address)))
    {
      sa = _gnetwork_ip_address_to_sockaddr (&(connection->_priv->proxy_ip_address),
					     _gnetwork_tcp_proxy_get_port (connection->_priv->
									   tcp_proxy_type),
					     &sa_size);
    }
  else
    {
      sa = _gnetwork_ip_address_to_sockaddr (&(connection->_priv->ip_address),
					     connection->_priv->port, &sa_size);
    }

  errno = 0;
  result = connect (connection->_priv->sockfd, sa, sa_size);
  g_free (sa);

  if (result != 0)
    {
      if (errno == EINPROGRESS)
	{
	  connection->_priv->channel = g_io_channel_unix_new (connection->_priv->sockfd);
	  g_io_channel_set_encoding (connection->_priv->channel, NULL, NULL);
	  g_io_channel_set_buffered (connection->_priv->channel, FALSE);

	  connection->_priv->source_cond = GNETWORK_IO_ANY;
	  connection->_priv->source_id =
	    gnetwork_thread_io_add_watch (connection->_priv->channel,
					  connection->_priv->source_cond,
					  (GIOFunc) connect_done_handler, connection);
	  return;
	}
      else if (gnetwork_ip_address_is_address (&(connection->_priv->proxy_ip_address)))
	{
	  GNetworkTcpProxyError error_type;
	  GNetworkDnsEntry *entry;

	  error_type = _gnetwork_tcp_proxy_error_from_errno (errno);
	  entry = gnetwork_dns_entry_new (connection->_priv->address,
					  &(connection->_priv->ip_address));

	  error = g_error_new_literal (GNETWORK_TCP_PROXY_ERROR, error_type, NULL);
	  error->message = _gnetwork_tcp_proxy_strerror (error_type,
							 connection->_priv->tcp_proxy_type, entry);
	  gnetwork_dns_entry_free (entry);
	}
      else
	{
	  error = get_connection_error_from_errno (errno, connection->_priv->address);
	}

      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
	gnetwork_tcp_connection_close (connection);
    }
  else
    {
      connection->_priv->channel = g_io_channel_unix_new (connection->_priv->sockfd);
      g_io_channel_set_encoding (connection->_priv->channel, NULL, NULL);
      g_io_channel_set_buffered (connection->_priv->channel, FALSE);

      connect_suceeded (connection);
    }
}


/* *************** *
 *  DNS Callbacks  *
 * *************** */

static void
dns_callback (const GSList * entries, GError * error, GNetworkTcpConnection * connection)
{
  /* We're done with the handle */
  connection->_priv->dns_handle = GNETWORK_DNS_HANDLE_INVALID;

  /* If we're still interested in connecting :-) */
  if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
    {
      /* Lookup Failed */
      if (error != NULL)
	{
	  gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);

	  if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
	    gnetwork_tcp_connection_close (connection);
	}
      else if (entries != NULL)
	{
	  memcpy (&(connection->_priv->ip_address),
		  gnetwork_dns_entry_get_ip_address (entries->data), sizeof (GNetworkIpAddress));
	  g_object_notify (G_OBJECT (connection), "ip-address");

	  /* Lookup succeeded, we need a proxy, and we've already gotten the proxy return... */
	  if (gnetwork_tcp_proxy_get_use_proxy (connection->_priv->tcp_proxy_type,
						connection->_priv->address))
	    {
	      if (gnetwork_ip_address_is_address (&(connection->_priv->proxy_ip_address)))
		{
		  open_client_connection (connection);
		}
	    }
	  /* Lookup suceeded, and we don't need a proxy */
	  else
	    {
	      open_client_connection (connection);
	    }
	}
      else
	{
	  g_assert_not_reached ();
	}
    }
}


static void
proxy_dns_callback (const GSList * entries, GError * error, GNetworkTcpConnection * connection)
{
  /* We're done with the proxy lookup */
  connection->_priv->proxy_dns_handle = GNETWORK_DNS_HANDLE_INVALID;

  /* If we're still interested in connecting :-) */
  if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
    {
      if (entries != NULL)
	{
	  memcpy (&(connection->_priv->proxy_ip_address),
		  gnetwork_dns_entry_get_ip_address (entries->data), sizeof (GNetworkIpAddress));

	  if (gnetwork_ip_address_is_address (&(connection->_priv->ip_address)))
	    {
	      open_client_connection (connection);
	    }
	}
      /* Lookup Failed */
      else if (error != NULL)
	{
	  gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);

	  if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
	    gnetwork_tcp_connection_close (connection);
	}
      /* Otherwise, it suceeded, and if the (real) destination lookup is done
         as well, start the connection */
      else
	{
	  g_assert_not_reached ();
	}
    }
}


/* *********************************** *
 *  GNetworkConnectionIface Functions  *
 * *********************************** */

static void
gnetwork_tcp_connection_open (GNetworkTcpConnection * connection)
{
  GObject *object;

  g_return_if_fail (GNETWORK_IS_TCP_CONNECTION (connection));
  g_return_if_fail (connection->_priv->cxn_status == GNETWORK_CONNECTION_CLOSED);

  object = G_OBJECT (connection);

  connection->_priv->bytes_received = 0;
  connection->_priv->bytes_sent = 0;
  connection->_priv->cxn_status = GNETWORK_CONNECTION_OPENING;
  connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_LOOKUP;

  g_object_freeze_notify (object);
  g_object_notify (object, "bytes-received");
  g_object_notify (object, "bytes-sent");
  g_object_notify (object, "tcp-status");
  g_object_notify (object, "status");
  g_object_thaw_notify (object);

  /* If the app called gnetwork_connection_close() while in ::notify above, the tcp status would've
     changed, and we can bail */
  if (connection->_priv->tcp_status != GNETWORK_TCP_CONNECTION_LOOKUP)
    return;

  connection->_priv->proxy_dns_handle = GNETWORK_DNS_HANDLE_INVALID;
  connection->_priv->dns_handle = GNETWORK_DNS_HANDLE_INVALID;

  if (connection->_priv->cxn_type == GNETWORK_CONNECTION_CLIENT)
    {
      /* If we need to use a proxy... */
      if (gnetwork_tcp_proxy_get_use_proxy (connection->_priv->tcp_proxy_type,
					    connection->_priv->address))
	{
	  gchar *proxy_host;
	  GNetworkIpAddress ip_address = GNETWORK_IP_ADDRESS_INIT;

	  proxy_host = _gnetwork_tcp_proxy_get_host (connection->_priv->tcp_proxy_type);

	  if (gnetwork_ip_address_set_from_string (&ip_address, proxy_host))
	    {
	      GSList *list;

	      list = g_slist_prepend (NULL, gnetwork_dns_entry_new (proxy_host, &ip_address));

	      proxy_dns_callback (list, NULL, connection);
	      gnetwork_dns_entry_free (list->data);
	      g_slist_free (list);
	    }
	  /* Otherwise, start the lookup */
	  else
	    {
	      connection->_priv->proxy_dns_handle =
		gnetwork_dns_get (proxy_host, (GNetworkDnsCallbackFunc) proxy_dns_callback,
				  g_object_ref (connection), g_object_unref);
	    }

	  g_free (proxy_host);
	}

      /* If the (real) destination address is already an IP address, just fake a
         return and call the callback */
      if (gnetwork_ip_address_is_address (&(connection->_priv->ip_address)))
	{
	  GSList *list;

	  list = g_slist_prepend (NULL,
				  gnetwork_dns_entry_new (connection->_priv->address,
							  &(connection->_priv->ip_address)));

	  dns_callback (list, NULL, connection);
	  gnetwork_dns_entry_free (list->data);
	  g_slist_free (list);
	}
      /* Othwerise, start a lookup for that too */
      else
	{
	  connection->_priv->dns_handle =
	    gnetwork_dns_get (connection->_priv->address, (GNetworkDnsCallbackFunc) dns_callback,
			      g_object_ref (connection), g_object_unref);
	}
    }
  else
    {
      if (connection->_priv->sockfd < 0)
	{
	  g_warning ("%s: You cannot open a server connection without first setting the socket "
		     "property on the connection to the accepted socket.", G_STRLOC);
	  return;
	}

      connection->_priv->channel = g_io_channel_unix_new (connection->_priv->sockfd);
      g_io_channel_set_encoding (connection->_priv->channel, NULL, NULL);
      g_io_channel_set_buffered (connection->_priv->channel, FALSE);

#ifdef _USE_SSL
      if (connection->_priv->ssl_enabled)
	{
	  open_ssl_connection (connection);
	}
      else
#endif /* _USE_SSL */
	{
	  connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);
	  connection->_priv->source_id =
	    gnetwork_thread_io_add_watch (connection->_priv->channel,
					  connection->_priv->source_cond,
					  (GIOFunc) io_channel_handler, connection);

	  connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_OPEN;
	  connection->_priv->cxn_status = GNETWORK_CONNECTION_OPEN;

	  g_object_freeze_notify (object);
	  g_object_notify (object, "tcp-status");
	  g_object_notify (object, "status");
	  g_object_thaw_notify (object);
	}
    }
}


/* gnetwork_tcp_connection_close() is at the start of the sources */


static void
gnetwork_tcp_connection_send (GNetworkTcpConnection * connection, gconstpointer data, gulong length)
{
  BufferItem *item;

  g_return_if_fail (GNETWORK_IS_TCP_CONNECTION (connection));
  g_return_if_fail (connection->_priv->tcp_status == GNETWORK_TCP_CONNECTION_OPEN);

  item = g_new (BufferItem, 1);
  item->data = g_malloc (length + 1);
  G_UCHAR (item->data)[length] = 0;
  memcpy (item->data, data, length);
  item->length = length;

  connection->_priv->buffer = g_slist_append (connection->_priv->buffer, item);

  /* Watch for writability if we're not already */
  if (!(connection->_priv->source_cond & G_IO_OUT))
    {
      if (connection->_priv->source_id != 0)
	gnetwork_thread_source_remove (connection->_priv->source_id);

      connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_OUT | G_IO_ERR | G_IO_HUP);
      connection->_priv->source_id =
	gnetwork_thread_io_add_watch (connection->_priv->channel, connection->_priv->source_cond,
				      (GIOFunc) io_channel_handler, connection);
    }
}


static void
gnetwork_tcp_connection_connection_iface_init (GNetworkConnectionIface * iface)
{
  iface->open = (GNetworkConnectionFunc) gnetwork_tcp_connection_open;
  iface->close = (GNetworkConnectionFunc) gnetwork_tcp_connection_close;
  iface->send = (GNetworkConnectionSendFunc) gnetwork_tcp_connection_send;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_tcp_connection_set_property (GObject * object, guint property, const GValue * value,
				      GParamSpec * param_spec)
{
  GNetworkTcpConnection *connection = GNETWORK_TCP_CONNECTION (object);

  switch (property)
    {
    case ADDRESS:
      {
	const gchar *address = g_value_get_string (value);

	g_return_if_fail (address == NULL || address[0] != '\0');
	g_return_if_fail (connection->_priv->tcp_status == GNETWORK_TCP_CONNECTION_CLOSED ||
			  connection->_priv->cxn_type == GNETWORK_CONNECTION_SERVER);

	g_free (connection->_priv->address);
	connection->_priv->address = g_strdup (address);

	if (connection->_priv->cxn_type == GNETWORK_CONNECTION_CLIENT)
	  {
	    gnetwork_ip_address_set_from_string (&(connection->_priv->ip_address), address);
	    g_object_notify (object, "ip-address");
	  }
      }
      break;
    case PORT:
      g_return_if_fail (connection->_priv->tcp_status < GNETWORK_TCP_CONNECTION_OPENING);

      connection->_priv->port = g_value_get_uint (value);
      break;
    case PROXY_TYPE:
      g_return_if_fail (connection->_priv->tcp_status < GNETWORK_TCP_CONNECTION_PROXYING);

      connection->_priv->tcp_proxy_type = g_value_get_enum (value);
      gnetwork_ip_address_init (&(connection->_priv->proxy_ip_address));
      break;

    case SOCKET_FD:
      g_return_if_fail (connection->_priv->tcp_status == GNETWORK_TCP_CONNECTION_CLOSED);

      connection->_priv->sockfd = GPOINTER_TO_INT (g_value_get_pointer (value));

      if (connection->_priv->sockfd > 0)
	{
	  struct sockaddr *sa;
	  socklen_t sa_size;

	  sa_size = MAX (sizeof (struct sockaddr_in), sizeof (struct sockaddr_in6));
	  sa = g_malloc0 (sa_size);
	  getsockname (connection->_priv->sockfd, sa, &sa_size);

	  _gnetwork_ip_address_set_from_sockaddr (&(connection->_priv->local_address), sa);
	  connection->_priv->local_port = _gnetwork_sockaddr_get_port (sa);
	  g_free (sa);
	}
      else
	{
	  gnetwork_ip_address_init (&(connection->_priv->local_address));
	  connection->_priv->local_port = 0;
	}

      g_object_freeze_notify (object);
      g_object_notify (object, "local-address");
      g_object_notify (object, "local-port");
      g_object_thaw_notify (object);
      break;

    case CXN_TYPE:
      connection->_priv->cxn_type = g_value_get_enum (value);
      break;
    case CXN_BUFFER_SIZE:
      g_return_if_fail (connection->_priv->tcp_status < GNETWORK_TCP_CONNECTION_OPENING);
      connection->_priv->buffer_size = g_value_get_uint (value);
      break;

#ifdef _USE_SSL
    case SSL_ENABLED:
      g_return_if_fail (connection->_priv->tcp_status < GNETWORK_TCP_CONNECTION_AUTHENTICATING);

      connection->_priv->ssl_enabled = g_value_get_boolean (value);
      break;
    case SSL_AUTH_TYPE:
      g_return_if_fail (connection->_priv->tcp_status < GNETWORK_TCP_CONNECTION_AUTHENTICATING);

      connection->_priv->auth_type = g_value_get_enum (value);
      break;
    case SSL_CA_FILE:
      g_return_if_fail (connection->_priv->tcp_status < GNETWORK_TCP_CONNECTION_AUTHENTICATING);

      g_free (connection->_priv->ca_file);
      connection->_priv->ca_file = g_value_dup_string (value);
      break;
    case SSL_CERT_FILE:
      g_return_if_fail (connection->_priv->tcp_status < GNETWORK_TCP_CONNECTION_AUTHENTICATING);

      g_free (connection->_priv->cert_file);
      connection->_priv->cert_file = g_value_dup_string (value);
      break;
    case SSL_KEY_FILE:
      g_return_if_fail (connection->_priv->tcp_status < GNETWORK_TCP_CONNECTION_AUTHENTICATING);

      g_free (connection->_priv->key_file);
      connection->_priv->key_file = g_value_dup_string (value);
      break;
#else /* !_USE_SSL */
    case SSL_ENABLED:
    case SSL_AUTH_TYPE:
    case SSL_CA_FILE:
    case SSL_CERT_FILE:
    case SSL_KEY_FILE:
      g_warning (G_STRLOC ": SSL properties cannot be set because this version of the GNetwork "
		 "library was compiled without SSL support.");
      break;
#endif /* _USE_SSL */

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_tcp_connection_get_property (GObject * object, guint property, GValue * value,
				      GParamSpec * param_spec)
{
  GNetworkTcpConnection *connection = GNETWORK_TCP_CONNECTION (object);

  switch (property)
    {
      /* GNetworkTcpConnection */
    case TCP_STATUS:
      g_value_set_enum (value, connection->_priv->tcp_status);
      break;
    case ADDRESS:
      g_value_set_string (value, connection->_priv->address);
      break;
    case PORT:
      g_value_set_uint (value, connection->_priv->port);
      break;
    case LOCAL_ADDRESS:
      g_value_set_boxed (value, &(connection->_priv->local_address));
      break;
    case LOCAL_PORT:
      g_value_set_uint (value, connection->_priv->local_port);
      break;
    case IP_ADDRESS:
      g_value_set_boxed (value, &(connection->_priv->ip_address));
      break;
    case SOCKET_FD:
      g_value_set_pointer (value, GINT_TO_POINTER (connection->_priv->sockfd));
      break;
      /* SSL */
    case SSL_ENABLED:
      g_value_set_boolean (value, connection->_priv->ssl_enabled);
      break;
    case SSL_AUTH_TYPE:
      g_value_set_enum (value, connection->_priv->auth_type);
      break;
    case SSL_CA_FILE:
      g_value_set_string (value, connection->_priv->ca_file);
      break;
    case SSL_CERT_FILE:
      g_value_set_string (value, connection->_priv->cert_file);
      break;
    case SSL_KEY_FILE:
      g_value_set_string (value, connection->_priv->key_file);
      break;

      /* GNetworkConnectionIface */
    case CXN_TYPE:
      g_value_set_enum (value, connection->_priv->cxn_type);
      break;
    case CXN_STATUS:
      g_value_set_enum (value, connection->_priv->cxn_status);
      break;
    case CXN_BUFFER_SIZE:
      g_value_set_uint (value, connection->_priv->buffer_size);
      break;
    case CXN_BYTES_RECEIVED:
      g_value_set_ulong (value, connection->_priv->bytes_received);
      break;
    case CXN_BYTES_SENT:
      g_value_set_ulong (value, connection->_priv->bytes_sent);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_tcp_connection_dispose (GObject * object)
{
  GNetworkTcpConnection *connection = GNETWORK_TCP_CONNECTION (object);

  if (connection->_priv->tcp_status > GNETWORK_TCP_CONNECTION_CLOSED)
    gnetwork_tcp_connection_close (connection);

  if (G_OBJECT_CLASS (parent_class)->dispose)
    (*G_OBJECT_CLASS (parent_class)->dispose) (object);
}


static void
gnetwork_tcp_connection_finalize (GObject * object)
{
  GNetworkTcpConnection *connection = GNETWORK_TCP_CONNECTION (object);

  g_free (connection->_priv->address);

  g_free (connection->_priv->ca_file);
  g_free (connection->_priv->cert_file);
  g_free (connection->_priv->key_file);

  g_free (connection->_priv);

  if (G_OBJECT_CLASS (parent_class)->finalize)
    (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_tcp_connection_base_init (gpointer class)
{
#if 0
#ifdef _USE_SSL
  _gnetwork_ssl_initialize ();
#endif /* _USE_SSL */
#endif /* 0 */
  _gnetwork_tcp_proxy_initialize ();
}


static void
gnetwork_tcp_connection_base_finalize (gpointer class)
{
#if 0
#ifdef _USE_SSL
  _gnetwork_ssl_shutdown ();
#endif /* _USE_SSL */
#endif /* 0 */
  _gnetwork_tcp_proxy_shutdown ();
}


static void
gnetwork_tcp_connection_class_init (GNetworkTcpConnectionClass * class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  parent_class = g_type_class_peek_parent (class);

  object_class->get_property = gnetwork_tcp_connection_get_property;
  object_class->set_property = gnetwork_tcp_connection_set_property;
  object_class->dispose = gnetwork_tcp_connection_dispose;
  object_class->finalize = gnetwork_tcp_connection_finalize;

  g_object_class_install_property (object_class, TCP_STATUS,
				   g_param_spec_enum ("tcp-status", _("TCP/IP Connection Status"),
						      _("The current status of the TCP/IP "
							"connection."),
						      GNETWORK_TYPE_TCP_CONNECTION_STATUS,
						      GNETWORK_TCP_CONNECTION_CLOSED,
						      G_PARAM_READABLE));

  g_object_class_install_property (object_class, PROXY_TYPE,
				   g_param_spec_enum ("proxy-type", _("Proxy Type"),
						      _("The type of proxy to use, depending on "
							"the protocol."),
						      GNETWORK_TYPE_TCP_PROXY_TYPE,
						      GNETWORK_TCP_PROXY_HTTP, G_PARAM_READWRITE));
  g_object_class_install_property (object_class, ADDRESS,
				   g_param_spec_string ("address", _("Address"),
							_("The hostname or IP address to connect "
							  "to."), NULL, G_PARAM_READWRITE));
  g_object_class_install_property (object_class, IP_ADDRESS,
				   g_param_spec_boxed ("ip-address", _("IP Address"),
						       _("The IP address to connect to."),
						       GNETWORK_TYPE_IP_ADDRESS, G_PARAM_READABLE));
  g_object_class_install_property (object_class, PORT,
				   g_param_spec_uint ("port", _("Port Number"),
						      _("The port number to connect to."),
						      0, 65535, 0, G_PARAM_READWRITE));
  g_object_class_install_property (object_class, LOCAL_ADDRESS,
				   g_param_spec_boxed ("local-address", _("Local Address"),
						       _("The IP address of this computer."),
						       GNETWORK_TYPE_IP_ADDRESS, G_PARAM_READABLE));
  g_object_class_install_property (object_class, LOCAL_PORT,
				   g_param_spec_uint ("local-port",
						      _("Local Port Number"),
						      _("The local port number we are connected "
							"through."),
						      0, 65535, 0, G_PARAM_READABLE));
  g_object_class_install_property (object_class, SOCKET_FD,
				   g_param_spec_pointer ("socket", _("Socket File Descriptor"),
							 _("The socket file descriptor."),
							 (G_PARAM_PRIVATE | G_PARAM_READWRITE)));

  /* SSL Properties */
  g_object_class_install_property (object_class, SSL_ENABLED,
				   g_param_spec_boolean ("ssl-enabled", _("SSL Enabled"),
							 _("Whether or not SSL will be used with "
							   "this connection."), FALSE,
							 G_PARAM_READWRITE));
  g_object_class_install_property (object_class, SSL_AUTH_TYPE,
				   g_param_spec_enum ("authentication-type",
						      _("Authentication Type"),
						      _("What type of SSL authentication should be "
							"performed with this connection."),
						      GNETWORK_TYPE_SSL_AUTH_TYPE,
						      GNETWORK_SSL_AUTH_ANONYMOUS,
						      G_PARAM_READWRITE));
  g_object_class_install_property (object_class, SSL_CA_FILE,
				   g_param_spec_string ("authority-file",
							_("Certificate Authorities File"),
							_("The path to a file which contains the "
							  "X.509 certificates of trusted signers."),
							NULL,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class, SSL_CERT_FILE,
				   g_param_spec_string ("certificate-file", _("Certificate File"),
							_("The path to a file which contains X.509 "
							  "certificates to distribute to clients."),
							NULL, G_PARAM_READWRITE));
  g_object_class_install_property (object_class, SSL_KEY_FILE,
				   g_param_spec_string ("key-file", _("Key File"),
							_("The path to a file which contains X.509 "
							  "keys to distribute to clients."), NULL,
							G_PARAM_READWRITE));

  /* GNetworkConnectionIface Properties */
  g_object_class_override_property (object_class, CXN_TYPE, "connection-type");
  g_object_class_override_property (object_class, CXN_STATUS, "status");
  g_object_class_override_property (object_class, CXN_BYTES_RECEIVED, "bytes-sent");
  g_object_class_override_property (object_class, CXN_BYTES_SENT, "bytes-received");
  g_object_class_override_property (object_class, CXN_BUFFER_SIZE, "buffer-size");

  /* Signals */
  signals[CERTIFICATE_ERROR] =
    g_signal_new ("certificate-error",
		  GNETWORK_TYPE_TCP_CONNECTION,
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GNetworkTcpConnectionClass, certificate_error),
		  NULL, NULL, _gnetwork_marshal_VOID__BOXED_FLAGS,
		  G_TYPE_NONE, 2, GNETWORK_TYPE_SSL_CERT, GNETWORK_TYPE_SSL_CERT_ERROR_FLAGS);
}


static void
gnetwork_tcp_connection_instance_init (GNetworkTcpConnection * connection)
{
  connection->_priv = g_new (GNetworkTcpConnectionPrivate, 1);

  connection->_priv->cxn_type = GNETWORK_CONNECTION_CLIENT;
  connection->_priv->cxn_status = GNETWORK_CONNECTION_CLOSED;
  connection->_priv->tcp_status = GNETWORK_TCP_CONNECTION_CLOSED;

  connection->_priv->address = NULL;
  gnetwork_ip_address_init (&(connection->_priv->ip_address));

  connection->_priv->proxy_dns_handle = GNETWORK_DNS_HANDLE_INVALID;
  connection->_priv->dns_handle = GNETWORK_DNS_HANDLE_INVALID;

  gnetwork_ip_address_init (&(connection->_priv->local_address));
  gnetwork_ip_address_init (&(connection->_priv->proxy_ip_address));

  connection->_priv->ca_file = NULL;
  connection->_priv->cert_file = NULL;
  connection->_priv->key_file = NULL;

  connection->_priv->buffer = NULL;

  connection->_priv->channel = NULL;
  connection->_priv->source_id = 0;
  connection->_priv->source_cond = 0;
  connection->_priv->sockfd = -1;
}


/* ************ *
 *  PUBLIC API  *
 * ************ */

GType
gnetwork_tcp_connection_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      static const GTypeInfo info = {
	sizeof (GNetworkTcpConnectionClass),
	(GBaseInitFunc) gnetwork_tcp_connection_base_init,
	(GBaseFinalizeFunc) gnetwork_tcp_connection_base_finalize,
	(GClassInitFunc) gnetwork_tcp_connection_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkTcpConnection),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_tcp_connection_instance_init,
	NULL			/* value table */
      };
      static const GInterfaceInfo cxn_info = {
	(GInterfaceInitFunc) gnetwork_tcp_connection_connection_iface_init,
	NULL, NULL
      };

      type = g_type_register_static (G_TYPE_OBJECT, "GNetworkTcpConnection", &info, 0);

      g_type_add_interface_static (type, GNETWORK_TYPE_CONNECTION, &cxn_info);
    }

  return type;
}
