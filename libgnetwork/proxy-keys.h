/* 
 * GNetwork Library: libgnetwork/proxy-keys.h
 *
 * Copyright (C) 2001-2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __PROXY_KEYS_H__
#define __PROXY_KEYS_H__ 1

#define HTTP_PROXY_DIR			"/system/http_proxy"
#define HTTP_PROXY_USE_PROXY_KEY	"/system/http_proxy/use_http_proxy"
#define HTTP_PROXY_HOST_KEY		"/system/http_proxy/host"
#define HTTP_PROXY_PORT_KEY		"/system/http_proxy/port"
#define HTTP_PROXY_USE_AUTH_KEY		"/system/http_proxy/use_authentication"
#define HTTP_PROXY_AUTH_USER_KEY	"/system/http_proxy/authentication_user"
#define HTTP_PROXY_AUTH_PASS_KEY	"/system/http_proxy/authentication_password"
#define PROXY_EXCEPTIONS_KEY		"/system/http_proxy/ignore_hosts"

#define PROXY_DIR			"/system/proxy"
#define PROXY_MODE_KEY			"/system/proxy/mode"

#define FTP_PROXY_HOST_KEY		"/system/proxy/ftp_host"
#define FTP_PROXY_PORT_KEY		"/system/proxy/ftp_port"

#define HTTPS_PROXY_HOST_KEY		"/system/proxy/secure_host"
#define HTTPS_PROXY_PORT_KEY		"/system/proxy/secure_port"

#define SOCKS_PROXY_HOST_KEY		"/system/proxy/socks_host"
#define SOCKS_PROXY_PORT_KEY		"/system/proxy/socks_port"
#define SOCKS_PROXY_VERSION_KEY		"/system/proxy/socks_version"

#endif /* !__PROXY_KEYS_H__ */
