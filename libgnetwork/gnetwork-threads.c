/* 
 * GNetwork Library: libgnetwork/gnetwork-threads.c
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-threads.h"


typedef struct
{
  GThreadFunc thread_func;
  gpointer data;
  GDestroyNotify notify;
  GMainContext *context;
}
ThreadData;


static GMainContext *
thread_access_context (gboolean set_context, GMainContext * context)
{
  static GStaticPrivate context_key = G_STATIC_PRIVATE_INIT;
  GMainContext *tmp = g_static_private_get (&context_key);

  if (set_context)
    g_static_private_set (&context_key, context, (GDestroyNotify) g_main_context_unref);

  return tmp;
}


static void
thread_function (ThreadData * data, gpointer user_data)
{
  if (data != NULL)
    {
      if (data->context != NULL)
	{
	  gnetwork_thread_set_context (data->context);
	}

      (*(data->thread_func)) (data->data);

      if (data->notify != NULL)
	(*(data->notify)) (data->data);

      if (data->context != NULL)
	{
	  gnetwork_thread_set_context (NULL);
	}

      g_free (data);
    }
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

/**
 * gnetwork_thread_set_context:
 * @context: the main context to set for the current thread.
 * 
 * When a thread you have created will run it's own #GMainLoop, you should
 * associate the new mainloop's @context with the thread via this function.
 *
 * Alternatively, you can avoid using this function if you create your @context
 * first, and then create your thread with gnetwork_thread_new(). The
 * setting/unsetting of the context will be handled automatically.
 *
 * Since: 1.0
 **/
void
gnetwork_thread_set_context (GMainContext * context)
{
  thread_access_context (TRUE, context);
}


/**
 * gnetwork_thread_get_context:
 * 
 * This function is used to retrieve a reference to this thread's #GMainContext
 * which was set with gnetwork_thread_set_context(). The returned value should
 * be unreferenced with g_main_context_unref() when no longer needed.
 * 
 * The main use of this function is to get the thread's context when manually
 * creating #GSource functions. Typically you will not need this function.
 * 
 * Returns: the current thread's associated #GMainContext.
 *
 * Since: 1.0
 **/
GMainContext *
gnetwork_thread_get_context (void)
{
  GMainContext *context;

  context = thread_access_context (FALSE, NULL);
  if (context != NULL)
    g_main_context_ref (context);

  return context;
}


G_LOCK_DEFINE_STATIC (threadpool);

/**
 * gnetwork_thread_new:
 * @func: the function to use for a thread.
 * @data: the user_data to pass @func, or %NULL.
 * @notify: a function to call with @data as it's pointer when @func has finished.
 * @context: the #GMainContext for the #GMainLoop which will run in this thread, or %NULL.
 * @error: a location to contain thread errors, or %NULL.
 * 
 * Convenience function to run @func in a non-joinable thread. This function
 * uses a global thread pool to cache up to 2 unused threads, improving the
 * speed of creating a thread in most cases. The GNetwork DNS functions use this
 * to create threaded DNS lookups, so most applications which use this library
 * will benefit from using this function. After @func has returned, @notify will be
 * called from inside the thread with @data as it's argument.
 *
 * Returns: %TRUE if the thread could be created, %FALSE if an error occurred.
 * 
 * Since: 1.0
 **/
gboolean
gnetwork_thread_new (GThreadFunc func, gpointer data, GDestroyNotify notify, GMainContext * context,
		     GError ** error)
{
  static volatile GThreadPool *threadpool = NULL;
  ThreadData *tdata;

  g_return_val_if_fail (func != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!g_thread_supported ())
    g_thread_init (NULL);

  G_LOCK (threadpool);

  if (threadpool == NULL)
    {
      threadpool = g_thread_pool_new ((GFunc) thread_function, NULL, -1, FALSE, error);

      if (threadpool != NULL)
	{
	  g_thread_pool_set_max_unused_threads (2);
	  g_timeout_add (600000, (GSourceFunc) g_thread_pool_stop_unused_threads, NULL);
	}
      else
	{
	  return FALSE;
	}
    }

  if (context != NULL)
    g_main_context_ref (context);

  tdata = g_new0 (ThreadData, 1);

  tdata->thread_func = func;
  tdata->data = data;
  tdata->notify = notify;
  tdata->context = context;

  /* We can ignore errors here, the thread will be created eventually. */
  g_thread_pool_push ((GThreadPool *) threadpool, tdata, NULL);

  G_UNLOCK (threadpool);

  return TRUE;
}


/**
 * gnetwork_thread_idle_add:
 * @func: the function to run.
 * @data: the user data to pass to @func.
 *
 * Adds a function to be called whenever there are no higher priority events
 * pending to the current thread's main loop. The function is given the default
 * idle priority, G_PRIORITY_DEFAULT_IDLE. If the function returns FALSE it is
 * automatically removed from the list of event sources and will not be called
 * again.
 * 
 * Note: this function is actually a macro, so apart from taking it's reference,
 * it should behave like a normal function.
 * 
 * Returns: the source ID.
 *
 * Since: 1.0
 **/

/**
 * gnetwork_thread_idle_add_full:
 * @priority: the priority to give within the thread to running @func.
 * @func: the function to add.
 * @data: the user data to pass to @func.
 * @notify: a function capable of destroying @data.
 *
 * Adds an idle source which calls @func when there are no events with a higher
 * priority than @priority to the current thread's main loop. If @func returns
 * FALSE, it will automatically be removed from the main loop, and @data will be
 * destroyed by @notify. See g_idle_add() for more information.
 * 
 * Returns: the source ID.
 * 
 * Since: 1.0
 **/
guint
gnetwork_thread_idle_add_full (gint priority, GSourceFunc func, gpointer data,
			       GDestroyNotify notify)
{
  GSource *source;
  guint retval;

  source = g_idle_source_new ();

  if (priority != G_PRIORITY_DEFAULT_IDLE)
    g_source_set_priority (source, priority);

  g_source_set_callback (source, func, data, notify);

  retval = g_source_attach (source, gnetwork_thread_get_context ());
  g_source_unref (source);

  return retval;
}


/**
 * gnetwork_thread_timeout_add:
 * @interval: the interval in microseconds between runs of @func.
 * @func: the function to run.
 * @data: the user data to pass to @func.
 * 
 * Adds a timeout source which runs @func every @interval microseconds at the
 * default priority (%G_PRIORITY_DEFAULT) to the current thread's main loop. If
 * @func returns FALSE, it will automatically be removed from the main loop. See
 * gnetwork_thread_timeout_add_full() and g_timeout_add() for more information.
 * 
 * Note: this function is actually a macro, so apart from taking it's reference,
 * it should behave like a normal function.
 * 
 * Returns: the source ID.
 *
 * Since: 1.0
 **/

/**
 * gnetwork_thread_timeout_add_full:
 * @priority: the priority to give within the thread to running @func.
 * @interval: the interval in microseconds between runs of @func.
 * @func: the function to run.
 * @data: the user data to pass to @func.
 * @notify: a function capable of destroying @data, or %NULL.
 * 
 * Adds a timeout source which runs @func every @interval microseconds with the
 * given @priority to the current thread's main loop. If @func returns FALSE, it
 * will automatically be removed from the main loop, and @data will be destroyed
 * by @notify. See g_timeout_add() for more information.
 *
 * Returns: the source ID.
 * 
 * Since: 1.0
 **/
guint
gnetwork_thread_timeout_add_full (gint priority, guint interval, GSourceFunc func, gpointer data,
				  GDestroyNotify notify)
{
  GSource *source;
  guint retval;

  source = g_timeout_source_new (interval);

  if (priority != G_PRIORITY_DEFAULT)
    g_source_set_priority (source, priority);

  g_source_set_callback (source, func, data, notify);

  retval = g_source_attach (source, gnetwork_thread_get_context ());
  g_source_unref (source);

  return retval;
}


/**
 * gnetwork_thread_io_add_watch:
 * @channel: a GIOChannel.
 * @condition: the condition to watch for.
 * @func: the function to call when @condition is satisfied.
 * @data: the user data to pass to @func.
 *
 * Adds an event source to the current thread's main loop which calls @func when
 * @condition is satisfied on @channel. If @func returns %FALSE, the source will
 * be removed.
 * 
 * Note: this function is actually a macro, so apart from taking it's reference,
 * it should behave like a normal function.
 *
 * Returns: the event source ID.
 *
 * Since: 1.0
 **/

/**
 * gnetwork_thread_io_add_watch_full:
 * @channel: a GIOChannel.
 * @priority: the priority of the GIOChannel source.
 * @condition: the condition to watch for.
 * @func: the function to call when @condition is satisfied.
 * @data: the user data to pass to @func.
 * @notify: a function to call when the source is removed.
 *
 * Adds an event source to the current thread's main loop at @priority which
 * calls @func when @condition is satisfied on @channel. If @func returns
 * %FALSE, the source will be removed, and @notify will be called with @data as
 * it's argument.
 *
 * Returns: the event source ID.
 *
 * Since: 1.0
 **/
guint
gnetwork_thread_io_add_watch_full (GIOChannel * channel, gint priority, GIOCondition condition,
				   GIOFunc func, gpointer data, GDestroyNotify notify)
{
  GSource *source;
  guint retval;

  g_return_val_if_fail (channel != NULL, 0);
  g_return_val_if_fail (func != NULL, 0);
  g_return_val_if_fail (condition >= 0 && condition <= GNETWORK_IO_ANY, 0);

  source = g_io_create_watch (channel, condition);

  if (priority != G_PRIORITY_DEFAULT)
    g_source_set_priority (source, priority);

  g_source_set_callback (source, (GSourceFunc) func, data, notify);

  retval = g_source_attach (source, gnetwork_thread_get_context ());
  g_source_unref (source);

  return retval;
}


/**
 * gnetwork_thread_source_remove:
 * @id: the source tag to remove.
 * 
 * Removes the #GSource which matches @id from the current thread's main loop.
 * 
 * Returns: %TRUE if @id was found and removed, %FALSE if not.
 *
 * Since: 1.0
 **/
gboolean
gnetwork_thread_source_remove (guint id)
{
  GSource *source;

  g_return_val_if_fail (id != 0, FALSE);

  source = g_main_context_find_source_by_id (gnetwork_thread_get_context (), id);

  if (source != NULL)
    g_source_destroy (source);

  return (source != NULL);
}
