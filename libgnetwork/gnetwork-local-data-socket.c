/*
 * GNetwork Library: libgnetwork/gnetwork-local-data-socket.c
 *
 * Copyright (C) 2001-2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-local-data-socket.h"

#include "gnetwork-buffer.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/un.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <glib/gi18n.h>


#define GNETWORK_LOCAL_DATA_SOCKET_GET_PRIVATE(object)	(GNETWORK_LOCAL_DATA_SOCKET (object)->_priv)


/**
 * GNetworkLocalDataSocket:
 * 
 * The structure for local data socket object instances. This structures
 * contains no public members.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkLocalDataSocketClass:
 * 
 * The structure for the local data socket object class. All signals and methods
 * are parent-relative, but may be %NULL.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkLocalDataSocketState:
 * @GNETWORK_LOCAL_DATA_SOCKET_STATE_AUTHENTICATING: the socket is authenticating using local socket credentials.
 * @GNETWORK_LOCAL_DATA_SOCKET_STATE_OPEN: the local socket is open.
 * @GNETWORK_LOCAL_DATA_SOCKET_STATE_LAST: the highest integer used by GNetworkLocalDataSocket.
 * 
 * An enumeration of values used by local data sockets in the "state" property.
 * Subclasses should not override any of the integers above, instead use the
 * LAST and FIRST items to stake out unique integers.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkLocalDataSocketAuthFlags:
 * @GNETWORK_LOCAL_DATA_SOCKET_AUTH_NONE: no authentication will be performed.
 * @GNETWORK_LOCAL_DATA_SOCKET_AUTH_SAME_PROCESS: the connections must be from the same process.
 * @GNETWORK_LOCAL_DATA_SOCKET_AUTH_SAME_USER: the connections must be from the same user.
 * @GNETWORK_LOCAL_DATA_SOCKET_AUTH_SAME_GROUP: the connections must be from the same group.
 * 
 * A bitwise flags value used to determine the level of authentication that will
 * be performed on incoming sockets.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkLocalDataSocketErrorFlags:
 * @GNETWORK_LOCAL_DATA_SOCKET_ERROR_NONE: no error occurred.
 * @GNETWORK_LOCAL_DATA_SOCKET_ERROR_NO_CREDENTIALS: credentials were expected, but none were received.
 * @GNETWORK_LOCAL_DATA_SOCKET_ERROR_DIFFERENT_PROCESS: the given credentials are from a different process, and the authentication flags require the same process.
 * @GNETWORK_LOCAL_DATA_SOCKET_ERROR_DIFFERENT_USER: the given credentials are from a different user, and the authentication flags require the same user.
 * @GNETWORK_LOCAL_DATA_SOCKET_ERROR_DIFFERENT_GROUP: the given credentials are from a different group, and the authentication flags require the same group.
 * 
 * A bitwise flags value used to list possible errors which occurred for a local
 * data socket.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_LOCAL_DATA_SOCKET:
 * @object: the pointer to cast.
 * 
 * Casts the #GNetworkLocalDataSocket derived pointer at @object into a
 * (GNetworkLocalDataSocket*) pointer. Depending on the current debugging level,
 * this function may invoke certain runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_LOCAL_DATA_SOCKET:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GNetworkLocalDataSocket (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_LOCAL_DATA_SOCKET_CLASS:
 * @klass: the pointer to cast.
 * 
 * Casts a the #GNetworkLocalDataSocketClass derieved pointer at @klass into a
 * (GNetworkLocalDataSocketClass*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_LOCAL_DATA_SOCKET_CLASS:
 * @klass: the pointer to check.
 * 
 * Checks whether @klass is a #GNetworkLocalDataSocketClass (or derived) class.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_LOCAL_DATA_SOCKET_GET_CLASS:
 * @object: the pointer to check.
 * 
 * Retrieves a pointer to the #GNetworLocalkDataSocketClass for the
 * #GNetworkLocalDataSocket (or derived) instance @object.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_TYPE_LOCAL_DATA_SOCKET:
 * 
 * The registered #GType of #GNetworkLocalDataSocket.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkLocalSocketCredentials:
 * 
 * The structure for local data socket credentials.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_LOCAL_SOCKET_CREDENTIALS:
 * @cred: the pointer to cast.
 * 
 * Casts the #GNetworkLocalSocketCredentials pointer at @cred into a
 * (GNetworkLocalSocketCredentials*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_LOCAL_SOCKET_CREDENTIALS:
 * @cred: the pointer to check.
 * 
 * Checks whether @cred is a #GNetworkLocalSocketCredentials structure.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_TYPE_LOCAL_SOCKET_CREDENTIALS:
 * 
 * The registered #GType of #GNetworkLocalSocketCredentials structures.
 * 
 * Since: 1.0
 **/


/* ************** *
 *  Enumerations  *
 * ************** */

enum
{
  PROP_0,

  AUTH_FLAGS,
  CREDENTIALS,
  FILENAME,
  SOCKET_FD
};


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkLocalDataSocketPrivate
{
  /* Properties */
  gchar *filename;
  GNetworkLocalSocketCredentials *credentials;
  gint sockfd;

  /* Object Data */
  GSList *buffer;
  GIOChannel *channel;
  gulong notify_id;
  guint source_id;
  GIOCondition source_cond:6;

  /* Property Bits */
  GNetworkLocalDataSocketAuthFlags auth_flags:3;

  /* Data Bits */
  gboolean creds_done:1;
};


struct _GNetworkLocalSocketCredentials
{
  GTypeClass g_class;

  struct ucred data;
};


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer gnetwork_local_data_socket_parent_class = NULL;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static inline void
read_descriptors (GNetworkParams * params, struct cmsghdr * header)  
{
  GValueArray *ar;
  guint i;
  GValue value = { 0 };

  gnetwork_params_get (params, "socket-descriptors", &ar, NULL);

  if (ar == NULL)
    ar = g_value_array_new (header->cmsg_len / sizeof (int));

  g_value_init (&value, G_TYPE_POINTER);

  for (i = 0; i < header->cmsg_len / sizeof (int); i++)
    {
      g_value_set_pointer (&value, GINT_TO_POINTER (*((int *) CMSG_DATA (header) + i)));
      g_value_array_insert (ar, i, &value);
      g_value_reset (&value);
    }

  gnetwork_params_set (params, "socket-descriptors", ar, NULL);
  g_value_array_free (ar);
}


static inline void
read_credentials (GNetworkParams * params, struct cmsghdr * header)
{
  struct ucred *cred_header;
  GNetworkLocalSocketCredentials cred;
			    
  cred_header = (struct ucred *) CMSG_DATA (header);

  cred.g_class.g_type = GNETWORK_TYPE_LOCAL_SOCKET_CREDENTIALS;
  cred.data = *cred_header;

  gnetwork_params_set (params, "local-credentials", &cred, NULL);
}


static void
set_error_param_for_connect_result (GNetworkParams * params, gint error_val, const gchar * filename)
{
  GError error;

  switch (error_val)
    {
    case ECONNREFUSED:
      error.domain = GNETWORK_DATA_SOCKET_ERROR;
      error.code = GNETWORK_DATA_SOCKET_ERROR_REFUSED;
      error.message = g_strdup_printf (_("The file at \"%s\" is not a local networking server."),
				       filename);
      break;

    case EPERM:
      error.domain = GNETWORK_SOCKET_ERROR;
      error.code = GNETWORK_SOCKET_ERROR_PERMISSIONS;
      error.message = g_strdup_printf (_("The credentials given to the local server at \"%s\" are "
					 "not valid."), filename);
      break;
    case EPIPE:
      error.domain = GNETWORK_SOCKET_ERROR;
      error.domain = GNETWORK_DATA_SOCKET_ERROR_HOST_CLOSED;
      error.message = g_strdup_printf (_("The local networking server at \"%s\" closed the "
				         "connection."), filename);
      break;
    default:
      error.domain = GNETWORK_SOCKET_ERROR;
      error.code = GNETWORK_SOCKET_ERROR_INTERNAL;
      error.message = g_strdup_printf (_("The file \"%s\" could not be used as a local socket "
					 "because an error occurred inside the GNetwork library."),
				       filename);
      break;
    }

  gnetwork_params_set (params, "socket-error", &error,
		       "socket-error-flags", (G_LOG_FLAG_FATAL | G_LOG_LEVEL_ERROR), NULL);

  g_free (error.message);
}


/* ********************** *
 *  Connection Functions  *
 * ********************** */

static gboolean
io_channel_handler (GNetworkObject * object, GIOChannel * channel, GIOCondition cond, gpointer data)
{
  GNetworkLocalDataSocketPrivate *priv;
  GNetworkParams *params;
  GNetworkBuffer *buffer;
  gboolean retval;
  gint state;

  g_object_get (object, "socket-state", &state, NULL);

  if (state <= GNETWORK_SOCKET_STATE_CLOSED)
    return FALSE;

  /* Error and EOF */
  if (cond & (G_IO_ERR | G_IO_HUP))
    {
      gnetwork_socket_close (GNETWORK_SOCKET (object));
      return FALSE;
    }

  priv = GNETWORK_LOCAL_DATA_SOCKET_GET_PRIVATE (object);
  retval = FALSE;

  /* Read */
  if (cond & (G_IO_IN | G_IO_PRI))
    {
      gsize bytes_read;
      struct msghdr msg;
      guint i;

      errno = 0;
      bytes_read = recvmsg (priv->sockfd, &msg, 0);

      /* EOF */
      if (bytes_read == 0)
	{
	  gnetwork_socket_close (GNETWORK_SOCKET (object));
	  retval = FALSE;
	}
      /* Error */
      else if (bytes_read < 0)
	{
	  GError error;

	  if (errno == EAGAIN)
	    return TRUE;

	  params = gnetwork_params_new (G_OBJECT_TYPE (object));

	  error.domain = GNETWORK_DATA_SOCKET_ERROR;
	  error.code = GNETWORK_DATA_SOCKET_ERROR_RECEIVE_FAILED;
	  error.message = g_strdup_printf (_("There was an error reading data from the local "
					     "socket \"%s\"."), priv->filename);

	  gnetwork_params_set (params, "socket-error-flags", G_LOG_LEVEL_CRITICAL,
			       "socket-error", &error, NULL);
	  g_free (error.message);

	  retval = gnetwork_socket_error (GNETWORK_SOCKET (object), params);
	  gnetwork_params_unref (params);

	  if (!retval)
	    gnetwork_socket_close (GNETWORK_SOCKET (object));
	}
      /* Success */
      else
	{
	  struct cmsghdr *header;

	  params = gnetwork_params_new (G_OBJECT_TYPE (object));

	  header = CMSG_FIRSTHDR (&msg);

	  while (header != NULL)
	    {
	      if (header->cmsg_level == SOL_SOCKET)
		{
		  switch (header->cmsg_type)
		    {
		    case SCM_RIGHTS:
		      read_descriptors (params, header);
		      break;
		    /* Credentials */
		    case SCM_CREDENTIALS:
		      read_credentials (params, header);
		      break;
		    default:
		      break;
		    }
		}

	      header = CMSG_NXTHDR (&msg, header);
	    }

	  for (i = 0; i < msg.msg_iovlen; i++)
	    {
	      buffer = gnetwork_buffer_new_with_data (msg.msg_iov[i].iov_base,
						      msg.msg_iov[i].iov_len);

	      gnetwork_params_set (params, "raw-data", buffer, NULL);
	      gnetwork_buffer_unref (buffer);

	      gnetwork_data_socket_received (GNETWORK_DATA_SOCKET (object), params);
	    }

	  gnetwork_params_unref (params);
	}

      return TRUE;
    }

  /* Write */
  if (cond & G_IO_OUT)
    {
      GNetworkDataSocketDirection dir;

      g_object_get (socket, "direction", &dir, NULL);

      if (priv->buffer != NULL)
	{
	  GValueArray *fd_array;

	  struct msghdr msg;
	  struct cmsghdr *cmsg;
	  gsize bytes_sent;

	  params = priv->buffer->data;

	  gnetwork_params_get (params, "raw-data", &buffer, "local-descriptors", &fd_array, NULL);

	  if (GNETWORK_IS_BUFFER (buffer))
	    {
	      msg.msg_iov = g_new0 (struct iovec, 1);

	      msg.msg_iov[0].iov_base = (gpointer) gnetwork_buffer_get_data (buffer);
	      msg.msg_iov[0].iov_len = gnetwork_buffer_get_length (buffer);

	      gnetwork_buffer_unref (buffer);

	      msg.msg_iovlen = 1;
	    }
	  else
	    {
	      msg.msg_iov = NULL;
	      msg.msg_iovlen = 0;
	    }

	  if (dir == GNETWORK_DATA_SOCKET_OUTGOING && !priv->creds_done &&
	      priv->auth_flags > GNETWORK_LOCAL_DATA_SOCKET_AUTH_NONE)
	    {
	      struct ucred *creddata;

	      if (fd_array != NULL)
		{
		  gint *fds;
		  guint i;

		  msg.msg_controllen = (CMSG_SPACE (sizeof (int) * fd_array->n_values) +
					CMSG_SPACE (sizeof (struct ucred)));
		  msg.msg_control = g_new0 (guchar, msg.msg_controllen);

		  /* Create the FDs header */
		  cmsg = CMSG_FIRSTHDR (&msg);
		  cmsg->cmsg_level = SOL_SOCKET;
		  cmsg->cmsg_type = SCM_RIGHTS;
		  cmsg->cmsg_len = CMSG_LEN (sizeof (int) * fd_array->n_values);

		  fds = (gint *) CMSG_DATA (cmsg);

		  for (i = 0; i < fd_array->n_values; i++)
		    fds[i] = g_value_get_int (g_value_array_get_nth (fd_array, i));

		  g_value_array_free (fd_array);

		  cmsg = CMSG_NXTHDR (&msg, cmsg);
		  cmsg->cmsg_level = SOL_SOCKET;
		  cmsg->cmsg_type = SCM_CREDENTIALS;
		  cmsg->cmsg_len = CMSG_LEN (sizeof (struct ucred));
		  creddata = (struct ucred *) CMSG_DATA (cmsg);

		  creddata->pid = getpid ();
		  creddata->uid = getuid ();
		  creddata->gid = getgid ();
		}
	      else
		{
		  msg.msg_controllen = CMSG_SPACE (sizeof (struct ucred));
		  msg.msg_control = g_new0 (guchar, msg.msg_controllen);

		  cmsg = CMSG_FIRSTHDR (&msg);
		  cmsg->cmsg_level = SOL_SOCKET;
		  cmsg->cmsg_type = SCM_CREDENTIALS;
		  cmsg->cmsg_len = CMSG_LEN (sizeof (struct ucred));
		  creddata = (struct ucred *) CMSG_DATA (cmsg);

		  creddata->pid = getpid ();
		  creddata->uid = getuid ();
		  creddata->gid = getgid ();
		}
	    }
	  else if (fd_array != NULL)
	    {
	      gint *fds;
	      guint i;

	      msg.msg_controllen = CMSG_SPACE (sizeof (int) * fd_array->n_values);
	      msg.msg_control = g_new0 (guchar, msg.msg_controllen);

	      /* Create the FDs header */
	      cmsg = CMSG_FIRSTHDR (&msg);
	      cmsg->cmsg_level = SOL_SOCKET;
	      cmsg->cmsg_type = SCM_RIGHTS;
	      cmsg->cmsg_len = CMSG_LEN (sizeof (int) * fd_array->n_values);

	      fds = (gint *) CMSG_DATA (cmsg);

	      for (i = 0; i < fd_array->n_values; i++)
		fds[i] = g_value_get_int (g_value_array_get_nth (fd_array, i));

	      g_value_array_free (fd_array);
	    }
	  else
	    {
	      msg.msg_controllen = 0;
	      msg.msg_control = 0;
	    }

	  bytes_sent = sendmsg (priv->sockfd, &msg, 0);
      
	  if (bytes_sent == 0)
	    {
	      gnetwork_socket_close (GNETWORK_SOCKET (object));
	      retval = FALSE;
	    }
	  /* Error */
	  else if (bytes_sent < 0)
	    {
	      GError error;

	      if (errno == EAGAIN)
		return TRUE;

	      params = gnetwork_params_new (G_OBJECT_TYPE (object));

	      error.domain = GNETWORK_DATA_SOCKET_ERROR;
	      error.code = GNETWORK_DATA_SOCKET_ERROR_SEND_FAILED;
	      error.message = g_strdup_printf (_("There was an error sending data to the local "
						 "socket at \"%s\"."), priv->filename);

	      gnetwork_params_set (params, "socket-error-flags", G_LOG_LEVEL_CRITICAL,
				   "socket-error", &error, NULL);
	      g_free (error.message);

	      retval = gnetwork_socket_error (GNETWORK_SOCKET (object), params);
	      gnetwork_params_unref (params);

	      if (!retval)
		gnetwork_socket_close (GNETWORK_SOCKET (object));
	    }
	}

      /* We've got nothing else to send, so stop listening for G_IO_OUT. */
      if (priv->buffer == NULL && retval)
	{
	  gnetwork_object_remove_source (object, priv->source_id);
	  priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);
	  priv->source_id = gnetwork_object_add_io_watch (object, priv->channel, priv->source_cond,
							  io_channel_handler, NULL);
	  retval = FALSE;
	}
    }

  return retval;
}


static void
finish_open (GNetworkObject * object, GNetworkLocalDataSocketPrivate * priv)
{
  gint state;

  priv->channel = g_io_channel_unix_new (priv->sockfd);
  g_io_channel_set_encoding (priv->channel, NULL, NULL);
  g_io_channel_set_buffered (priv->channel, FALSE);

  if (priv->auth_flags > GNETWORK_LOCAL_DATA_SOCKET_AUTH_NONE)
    state = GNETWORK_LOCAL_DATA_SOCKET_STATE_AUTHENTICATING;
  else
    state = GNETWORK_SOCKET_STATE_OPEN;

  gnetwork_socket_set_state (GNETWORK_SOCKET (object), state);

  if (priv->buffer != NULL)
    priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP | G_IO_OUT);
  else
    priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);

  priv->source_id = gnetwork_object_add_io_watch (object, priv->channel, priv->source_cond,
						  io_channel_handler, NULL);
}


/* connect() Is Done */
static gboolean
connect_done_handler (GNetworkObject *object, GIOChannel * channel, GIOCondition cond,
		      gpointer user_data)
{
  GNetworkLocalDataSocketPrivate *priv;
  GNetworkParams *params;
  gint result, error_val, length;

  errno = 0;
  error_val = 0;
  length = 0;
  priv = GNETWORK_LOCAL_DATA_SOCKET_GET_PRIVATE (object);
  result = getsockopt (priv->sockfd, SOL_SOCKET, SO_ERROR, &error_val, &length);

  /* Couldn't get socket options */
  if (result != 0)
    {
      GError error;

      params = gnetwork_params_new (G_OBJECT_TYPE (object));

      error.domain = GNETWORK_SOCKET_ERROR;
      error.code = GNETWORK_SOCKET_ERROR_INTERNAL;
      error.message = g_strdup_printf (_("The file \"%s\" could not be used as a local socket "
					 "because an error occurred inside the GNetwork library."),
				       priv->filename);

      gnetwork_params_set (params, "socket-error", &error,
			   "socket-error-flags", (G_LOG_FLAG_FATAL | G_LOG_LEVEL_ERROR), NULL);
      g_free (error.message);
      
      gnetwork_socket_error (GNETWORK_SOCKET (object), params);
      gnetwork_params_unref (params);

      gnetwork_socket_close (GNETWORK_SOCKET (object));
    }
  else if (error_val != 0)
    {
      params = gnetwork_params_new (G_OBJECT_TYPE (object));

      set_error_param_for_connect_result (params, error_val, priv->filename);
      
      gnetwork_socket_error (GNETWORK_SOCKET (object), params);
      gnetwork_params_unref (params);

      gnetwork_socket_close (GNETWORK_SOCKET (object));
    }
  else
    {
      finish_open (object, priv);
    }

  return FALSE;
}


/* ************************** *
 *  GNetworkSocket Functions  *
 * ************************** */

static void
emit_fatal_internal_error (GNetworkSocket * socket, const gchar * filename)
{
  GError error;
  GNetworkParams *params;

  error.domain = GNETWORK_SOCKET_ERROR;
  error.code = GNETWORK_SOCKET_ERROR_INTERNAL,
  error.message = g_strdup_printf (_("The file \"%s\" could not be used as a connection because an "
				     "error occurred inside the GNetwork library."), filename);

  params = gnetwork_params_new (G_OBJECT_TYPE (socket));
  gnetwork_params_set (params, "socket-error", &error,
		       "socket-error-flags", (G_LOG_FLAG_FATAL | G_LOG_LEVEL_ERROR), NULL);
  g_free (error.message);

  gnetwork_socket_error (socket, params);
  gnetwork_params_unref (params);

  gnetwork_socket_close (socket);
}


static void
gnetwork_local_data_socket_open (GNetworkSocket * sock)
{
  GNetworkLocalDataSocketPrivate *priv;
  GNetworkDataSocketDirection dir;
  gint flags;

  g_return_if_fail (GNETWORK_IS_LOCAL_DATA_SOCKET (sock));

  /* Chain to parent */
  if (GNETWORK_SOCKET_CLASS (gnetwork_local_data_socket_parent_class)->open != NULL)
    (*GNETWORK_SOCKET_CLASS (gnetwork_local_data_socket_parent_class)->open) (sock);

  dir = GNETWORK_DATA_SOCKET_UNKNOWN;
  g_object_get (sock, "direction", &dir, NULL);

  if (dir == GNETWORK_DATA_SOCKET_UNKNOWN)
    return;

  priv = GNETWORK_LOCAL_DATA_SOCKET_GET_PRIVATE (sock);

  /* If the socket property hasn't been set, we need to create one. */
  if (priv->sockfd < 0)
    {
      errno = 0;
      priv->sockfd = socket (AF_UNIX, SOCK_STREAM, 0);

      if (priv->sockfd < 0)
	{
	  emit_fatal_internal_error (sock, priv->filename);
	  return;
	}

      g_object_notify (G_OBJECT (sock), "socket");
    }

  /* Retrieve the current socket flags */
  flags = fcntl (priv->sockfd, F_GETFL, 0);
  if (flags == -1)
    {
      emit_fatal_internal_error (sock, priv->filename);
      return;
    }

  /* Add the non-blocking flag */
  if (fcntl (priv->sockfd, F_SETFL, flags | O_NONBLOCK) == -1)
    {
      emit_fatal_internal_error (sock, priv->filename);
      return;
    }

  switch (dir)
    {
    case GNETWORK_DATA_SOCKET_INCOMING:
      /* Servers */
	  
      break;
    case GNETWORK_DATA_SOCKET_OUTGOING:
      /* Clients */
      {
	struct sockaddr_un sun;
	gint result;

	memset (&sun, 0, sizeof (sun));

	sun.sun_family = AF_UNIX;
	strncpy (sun.sun_path, priv->filename, G_N_ELEMENTS (sun.sun_path));

	errno = 0;
	result = connect (priv->sockfd, (struct sockaddr *) &(sun), sizeof (sun));

	if (result != 0)
	  {
	    gint my_errno = errno;

	    if (my_errno == EINPROGRESS)
	      {
		priv->channel = g_io_channel_unix_new (priv->sockfd);
		g_io_channel_set_encoding (priv->channel, NULL, NULL);
		g_io_channel_set_buffered (priv->channel, FALSE);

		priv->source_cond = (G_IO_IN | G_IO_OUT | G_IO_PRI | G_IO_ERR | G_IO_HUP);
		priv->source_id = gnetwork_object_add_io_watch (GNETWORK_OBJECT (sock),
								priv->channel, priv->source_cond,
								connect_done_handler, NULL);
		return;
	      }
	    else if (my_errno != EISCONN)
	      {
		GNetworkParams *params = gnetwork_params_new (G_OBJECT_TYPE (sock));

		set_error_param_for_connect_result (params, my_errno, priv->filename);

		gnetwork_socket_error (sock, params);
		gnetwork_params_unref (params);

		gnetwork_socket_close (sock);
	        return;
	      }
	  }
      }
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  finish_open (GNETWORK_OBJECT (sock), priv);
}


static void
gnetwork_local_data_socket_close (GNetworkSocket * sock)
{
  GNetworkLocalDataSocketPrivate *priv;
  gint state;

  g_return_if_fail (GNETWORK_IS_LOCAL_DATA_SOCKET (sock));

  state = GNETWORK_SOCKET_STATE_CLOSED;
  g_object_get (sock, "socket-state", &state, NULL);

  if (state == GNETWORK_SOCKET_STATE_CLOSED || state == GNETWORK_SOCKET_STATE_CLOSING)
    return;

  gnetwork_socket_set_state (sock, GNETWORK_SOCKET_STATE_CLOSING);

  priv = GNETWORK_LOCAL_DATA_SOCKET_GET_PRIVATE (sock);
  
  switch (state)
    {
      /* Fully Open & Ready. */
    case GNETWORK_LOCAL_DATA_SOCKET_STATE_AUTHENTICATING:
    case GNETWORK_SOCKET_STATE_OPENING:
    case GNETWORK_SOCKET_STATE_OPEN:
      if (priv->source_id != 0)
	{
	  gnetwork_object_remove_source (GNETWORK_OBJECT (sock), priv->source_id);
	  priv->source_id = 0;
	  priv->source_cond = 0;
	}

      if (priv->channel != NULL)
	{
	  g_io_channel_shutdown (priv->channel, FALSE, NULL);
	  g_io_channel_unref (priv->channel);
	  priv->channel = NULL;
	}
      else if (priv->sockfd > 0)
	{
	  shutdown (priv->sockfd, SHUT_RDWR);
	  close (priv->sockfd);
	}
      break;

    default:
      break;
    }

  priv->sockfd = -1;
  gnetwork_socket_set_state (sock, GNETWORK_SOCKET_STATE_CLOSED);

  if (GNETWORK_SOCKET_CLASS (gnetwork_local_data_socket_parent_class)->close != NULL)
    (*GNETWORK_SOCKET_CLASS (gnetwork_local_data_socket_parent_class)->close) (sock);
}


static void
gnetwork_local_data_socket_send (GNetworkDataSocket * sock, GNetworkParams * params)
{
  GNetworkLocalDataSocketPrivate *priv;
  GNetworkBuffer *buffer;

  priv = GNETWORK_LOCAL_DATA_SOCKET_GET_PRIVATE (sock);

  gnetwork_params_get (params, "raw-data", &buffer, NULL);

  if (buffer == NULL)
    return;

  if (gnetwork_buffer_get_length (buffer) == 0)
    {
      gnetwork_buffer_unref (buffer);
      return;
    }

  priv->buffer = g_slist_append (priv->buffer, gnetwork_params_ref (params));

  /* Watch for writability if we're not already */
  if (!(priv->source_cond & G_IO_OUT))
    {
      if (priv->source_id != 0)
	gnetwork_object_remove_source (GNETWORK_OBJECT (sock), priv->source_id);

      priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_OUT | G_IO_ERR | G_IO_HUP);
      priv->source_id = gnetwork_object_add_io_watch (GNETWORK_OBJECT (sock), priv->channel,
						      priv->source_cond, io_channel_handler,
						      socket);
    }

  if (GNETWORK_DATA_SOCKET_CLASS (gnetwork_local_data_socket_parent_class)->send != NULL)
    (*GNETWORK_DATA_SOCKET_CLASS (gnetwork_local_data_socket_parent_class)->send) (sock, params);
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_local_data_socket_set_property (GObject * object, guint property, const GValue * value,
					 GParamSpec * pspec)
{
  GNetworkLocalDataSocketPrivate *priv;
  gint state;

  state = GNETWORK_SOCKET_STATE_CLOSED;
  g_object_get (object, "socket-state", &state, NULL);

  g_return_if_fail (state == GNETWORK_SOCKET_STATE_CLOSED);

  priv = GNETWORK_LOCAL_DATA_SOCKET_GET_PRIVATE (object);

  switch (property)
    {
    case AUTH_FLAGS:
      priv->auth_flags = g_value_get_flags (value);
      break;

    case FILENAME:
      {
	const gchar *filename = g_value_get_string (value);

	g_return_if_fail (filename == NULL || filename[0] != '\0');

	g_free (priv->filename);

	if (filename != NULL)
	  {
	    priv->filename = g_strdup (filename);
	  }
	else
	  {
	    static guint index_ = 0;
	    guint pid;

	    pid = getpid ();

	    priv->filename = g_strdup_printf ("%s" G_DIR_SEPARATOR_S "%s" G_DIR_SEPARATOR_S
					      "%x-%x-%x%x", g_get_tmp_dir (), g_get_user_name (),
					      pid, index_, g_rand_int (NULL) ^ pid,
					      g_rand_int (NULL) ^ index_);

	    index_++;
	  }
      }
      break;

    case SOCKET_FD:
      priv->sockfd = g_value_get_int (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_local_data_socket_get_property (GObject * object, guint property, GValue * value,
				    GParamSpec * pspec)
{
  GNetworkLocalDataSocketPrivate *priv = GNETWORK_LOCAL_DATA_SOCKET_GET_PRIVATE (object);

  switch (property)
    {
      /* GNetworkLocalDataSocket */
    case AUTH_FLAGS:
      g_value_set_flags (value, priv->auth_flags);
      break;
    case CREDENTIALS:
      g_value_set_boxed (value, priv->credentials);
      break;
    case FILENAME:
      g_value_set_string (value, priv->filename);
      break;
    case SOCKET_FD:
      g_value_set_int (value, priv->sockfd);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_local_data_socket_finalize (GObject * object)
{
  GNetworkLocalDataSocketPrivate *priv = GNETWORK_LOCAL_DATA_SOCKET_GET_PRIVATE (object);

  g_free (priv->credentials);
  g_free (priv->filename);
  g_free (priv);

  if (G_OBJECT_CLASS (gnetwork_local_data_socket_parent_class)->finalize)
    (*G_OBJECT_CLASS (gnetwork_local_data_socket_parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_local_data_socket_class_init (GNetworkLocalDataSocketClass * class)
{
  GObjectClass *object_class;
  GNetworkObjectClass *netobj_class;
  GNetworkSocketClass *socket_class;
  GNetworkDataSocketClass *data_class;

  gnetwork_local_data_socket_parent_class = g_type_class_peek_parent (class);

  object_class = G_OBJECT_CLASS (class);
  netobj_class = GNETWORK_OBJECT_CLASS (class);
  socket_class = GNETWORK_SOCKET_CLASS (class);
  data_class = GNETWORK_DATA_SOCKET_CLASS (class);

  object_class->get_property = gnetwork_local_data_socket_get_property;
  object_class->set_property = gnetwork_local_data_socket_set_property;
  object_class->finalize = gnetwork_local_data_socket_finalize;

  socket_class->open = gnetwork_local_data_socket_open;
  socket_class->close = gnetwork_local_data_socket_close;

  data_class->send = gnetwork_local_data_socket_send;

  /* Properties */
  g_object_class_install_property (object_class, AUTH_FLAGS,
				   g_param_spec_flags ("local-auth-flags",
						       _("Authentication Flags"),
						       _("The types of authentication to perform "
							 "on this socket."),
						       GNETWORK_TYPE_LOCAL_DATA_SOCKET_AUTH_FLAGS,
						       GNETWORK_LOCAL_DATA_SOCKET_AUTH_NONE,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class, CREDENTIALS,
				   g_param_spec_boxed ("local-credentials", _("Credentials"),
						       _("The credentials structure to use when "
							 "authenticating this connection (sent to "
							 "local servers for outgoing sockets, used "
							 "to verify clients for incoming "
							 "sockets)."),
						       GNETWORK_TYPE_LOCAL_SOCKET_CREDENTIALS,
						       G_PARAM_READABLE));
  g_object_class_install_property (object_class, FILENAME,
				   g_param_spec_string ("local-filename", _("Filename"),
							_("The filename of the socket."), NULL,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class, SOCKET_FD,
				   g_param_spec_int ("socket", _("Socket File Descriptor"),
						     _("The socket file descriptor. Useful mainly "
						       "for GNetworkLocalServer and subclasses."),
						     -1, G_MAXINT, -1, G_PARAM_READWRITE));

  /* Signal/Method Parameters */
  gnetwork_object_class_install_param (netobj_class,
				       g_param_spec_boxed ("local-credentials",
							   _("Local Socket Credentials"),
							   _("Credentials structure sent to the "
							     "server or received from a client."),
							   GNETWORK_TYPE_LOCAL_SOCKET_CREDENTIALS,
							   G_PARAM_READWRITE));
  gnetwork_object_class_install_param (netobj_class,
				       g_param_spec_value_array ("local-descriptors",
								 _("Socket Descriptors"),
								 _("A value array of open socket "
								   "file descriptors, used to "
								   "allow clients access to "
								   "files over a networking socket "
								   "on Unix."),
								 g_param_spec_int
								 ("socket-descriptor",
								  _("Socket File Descriptor"),
								  _("An open file descriptor."),
								  -1, G_MAXINT, 0,
								  G_PARAM_READABLE),
								 G_PARAM_READWRITE));

  g_type_class_add_private (class, sizeof (GNetworkLocalDataSocketPrivate));
}


static void
gnetwork_local_data_socket_instance_init (GNetworkLocalDataSocket * socket)
{
  socket->_priv = G_TYPE_INSTANCE_GET_PRIVATE ((socket), GNETWORK_TYPE_LOCAL_DATA_SOCKET,
					       GNetworkLocalDataSocketPrivate);

  socket->_priv->filename = NULL;
  socket->_priv->credentials = NULL;
  socket->_priv->sockfd = -1;
  socket->_priv->auth_flags = GNETWORK_LOCAL_DATA_SOCKET_AUTH_NONE;

  socket->_priv->buffer = NULL;
  socket->_priv->channel = NULL;
  socket->_priv->notify_id = 0;
  socket->_priv->source_id = 0;
  socket->_priv->source_cond = 0;
}


/* ************ *
 *  PUBLIC API  *
 * ************ */

GType
gnetwork_local_data_socket_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      static const GTypeInfo info = {
	sizeof (GNetworkLocalDataSocketClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_local_data_socket_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkLocalDataSocket),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_local_data_socket_instance_init,
	NULL			/* value table */
      };

      type = g_type_register_static (GNETWORK_TYPE_DATA_SOCKET, "GNetworkLocalDataSocket",
				     &info, 0);
    }

  return type;
}


/* ************************************************************************** *
 *  GNetworkLocalSocketCredentials                                            *
 * ************************************************************************** */

GType
gnetwork_local_socket_credentials_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      type = g_boxed_type_register_static ("GNetworkLocalSocketCredentials",
					   gnetwork_local_socket_credentials_dup, g_free);
    }

  return type;
}


/**
 * gnetwork_local_socket_credentials_new:
 * 
 * Creates a new set of local socket credentials for use by this application's
 * sockets. This function typically won't be used by applications.
 * 
 * Returns: the ID of the process which sent these credentials.
 * 
 * Since: 1.0
 **/
GNetworkLocalSocketCredentials *
gnetwork_local_socket_credentials_new (void)
{
  GNetworkLocalSocketCredentials *retval;

  retval = g_new0 (GNetworkLocalSocketCredentials, 1);

  retval->g_class.g_type = GNETWORK_TYPE_LOCAL_SOCKET_CREDENTIALS;

  retval->data.pid = getpid ();
  retval->data.uid = getuid ();
  retval->data.gid = getgid ();

  return retval;
}


/**
 * gnetwork_local_socket_credentials_get_pid:
 * @cred: the credentials to examine.
 * 
 * Retrieves the ID of the process which sent these credentials.
 * 
 * Returns: the ID of the process which sent these credentials.
 * 
 * Since: 1.0
 **/
guint
gnetwork_local_socket_credentials_get_pid (const GNetworkLocalSocketCredentials * cred)
{
  g_return_val_if_fail (GNETWORK_IS_LOCAL_SOCKET_CREDENTIALS (cred), 0);

  return cred->data.pid;
}


/**
 * gnetwork_local_socket_credentials_get_uid:
 * @cred: the credentials to examine.
 * 
 * Retreives the ID of the user who sent these credentials.
 * 
 * Returns: the ID of the user who sent these credentials.
 * 
 * Since: 1.0
 **/
guint
gnetwork_local_socket_credentials_get_uid (const GNetworkLocalSocketCredentials * cred)
{
  g_return_val_if_fail (GNETWORK_IS_LOCAL_SOCKET_CREDENTIALS (cred), 0);

  return cred->data.uid;
}


/**
 * gnetwork_local_socket_credentials_get_gid:
 * @cred: the credentials to examine.
 * 
 * Retreives the ID of the group which sent these credentials. Information about
 * this group can be retrieved with getgrnam().
 * 
 * Returns: the ID of the user who sent these credentials.
 * 
 * Since: 1.0
 **/
guint
gnetwork_local_socket_credentials_get_gid (const GNetworkLocalSocketCredentials * cred)
{
  g_return_val_if_fail (GNETWORK_IS_LOCAL_SOCKET_CREDENTIALS (cred), 0);

  return cred->data.gid;
}


/**
 * gnetwork_local_socket_credentials_dup:
 * @cred: the credentials to copy.
 * 
 * Creates a deep copy of the data in @cred. The returned value must be freed
 * with gnetwork_local_socket_credentials_free() when no longer needed.
 * 
 * Returns: A copy of #cred.
 * 
 * Since: 1.0
 **/
gpointer
gnetwork_local_socket_credentials_dup (gpointer cred)
{
  g_return_val_if_fail (cred == NULL || GNETWORK_IS_LOCAL_SOCKET_CREDENTIALS (cred), NULL);

  if (cred == NULL)
    return NULL;

    return g_memdup (cred, sizeof (GNetworkLocalSocketCredentials));  
}


/**
 * gnetwork_local_socket_credentials_free:
 * @cred: the credentials to free.
 * 
 * Frees the data used by #cred.
 * 
 * Since: 1.0
 **/
void
gnetwork_local_socket_credentials_free (gpointer cred)
{
  g_free (cred);
}
