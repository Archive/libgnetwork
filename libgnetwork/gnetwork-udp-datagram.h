/* 
 * GNetwork Library: libgnetwork/gnetwork-udp-datagram.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_UDP_DATAGRAM_H__
#define __GNETWORK_UDP_DATAGRAM_H__ 1

#include "gnetwork-ip-address.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_UDP_DATAGRAM	      (gnetwork_udp_datagram_get_type ())
#define GNETWORK_UDP_DATAGRAM(obj)	      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNETWORK_TYPE_UDP_DATAGRAM, GNetworkUdpDatagram))
#define GNETWORK_UDP_DATAGRAM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_UDP_DATAGRAM, GNetworkUdpDatagramClass))
#define GNETWORK_IS_UDP_DATAGRAM(obj)	      (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNETWORK_TYPE_UDP_DATAGRAM))
#define GNETWORK_IS_UDP_DATAGRAM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_UDP_DATAGRAM))
#define GNETWORK_UDP_DATAGRAM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GNETWORK_TYPE_UDP_DATAGRAM, GNetworkUdpDatagramClass))
#define GNETWORK_UDP_DATAGRAM_ERROR	      (gnetwork_udp_datagram_error_get_quark ())
#define GNETWORK_TYPE_UDP_TARGET	      (gnetwork_udp_target_get_type ())


typedef enum
{
  GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_TTL,
  GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_BROADCAST,
  GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_REUSE
}
GNetworkUdpDatagramError;

typedef enum			/* <prefix=GNETWORK_UDP_DATAGRAM_TTL> */
{
  GNETWORK_UDP_DATAGRAM_TTL_DEFAULT = -1,
  GNETWORK_UDP_DATAGRAM_TTL_LOCAL_HOST = 0,
  GNETWORK_UDP_DATAGRAM_TTL_ONE_LINK = 1,
  GNETWORK_UDP_DATAGRAM_TTL_LOCAL_NET = 31,
  GNETWORK_UDP_DATAGRAM_TTL_REGION = 63,
  GNETWORK_UDP_DATAGRAM_TTL_CONTINENT = 127,
  GNETWORK_UDP_DATAGRAM_TTL_GLOBAL = 255
}
GNetworkUdpDatagramTtl;


typedef struct _GNetworkUdpDatagram GNetworkUdpDatagram;
typedef struct _GNetworkUdpDatagramClass GNetworkUdpDatagramClass;
typedef struct _GNetworkUdpDatagramPrivate GNetworkUdpDatagramPrivate;

typedef struct _GNetworkUdpTarget GNetworkUdpTarget;


struct _GNetworkUdpDatagram
{
  /* < private > */
  GObject parent;

  GNetworkUdpDatagramPrivate *_priv;
};

struct _GNetworkUdpDatagramClass
{
  /* <private> */
  GObjectClass parent_class;

  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_udp_datagram_get_type (void) G_GNUC_CONST;

void gnetwork_udp_datagram_send_to (GNetworkUdpDatagram * udp, const gchar * host, guint16 port,
				    gconstpointer data, glong length);

GType gnetwork_udp_target_get_type (void) G_GNUC_CONST;

GNetworkUdpTarget *gnetwork_udp_target_new (const gchar * host, guint16 port);

G_CONST_RETURN GNetworkIpAddress *gnetwork_udp_target_get_ip_address (const GNetworkUdpTarget *
								      target);
void gnetwork_udp_target_set_ip_address (GNetworkUdpTarget * target,
					 const GNetworkIpAddress * address);

G_CONST_RETURN gchar *gnetwork_udp_target_get_host (const GNetworkUdpTarget * target);
void gnetwork_udp_target_set_host (GNetworkUdpTarget * target, const gchar * host);

guint16 gnetwork_udp_target_get_port (const GNetworkUdpTarget * target);
void gnetwork_udp_target_set_port (GNetworkUdpTarget * target, guint16 port);

GNetworkUdpTarget *gnetwork_udp_target_dup (const GNetworkUdpTarget * src);
void gnetwork_udp_target_free (GNetworkUdpTarget * target);

GQuark gnetwork_udp_datagram_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* __GNETWORK_UDP_DATAGRAM_H__ */
