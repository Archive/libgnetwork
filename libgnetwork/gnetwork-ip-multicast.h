/* 
 * GNetwork Library: libgnetwork/gnetwork-ip-multicast.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_IP_MULTICAST_H__
#define __GNETWORK_IP_MULTICAST_H__ 1

#include "gnetwork-udp-datagram.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_IP_MULTICAST	      (gnetwork_ip_multicast_get_type ())
#define GNETWORK_IP_MULTICAST(obj)	      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNETWORK_TYPE_IP_MULTICAST, GNetworkIpMulticast))
#define GNETWORK_IP_MULTICAST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_IP_MULTICAST, GNetworkIpMulticastClass))
#define GNETWORK_IS_IP_MULTICAST(obj)	      (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNETWORK_TYPE_IP_MULTICAST))
#define GNETWORK_IS_IP_MULTICAST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_IP_MULTICAST))
#define GNETWORK_IP_MULTICAST_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GNETWORK_TYPE_IP_MULTICAST, GNetworkIpMulticastClass))

#define GNETWORK_IP_MULTICAST_ERROR	      (gnetwork_ip_multicast_error_get_quark ())


typedef enum
{
  GNETWORK_IP_MULTICAST_ERROR_CANNOT_SET_TTL,
  GNETWORK_IP_MULTICAST_ERROR_CANNOT_SET_LOOPBACK,
  GNETWORK_IP_MULTICAST_ERROR_JOIN_FAILED,
  GNETWORK_IP_MULTICAST_ERROR_LEAVE_FAILED,
  GNETWORK_IP_MULTICAST_ERROR_UNSUPPORTED_INTERFACE,
  GNETWORK_IP_MULTICAST_ERROR_IPV6_NOT_SUPPORTED
}
GNetworkIpMulticastError;


typedef struct _GNetworkIpMulticast GNetworkIpMulticast;
typedef struct _GNetworkIpMulticastClass GNetworkIpMulticastClass;
typedef struct _GNetworkIpMulticastPrivate GNetworkIpMulticastPrivate;


struct _GNetworkIpMulticast
{
  /* <private> */
  GNetworkUdpDatagram parent;

  GNetworkIpMulticastPrivate *_priv;
};

struct _GNetworkIpMulticastClass
{
  /* <private> */
  GNetworkUdpDatagramClass parent_class;

  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_ip_multicast_get_type (void) G_GNUC_CONST;

GNetworkIpMulticast *gnetwork_ip_multicast_new (const gchar * interface, guint port);

void gnetwork_ip_multicast_join_group (GNetworkIpMulticast * multicast,
				       const GNetworkIpAddress * group);
void gnetwork_ip_multicast_leave_group (GNetworkIpMulticast * multicast,
					const GNetworkIpAddress * group);
void gnetwork_ip_multicast_clear_groups (GNetworkIpMulticast * multicast);

GQuark gnetwork_ip_multicast_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* !__GNETWORK_IP_MULTICAST_H__ */
