/*
 * GNetwork Library: libgnetwork/gnetwork-tcp-server.h
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_TCP_SERVER_H__
#define __GNETWORK_TCP_SERVER_H__

#include "gnetwork-server.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_TCP_SERVER		(gnetwork_tcp_server_get_type ())
#define GNETWORK_TCP_SERVER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNETWORK_TYPE_TCP_SERVER, GNetworkTcpServer))
#define GNETWORK_TCP_SERVER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_TCP_SERVER, GNetworkTcpServerClass))
#define GNETWORK_IS_TCP_SERVER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNETWORK_TYPE_TCP_SERVER))
#define GNETWORK_IS_TCP_SERVER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_TCP_SERVER))
#define GNETWORK_TCP_SERVER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GNETWORK_TYPE_TCP_SERVER, GNetworkTcpServerClass))


typedef struct _GNetworkTcpServer GNetworkTcpServer;
typedef struct _GNetworkTcpServerClass GNetworkTcpServerClass;
typedef struct _GNetworkTcpServerPrivate GNetworkTcpServerPrivate;


struct _GNetworkTcpServer
{
  /*<private> */
  GObject parent;

  GNetworkTcpServerPrivate *_priv;
};


struct _GNetworkTcpServerClass
{
  /* < private > */
  GObjectClass parent_class;

  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_tcp_server_get_type (void) G_GNUC_CONST;

GNetworkTcpServer *gnetwork_tcp_server_new (const gchar * interface, guint port);


#define GNETWORK_TYPE_TCP_SERVER_CREATION_DATA	    (gnetwork_tcp_server_creation_data_get_type ())
#define GNETWORK_TCP_SERVER_CREATION_DATA(boxed)    ((GNetworkTcpServerCreationData *) (boxed))
#define GNETWORK_IS_TCP_SERVER_CREATION_DATA(boxed) ((boxed) != NULL && ((GTypeClass *) (boxed))->g_type == GNETWORK_TYPE_TCP_SERVER_CREATION_DATA)

typedef struct _GNetworkTcpServerCreationData GNetworkTcpServerCreationData;

GType gnetwork_tcp_server_creation_data_get_type (void) G_GNUC_CONST;

G_CONST_RETURN gchar * gnetwork_tcp_server_creation_data_get_address (const GNetworkTcpServerCreationData * data);
guint16 gnetwork_tcp_server_creation_data_get_port (const GNetworkTcpServerCreationData * data);
gconstpointer gnetwork_tcp_server_creation_data_get_socket (const GNetworkTcpServerCreationData * data);
GNetworkTcpServerCreationData *gnetwork_tcp_server_creation_data_dup (const GNetworkTcpServerCreationData * src);
void gnetwork_tcp_server_creation_data_free (GNetworkTcpServerCreationData * data);


G_END_DECLS

#endif /* __GNETWORK_TCP_SERVER_H__ */
