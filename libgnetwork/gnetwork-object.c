/*
 * GNetwork Library: libgnetwork/gnetwork-object.c
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-object.h"
#include "gnetwork-params.h"

#include "gnetwork-utils.h"

#include <glib/gi18n.h>


/**
 * GNetworkObject:
 * 
 * The base object structure used by GNetwork. This structure contains no public
 * members and should only be accessed using the functions below.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkObjectClass:
 * 
 * The base object class structure used by objects in GNetwork. This structure
 * contains no public members.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkObjectIoFunc:
 * @object: the object this callback is attached to.
 * @channel: the channel used for this object.
 * @condition: the condition this function was called for.
 * @user_data: the user data given when this callback was registered.
 * 
 * A callback function called when an event matching @condition occurs on
 * @channel. The thread-safety lock on @object will be held for the duration of
 * the callback, so this function should not lock @object. When this function
 * has returned, the lock will be released.
 * 
 * Returns: %TRUE if the function should remain installed, %FALSE if it should not.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkObjectSourceFunc:
 * @object: the object this callback is attached to.
 * @user_data: the user data given when this callback was registered.
 * 
 * A callback function called when the event for the given source has occurred.
 * The thread-safety lock on @object will be held for the duration of the
 * callback, so this function should not lock @object. When this function has
 * returned, the lock will be released.
 * 
 * Returns: %TRUE if the function should remain installed, %FALSE if it should not.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_OBJECT_LOCK:
 * @object: the #GNetworkObject to lock.
 * 
 * Acquires a thread-safety lock on @object.
 * 
 * This is just a stylistic macro wrapping gnetwork_object_lock().
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_OBJECT_UNLOCK:
 * @object: the #GNetworkObject to unlock.
 *
 * Releases the thread-safety lock on @object acquired by a prior call to
 * GNETWORK_OBJECT_LOCK().
 * 
 * This is just a stylistic macro wrapping gnetwork_object_unlock().
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_OBJECT_TRYLOCK:
 * @object: the #GNetworkObject to attempt to lock.
 *
 * Attempts to acquire the thread-safety lock on @object without freezing the
 * current thread. If the lock could be acquired, this function will return
 * %TRUE. If the lock could not be immediately acquired, this function will
 * return %FALSE. If a the lock has been acquired, it must be released with
 * GNETWORK_OBJECT_UNLOCK().
 * 
 * This is just a stylistic macro wrapping gnetwork_object_trylock().
 *
 * Since: 1.0
 **/

/**
 * GNETWORK_OBJECT:
 * @object: the pointer to cast.
 * 
 * Casts the #GNetworkObject derived pointer at @object into a (GNetworkObject*)
 * pointer. Depending on the current debugging level, this function may invoke
 * certain runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_OBJECT:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GNetworkObject (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_OBJECT_CLASS:
 * @klass: the pointer to cast.
 * 
 * Casts a the #GNetworkObjectClass derieved pointer at @klass into a
 * (GNetworkObjectClass*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_OBJECT_CLASS:
 * @klass: the pointer to check.
 * 
 * Checks whether @klass is a #GNetworkObjectClass (or derived) class.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_OBJECT_GET_CLASS:
 * @object: the pointer to check.
 * 
 * Retrieves a pointer to the #GNetworkObjectClass for the #GNetworkObject (or
 * derived) instance @object.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_TYPE_OBJECT:
 * 
 * The registered #GType of #GNetworkObject.
 * 
 * Since: 1.0
 **/


/* **************** *
 *  Private Macros  *
 * **************** */

#define GNETWORK_OBJECT_GET_PRIVATE(object) (GNETWORK_OBJECT (object)->_priv)

#define LOCK_OBJECT(object) (g_static_mutex_lock (&((object)->_priv->mutex)))
#define UNLOCK_OBJECT(object) (g_static_mutex_unlock (&((object)->_priv->mutex)))
#define TRYLOCK_OBJECT(object) (g_static_mutex_trylock (&((object)->_priv->mutex)))


/* ************** *
 *  Enumerations  *
 * ************** */

enum
{
  PROP_0,

  MAIN_CONTEXT
};


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkObjectPrivate
{
  GStaticMutex mutex;
  GMainContext *context;

  GHashTable *sources;
};

typedef struct
{
  GNetworkObject *object;

  union {
    GNetworkObjectSourceFunc source_func;
    GNetworkObjectIoFunc io_func;
  } func;
  gpointer user_data;
  GDestroyNotify notify;
}
GNetworkObjectSourceCallbackData;


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer gnetwork_object_parent_class = NULL;


/* **************************** *
 *  GSource Proxying Functions  *
 * **************************** */

static void
gnetwork_object_source_callback_data_free (GNetworkObjectSourceCallbackData * data)
{
  if (data->notify != NULL && data->user_data != NULL)
    (*data->notify) (data->user_data);

  g_free (data);
}


static gboolean
source_callback_proxy (GNetworkObjectSourceCallbackData * data)
{
  gboolean retval;

  LOCK_OBJECT (data->object);
  retval = (*data->func.source_func) (data->object, data->user_data);
  UNLOCK_OBJECT (data->object);

  return retval;
}


static gboolean
io_callback_proxy (GIOChannel * channel, GIOCondition condition,
		   GNetworkObjectSourceCallbackData * data)
{
  gboolean retval;

  LOCK_OBJECT (data->object);
  retval = (*data->func.io_func) (data->object, channel, condition, data->user_data);
  UNLOCK_OBJECT (data->object);

  return retval;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_object_set_property (GObject * object, guint property, const GValue * value,
			      GParamSpec * pspec)
{
  GNetworkObjectPrivate *priv = GNETWORK_OBJECT_GET_PRIVATE (object);

  switch (property)
    {
    case MAIN_CONTEXT:
      _gnetwork_hash_table_clear (priv->sources);

      if (priv->context != NULL)
	g_main_context_unref (priv->context);

      priv->context = g_value_dup_boxed (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_object_get_property (GObject * object, guint property, GValue * value, GParamSpec * pspec)
{
  GNetworkObjectPrivate *priv = GNETWORK_OBJECT_GET_PRIVATE (object);

  switch (property)
    {
    case MAIN_CONTEXT:
      g_value_set_boxed (value, priv->context);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_object_finalize (GObject * object)
{
  GNetworkObjectPrivate *priv = GNETWORK_OBJECT_GET_PRIVATE (object);

  g_hash_table_destroy (priv->sources);

  if (priv->context != NULL)
    g_main_context_unref (priv->context);

  g_free (priv);

  if (G_OBJECT_CLASS (gnetwork_object_parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (gnetwork_object_parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_object_class_init (GNetworkObjectClass * class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);

  gnetwork_object_parent_class = g_type_class_peek_parent (class);

  gobject_class->set_property = gnetwork_object_set_property;
  gobject_class->get_property = gnetwork_object_get_property;
  gobject_class->finalize = gnetwork_object_finalize;

  /* GObject Properties */
  g_object_class_install_property (gobject_class, MAIN_CONTEXT,
				   g_param_spec_boxed ("main-context", _("Main Context"),
						       _("The GMainContext this object is "
							 "associated with."), G_TYPE_MAIN_CONTEXT,
						       G_PARAM_READWRITE));

  g_type_class_add_private (class, sizeof (GNetworkObjectPrivate));
}


static void
gnetwork_object_instance_init (GNetworkObject * object)
{
  object->_priv = G_TYPE_INSTANCE_GET_PRIVATE (object, GNETWORK_TYPE_OBJECT, GNetworkObjectPrivate);

  g_static_mutex_init (&(object->_priv->mutex));
  object->_priv->context = NULL;

  object->_priv->sources = g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL,
						  (GDestroyNotify) g_source_destroy);
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_object_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      static const GTypeInfo info = {
	sizeof (GNetworkObjectClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_object_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkObject),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_object_instance_init,
	NULL			/* value table */
      };

      type = g_type_register_static (G_TYPE_OBJECT, "GNetworkObject", &info, G_TYPE_FLAG_ABSTRACT);
    }

  return type;
}

/**
 * gnetwork_object_lock:
 * @object: the object to lock.
 *
 * Acquires the thread-safety lock on @object. This will freeze the current
 * thread until the lock can be acquired, and will freeze any other threads
 * attemping to get acquire the lock on @object until
 * gnetwork_object_unlock() is called.
 *
 * Since: 1.0
 **/
void
gnetwork_object_lock (GNetworkObject * object)
{
  g_return_if_fail (GNETWORK_IS_OBJECT (object));

  LOCK_OBJECT (object);
}


/**
 * gnetwork_object_unlock:
 * @object: the object to unlock.
 *
 * Releases the thread-safety lock on @object acquired by a prior call to
 * gnetwork_object_lock().
 *
 * Since: 1.0
 **/
void
gnetwork_object_unlock (GNetworkObject * object)
{
  g_return_if_fail (GNETWORK_IS_OBJECT (object));

  UNLOCK_OBJECT (object);
}


/**
 * gnetwork_object_trylock:
 * @object: the object to attempt to lock.
 *
 * Attempts to acquire the thread-safety lock on @object without freezing the
 * current thread. If the lock could be acquired, this function will return
 * %TRUE. If the lock could not be immediately acquired, this function will
 * return %FALSE. If a the lock has been acquired, it must be released with
 * gnetwork_object_unlock().
 * 
 * Returns: whether or not the lock could be acquired.
 *
 * Since: 1.0
 **/
gboolean
gnetwork_object_trylock (GNetworkObject * object)
{
  g_return_val_if_fail (GNETWORK_IS_OBJECT (object), FALSE);

  return TRYLOCK_OBJECT (object);
}


/**
 * gnetwork_object_add_idle:
 * @object: the object.
 * @func: the function to run.
 * @user_data: the user data to pass to the function.
 * 
 * Attaches an idle handler to the main context of @object. See
 * gnetwork_object_add_idle_full() for more information.
 * 
 * Since: 1.0.
 **/

/**
 * gnetwork_object_add_idle_full:
 * @object: the object.
 * @priority: the priority to run the function.
 * @func: the function to run.
 * @user_data: the user data to pass to the function.
 * @notify: a function to be called on the user data after the handler has been removed.
 * 
 * Attaches an idle handler to the main context of @object. An idle handler is
 * a function which is run whenever no other higher priority handlers are to be
 * run. When @func returns %TRUE, the idle handler is left installed, and when
 * it returns %FALSE, it is removed, and @notify is called with @user_data.
 * 
 * <note>When the "main-context" property is changed, all existing sources are
 * destroyed, so if you would like to keep this source installed across changes
 * to an object's context, you should connect to the "notify::main-context"
 * signal and re-add this source.</note>
 * 
 * Returns: the source ID of the idle handler.
 * 
 * Since: 1.0.
 **/
guint
gnetwork_object_add_idle_full (GNetworkObject * object, gint priority,
			       GNetworkObjectSourceFunc func, gpointer user_data,
			       GDestroyNotify notify)
{
  GNetworkObjectSourceCallbackData *data;
  GSource *source;
  guint retval;

  g_return_val_if_fail (GNETWORK_IS_OBJECT (object), 0);
  g_return_val_if_fail (func != NULL, 0);
  g_return_val_if_fail (notify == NULL || user_data != NULL, 0);

  source = g_idle_source_new ();

  data = g_new0 (GNetworkObjectSourceCallbackData, 1);

  data->object = object;
  data->func.source_func = func;
  data->user_data = user_data;
  data->notify = notify;

  g_source_set_priority (source, priority);
  g_source_set_callback (source, (GSourceFunc) source_callback_proxy, data,
			 (GDestroyNotify) gnetwork_object_source_callback_data_free);

  retval = g_source_attach (source, object->_priv->context);

  g_hash_table_insert (object->_priv->sources, GUINT_TO_POINTER (retval), source);
  g_source_unref (source);

  return retval;
}


/**
 * gnetwork_object_add_timeout:
 * @object: the object.
 * @interval: the time between runs of this function.
 * @func: the function to run.
 * @user_data: the user data to pass to the function.
 * 
 * Attaches a timeout handler to the main context of @object. See
 * gnetwork_object_add_timeout_full() for more information.
 * 
 * Since: 1.0.
 **/

/**
 * gnetwork_object_add_timeout_full:
 * @object: the object.
 * @interval: the time between runs of the function.
 * @priority: the priority to run the function.
 * @func: the function to run.
 * @user_data: the user data to pass to the function.
 * @notify: a function to be called on the user data after the handler has been removed.
 * 
 * Attaches an timeout handler to the main context of @object. An timeout
 * handler is a function which is run every @interval miliseconds when no other
 * higher priority handlers are to be run. When @func returns %TRUE, the handler
 * is left installed, and when it returns %FALSE, it is removed, and @notify is
 * called with @user_data.
 * 
 * <note>When the "main-context" property is changed, all existing sources are
 * destroyed, so if you would like to keep this source installed across changes
 * to an object's context, you should connect to the "notify::main-context"
 * signal and re-add this source when that signal is emitted.</note>
 * 
 * Returns: the source ID of the timeout handler.
 * 
 * Since: 1.0.
 **/
guint
gnetwork_object_add_timeout_full (GNetworkObject * object, gint interval, gint priority,
				  GNetworkObjectSourceFunc func, gpointer user_data,
				  GDestroyNotify notify)
{
  GNetworkObjectSourceCallbackData *data;
  GSource *source;
  guint retval;

  g_return_val_if_fail (GNETWORK_IS_OBJECT (object), 0);
  g_return_val_if_fail (func != NULL, 0);
  g_return_val_if_fail (notify == NULL || user_data != NULL, 0);

  source = g_timeout_source_new (interval);

  data = g_new0 (GNetworkObjectSourceCallbackData, 1);

  data->object = object;
  data->func.source_func = func;
  data->user_data = user_data;
  data->notify = notify;

  g_source_set_priority (source, priority);
  g_source_set_callback (source, (GSourceFunc) source_callback_proxy, data,
			 (GDestroyNotify) gnetwork_object_source_callback_data_free);

  retval = g_source_attach (source, object->_priv->context);

  g_hash_table_insert (object->_priv->sources, GUINT_TO_POINTER (retval), source);
  g_source_unref (source);

  return retval;
}


/**
 * gnetwork_object_add_io_watch:
 * @object: the object.
 * @channel: the IO channel to watch.
 * @condition: the condition flags to watch for.
 * @func: the function to run.
 * @user_data: the user data to pass to the function.
 * 
 * Attaches an IO handler to the main context of @object. See
 * gnetwork_object_add_io_watch_full() for more information.
 * 
 * Since: 1.0.
 **/

/**
 * gnetwork_object_add_io_watch_full:
 * @object: the object.
 * @channel: the IO channel to watch.
 * @condition: the condition flags to watch for.
 * @priority: the priority to run the function.
 * @func: the function to run.
 * @user_data: the user data to pass to the function.
 * @notify: a function to be called on the user data after the handler has been removed.
 * 
 * Attaches an IO channel handler to @object. An IO channel handler is a
 * function which is run every time @condition is met on @channel. When @func
 * returns %TRUE, the handler is left installed, and when it returns %FALSE, it
 * is removed, and @notify is called with @user_data.
 * 
 * <note>When the "main-context" property is changed, all existing sources are
 * destroyed, so if you would like to keep this source installed across changes
 * to an object's context, you should connect to the "notify::main-context"
 * signal and re-add this source when that signal is emitted.</note>
 * 
 * Returns: the source ID of the IO handler.
 * 
 * Since: 1.0.
 **/
guint
gnetwork_object_add_io_watch_full (GNetworkObject * object, GIOChannel * channel,
				   GIOCondition condition, gint priority, GNetworkObjectIoFunc func,
				   gpointer user_data, GDestroyNotify notify)
{
  GNetworkObjectSourceCallbackData *data;
  GSource *source;
  guint retval;

  g_return_val_if_fail (GNETWORK_IS_OBJECT (object), 0);
  g_return_val_if_fail (channel != NULL, 0);
  g_return_val_if_fail (_gnetwork_flags_value_is_valid (G_TYPE_IO_CONDITION, condition), 0);
  g_return_val_if_fail (func != NULL, 0);
  g_return_val_if_fail (notify == NULL || user_data != NULL, 0);

  source = g_io_create_watch (channel, condition);

  data = g_new0 (GNetworkObjectSourceCallbackData, 1);

  data->object = object;
  data->func.io_func = func;
  data->user_data = user_data;
  data->notify = notify;

  g_source_set_priority (source, priority);
  g_source_set_callback (source, (GSourceFunc) io_callback_proxy, data,
			 (GDestroyNotify) gnetwork_object_source_callback_data_free);

  retval = g_source_attach (source, object->_priv->context);

  g_hash_table_insert (object->_priv->sources, GUINT_TO_POINTER (retval), source);
  g_source_unref (source);

  return retval;
}


/**
 * gnetwork_object_attach_source:
 * @object: the object.
 * @source: the source to attach.
 * 
 * Attaches a user-created event source to @object.
 * 
 * <note>When the "main-context" property is changed, all existing sources are
 * destroyed, so if you would like to keep this source installed across changes
 * to an object's context, you should connect to the "notify::main-context"
 * signal and re-add this source when that signal is emitted.</note>
 * 
 * Returns: the source ID of the handler.
 * 
 * Since: 1.0.
 **/
guint
gnetwork_object_attach_source (GNetworkObject * object, GSource * source)
{
  guint retval;

  g_return_val_if_fail (GNETWORK_IS_OBJECT (object), 0);
  g_return_val_if_fail (source != NULL, 0);

  retval = g_source_attach (source, object->_priv->context);
  g_hash_table_insert (object->_priv->sources, GUINT_TO_POINTER (retval), source);

  return retval;
}


/**
 * gnetwork_object_remove_source:
 * @object: the object to use.
 * @tag: the source ID to remove.
 * 
 * Removes the event source (idle, timeout, IO, or user-defined handler) which
 * has the ID of @tag from @object.
 * 
 * Returns: %TRUE if the source was found and removed, %FALSE if it was not.
 * 
 * Since: 1.0
 **/
gboolean
gnetwork_object_remove_source (GNetworkObject * object, guint tag)
{
  g_return_val_if_fail (GNETWORK_IS_OBJECT (object), FALSE);
  g_return_val_if_fail (tag != 0, FALSE);

  return g_hash_table_remove (object->_priv->sources, GUINT_TO_POINTER (tag));
}


/**
 * gnetwork_object_class_install_param:
 * @klass: the socket class.
 * @pspec: the parameter definition.
 * 
 * Installs @pspec into @klass' possible #GNetworkParams parameters. This
 * function should be called from a subclass' class_init() function to add any
 * parameters the subclass uses to the list of data parameters available to
 * #GNetworkParams associated with this object type. For example:
 * 
 * <informalexample><programlisting>static void
 * my_socket_class_init (MySocketClass *klass)
 * {
 *   GNetworkObjectClass *object_class = GNETWORK_OBJECT_CLASS (klass);
 * <!-- -->
 *   gnetwork_socket_class_install_param (object_class,
 *     g_param_spec_string ("my-socket-address", "Address",
 *                          "A destination or sender address", NULL,
 *                          G_PARAM_READABLE));
 * }</programlisting></informalexample>
 * 
 * Since: 1.0
 **/
void
gnetwork_object_class_install_param (GNetworkObjectClass * klass, GParamSpec * pspec)
{
  g_return_if_fail (GNETWORK_IS_OBJECT_CLASS (klass));
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));

  gnetwork_params_install_param (pspec, G_OBJECT_CLASS_TYPE (klass));
}


/**
 * gnetwork_object_class_find_param:
 * @klass: the class to examine.
 * @name: the name of the data parameter.
 * 
 * Retrieves the description of the data parameter in @klass which matches
 * @name. The returned value should not be modified or freed.
 * 
 * Returns: a #GParamSpec structure.
 * 
 * Since: 1.0
 **/
GParamSpec *
gnetwork_object_class_find_param (GNetworkObjectClass * klass, const gchar * name)
{
  g_return_val_if_fail (GNETWORK_IS_OBJECT_CLASS (klass), NULL);
  g_return_val_if_fail (name != NULL && name[0] != '\0', NULL);

  return gnetwork_params_find (G_OBJECT_CLASS_TYPE (klass), name);
}


/**
 * gnetwork_object_class_list_params:
 * @klass: the class to examine.
 * @n_params: a location to store the number of returned items.
 * 
 * Retrieves an array of all the currently installed data parameters from
 * @klass. The number of returned items will be placed in @n_params. This
 * function retrieves all available data parameters. To retrieve a list of
 * parameters installed by a particular subclass, use
 * gnetwork_object_class_list_params_for_type(). The returned value must
 * be freed with g_free() when no longer needed.
 * 
 * Returns: an array of the installed data parameters.
 * 
 * Since: 1.0
 **/
GParamSpec **
gnetwork_object_class_list_params (GNetworkObjectClass * klass, guint * n_params)
{
  g_return_val_if_fail (GNETWORK_IS_OBJECT_CLASS (klass), NULL);
  g_return_val_if_fail (n_params != NULL, NULL);

  return gnetwork_params_list_all (G_OBJECT_CLASS_TYPE (klass), n_params);
}


/**
 * gnetwork_object_class_list_params_for_type:
 * @klass: the class to examine.
 * @type: the type to query.
 * 
 * Retrieves a list of the data parameters installed for @type from @klass.
 * The returned list contains only those parameters installed by @type, and must
 * be freed with g_list_free() when no longer needed. For a complete listing,
 * use gnetwork_socket_class_list_params().
 * 
 * Returns: a newly allocated list of #GParamSpec items.
 * 
 * Since: 1.0
 **/
GList *
gnetwork_object_class_list_params_for_type (GNetworkObjectClass * klass, GType type)
{
  g_return_val_if_fail (GNETWORK_IS_OBJECT_CLASS (klass), NULL);
  g_return_val_if_fail (g_type_is_a (G_OBJECT_CLASS_TYPE (klass), type), NULL);

  return gnetwork_params_list_all_for_type (type);
}


static gpointer
gnetwork_gmain_context_ref (gpointer data)
{
  g_main_context_ref (data);

  return data;
}

GType
gnetwork_gmain_context_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      type = g_boxed_type_register_static ("GMainContext", gnetwork_gmain_context_ref,
					   (GBoxedFreeFunc) g_main_context_unref);
    }

  return type;
}
