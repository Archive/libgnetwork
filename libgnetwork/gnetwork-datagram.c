/*
 * GNetwork Library: libgnetwork/gnetwork-datagram.c
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */


#include "gnetwork-datagram.h"

#include "gnetwork-type-builtins.h"
#include "gnetwork-errors.h"
#include "gnetwork-utils.h"

#include "marshal.h"

#include <glib/gi18n.h>
#include <string.h>


enum
{
  RECEIVED,
  SENT,
  ERROR,
  LAST_SIGNAL
};


typedef struct _EnumString
{
  const guint value;
  const gchar *const str;
}
EnumString;


static gint signals[LAST_SIGNAL] = { 0 };


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_datagram_base_init (gpointer g_iface)
{
  static gboolean initialized = FALSE;

  if (!initialized)
    {
      signals[RECEIVED] =
	g_signal_new ("received",
		      GNETWORK_TYPE_DATAGRAM,
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (GNetworkDatagramIface, received),
		      NULL, NULL, _gnetwork_marshal_VOID__BOXED_POINTER_ULONG,
		      G_TYPE_NONE, 3, G_TYPE_VALUE, G_TYPE_POINTER, G_TYPE_ULONG);
      signals[SENT] =
	g_signal_new ("sent",
		      GNETWORK_TYPE_DATAGRAM,
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (GNetworkDatagramIface, sent),
		      NULL, NULL, _gnetwork_marshal_VOID__BOXED_POINTER_ULONG,
		      G_TYPE_NONE, 3, G_TYPE_VALUE, G_TYPE_POINTER, G_TYPE_ULONG);
      signals[ERROR] =
	g_signal_new ("error",
		      GNETWORK_TYPE_DATAGRAM,
		      (G_SIGNAL_RUN_FIRST | G_SIGNAL_DETAILED),
		      G_STRUCT_OFFSET (GNetworkDatagramIface, error),
		      NULL, NULL, _gnetwork_marshal_VOID__BOXED_BOXED,
		      G_TYPE_NONE, 2, G_TYPE_VALUE, G_TYPE_ERROR);

      g_object_interface_install_property (g_iface,
					   g_param_spec_enum ("status", _("Datagram Status"),
							      _("The status of this datagram "
								"socket."),
							      GNETWORK_TYPE_DATAGRAM_STATUS,
							      GNETWORK_DATAGRAM_CLOSED,
							      G_PARAM_READABLE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_ulong ("bytes-received",
							       _("Bytes Received"),
							       _("The number of bytes received "
								 "through this datagram socket."),
							       0, G_MAXULONG, 0, G_PARAM_READABLE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_ulong ("bytes-sent", _("Bytes Sent"),
							       _("The number of bytes sent through "
								 "this datagram socket."),
							       0, G_MAXULONG, 0, G_PARAM_READABLE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_uint ("buffer-size", _("Buffer Size"),
							      _("The maximum size in bytes of "
								"outgoing and incoming data "
								"packets."), 0, G_MAXUINT, 2048,
							      G_PARAM_READWRITE));

      initialized = TRUE;
    }
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_datagram_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      static const GTypeInfo info = {
	sizeof (GNetworkDatagramIface),	/* class_size */
	gnetwork_datagram_base_init,	/* base_init */
	NULL,			/* base_finalize */
	NULL,
	NULL,			/* class_finalize */
	NULL,			/* class_data */
	0,
	0,			/* n_preallocs */
	NULL
      };

      type = g_type_register_static (G_TYPE_INTERFACE, "GNetworkDatagram", &info, 0);

      g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
    }

  return type;
}


/**
 * gnetwork_datagram_open:
 * @datagram: the datagram to open.
 *
 * Starts the datagram process for @datagram.
 *
 * Since: 1.0
 **/
void
gnetwork_datagram_open (GNetworkDatagram * datagram)
{
  GNetworkDatagramIface *iface;

  g_return_if_fail (GNETWORK_IS_DATAGRAM (datagram));

  iface = GNETWORK_DATAGRAM_GET_IFACE (datagram);

  g_return_if_fail (iface->open != NULL);

  (*iface->open) (datagram);
}


/**
 * gnetwork_datagram_close:
 * @datagram: the datagram to close.
 *
 * Closes the @datagram in question.
 *
 * Since: 1.0
 **/
void
gnetwork_datagram_close (GNetworkDatagram * datagram)
{
  GNetworkDatagramIface *iface;

  g_return_if_fail (GNETWORK_IS_DATAGRAM (datagram));

  iface = GNETWORK_DATAGRAM_GET_IFACE (datagram);

  g_return_if_fail (iface->close != NULL);

  (*iface->close) (datagram);
}


/**
 * gnetwork_datagram_send:
 * @datagram: the datagram object to send through.
 * @destination: implementation-specific destination information.
 * @data: the data to send.
 * @length: the length of @data in bytes, or %-1.
 *
 * Sends the data in @data through @datagram to @destination. If @length is less
 * than zero, @data is assumed to be terminated by %0, and the necessary
 * calculations for @length will be performed.
 *
 * Since: 1.0
 **/
void
gnetwork_datagram_send (GNetworkDatagram * datagram, const GValue * destination, gconstpointer data,
			glong length)
{
  GNetworkDatagramIface *iface;

  g_return_if_fail (GNETWORK_IS_DATAGRAM (datagram));
  g_return_if_fail (data != NULL);
  g_return_if_fail (length != 0);

  iface = GNETWORK_DATAGRAM_GET_IFACE (datagram);

  g_return_if_fail (iface->send != NULL);

  (*iface->send) (datagram, destination, data, length);
}


/**
 * gnetwork_datagram_received:
 * @datagram: the datagram to use.
 * @source: implementation-specific information about the sender.
 * @data: the data being recieved.
 * @length: the length of @data in bytes.
 *
 * Emits the "received" signal for @datagram, using the values in @source,
 * @data and @length. Implementations of the #GNetworkDatagramIface interface
 * should use this function to emit the proper signal when data has been 
 * received.
 *
 * Since: 1.0
 **/
void
gnetwork_datagram_received (GNetworkDatagram * datagram, const GValue * source,
			    gconstpointer data, gulong length)
{
  g_return_if_fail (GNETWORK_IS_DATAGRAM (datagram));
  g_return_if_fail (data != NULL);
  g_return_if_fail (length > 0);

  if (source != NULL)
    {
      GValue value = { 0 };

      g_value_init (&value, G_VALUE_TYPE (source));
      g_value_copy (source, &value);
      g_signal_emit (datagram, signals[RECEIVED], 0, &value, data, length);
      g_value_unset (&value);
    }
  else
    {
      g_signal_emit (datagram, signals[RECEIVED], 0, NULL, data, length);
    }
}


/**
 * gnetwork_datagram_sent:
 * @datagram: the datagram to use.
 * @destination: implementation-specific information about the destination.
 * @data: the data being recieved.
 * @length: the length of @data in bytes.
 *
 * Emits the "sent" signal for @datagram, using the values in @source,
 * @data and @length. Implementations of the #GNetworkDatagramIface interface
 * should use this function to emit the proper signal when data has been 
 * sent.
 *
 * Since: 1.0
 **/
void
gnetwork_datagram_sent (GNetworkDatagram * datagram, const GValue * destination,
			gconstpointer data, gulong length)
{
  g_return_if_fail (GNETWORK_IS_DATAGRAM (datagram));
  g_return_if_fail (data != NULL);
  g_return_if_fail (length > 0);

  if (destination != NULL)
    {
      GValue value = { 0 };

      g_value_init (&value, G_VALUE_TYPE (destination));
      g_value_copy (destination, &value);
      g_signal_emit (datagram, signals[SENT], 0, &value, data, length);
      g_value_unset (&value);
    }
  else
    {
      g_signal_emit (datagram, signals[SENT], 0, NULL, data, length);
    }
}


/**
 * gnetwork_datagram_error:
 * @datagram: the datagram to use.
 * @info: implementation-specific information about the target who caused the error.
 * @error: the error structure.
 *
 * Emits the "error" signal for @datagram, using @info and @error.
 *
 * Since: 1.0
 **/
void
gnetwork_datagram_error (GNetworkDatagram * datagram, const GValue * info, const GError * error)
{
  g_return_if_fail (GNETWORK_IS_DATAGRAM (datagram));
  g_return_if_fail (error != NULL);

  if (info != NULL)
    {
      GValue value = { 0 };

      g_value_init (&value, G_VALUE_TYPE (info));
      g_value_copy (info, &value);
      g_signal_emit (datagram, signals[ERROR], error->domain, &value, error);
      g_value_unset (&value);
    }
  else
    {
      g_signal_emit (datagram, signals[ERROR], error->domain, NULL, error);
    }
}


G_LOCK_DEFINE_STATIC (quark);

GQuark
gnetwork_datagram_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (quark);

  if (quark == 0)
    {
      quark = g_quark_from_static_string ("gnetwork-datagram-error");
    }

  G_UNLOCK (quark);

  return quark;
}
