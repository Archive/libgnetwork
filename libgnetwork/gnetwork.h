/*
 * GNetwork Library: libgnetwork/gnetwork.h
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_H__
#define __GNETWORK_H__ 1

#include "gnetwork-buffer.h"
#include "gnetwork-data-socket.h"
#include "gnetwork-interfaces.h"
#include "gnetwork-local-data-socket.h"
#include "gnetwork-object.h"
#include "gnetwork-server-socket.h"
#include "gnetwork-socket.h"

#include "gnetwork-connection.h"
#include "gnetwork-datagram.h"
#include "gnetwork-dns.h"
#include "gnetwork-errors.h"
#include "gnetwork-ip-address.h"
#include "gnetwork-ip-multicast.h"
#include "gnetwork-server.h"
#include "gnetwork-ssl.h"
#include "gnetwork-tcp-connection.h"
#include "gnetwork-tcp-server.h"
#include "gnetwork-tcp-proxy.h"
#include "gnetwork-udp-datagram.h"
#include "gnetwork-unix-connection.h"
#include "gnetwork-unix-server.h"
#include "gnetwork-threads.h"

#endif /* !__GNETWORK_H__ */
