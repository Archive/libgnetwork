/* 
 * GNetwork Library: libgnetwork/gnetwork-ssl-private.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_SSL_PRIVATE_H__
#define __GNETWORK_SSL_PRIVATE_H__

#include "gnetwork-connection.h"
#include "gnetwork-ssl.h"

G_BEGIN_DECLS


#ifdef _USE_SSL


/* ************************************************************************** *
 *  Authentication                                                            *
 * ************************************************************************** */

#define GNETWORK_TYPE_SSL_AUTH	(_gnetwork_ssl_auth_get_type ())
#define GNETWORK_SSL_AUTH(ptr)	((GNetworkSslAuth *) (ptr))


typedef struct _GNetworkSslAuth GNetworkSslAuth;

GType _gnetwork_ssl_auth_get_type (void) G_GNUC_CONST;

GNetworkSslAuth *_gnetwork_ssl_auth_new (GNetworkSslAuthType auth_type,
					 GNetworkConnectionType connection_type);
GNetworkSslAuth *_gnetwork_ssl_auth_ref (GNetworkSslAuth * auth);
void _gnetwork_ssl_auth_unref (GNetworkSslAuth * auth);

GNetworkSslAuthType _gnetwork_ssl_auth_get_auth_type (GNetworkSslAuth * auth);
GNetworkConnectionType _gnetwork_ssl_auth_get_connection_type (GNetworkSslAuth * auth);

/* Certificate Authentication */
void _gnetwork_ssl_auth_set_authority_file (GNetworkSslAuth * auth, const gchar * file);
void _gnetwork_ssl_auth_set_certs_and_keys_files (GNetworkSslAuth * auth, const gchar * certs_file,
						  const gchar * keys_file);

void _gnetwork_ssl_auth_set_server_hostname (GNetworkSslAuth * auth, const gchar * hostname);
G_CONST_RETURN gchar *_gnetwork_ssl_auth_get_server_hostname (GNetworkSslAuth * auth);


/* ************************************************************************** *
 *  SSL GIOChannel                                                            *
 * ************************************************************************** */

GIOChannel * _gnetwork_io_channel_ssl_new (GIOChannel * parent, GNetworkSslAuth * auth,
					   GError ** error);

#endif /* _USE_SSL */


G_END_DECLS

#endif /* !__GNETWORK_SSL_H__ */
