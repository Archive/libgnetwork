/*
 * GNetwork Library: libgnetwork/gnetwork-server.h
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GNETWORK_SERVER_H__
#define __GNETWORK_SERVER_H__

#include "gnetwork-macros.h"
#include "gnetwork-connection.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_SERVER		(gnetwork_server_get_type ())
#define GNETWORK_SERVER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNETWORK_TYPE_SERVER, GNetworkServer))
#define GNETWORK_IS_SERVER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNETWORK_TYPE_SERVER))
#define GNETWORK_SERVER_GET_IFACE(obj)	(G_TYPE_INSTANCE_GET_INTERFACE ((obj), GNETWORK_TYPE_SERVER, GNetworkServerIface))
#define GNETWORK_SERVER_CALL_PARENT(obj,method,args) \
  (GNETWORK_INTERFACE_CALL_PARENT_WITH_DEFAULT (GNETWORK_SERVER_GET_IFACE, GNetworkConnectionIface, obj, method, args, NULL))

#define GNETWORK_SERVER_ERROR		(gnetwork_server_error_get_quark ())


typedef enum /* <prefix=GNETWORK_SERVER_STATUS> */
{
  GNETWORK_SERVER_CLOSING,
  GNETWORK_SERVER_CLOSED,
  GNETWORK_SERVER_OPENING,
  GNETWORK_SERVER_OPEN
}
GNetworkServerStatus;


typedef enum /* <prefix=GNETWORK_SERVER_ERROR> */
{
  GNETWORK_SERVER_ERROR_INTERNAL,

  GNETWORK_SERVER_ERROR_TOO_MANY_CONNECTIONS,

  GNETWORK_SERVER_ERROR_NO_MEMORY,
  GNETWORK_SERVER_ERROR_PERMISSIONS,
  GNETWORK_SERVER_ERROR_TOO_MANY_PROCESSES,
  GNETWORK_SERVER_ERROR_ALREADY_EXISTS
}
GNetworkServerError;


typedef struct _GNetworkServer GNetworkServer;
typedef struct _GNetworkServerIface GNetworkServerIface;

typedef GNetworkConnection *(*GNetworkServerCreateFunc) (GNetworkServer * server,
							 const GValue * server_data,
							 gpointer user_data,
							 GError ** error);

typedef void (*GNetworkServerFunc) (GNetworkServer * server);
typedef void (*GNetworkServerSetCreateFunc) (GNetworkServer * server, GNetworkServerCreateFunc func,
					     gpointer data, GDestroyNotify notify);


struct _GNetworkServerIface
{
  /* < private > */
  GTypeInterface g_iface;

  /* < public > */
  /* Signals */
  void (*new_connection)       (GNetworkServer * server,
				GNetworkConnection * connection);
  void (*error)		       (GNetworkServer * server,
				GError * error);

  /* Methods */
  GNetworkServerFunc		open;
  GNetworkServerFunc		close;
  GNetworkServerSetCreateFunc	set_create_func;

  /* < private > */
  void (*__gnetwork_reserved_1);
  void (*__gnetwork_reserved_2);
  void (*__gnetwork_reserved_3);
  void (*__gnetwork_reserved_4);
};


GType gnetwork_server_get_type (void) G_GNUC_CONST;

/* Methods */
void gnetwork_server_open (GNetworkServer * server);
void gnetwork_server_close (GNetworkServer * server);
void gnetwork_server_set_create_func (GNetworkServer * server, GNetworkServerCreateFunc func,
				      gpointer data, GDestroyNotify notify);

/* Signals */
void gnetwork_server_new_connection (GNetworkServer * server, GNetworkConnection * connection);
void gnetwork_server_error (GNetworkServer * server, const GError * error);


/* Error Reporting */
G_CONST_RETURN gchar *gnetwork_server_strerror (GNetworkServerError error);
GQuark gnetwork_server_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* __GNETWORK_SERVER_H__ */
