/* 
 * GNetwork Library: libgnetwork/gnetwork-tcp-proxy-private.h
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_TCP_PROXY_PRIVATE_H__
#define __GNETWORK_TCP_PROXY_PRIVATE_H__ 1

#include "gnetwork-dns.h"
#include "gnetwork-tcp-proxy.h"
#include "gnetwork-utils.h"

G_BEGIN_DECLS


void _gnetwork_tcp_proxy_initialize (void);
void _gnetwork_tcp_proxy_shutdown (void);

void _gnetwork_io_channel_proxy_new (GIOChannel * parent, GNetworkTcpProxyType type,
				     const GNetworkDnsEntry * destination, guint16 port,
				     GNetworkIOCallback func, gpointer data, GDestroyNotify notify);

gchar *_gnetwork_tcp_proxy_get_host (GNetworkTcpProxyType type);
guint _gnetwork_tcp_proxy_get_port (GNetworkTcpProxyType type);
GNetworkTcpProxyError _gnetwork_tcp_proxy_error_from_errno (gint en);
gchar *_gnetwork_tcp_proxy_strerror (GNetworkTcpProxyError error, GNetworkTcpProxyType type,
				     const GNetworkDnsEntry * destination);


G_END_DECLS

#endif /* __GNETWORK_TCP_PROXY_PRIVATE_H__ */
