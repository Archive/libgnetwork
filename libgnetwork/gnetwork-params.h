/*
 * GNetwork Library: libgnetwork/gnetwork-params.h
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_PARAMS_H__
#define __GNETWORK_PARAMS_H__

#include <glib-object.h>

G_BEGIN_DECLS


#define GNETWORK_TYPE_PARAMS \
  (gnetwork_params_get_type ())
#define GNETWORK_PARAMS(ptr) \
  ((GNetworkParams *) (ptr))
#define GNETWORK_IS_PARAMS(ptr) \
  (ptr != NULL && G_TYPE_FROM_CLASS (ptr) == GNETWORK_TYPE_PARAMS)


typedef struct _GNetworkParams GNetworkParams;


GType gnetwork_params_get_type (void) G_GNUC_CONST;

GNetworkParams *gnetwork_params_new (GType type);
GType gnetwork_params_get_associated_type (GNetworkParams * params);

GNetworkParams *gnetwork_params_dup (GNetworkParams * params, const gchar *first_name, ...);
GNetworkParams *gnetwork_params_dupv (GNetworkParams * params, GStrv names);

void gnetwork_params_copy_param (GNetworkParams * src, GNetworkParams * dest, const gchar * name);

void gnetwork_params_set (GNetworkParams * params, const gchar *first_name, ...);
void gnetwork_params_set_valist (GNetworkParams * params, const gchar * first_name,
				 va_list var_args);
void gnetwork_params_set_value (GNetworkParams * params, const gchar *name, const GValue * value);

void gnetwork_params_get (GNetworkParams * params, const gchar *first_name, ...);
void gnetwork_params_get_valist (GNetworkParams * params, const gchar *first_name,
				 va_list var_args);
void gnetwork_params_get_value (GNetworkParams * params, const gchar *name, GValue * value);

gboolean gnetwork_params_is_set (GNetworkParams * params, const gchar * name);
gboolean gnetwork_params_unset (GNetworkParams * params, const gchar * name);

GList *gnetwork_params_list (GNetworkParams * params);
GList *gnetwork_params_list_for_type (GNetworkParams * params, GType type);

GNetworkParams *gnetwork_params_ref (GNetworkParams * params);
void gnetwork_params_unref (GNetworkParams * params);

void gnetwork_params_install_param (GParamSpec *pspec, GType type);
GParamSpec *gnetwork_params_find (GType type, const gchar * name);
GParamSpec **gnetwork_params_list_all (GType type, guint * n_params);
GList *gnetwork_params_list_all_for_type (GType type);


G_END_DECLS

#endif /* __GNETWORK_PARAMS_H__ */
