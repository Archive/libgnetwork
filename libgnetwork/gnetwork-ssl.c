/*
 * GNetwork Library: libgnetwork/gnetwork-ssl.c
 *
 * Copyright (C) 2003, 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-ssl-private.h"

#include "gnetwork-connection.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"

#include "marshal.h"

#include <glib/gi18n.h>

#include <stdlib.h>


/* ************************************************************************** *
 *  Certificate Manipulation                                                  *
 * ************************************************************************** */

/* SSL-Only */
#if defined _USE_SSL

#if defined _USE_GNUTLS
# include <gnutls/gnutls.h>
# include <gnutls/x509.h>
# define GNUTLS_DATUM(ptr)	((gnutls_datum *) (ptr))
#elif defined _USE_OPENSSL
# include <openssl/ssl.h>
# define X509_DATA(ptr)		((X509 *) (ptr))
#endif /* _USE_GNUTLS, _USE_OPENSSL */


/* ******* *
 *  Types  *
 * ******* */

#define SECONDS_IN_MINUTE	(60)
#define SECONDS_IN_HOUR		(SECONDS_IN_MINUTE * 60)
#define SECONDS_IN_DAY		(SECONDS_IN_HOUR * 24)
#define SECONDS_IN_WEEK		(SECONDS_IN_DAY * 7)
#define STARTING_TIME_STR_SIZE	(128)


struct _GNetworkSslCert
{
  GTypeClass g_class;

#if defined _USE_GNUTLS
  gnutls_datum *data;
  gnutls_x509_crt cert;
#elif defined _USE_OPENSSL
  X509 *data;
#endif /* _USE_GNUTLS, _USE_OPENSSL */

  GNetworkSslCertType cert_type:2;
};


/* ************* *
 *  Private API  *
 * ************* */

static inline GNetworkSslCertErrorFlags
get_ssl_cert_error_from_error_num (gint error_num)
{
  GNetworkSslCertErrorFlags error = GNETWORK_SSL_CERT_ERROR_UNKNOWN;

#if defined _USE_GNUTLS
/* GNUTLS Version */

  /* FIXME: How/should we handle error_num < 0? */
  if (error > 0)
    {
      if (error_num & GNUTLS_CERT_NOT_TRUSTED)
	error |= GNETWORK_SSL_CERT_ERROR_NOT_TRUSTED;

      if (error_num & GNUTLS_CERT_INVALID)
	error |= GNETWORK_SSL_CERT_ERROR_INVALID;

      if (error_num & GNUTLS_CERT_REVOKED)
	error |= GNETWORK_SSL_CERT_ERROR_REVOKED;
    }

#elif defined _USE_OPENSSL
/* OpenSSL Version */

  g_warning ("%s: FIXME: Implement OpenSSL error -> GNetworkSslCertError mapping.", G_STRLOC);

#endif /* _USE_GNUTLS, _USE_OPENSSL */

  return error;
}

static GNetworkSslCert *
_gnetwork_ssl_cert_new (gconstpointer data, GNetworkSslCertType type)
{
  GNetworkSslCert *cert;

  cert = g_new (GNetworkSslCert, 1);
  cert->g_class.g_type = GNETWORK_TYPE_SSL_CERT;

  cert->cert_type = type;

#if defined _USE_GNUTLS
  cert->cert = NULL;

  cert->data = g_new0 (gnutls_datum, 1);
  cert->data->size = ((gnutls_datum *) (data))->size;
  cert->data = g_memdup (((gnutls_datum *) (data))->data, ((gnutls_datum *) (data))->size);
#elif defined _USE_OPENSSL
  cert->data = X509_dup (data);
#endif /* _USE_GNUTLS */

  return cert;
}

#endif /* _USE_SSL */
/* End SSL-Only */



/* ************ *
 *  Public API  *
 * ************ */

GType
gnetwork_ssl_cert_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      type = g_boxed_type_register_static ("GNetworkSslCert",
					   (GBoxedCopyFunc) gnetwork_ssl_cert_dup,
					   (GBoxedFreeFunc) gnetwork_ssl_cert_free);
    }

  return type;
}


/**
 * gnetwork_ssl_cert_dup:
 * @cert: the certificate to duplicate.
 * 
 * Creates a duplicate of the data in @cert.
 * 
 * Returns: a copy of @cert.
 * 
 * Since: 1.0
 **/
GNetworkSslCert *
gnetwork_ssl_cert_dup (const GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  GNetworkSslCert *retval;

  g_return_val_if_fail (cert == NULL || GNETWORK_IS_SSL_CERT (cert), NULL);

  if (cert != NULL)
    retval = _gnetwork_ssl_cert_new (cert->data, cert->cert_type);
  else
    retval = NULL;

  return retval;
#else
  return NULL;
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_free:
 * @cert: the certificate to free.
 * 
 * Destroys the data in @cert.
 * 
 * Since: 1.0
 **/
void
gnetwork_ssl_cert_free (GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_if_fail (cert == NULL || GNETWORK_IS_SSL_CERT (cert));

  if (cert == NULL)
    return;

#if defined _USE_GNUTLS

  gnutls_x509_crt_deinit (cert->cert);
  g_free (cert->data->data);
  g_free (cert->data);

#elif defined _USE_OPENSSL
  
  X509_free (cert->cert);

#endif /* _USE_GNUTLS, _USE_OPENSSL */

  g_free (cert);
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_get_cert_type:
 * @cert: the certificate to examine.
 * 
 * Retrieves the type of certificate stored in @cert.
 * 
 * Returns: the type of certificate in @cert.
 * 
 * Since: 1.0
 **/
GNetworkSslCertType
gnetwork_ssl_cert_get_cert_type (const GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_val_if_fail (cert != NULL, GNETWORK_SSL_CERT_INVALID);

  return cert->cert_type;
#else
  return GNETWORK_SSL_CERT_INVALID;
#endif /* _USE_SSL */
}

#if 0
/**
 * gnetwork_ssl_cert_get_name:
 * @cert: the certificate to examine.
 * 
 * Retrieves the name of @cert. The returned value should not be modified or freed.
 * 
 * Returns: the name of @cert.
 * 
 * Since: 1.0
 **/
gchar *
gnetwork_ssl_cert_get_name (const GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  gchar *retval;

  g_return_val_if_fail (GNETWORK_IS_SSL_CERT (cert), NULL);
  g_return_val_if_fail (cert->cert_type == GNETWORK_SSL_CERT_X509, NULL);

  retval = NULL;

#if defined _USE_GNUTLS
  {
    
  }
#elif defined _USE_OPENSSL
  
#endif /* _USE_GNUTLS, _USE_OPENSSL */

  return retval;
#else
  return NULL;
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_get_email:
 * @cert: the certificate to examine.
 * 
 * Retrieves the e-mail address of the person or organization which owns @cert.
 * The returned value should not be modified or freed.
 * 
 * Returns: the e-mail address for @cert.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_ssl_cert_get_email (GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_val_if_fail (cert != NULL, NULL);
  g_return_val_if_fail (cert->cert_type == GNETWORK_SSL_CERT_X509, NULL);

  return cert->email;
#else
  return NULL;
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_get_organization:
 * @cert: the certificate to examine.
 * 
 * Retrieves the name of the organization which owns @cert. The returned value
 * should not be modified or freed.
 * 
 * Returns: the organization name for @cert.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_ssl_cert_get_organization (GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_val_if_fail (cert != NULL, NULL);
  g_return_val_if_fail (cert->cert_type == GNETWORK_SSL_CERT_X509, NULL);

  return cert->organization;
#else
  return NULL;
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_get_department:
 * @cert: the certificate to examine.
 * 
 * Retrieves the name of the department which owns @cert. The returned value
 * should not be modified or freed.
 * 
 * Returns: the department name for @cert.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_ssl_cert_get_department (GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_val_if_fail (cert != NULL, NULL);
  g_return_val_if_fail (cert->cert_type == GNETWORK_SSL_CERT_X509, NULL);

  return cert->department;
#else
  return NULL;
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_get_city:
 * @cert: the certificate to examine.
 * 
 * Retrieves the name of the city where the owner of @cert is located. The
 * returned value should not be modified or freed.
 * 
 * Returns: the city name for @cert.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_ssl_cert_get_city (GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_val_if_fail (cert != NULL, NULL);
  g_return_val_if_fail (cert->cert_type == GNETWORK_SSL_CERT_X509, NULL);

  return cert->city;
#else
  return NULL;
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_get_province:
 * @cert: the certificate to examine.
 * 
 * Retrieves the name of the province where the owner of @cert is located. The
 * returned value should not be modified or freed.
 * 
 * Returns: the province name for @cert.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_ssl_cert_get_province (GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_val_if_fail (cert != NULL, NULL);
  g_return_val_if_fail (cert->cert_type == GNETWORK_SSL_CERT_X509, NULL);

  return cert->province;
#else
  return NULL;
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_get_country:
 * @cert: the certificate to examine.
 * 
 * Retrieves the name of the country where the owner of @cert is located. The
 * returned value should not be modified or freed.
 * 
 * Returns: the country name for @cert.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_ssl_cert_get_country (GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_val_if_fail (cert != NULL, NULL);
  g_return_val_if_fail (cert->cert_type == GNETWORK_SSL_CERT_X509, NULL);

  return cert->country;
#else
  return NULL;
#endif /* _USE_SSL */
}

#endif /* 0 */


/**
 * gnetwork_ssl_cert_get_expiration:
 * @cert: the certificate to examine.
 * 
 * Retrieves the UNIX time_t (seconds since the epoch) when @cert will expire.
 * 
 * Returns: the expiration time of @cert.
 * 
 * Since: 1.0
 **/
glong
gnetwork_ssl_cert_get_expiration (const GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_val_if_fail (cert != NULL, -1);
  g_return_val_if_fail (cert->cert_type == GNETWORK_SSL_CERT_X509, -1);

#if defined _USE_GNUTLS
  return gnutls_x509_crt_get_expiration_time (cert->cert);
#elif defined _USE_OPENSSL
  return -1;
#endif /* _USE_GNUTLS, _USE_OPENSSL */
#else
  return -1;
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_get_activation:
 * @cert: the certificate to examine.
 * 
 * Retrieves the UNIX time_t (seconds since the epoch) after which @cert will be
 * activated.
 * 
 * Returns: the activation time of @cert.
 * 
 * Since: 1.0
 **/
glong
gnetwork_ssl_cert_get_activation (const GNetworkSslCert * cert)
{
#ifdef _USE_SSL
  g_return_val_if_fail (cert != NULL, -1);
  g_return_val_if_fail (cert->cert_type == GNETWORK_SSL_CERT_X509, -1);

#if defined _USE_GNUTLS
  return gnutls_x509_crt_get_activation_time (cert->cert);
#elif defined _USE_OPENSSL
  return -1;
#endif /* _USE_GNUTLS, _USE_OPENSSL */
#else
  return -1;
#endif /* _USE_SSL */
}


/**
 * gnetwork_ssl_cert_get_errors:
 * @certificate: the certificate in question.
 * @errors: the bitmask of errors with @certificate.
 * 
 * Generates a list of #GError structures for each flag in @errors. This is
 * primarily useful for retrieving the error strings and their corresponding
 * error flag. The data of the returned list should be freed with
 * g_error_free(), and the list itself should be freed with g_slist_free().
 * 
 * Returns: a newly allocated list of #GError structures.
 * 
 * Since: 1.0
 **/
GSList *
gnetwork_ssl_cert_get_errors (GNetworkSslCert * certificate, GNetworkSslCertErrorFlags errors)
{
  GNetworkSslCertErrorFlags iter;
  GSList *retval;

  g_return_val_if_fail (_gnetwork_flags_value_is_valid (GNETWORK_TYPE_SSL_CERT_ERROR_FLAGS, errors),
			NULL);

  retval = NULL;

  for (iter = 1; iter <= errors; iter = iter << 1)
    {
      if (iter & errors)
	{
	  gchar * str;

	  switch (iter)
	    {
	      /* cert == NULL */
	    case GNETWORK_SSL_CERT_ERROR_NO_CERTIFICATE:
	      str = g_strdup (_("The connection did not provide a certificate."));
	      break;
	    case GNETWORK_SSL_CERT_ERROR_INVALID:
	      str = g_strdup (_("The data received was not a valid certificate."));
	      break;
	    case GNETWORK_SSL_CERT_ERROR_CORRUPTED:
	      str = g_strdup (_("The certificate is corrupt."));
	      break;
	    case GNETWORK_SSL_CERT_ERROR_REVOKED:
	      str = g_strdup (_("The certificate has been revoked, it is most likely a fraud."));
	      break;
	    case GNETWORK_SSL_CERT_ERROR_NOT_TRUSTED:
	      str = g_strdup (_("The certificate has not been signed by a trusted authority."));
	      break;
	    case GNETWORK_SSL_CERT_ERROR_UNSUPPORTED_TYPE:
	      str = g_strdup (_("The certificate is not an X.509 certificate."));
	      break;

	      /* cert != NULL */
	    case GNETWORK_SSL_CERT_ERROR_CANNOT_PARSE:
	      str = g_strdup (_("The certificate could not be parsed."));
	      break;
	    case GNETWORK_SSL_CERT_ERROR_EXPIRED:
#ifdef _USE_SSL
	      {
		const gchar *format;
		glong expiration, difference;
		expiration = gnetwork_ssl_cert_get_expiration (certificate);
		difference = time (NULL) - expiration;

		if (difference > SECONDS_IN_DAY * 7)
		  {
		    format = _("The certificate expired on %x, at %X.");
		  }
		else if (difference > SECONDS_IN_DAY)
		  {
		    format = _("The certificate expired on %A, at %X.");
		  }
		else if (difference > 0)
		  {
		    format = _("The certificate expired today at %X.");
		  }
		else
		  {
		    format = NULL;
		  }

		if (format != NULL)
		  {
		    GDate date;
		    gchar *time_str;
		    gsize result;
		    guint i = 1;

		    g_date_clear (&date, 1);
		    g_date_set_time (&date, expiration);

		    do
		      {
			time_str = g_new0 (gchar, STARTING_TIME_STR_SIZE * i + 1);

			result = g_date_strftime (time_str, STARTING_TIME_STR_SIZE * i, format,
						  &date);

			if (result == 0)
			  {
			    i++;
			    time_str = g_realloc (time_str, STARTING_TIME_STR_SIZE * i + 1);
			  }
		      }
		    while (result == 0);

		    str = g_locale_to_utf8 (time_str, -1, NULL, NULL, NULL);
		    g_free (time_str);
		  }
		else
		  {
		    str = NULL;
		  }
	      }
#else
	      str = g_strdup (_("The certificate has expired"));
#endif /* _USE_SSL */
	      break;
	    case GNETWORK_SSL_CERT_ERROR_NOT_ACTIVATED:
#ifdef _USE_SSL
	      {
		const gchar *format;
		glong activation, difference;

		activation = gnetwork_ssl_cert_get_activation (certificate);
		difference = time (NULL) - activation;

		if (difference > SECONDS_IN_DAY * 7)
		  {
		    format = _("The certificate is not usable until %x, at %X.");
		  }
		else if (difference > SECONDS_IN_DAY)
		  {
		    format = _("The certificate is not usable until %A, at %X.");
		  }
		else if (difference > 0)
		  {
		    format = _("The certificate is not usable until %X.");
		  }
		else
		  {
		    format = NULL;
		  }

		if (format != NULL)
		  {
		    gchar *time_str;
		    GDate date;
		    gsize result;
		    guint i = 1;

		    g_date_clear (&date, 1);
		    g_date_set_time (&date, activation);

		    do
		      {
			time_str = g_new0 (gchar, STARTING_TIME_STR_SIZE * i + 1);

			result = g_date_strftime (time_str, STARTING_TIME_STR_SIZE * i, format,
						  &date);

			if (result == 0)
			  {
			    i++;
			    time_str = g_realloc (time_str, STARTING_TIME_STR_SIZE * i + 1);
			  }
		      }
		    while (result == 0);

		    str = g_locale_to_utf8 (time_str, -1, NULL, NULL, NULL);
		    g_free (time_str);
		  }
		else
		  {
		    str = NULL;
		  }
	      }
#else
	      str = g_strdup (_("The certificate is not usable yet."));
#endif /* _USE_SSL */
	      break;

	    case GNETWORK_SSL_CERT_ERROR_HOSTNAME_MISMATCH:
	      str = g_strdup (_("The certificate is for a different host."));
	      break;

	    default:
	      str = NULL;
	      break;
	    }

	  if (str != NULL)
	    {
	      GError * error = g_error_new_literal (GNETWORK_SSL_CERT_ERROR, iter, NULL);
	      error->message = str;

	      retval = g_slist_prepend (retval, error);
	    }
	}
    }

  return retval;
}


G_LOCK_DEFINE_STATIC (cert_quark);

GQuark
gnetwork_ssl_cert_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (cert_quark);
  if (quark == 0)
    {
      quark = g_quark_from_static_string ("gnetwork-ssl-cert-error");
    }
  G_UNLOCK (cert_quark);

  return quark;
}


/* SSL-Only */
#if defined _USE_SSL

/* ************************************************************************** *
 *  SSL Authentication                                                        *
 * ************************************************************************** */

struct _GNetworkSslAuth
{
  gint ref;

#if defined _USE_GNUTLS
/* GNUTLS Version */

  union
  {
    gpointer cred;
    gnutls_anon_client_credentials anon_client;
    gnutls_anon_server_credentials anon_server;
    gnutls_certificate_credentials certificate;
  }
  cred;

#elif defined _USE_OPENSSL
/* OpenSSL Version */

  SSL_CTX *context;

#endif				/* _USE_GNUTLS, _USE_OPENSSL */

  /* Certificate Stuff */
  gchar *server_hostname;

  /* Type Bits */
  GNetworkSslAuthType auth_type:3;
  GNetworkConnectionType connection_type:2;
};


/* ************* *
 *  Library API  *
 * ************* */

G_LOCK_DEFINE_STATIC (dh_params);

/**
 * _gnetwork_ssl_auth_new:
 * @auth_type: the type of authentication to perform.
 * @connection_type: the connection type this authentication is for.
 *
 * Creates a new #GNetworkSslAuth-struct structure which is automatically used
 * by SSL GIOChannels created with gnetwork_io_channel_ssl_new() to properly
 * authenticate over a network connection.
 *
 * Returns: a new authentication structure.
 *
 * Since: 1.0
 **/
GNetworkSslAuth *
_gnetwork_ssl_auth_new (GNetworkSslAuthType auth_type, GNetworkConnectionType connection_type)
{
  GNetworkSslAuth *auth;

  g_return_val_if_fail (auth_type == GNETWORK_SSL_AUTH_ANONYMOUS &&
			auth_type == GNETWORK_SSL_AUTH_CERTIFICATE, NULL);
  g_return_val_if_fail (connection_type == GNETWORK_CONNECTION_CLIENT ||
			connection_type == GNETWORK_CONNECTION_SERVER, NULL);

  auth = g_new0 (GNetworkSslAuth, 1);

  auth->auth_type = auth_type;
  auth->connection_type = connection_type;

  auth->ref = 1;

#ifdef _USE_GNUTLS
/* GNUTLS Version */

  switch (auth_type)
    {
    case GNETWORK_SSL_AUTH_ANONYMOUS:
      if (connection_type == GNETWORK_CONNECTION_CLIENT)
	{
	  gnutls_anon_allocate_client_credentials (&(auth->cred.anon_client));
	}
      else
	{
	  gnutls_anon_allocate_server_credentials (&(auth->cred.anon_server));
	}
      break;

    case GNETWORK_SSL_AUTH_CERTIFICATE:
      gnutls_certificate_allocate_credentials (&(auth->cred.certificate));

      if (auth->connection_type == GNETWORK_CONNECTION_SERVER)
	{
	  static volatile gnutls_dh_params dh_params = NULL;

	  G_LOCK (dh_params);

	  if (dh_params == NULL)
	    {
	      gnutls_dh_params_init ((gnutls_dh_params *) &dh_params);
	      gnutls_dh_params_generate2 (dh_params, 1024);
	    }

	  gnutls_certificate_set_dh_params (auth->cred.certificate, dh_params);

	  G_UNLOCK (dh_params);
	}
      break;

    default:
      g_assert_not_reached ();
      break;
    }

#elif defined _USE_OPENSSL
/* OpenSSL Version */

  if (connection_type == GNETWORK_CONNECTION_CLIENT)
    {
      auth->context = SSL_CTX_new (TLSv1_client_method ());
    }
  else
    {
      auth->context = SSL_CTX_new (TLSv1_server_method ());
    }

  switch (auth_type)
    {
    case GNETWORK_SSL_AUTH_CERTIFICATE:
      break;
    }
#endif /* _USE_GNUTLS, _USE_OPENSSL */

  return auth;
}


/**
 * _gnetwork_ssl_auth_ref:
 * @auth: the authentication scheme to reference.
 *
 * Increases the reference count of @auth by one. When the reference is no
 * longer needed, it should be released with gnetwork_ssl_auth_unref().
 *
 * Returns: a reference to @auth.
 *
 * Since: 1.0
 **/
GNetworkSslAuth *
_gnetwork_ssl_auth_ref (GNetworkSslAuth * auth)
{
  if (auth == NULL)
    return NULL;

  auth->ref++;

  return auth;
}


/**
 * _gnetwork_ssl_auth_unref:
 * @auth: the authentication scheme to unreference.
 *
 * Decreases the reference count of @auth by one. When the reference count drops
 * to zero, @auth will be destroyed.
 *
 * Since: 1.0
 **/
void
_gnetwork_ssl_auth_unref (GNetworkSslAuth * auth)
{
  if (auth == NULL || auth->ref <= 0)
    return;

  auth->ref--;

  if (auth->ref == 0)
    {
#ifdef _USE_GNUTLS
/* GNUTLS Version */

      switch (auth->auth_type)
	{
	case GNETWORK_SSL_AUTH_ANONYMOUS:
	  if (auth->connection_type == GNETWORK_CONNECTION_CLIENT)
	    {
	      gnutls_anon_free_client_credentials (auth->cred.anon_client);
	    }
	  else
	    {
	      gnutls_anon_free_server_credentials (auth->cred.anon_server);
	    }
	  break;

	case GNETWORK_SSL_AUTH_CERTIFICATE:
	  gnutls_certificate_free_credentials (auth->cred.certificate);
	  break;

	default:
	  g_assert_not_reached ();
	  break;
	}

#elif defined _USE_OPENSSL
/* OpenSSL Version */

      SSL_CTX_free (auth->context);

#endif /* _USE_GNUTLS, _USE_OPENSSL */

      g_free (auth->server_hostname);
    }
}


/**
 * _gnetwork_ssl_auth_get_auth_type:
 * @auth: the authentication scheme to examine.
 * 
 * Retrieves the authentication type used by @auth.
 *
 * Returns: the authentication type.
 * 
 * Since: 1.0
 **/
GNetworkSslAuthType
_gnetwork_ssl_auth_get_auth_type (GNetworkSslAuth * auth)
{
  g_return_val_if_fail (auth != NULL && auth->ref > 0, GNETWORK_SSL_AUTH_INVALID);

  return auth->auth_type;
}


/**
 * _gnetwork_ssl_auth_get_connection_type:
 * @auth: the authentication scheme to examine.
 *
 * Retrieves the connection type of @auth.
 *
 * Returns: the connection type.
 * 
 * Since: 1.0
 **/
GNetworkConnectionType
_gnetwork_ssl_auth_get_connection_type (GNetworkSslAuth * auth)
{
  g_return_val_if_fail (auth != NULL && auth->ref > 0, GNETWORK_CONNECTION_INVALID);

  return auth->connection_type;
}


/**
 * _gnetwork_ssl_auth_set_authority_file:
 * @auth: the authentication scheme to modify.
 * @certs_file: the path to a file containing certificates to use.
 * @keys_file: the path to a file containing the server keys to use.
 *
 * Modifies @auth to use the certificates in @certs_file when authenticating. @certs_file
 * must be a certificate file in PEM format. Server schemes must also use @keys_file.
 *
 * Note: @auth must be a %GNETWORK_SSL_AUTH_CERTIFICATE scheme.
 * 
 * Since: 1.0
 **/
void
_gnetwork_ssl_auth_set_authority_file (GNetworkSslAuth * auth, const gchar * file)
{
  g_return_if_fail (auth != NULL);
  g_return_if_fail (auth->auth_type != GNETWORK_SSL_AUTH_CERTIFICATE);
  g_return_if_fail (file != NULL && g_file_test (file, G_FILE_TEST_EXISTS));

#if defined _USE_GNUTLS
/* GNUTLS Version */

  if (file != NULL)
    gnutls_certificate_set_x509_trust_file (auth->cred.certificate, file, GNUTLS_X509_FMT_PEM);

#elif defined _USE_OPENSSL
/* OpenSSL Version */

  SSL_CTX_use_certificate_chain_file (auth->context, file);

#endif /* _USE_GNUTLS, _USE_OPENSSL */
}


/**
 * _gnetwork_ssl_auth_set_certificate_keys_file:
 * @auth: the authentication scheme to modify.
 * @certs_file: the path to a file containing certificates to use.
 * @keys_file: the path to a file containing the server keys to use.
 *
 * Modifies @auth to use the certificates in @certs_file when authenticating. @certs_file
 * must be a certificate file in PEM format. Server schemes must also use @keys_file.
 *
 * Note: @auth must be a %GNETWORK_SSL_AUTH_CERTIFICATE scheme.
 * 
 * Since: 1.0
 **/
void
_gnetwork_ssl_auth_set_certs_and_keys_files (GNetworkSslAuth * auth, const gchar * certs_file,
					     const gchar * keys_file)
{
  g_return_if_fail (auth != NULL);
  g_return_if_fail (auth->auth_type != GNETWORK_SSL_AUTH_CERTIFICATE);
  g_return_if_fail (certs_file != NULL && g_file_test (certs_file, G_FILE_TEST_EXISTS));
  g_return_if_fail (keys_file != NULL && g_file_test (keys_file, G_FILE_TEST_EXISTS));

#if defined _USE_GNUTLS
/* GNUTLS Version */

    gnutls_certificate_set_x509_key_file (auth->cred.certificate, certs_file, keys_file,
					  GNUTLS_X509_FMT_PEM);

#elif defined _USE_OPENSSL
/* OpenSSL Version */

  SSL_CTX_use_certificate_chain_file (auth->context, file);

#endif /* _USE_GNUTLS, _USE_OPENSSL */
}


/**
 * _gnetwork_ssl_auth_set_server_hostname:
 * @auth: the authentication scheme to modify.
 * @hostname: the hostname to be used.
 * 
 * Modifies @auth to use @hostname as the desired local hostname when
 * authenticating. For server connections, the first valid certificate that
 * matches @hostname will be given to clients. For client connections, this
 * hostname will be used to validate that the server's certificate matches.
 *
 * Note: @auth must be a %GNETWORK_SSL_AUTH_CERTIFICATE scheme.
 * 
 * Since: 1.0
 **/
void
_gnetwork_ssl_auth_set_server_hostname (GNetworkSslAuth * auth, const gchar * hostname)
{
  g_return_if_fail (auth != NULL);
  g_return_if_fail (auth->auth_type != GNETWORK_SSL_AUTH_CERTIFICATE);
  g_return_if_fail (hostname != NULL && hostname[0] != '\0');

  g_free (auth->server_hostname);
  auth->server_hostname = g_strdup (hostname);
}


/**
 * _gnetwork_ssl_auth_get_server_hostname:
 * @auth: the authentication scheme to examine.
 * 
 * Retrieves the server hostname used by @auth. The returned value should not
 * be modified or freed. See gnetwork_ssl_auth_set_server_hostname() for more
 * information. 
 * 
 * Note: @auth must be a %GNETWORK_SSL_AUTH_CERTIFICATE scheme.
 *
 * Returns: the hostname of @auth.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
_gnetwork_ssl_auth_get_server_hostname (GNetworkSslAuth * auth)
{
  g_return_val_if_fail (auth != NULL, NULL);
  g_return_val_if_fail (auth->auth_type != GNETWORK_SSL_AUTH_CERTIFICATE, NULL);

  return auth->server_hostname;
}


GType
_gnetwork_ssl_auth_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      type = g_boxed_type_register_static ("GNetworkSslAuth",
					   (GBoxedCopyFunc) _gnetwork_ssl_auth_ref,
					   (GBoxedFreeFunc) _gnetwork_ssl_auth_unref);
    }

  return type;
}


/* ************************************************************************** *
 *  GNetworkIOChannelSsl                                                      *
 * ************************************************************************** */

#define GNETWORK_IO_CHANNEL_SSL(ptr)	((GNetworkIOChannelSsl *) (ptr))
#define G_IO_CHANNEL(ptr)		((GIOChannel *) (ptr))


/* *************** *
 *  Private Types  *
 * *************** */

typedef struct
{
  GIOChannel channel;

  GIOChannel *parent;
  GNetworkSslAuth *auth;

#if defined _USE_GNUTLS
  gnutls_session session;
#elif defined _USE_OPENSSL
  SSL *session;
#endif				/* _USE_GNUTLS, _USE_OPENSSL */

  gboolean open:1;
}
GNetworkIOChannelSsl;

typedef struct
{
  gint error_num;
  gint error;
  const gchar *const str;
}
IntEnumString;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static GError *
get_ssl_error_from_error_num (gint error_num)
{
  static const IntEnumString errors[] = {
#if defined _USE_GNUTLS

    {GNUTLS_E_SUCCESS, -1,
     N_("There was no error. If you see this message, it is a bug.")},
    {GNUTLS_E_AGAIN,
     GNETWORK_SSL_ERROR_TRY_AGAIN,
     N_("The SSL operation did not complete, try again.")},
    {GNUTLS_E_INTERRUPTED,
     GNETWORK_SSL_ERROR_INTERRUPTED,
     N_("The SSL operation did not complete, try again.")},

#elif defined _USE_OPENSSL

#endif /* _USE_GNUTLS, _USE_OPENSSL */
    {0, 0, NULL}
  };
  guint i;
  GError *error = NULL;

  for (i = 0; i < G_N_ELEMENTS (errors) && error == NULL; i++)
    {
      if (error_num == errors[i].error_num)
	{
	  error = g_error_new_literal (GNETWORK_SSL_ERROR, errors[i].error, _(errors[i].str));
	}
    }

  return error;
}


#if defined _USE_GNUTLS
static gssize
read_from_parent (GNetworkIOChannelSsl * channel, gchar * buffer, gsize length)
{
  GIOStatus status;
  gssize retval;
  gsize bytes_done;

  retval = 0;
  bytes_done = 0;
  status = g_io_channel_read_chars (channel->parent, buffer, length, &bytes_done, NULL);

  switch (status)
    {
    case G_IO_STATUS_NORMAL:
      retval = bytes_done;
      break;

    case G_IO_STATUS_EOF:
      retval = 0;
      break;

      /* We don't need to go overboard with this, errno will be set from the unix channel (the parent
         of parent) */
    case G_IO_STATUS_AGAIN:
    case G_IO_STATUS_ERROR:
      retval = -1;
      break;
    }

  return retval;
}


static gssize
write_to_parent (GNetworkIOChannelSsl * channel, const gchar * buffer, gsize length)
{
  GIOStatus status;
  gssize retval;
  gsize bytes_done;

  retval = 0;
  bytes_done = 0;
  status = g_io_channel_write_chars (channel->parent, buffer, length, &bytes_done, NULL);

  switch (status)
    {
    case G_IO_STATUS_NORMAL:
      retval = bytes_done;
      break;

    case G_IO_STATUS_EOF:
      retval = 0;
      break;

      /* We don't need to go overboard with this, errno will be set from the unix IO channel
         (which is some ancestor of channel). */
    case G_IO_STATUS_AGAIN:
    case G_IO_STATUS_ERROR:
      retval = -1;
      break;
    }

  return retval;
}


static GIOStatus
perform_handshake (GNetworkIOChannelSsl * channel, GError ** error)
{
  gint result;
  GIOStatus status;

  /* Basic Handshake */
  do
    {
      result = gnutls_handshake (channel->session);
    }
  while (result == GNUTLS_E_INTERRUPTED);

  if (result == GNUTLS_E_AGAIN)
    return G_IO_STATUS_AGAIN;

  /* The handshake failed. */
  if (result < 0)
    {
      if (error != NULL)
	*error = get_ssl_error_from_error_num (result);

      status = G_IO_STATUS_ERROR;
    }
  /* If we're a client and expecting a certificate, attempt to verify */
  else if (channel->auth->connection_type == GNETWORK_CONNECTION_CLIENT &&
	   channel->auth->auth_type == GNETWORK_SSL_AUTH_CERTIFICATE)
    {
      GNetworkSslCertErrorFlags errors = GNETWORK_SSL_CERT_ERROR_UNKNOWN;

      result = gnutls_certificate_verify_peers (channel->session);

      /* Couldn't get the certificate */
      if (result != GNUTLS_E_SUCCESS)
	{
	  errors |= get_ssl_cert_error_from_error_num (result);
	  status = G_IO_STATUS_ERROR;
	}
      else if (gnutls_certificate_type_get (channel->session) == GNUTLS_CRT_X509)
	{
	  const gnutls_datum *certs;
	  gint cert_list_size, i;

	  certs = gnutls_certificate_get_peers (channel->session, &cert_list_size);

	  if (certs == NULL)
	    {
	      errors |= GNETWORK_SSL_CERT_ERROR_NO_CERTIFICATE;
	      status = G_IO_STATUS_ERROR;
	    }
	  else
	    {
	      /* Check each certificate we're given. */
	      for (i = 0, status = G_IO_STATUS_ERROR;
		   i < cert_list_size && status != G_IO_STATUS_NORMAL; i++)
		{
		  glong current_time;
		  gnutls_x509_crt x509cert;
		  GNetworkSslCert *cert;

		  gnutls_x509_crt_init (&x509cert);
		  gnutls_x509_crt_import (x509cert, certs + i, GNUTLS_X509_FMT_DER);
		  current_time = time (NULL);
		  cert = _gnetwork_ssl_cert_new (certs + i, GNETWORK_SSL_CERT_X509);

		  if (gnetwork_ssl_cert_get_expiration (cert) < current_time)
		    {
		      errors |= GNETWORK_SSL_CERT_ERROR_EXPIRED;
		      status = G_IO_STATUS_ERROR;
		    }

		  if (gnetwork_ssl_cert_get_activation (cert) > current_time)
		    {
		      errors |= GNETWORK_SSL_CERT_ERROR_NOT_ACTIVATED;
		      status = G_IO_STATUS_ERROR;
		    }

		  if (channel->auth->server_hostname != NULL &&
		      !gnutls_x509_crt_check_hostname (x509cert, channel->auth->server_hostname))
		    {
		      errors |= GNETWORK_SSL_CERT_ERROR_HOSTNAME_MISMATCH;
		      status = G_IO_STATUS_ERROR;
		    }

		  if (errors > 0 && error != NULL)
		    {
		      (*error)->domain = GNETWORK_SSL_CERT_ERROR;
		      (*error)->code = errors;
		      (*error)->message = (gchar *) cert;
		      break;
		    }
		  else
		    {
		      status = G_IO_STATUS_NORMAL;
		      gnetwork_ssl_cert_free (cert);
		    }
		}
	    }
	}
      else
	{
	  if (error != NULL)
	    {
	      (*error)->domain = GNETWORK_SSL_CERT_ERROR;
	      (*error)->code = GNETWORK_SSL_CERT_ERROR_UNSUPPORTED_TYPE;
	      (*error)->message = NULL;
	    }

	  status = G_IO_STATUS_ERROR;
	}
    }
  else
    {
      status = G_IO_STATUS_NORMAL;
    }

  if (status == G_IO_STATUS_NORMAL)
    channel->open = TRUE;

  return status;
}
#endif /* _USE_GNUTLS */


/* ********************** *
 *  GIOChannel Functions  *
 * ********************** */

static GSource *
io_ssl_create_watch (GIOChannel * channel, GIOCondition condition)
{
  return g_io_create_watch (GNETWORK_IO_CHANNEL_SSL (channel)->parent, condition);
}


static GIOStatus
io_ssl_read (GIOChannel * channel, gchar * buffer, gsize length, gsize * bytes_read,
	     GError ** error)
{
  gssize result;
  gsize real_bytes_read;

  g_return_val_if_fail (buffer != NULL, G_IO_STATUS_ERROR);
  g_return_val_if_fail (length > 0, G_IO_STATUS_ERROR);

  real_bytes_read = 0;

#if defined _USE_GNUTLS
/* GNUTLS Version */

  if (!GNETWORK_IO_CHANNEL_SSL (channel)->open)
    {
      GIOStatus status = perform_handshake (GNETWORK_IO_CHANNEL_SSL (channel), error);

      if (status != G_IO_STATUS_NORMAL)
	return status;
    }

  do
    {
      result = gnutls_record_recv (GNETWORK_IO_CHANNEL_SSL (channel)->session, buffer, length);

      if (result > 0)
	real_bytes_read += result;
    }
  while (result == GNUTLS_E_INTERRUPTED);

  if (result == 0)
    {
      *bytes_read = real_bytes_read;

      return G_IO_STATUS_EOF;
    }
  else if (result == GNUTLS_E_AGAIN)
    {
      *bytes_read = real_bytes_read;

      return G_IO_STATUS_AGAIN;
    }
  else if (result < 0)
    {
      *bytes_read = 0;

      if (error != NULL)
	*error = get_ssl_error_from_error_num (result);

      return G_IO_STATUS_ERROR;
    }
  else
    {
      *bytes_read = real_bytes_read;
    }

#elif defined _USE_OPENSSL
/* OpenSSL Version */

  result = SSL_read (session->session, buffer, length);

  /* Error */
  if (result > 0)
    {
      *bytes_read = result;
    }
  else
    {
      gint error_num = SSL_get_error (result);

      switch (error_num)
	{
	case SSL_ERROR_WANT_READ:
	case SSL_ERROR_WANT_WRITE:
	  return G_IO_STATUS_AGAIN;
	  break;
	case SSL_ERROR_ZERO_RETURN:
	  return G_IO_STATUS_EOF;
	  break;

	case SSL_ERROR_SYSCALL:
	  g_error_set (error, G_IO_CHANNEL_ERROR, g_io_channel_error_from_errno (errno),
		       g_strerror (errno));
	  break;

	case SSL_ERROR_SSL:
	  if (error != NULL)
	    *error = get_ssl_error_from_error_num (ERR_get_error ());
	  break;
	}

      return G_IO_STATUS_ERROR;
    }

#endif /* _USE_GNUTLS, _USE_OPENSSL */

  return G_IO_STATUS_NORMAL;
}


static GIOStatus
io_ssl_write (GIOChannel * channel, const gchar * buffer, gsize length, gsize * bytes_written,
	      GError ** error)
{
  gssize result;
  gsize real_bytes_written;
  GIOStatus retval;

  g_return_val_if_fail (buffer != NULL, G_IO_STATUS_ERROR);
  g_return_val_if_fail (length > 0, G_IO_STATUS_ERROR);

  real_bytes_written = 0;
  retval = G_IO_STATUS_NORMAL;

  do
    {
#if defined _USE_GNUTLS
/* GNUTLS Version */

      result = gnutls_record_send (GNETWORK_IO_CHANNEL_SSL (channel)->session,
				   buffer + real_bytes_written, length - real_bytes_written);

      /* Closed connection */
      if (result == 0)
	{
	  retval = G_IO_STATUS_EOF;
	}
      /* Actually sent the data. */
      else if (result > 0)
	{
	  real_bytes_written += result;
	}
      /* Send it again, sam */
      else if (result == GNUTLS_E_INTERRUPTED || result == GNUTLS_E_AGAIN)
	{
	  retval = G_IO_STATUS_AGAIN;
	}
      /* Other errors */
      else
	{
	  if (error != NULL)
	    *error = get_ssl_error_from_error_num (result);

	  retval = G_IO_STATUS_ERROR;
	}

#elif defined _USE_OPENSSL
/* OpenSSL Version (Loop Body) */

      result = SSL_write (session->session, buffer + real_bytes_written,
			  length - real_bytes_written);

      /* Error */
      if (result > 0)
	{
	  real_bytes_written += result;
	}
      else
	{
	  gint error_num = SSL_get_error (result);

	  switch (error_num)
	    {
	    case SSL_ERROR_WANT_READ:
	    case SSL_ERROR_WANT_WRITE:
	      retval = G_IO_STATUS_AGAIN;
	      break;

	    case SSL_ERROR_ZERO_RETURN:
	      retval = G_IO_STATUS_EOF;
	      break;

	    case SSL_ERROR_SYSCALL:
	      g_error_set (error, G_IO_CHANNEL_ERROR, g_io_channel_error_from_errno (errno),
			   g_strerror (errno));

	      retval = G_IO_STATUS_ERROR;
	      break;

	    case SSL_ERROR_SSL:
	      if (error != NULL)
		*error = get_ssl_error_from_error_num (ERR_get_error ());

	      retval = G_IO_STATUS_ERROR;
	      break;
	    }
	}

#endif /* _USE_GNUTLS, _USE_OPENSSL */
    }
  while (retval == G_IO_STATUS_NORMAL && result > 0 && real_bytes_written < length);

  if (bytes_written != NULL)
    *bytes_written = real_bytes_written;

  return retval;
}


static GIOStatus
io_ssl_close (GIOChannel * channel, GError ** error)
{
#if defined _USE_GNUTLS

  gnutls_bye (GNETWORK_IO_CHANNEL_SSL (channel)->session, GNUTLS_SHUT_RDWR);

#elif defined _USE_OPENSSL

  SSL_shutdown (GNETWORK_IO_CHANNEL_SSL (channel)->session);

#endif /* _USE_GNUTLS, _USE_OPENSSL */

  return g_io_channel_shutdown (GNETWORK_IO_CHANNEL_SSL (channel)->parent, FALSE, error);
}


static void
io_ssl_free (GIOChannel * channel)
{
  g_io_channel_unref (GNETWORK_IO_CHANNEL_SSL (channel)->parent);
  _gnetwork_ssl_auth_unref (GNETWORK_IO_CHANNEL_SSL (channel)->auth);
  g_free (channel);
}


static GIOStatus
io_ssl_set_flags (GIOChannel * channel, GIOFlags flags, GError ** error)
{
  return g_io_channel_set_flags (GNETWORK_IO_CHANNEL_SSL (channel)->parent, flags, error);
}


static GIOFlags
io_ssl_get_flags (GIOChannel * channel)
{
  return g_io_channel_get_flags (GNETWORK_IO_CHANNEL_SSL (channel)->parent);
}


/* ******************** *
 *  Library-Public API  *
 * ******************** */

GIOChannel *
_gnetwork_io_channel_ssl_new (GIOChannel * parent, GNetworkSslAuth * auth, GError ** error)
{
#if defined _USE_GNUTLS
  gnutls_credentials_type cred_type;
  static const gint protocol_priority[] = {
    GNUTLS_TLS1,
    GNUTLS_SSL3,
    0
  };
  static const gint cipher_priority[] = {
    GNUTLS_CIPHER_RIJNDAEL_128_CBC,
    GNUTLS_CIPHER_3DES_CBC,
    GNUTLS_CIPHER_RIJNDAEL_256_CBC,
    GNUTLS_CIPHER_ARCFOUR,
    0
  };
  static const gint compression_priority[] = {
    GNUTLS_COMP_ZLIB,
    GNUTLS_COMP_NULL,
    0
  };
  static const gint kx_priority[] = {
    GNUTLS_KX_DHE_RSA,
    GNUTLS_KX_RSA,
    GNUTLS_KX_DHE_DSS,
    0
  };
  static const gint mac_priority[] = {
    GNUTLS_MAC_SHA,
    GNUTLS_MAC_MD5,
    0
  };
#elif defined _USE_OPENSSL
  gint result;
#endif /* _USE_GNUTLS */
  static const GIOFuncs channel_funcs = {
    io_ssl_read,
    io_ssl_write,
    NULL,
    io_ssl_close,
    io_ssl_create_watch,
    io_ssl_free,
    io_ssl_set_flags,
    io_ssl_get_flags,
  };
  GNetworkIOChannelSsl *channel;

  g_return_val_if_fail (parent != NULL, NULL);
  g_return_val_if_fail (auth != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  channel = g_new0 (GNetworkIOChannelSsl, 1);

  /* GIOChannel initialization */
  G_IO_CHANNEL (channel)->funcs = (GIOFuncs *) (&channel_funcs);
  g_io_channel_init (G_IO_CHANNEL (channel));
  G_IO_CHANNEL (channel)->is_seekable = FALSE;
  io_ssl_get_flags (G_IO_CHANNEL (channel));
  g_io_channel_set_encoding (G_IO_CHANNEL (channel), NULL, NULL);
  g_io_channel_set_buffered (G_IO_CHANNEL (channel), FALSE);

  /* GNetworkIOChannelSsl initialization */
  g_io_channel_ref (parent);
  channel->parent = parent;

  channel->auth = _gnetwork_ssl_auth_ref (auth);

#if defined _USE_GNUTLS

  gnutls_init (&(channel->session), (auth->connection_type == GNETWORK_CONNECTION_CLIENT ?
				     GNUTLS_CLIENT : GNUTLS_SERVER));

  gnutls_transport_set_ptr (channel->session, (gnutls_transport_ptr) channel);
  gnutls_transport_set_pull_function (channel->session, (gnutls_pull_func) read_from_parent);
  gnutls_transport_set_push_function (channel->session, (gnutls_push_func) write_to_parent);
  gnutls_protocol_set_priority (channel->session, protocol_priority);
  gnutls_cipher_set_priority (channel->session, cipher_priority);
  gnutls_compression_set_priority (channel->session, compression_priority);
  gnutls_kx_set_priority (channel->session, kx_priority);
  gnutls_mac_set_priority (channel->session, mac_priority);

  switch (auth->auth_type)
    {
    case GNETWORK_SSL_AUTH_ANONYMOUS:
      cred_type = GNUTLS_CRD_ANON;
      break;
    case GNETWORK_SSL_AUTH_CERTIFICATE:
      cred_type = GNUTLS_CRD_CERTIFICATE;
      break;

    default:
      g_assert_not_reached ();
      cred_type = GNUTLS_CRD_ANON;
      break;
    }

  gnutls_credentials_set (channel->session, cred_type, auth->cred.cred);

  perform_handshake (channel, error);

#elif defined _USE_OPENSSL

  channel->session = SSL_new (auth->context);

  if (auth->connection_type == GNETWORK_CONNECTION_CLIENT)
    {
      result = SSL_connect (channel->session);
    }
  else
    {
      result = SSL_accept (channel->session);
    }

  if (result != 1)
    {
      gint error_num = ERR_get_error (result);

      if (error != NULL)
	*error = get_ssl_error_from_error_num (error_num);
    }

#endif /* _USE_GNUTLS, _USE_OPENSSL */

  return G_IO_CHANNEL (channel);
}
#endif /* _USE_SSL */


G_LOCK_DEFINE_STATIC (quark);

GQuark
gnetwork_ssl_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (quark);
  if (quark == 0)
    {
      quark = g_quark_from_static_string ("gnetwork-ssl-error");
    }
  G_UNLOCK (quark);

  return quark;
}
