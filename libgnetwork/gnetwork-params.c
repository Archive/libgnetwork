/*
 * GNetwork Library: libgnetwork/gnetwork-params.c
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-params.h"
#include "gnetwork-utils.h"

#include <gobject/gvaluecollector.h>

/**
 * GNetworkParams:
 * 
 * An opaque data type used to store data for a single method call or signal
 * emission. This structure contains no public members and should only be
 * accessed using the functions below.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_PARAMS:
 * @ptr: the pointer to cast.
 * 
 * Casts the pointer at @ptr into a (GNetworkParams*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_PARAMS:
 * @ptr: the pointer to check.
 * 
 * Checks whether @ptr is a #GNetworkParams structure.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_TYPE_PARAMS:
 * 
 * The registered #GType of #GNetworkParams.
 * 
 * Since: 1.0
 **/


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkParams
{
  GTypeClass g_class;
  guint ref;

  GType type;

  GHashTable *data;
};

typedef struct
{
  GList *list;
  GType type;
}
ListifyData;


/* ****************** *
 *  Global Variables  *
 * ****************** */

G_LOCK_DEFINE_STATIC (params_stack);

static GParamSpecPool *pspec_pool = NULL;

static GTrashStack *params_stack = NULL;
static GMemChunk *gparameter_memchunk = NULL;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

/* GParameter Utils */
static void
gnetwork_gparameter_free (GParameter * param)
{
  g_value_unset (&(param->value));
  g_mem_chunk_free (gparameter_memchunk, param);
}


static GParameter *
gnetwork_gparameter_dup (GParameter * param)
{
  GParameter *retval;
	  
  if (gparameter_memchunk != NULL)
    gparameter_memchunk = g_mem_chunk_create (GParameter, 1, G_ALLOC_AND_FREE);

  retval = g_chunk_new0 (GParameter, gparameter_memchunk);
  
  retval->name = param->name;
  g_value_init (&(retval->value), G_VALUE_TYPE (&(param->value)));
  g_value_copy (&(param->value), &(retval->value));

  return retval;
}


/* Hash Table Key Functions */
static guint
param_spec_hash (gconstpointer key)
{
  const GParamSpec *pspec;
  const gchar *ptr;
  guint h;

  pspec = key;
  h = pspec->owner_type;

  for (ptr = pspec->name; *ptr; ptr++)
    {
      if (*ptr == '_')
	h = (h << 5) - h + '-';
      else
	h = (h << 5) - h + *ptr;
    }

  return h;
  
}


static gboolean
param_spec_equal (gconstpointer key1, gconstpointer key2)
{
  gboolean retval;

  if (key1 == key2)
    {
      retval = TRUE;
    }
  else
    {
      const GParamSpec *pspec1, *pspec2;
      const gchar *name1, *name2;

      pspec1 = key1;
      pspec2 = key2;

      if (pspec1->owner_type == pspec2->owner_type)
	{
	  retval = TRUE;

	  for (name1 = pspec1->name, name2 = pspec2->name;
	       *name1 != '\0' && *name2 != '\0' && retval; name1++, name2++)
	    {
	      if ((g_ascii_toupper (*name1) == g_ascii_toupper (*name2)) ||
		  (*name1 == '-' && *name2 == '_') ||
		  (*name1 == '_' && *name2 == '-'))
		{
		  retval = TRUE;
		}
	      else
		{
		  retval = FALSE;
		}
	    }
	}
      else
	{
	  retval = FALSE;
	}
    }

  return retval;
}


/* Hash Table Utilities */
static void
listify_hash_table_for_type (GParamSpec * key, gpointer value, ListifyData *data)
{
  if (g_param_spec_pool_lookup (pspec_pool, key->name, data->type, FALSE) != NULL)
    data->list = g_list_prepend (data->list, value);
}


static void
listify_hash_table (gpointer key, gpointer value, GList **data)
{
  *data = g_list_prepend (*data, value);
}


/* Actual Getter/Setter */
static void
real_get_value (GNetworkParams * params, GParamSpec * pspec, GValue * value)
{
  GParamSpec *redirect;

  redirect = g_param_spec_get_redirect_target (pspec);
  if (redirect != NULL)
    {
      real_get_value (params, pspec, value);
    }
  else
    {
      GParameter *param = g_hash_table_lookup (params->data, pspec);

      if (param != NULL)
	g_value_copy (&(param->value), value);
      else
	g_param_value_set_default (pspec, value);
    }
}


static void
real_set_value (GNetworkParams * params, GParamSpec * pspec, const GValue * value)
{
  GParamSpec *redirect;

  redirect = g_param_spec_get_redirect_target (pspec);
  if (redirect != NULL)
    {
      real_set_value (params, pspec, value);
    }
  else
    {
      GParameter *param = g_hash_table_lookup (params->data, pspec);

      if (param == NULL)
	{
	  if (gparameter_memchunk != NULL)
	    gparameter_memchunk = g_mem_chunk_create (GParameter, 1, G_ALLOC_AND_FREE);

	  param = g_chunk_new0 (GParameter, gparameter_memchunk);

	  param->name = pspec->name;
	  g_value_init (&(param->value), G_PARAM_SPEC_VALUE_TYPE (pspec));

	  g_hash_table_insert (params->data, g_param_spec_ref (pspec), param);
	}
      else if (pspec->flags & G_PARAM_CONSTRUCT_ONLY)
	{
	  g_warning ("%s: socket data paramter `%s' for sockets of type `%s' may only be set once.",
		     G_STRFUNC, pspec->name, g_type_name (params->type));
	  return;
	}

      g_value_copy (value, &(param->value));
    }
}


/* GNetworkParams Trash Stack Functions */
static inline GNetworkParams *
get_empty_params (void)
{
  GNetworkParams *retval;

  retval = NULL;

  G_LOCK (params_stack);
  retval = g_trash_stack_pop (&params_stack);
  G_UNLOCK (params_stack);

  /* Nothing on the trash stack, create a new structure */
  if (retval == NULL)
    {
      retval = g_new0 (GNetworkParams, 1);
      retval->g_class.g_type = GNETWORK_TYPE_PARAMS;
  
      retval->data = g_hash_table_new_full (param_spec_hash, param_spec_equal,
					    (GDestroyNotify) g_param_spec_unref,
					    (GDestroyNotify) gnetwork_gparameter_free);
    }

  retval->ref = 1;

  return retval;
}


static inline void
delete_params (GNetworkParams * params)
{
  G_LOCK (params_stack);
  if (g_trash_stack_height (&params_stack) > 5)
    {
      g_hash_table_destroy (params->data);
      g_free (params);
    }
  else
    {
      _gnetwork_hash_table_clear (params->data);
      g_trash_stack_push (&params_stack, params);
    }
  G_UNLOCK (params_stack);
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_params_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      type = g_boxed_type_register_static ("GNetworkParams",
					   (GBoxedCopyFunc) gnetwork_params_ref,
					   (GBoxedFreeFunc) gnetwork_params_unref);

      pspec_pool = g_param_spec_pool_new (FALSE);
    }

  return type;
}


/**
 * gnetwork_params_new:
 * @type: the object type this data is for.
 * 
 * Creates a new data structure for use with @type. The data parameters
 * installed for @type will then be available within the returned
 * structure. The returned reference should be deleted with
 * gnetwork_params_unref() when no longer needed.
 * 
 * Returns: a reference to a newly allocated #GNetworkParams structure.
 * 
 * Since: 1.0
 **/
GNetworkParams *
gnetwork_params_new (GType type)
{
  GNetworkParams *params;

  g_return_val_if_fail (g_type_is_a (type, G_TYPE_OBJECT), NULL);

  params = get_empty_params ();
  params->type = type;

  return params;
}


/**
 * gnetwork_params_ref:
 * @params: the socket parameters to reference.
 * 
 * Creates a reference to the data in @params. The reference should be released
 * when no longer needed with gnetwork_params_unref ().
 * 
 * Returns: a pointer to a #GNetworkParams structure, or %NULL.
 * 
 * Since: 1.0
 **/
GNetworkParams *
gnetwork_params_ref (GNetworkParams * params)
{
  g_return_val_if_fail (params == NULL || GNETWORK_IS_PARAMS (params), NULL);

  if (params != NULL)
    params->ref++;

  return params;
}


/**
 * gnetwork_params_unref:
 * @params: the socket parameters to un-reference.
 * 
 * Deletes a reference to the data in @params. When all references to @params
 * have been deleted, the data will be freed from memory.
 * 
 * Since: 1.0
 **/
void
gnetwork_params_unref (GNetworkParams * params)
{
  g_return_if_fail (params == NULL || GNETWORK_IS_PARAMS (params));

  if (params == NULL)
    return;

  params->ref--;

  if (params->ref == 0)
    delete_params (params);
}


/**
 * gnetwork_params_list:
 * @params: the parameters structure to query.
 * 
 * Retrieves a list of name/value pairs for the items currently set in @params.
 * The list items must not be modified or freed, but the list itself must be
 * freed with g_list_free() when no longer needed.
 * 
 * Returns: a newly-allocated list of #GParameter structures.
 * 
 * Since: 1.0
 **/
GList *
gnetwork_params_list (GNetworkParams * params)
{
  GList *retval;

  g_return_val_if_fail (GNETWORK_IS_PARAMS (params), NULL);

  retval = NULL;

  g_hash_table_foreach (params->data, (GHFunc) listify_hash_table, &retval);

  return retval;
}


/**
 * gnetwork_params_list_for_type:
 * @params: the parameters structure to query.
 * @type: the type to list.
 * 
 * Retrieves a list of the name/value pairs currently set in @params which are
 * associated with @type. The returned list items should not be modified or
 * freed, but the list itself must be freed with g_list_free() when no longer
 * needed.
 * 
 * Returns: a newly-allocated list of #GParameter structures.
 * 
 * Since: 1.0
 **/
GList *
gnetwork_params_list_for_type (GNetworkParams * params, GType type)
{
  ListifyData data;

  g_return_val_if_fail (GNETWORK_IS_PARAMS (params), NULL);
  g_return_val_if_fail (g_type_is_a (params->type, type), NULL);

  data.list = NULL;
  data.type = type;

  g_hash_table_foreach (params->data, (GHFunc) listify_hash_table_for_type, &data);

  return data.list;
}


/**
 * gnetwork_params_copy_param:
 * @src: the parameters structure to copy from.
 * @dest: the parameters structure to copy to.
 * @name: the name of the parameter to copy.
 * 
 * Copies the @name parameter from @src to @dest. Both parameter structures must
 * both be associated with the registered type (or it's children) that owns
 * @name.
 * 
 * Since: 1.0
 **/
void
gnetwork_params_copy_param (GNetworkParams * src, GNetworkParams * dest, const gchar * name)
{
  GParamSpec *src_pspec, *dest_pspec;
  GParameter *param;

  g_return_if_fail (GNETWORK_IS_PARAMS (src));
  g_return_if_fail (GNETWORK_IS_PARAMS (dest));
  g_return_if_fail (name != NULL && name[0] != '\0');

  src_pspec = g_param_spec_pool_lookup (pspec_pool, name, src->type, TRUE);

  if (!g_type_is_a (dest->type, src->type))
    dest_pspec = g_param_spec_pool_lookup (pspec_pool, name, dest->type, TRUE);
  else
    dest_pspec = src_pspec;

  if (src_pspec == dest_pspec)
    {
      param = g_hash_table_lookup (src->data, src_pspec);

      g_hash_table_insert (dest->data, g_param_spec_ref (src_pspec),
			   gnetwork_gparameter_dup (param));
    }
  else
    {
      g_warning ("%s: The source `%s' structure, associated with `%s' does not share a property "
		 "named `%s' with the destination `%s', associated with `%s'.", G_STRFUNC,
		 g_type_name (src->g_class.g_type), g_type_name (src->type), name,
		 g_type_name (src->g_class.g_type), g_type_name (src->type));
    }
}


static void
copy_all_params (GParamSpec *key, GParameter *data, GHashTable *dest)
{
  g_hash_table_insert (dest, g_param_spec_ref (key), gnetwork_gparameter_dup (data));
}


/**
 * gnetwork_params_dupv:
 * @params: the parameter structure to copy.
 * @names: a %NULL-terminated array of the parameter names to copy.
 * 
 * Creates a semi-deep copy of @param, copying the parameters in @names.
 * If @names is %NULL, all the parameters are copied.
 * 
 * Returns: a newly allocated #GNetworkParams structure.
 * 
 * Since: 1.0
 **/
GNetworkParams *
gnetwork_params_dupv (GNetworkParams * params, GStrv names)
{
  GNetworkParams *retval;

  g_return_val_if_fail (GNETWORK_IS_PARAMS (params), NULL);
  g_return_val_if_fail (names == NULL || names[0] != NULL, NULL);

  retval = gnetwork_params_new (params->type);

  if (names != NULL)
    {
      guint i;

      for (i = 0; names[i] != NULL; i++)
	{
	  GParamSpec *pspec;
	  GParameter *param;

	  pspec = g_param_spec_pool_lookup (pspec_pool, names[i], params->type, TRUE);

	  if (pspec == NULL)
	    {
	      g_warning ("%s: `%s' structures associated with `%s' have no parameters named `%s'.",
			 G_STRFUNC, g_type_name (params->g_class.g_type),
			 g_type_name (params->type), names[i]);
	    }
	  else
	    {
	      param = g_hash_table_lookup (params->data, pspec);

	      g_hash_table_insert (retval->data, g_param_spec_ref (pspec),
				   gnetwork_gparameter_dup (param));
	    }
	}
    }
  else
    {
      g_hash_table_foreach (params->data, (GHFunc) copy_all_params, retval->data);
    }

  return retval;
}


/**
 * gnetwork_params_dup:
 * @params: the parameter structure to copy.
 * @first_name: the name of the first parameter to copy, or %NULL.
 * @Varargs: a list of parameter names to copy, terminated by %NULL.
 * 
 * Creates a semi-deep copy of @param, copying the parameters indicated.
 * If no parameters are indicated, all the parameters are copied.
 * 
 * Returns: a newly allocated #GNetworkParams structure.
 * 
 * Since: 1.0
 **/
GNetworkParams *
gnetwork_params_dup (GNetworkParams * params, const gchar * first_name, ...)
{
  GNetworkParams *retval;
  const gchar *name;
  va_list var_args;

  g_return_val_if_fail (GNETWORK_IS_PARAMS (params), NULL);
  g_return_val_if_fail (first_name == NULL || first_name[0] != '\0', NULL);

  retval = gnetwork_params_new (params->type);

  if (first_name != NULL)
    {
      name = first_name;

      va_start (var_args, first_name);
      while (name != NULL)
	{
	  GParamSpec *pspec;

	  pspec = g_param_spec_pool_lookup (pspec_pool, name, params->type, TRUE);

	  if (pspec == NULL)
	    {
	      g_warning ("%s: `%s' structures associated with `%s' have no parameters named `%s'.",
			 G_STRFUNC, g_type_name (params->g_class.g_type),
			 g_type_name (params->type), name);
	    }
	  else
	    {
	      GParameter *param = g_hash_table_lookup (params->data, pspec);

	      if (param != NULL)
		g_hash_table_insert (retval->data, g_param_spec_ref (pspec),
				     gnetwork_gparameter_dup (param));
	    }

	  name = va_arg (var_args, gchar *);
	}
      va_end (var_args);
    }
  else
    {
      g_hash_table_foreach (params->data, (GHFunc) copy_all_params, retval->data);
    }

  return retval;
}


/**
 * gnetwork_params_get_associated_type:
 * @params: the parameters structure to examine.
 * 
 * Retrieves the type that @params gets it's properties from.
 * 
 * Returns: a registered type.
 * 
 * Since: 1.0
 **/
GType
gnetwork_params_get_associated_type (GNetworkParams * params)
{
  g_return_val_if_fail (GNETWORK_IS_PARAMS (params), G_TYPE_INVALID);

  return params->type;
}


/**
 * gnetwork_params_is_set:
 * @params: the parameters structure to examine.
 * @name: the name of the parameter to check.
 * 
 * Checks to see if the @name parameter in the @params structure is set.
 * 
 * Returns: %TRUE if @name is set, %FALSE if it is not.
 * 
 * Since: 1.0
 **/
gboolean
gnetwork_params_is_set (GNetworkParams * params, const gchar * name)
{
  GParamSpec *pspec;

  g_return_val_if_fail (GNETWORK_IS_PARAMS (params), FALSE);
  g_return_val_if_fail (name != NULL && name[0] != '\0', FALSE);

  pspec = g_param_spec_pool_lookup (pspec_pool, name, params->type, TRUE);

  if (pspec == NULL)
    {
      g_warning ("%s: `%s' structures associated with `%s' have no parameters named `%s'.",
		 G_STRFUNC, g_type_name (params->g_class.g_type), g_type_name (params->type), name);
      return FALSE;
    }

  return (g_hash_table_lookup (params->data, pspec) != NULL);
}


/**
 * gnetwork_params_unset:
 * @params: the parameters structure to examine.
 * @name: the name of the parameter to remove.
 * 
 * Clears the @name parameter from @params. If the parameter was found and
 * cleared, %TRUE is returned, otherwise, %FALSE is returned.
 * 
 * Returns: %TRUE if @name was removed, %FALSE if it was not.
 * 
 * Since: 1.0
 **/
gboolean
gnetwork_params_unset (GNetworkParams * params, const gchar * name)
{
  GParamSpec *pspec;

  g_return_val_if_fail (GNETWORK_IS_PARAMS (params), FALSE);
  g_return_val_if_fail (name != NULL && name[0] != '\0', FALSE);

  pspec = g_param_spec_pool_lookup (pspec_pool, name, params->type, TRUE);

  if (pspec == NULL)
    {
      g_warning ("%s: `%s' structures associated with `%s' have no parameters named `%s'.",
		 G_STRFUNC, g_type_name (params->g_class.g_type), g_type_name (params->type), name);
      return FALSE;
    }

  return g_hash_table_remove (params->data, pspec);
}


/**
 * gnetwork_params_set_value:
 * @params: the socket parameters to modify.
 * @name: the name of the parameter to modify.
 * @value: a pointer to a GValue containing the new value.
 * 
 * Sets the parameter described by @name in @params to the contents of @value.
 * @value must be initialized to the same data type as the parameter described
 * by @name.
 * 
 * Since: 1.0
 **/
void
gnetwork_params_set_value (GNetworkParams * params, const gchar * name, const GValue * value)
{
  GParamSpec *pspec;

  g_return_if_fail (GNETWORK_IS_PARAMS (params));
  g_return_if_fail (name != NULL && name[0] != '\0');
  g_return_if_fail (G_IS_VALUE (value));

  pspec = g_param_spec_pool_lookup (pspec_pool, name, params->type, TRUE);

  if (pspec == NULL)
    {
      g_warning ("%s: `%s' structures associated with `%s' have no parameters named `%s'.",
                 G_STRFUNC, g_type_name (params->g_class.g_type), g_type_name (params->type), name);
    }
  else if (!(pspec->flags & G_PARAM_WRITABLE))
    {
      g_warning ("%s: the `%s' parameter for `%s' structures associated with `%s' is not writable.",
		 G_STRFUNC, pspec->name, g_type_name (params->g_class.g_type),
		 g_type_name (params->type));
    }
  else
    {
      /* Auto-conversion of the caller's value type */
      if (G_VALUE_TYPE (value) == G_PARAM_SPEC_VALUE_TYPE (pspec))
	{
	  real_set_value (params, pspec, value);
	}
      else if (g_value_type_transformable (G_VALUE_TYPE (value), G_PARAM_SPEC_VALUE_TYPE (pspec)))
	{
	  GValue tmp_value = { 0 };

	  g_value_init (&tmp_value, G_PARAM_SPEC_VALUE_TYPE (pspec));

	  g_value_transform (value, &tmp_value);
	  real_set_value (params, pspec, &tmp_value);
	  g_value_unset (&tmp_value);
	}
      else
	{
	  g_warning ("%s: the `%s' parameter for `%s' structures associated with `%s' requires a "
		     "value of type `%s', but was given a value of type `%s'", G_STRFUNC,
		     pspec->name, g_type_name (params->g_class.g_type), g_type_name (params->type),
		     g_type_name (G_PARAM_SPEC_VALUE_TYPE (pspec)), G_VALUE_TYPE_NAME (value));
	  return;
	}
    }
}


/**
 * gnetwork_params_set_valist:
 * @params: the socket data to modify.
 * @first_name: the name of the first parameter to retrieve.
 * @var_args: a list of name/return location pairs.
 * 
 * A variable-argument version of gnetwork_params_get() for language
 * bindings to use.
 * 
 * Since: 1.0
 **/
void
gnetwork_params_set_valist (GNetworkParams * params, const gchar * first_name, va_list var_args)
{
  const gchar *name;

  g_return_if_fail (GNETWORK_IS_PARAMS (params));
  g_return_if_fail (first_name != NULL && first_name[0] != '\0');

  name = first_name;

  while (name)
    {
      GValue value = { 0, };
      GParamSpec *pspec;
      gchar *error;

      pspec = g_param_spec_pool_lookup (pspec_pool, name, params->type, TRUE);

      if (pspec == NULL)
	{
	  g_warning ("%s: `%s' structures associated with `%s' have no parameters named `%s'.",
		     G_STRFUNC, g_type_name (params->g_class.g_type),
		     g_type_name (params->type), name);
	  break;
	}
      else if (!(pspec->flags & G_PARAM_WRITABLE))
	{
	  g_warning ("%s: the `%s' parameter for `%s' structures associated with `%s' is not "
		     "writable.", G_STRFUNC, pspec->name, g_type_name (params->g_class.g_type),
		     g_type_name (params->type));
	  break;
	}

      g_value_init (&value, G_PARAM_SPEC_VALUE_TYPE (pspec));

      real_get_value (params, pspec, &value);

      G_VALUE_LCOPY (&value, var_args, 0, &error);
      if (error != NULL)
	{
	  g_warning ("%s: %s", G_STRFUNC, error);
	  g_free (error);
	  g_value_unset (&value);
	  break;
	}

      g_value_unset (&value);

      name = va_arg (var_args, gchar *);
    }
}


/**
 * gnetwork_params_set:
 * @params: the socket parameters to modify.
 * @first_name: the name of the first data parameter to set.
 * @Varargs: pairs of parameter name and values, terminated by %NULL.
 * 
 * Modifies parameters in @params. @first_name is the name of the first
 * parameter to modify, followed by the value to use:
 * 
 * <informalexample><programlisting>gnetwork_params_set (data, "my-parameter", 2, NULL);</programlisting></informalexample>
 * 
 * The "name"/value pairs list should be terminated with a %NULL.
 * 
 * Since: 1.0
 **/
void
gnetwork_params_set (GNetworkParams * params, const gchar * first_name, ...)
{
  va_list var_args;

  g_return_if_fail (GNETWORK_IS_PARAMS (params));
  g_return_if_fail (first_name != NULL && first_name[0] != '\0');

  va_start (var_args, first_name);
  gnetwork_params_set_valist (params, first_name, var_args);
  va_end (var_args);
}


/**
 * gnetwork_params_get_value:
 * @params: the socket data to examine.
 * @name: the name of the parameter to retrieve.
 * @value: a pointer to the GValue return location.
 * 
 * Retrieves the socket data parameter described by @name from @params and
 * places it into @value. @value must be initialized to the same data type as
 * the property described by @name.
 * 
 * Since: 1.0
 **/
void
gnetwork_params_get_value (GNetworkParams * params, const gchar * name, GValue * value)
{
  GParamSpec *pspec;

  g_return_if_fail (GNETWORK_IS_PARAMS (params));
  g_return_if_fail (name != NULL && name[0] != '\0');
  g_return_if_fail (G_IS_VALUE (value));

  pspec = g_param_spec_pool_lookup (pspec_pool, name, params->type, TRUE);

  if (pspec == NULL)
    {
      g_warning ("%s: `%s' structures associated with `%s' have no parameters named `%s'.",
		 G_STRFUNC, g_type_name (params->g_class.g_type), g_type_name (params->type), name);
    }
  else if (!(pspec->flags & G_PARAM_READABLE))
    {
      g_warning ("%s: the `%s' parameter for `%s' structures associated with `%s' is not readable.",
		 G_STRFUNC, pspec->name, g_type_name (params->g_class.g_type),
		 g_type_name (params->type));
    }
  else
    {
      GValue *param_value, tmp_value = { 0, };

      /* Auto-conversion of the caller's value type */
      if (G_VALUE_TYPE (value) == G_PARAM_SPEC_VALUE_TYPE (pspec))
	{
	  g_value_reset (value);
	  param_value = value;
	}
      else if (!g_value_type_transformable (G_PARAM_SPEC_VALUE_TYPE (pspec), G_VALUE_TYPE (value)))
	{
	  g_warning ("%s: the `%s' parameter for `%s' structures associated with `%s' requires a "
		     "value of type `%s', but was given a value of type `%s'", G_STRFUNC,
		     pspec->name, g_type_name (params->g_class.g_type), g_type_name (params->type),
		     g_type_name (G_PARAM_SPEC_VALUE_TYPE (pspec)), G_VALUE_TYPE_NAME (value));
	  return;
	}
      else
	{
	  g_value_init (&tmp_value, G_PARAM_SPEC_VALUE_TYPE (pspec));
	  param_value = &tmp_value;
	}

      real_get_value (params, pspec, param_value);

      if (param_value != value)
	{
	  g_value_transform (param_value, value);
	  g_value_unset (&tmp_value);
	}
    }
}


/**
 * gnetwork_params_get_valist:
 * @params: the socket data to examine.
 * @first_name: the name of the first parameter to retrieve.
 * @var_args: a list of name/return location pairs.
 * 
 * A variable-argument version of gnetwork_params_get() for language
 * bindings to use.
 * 
 * Since: 1.0
 **/
void
gnetwork_params_get_valist (GNetworkParams * params, const gchar * first_name,
			    va_list var_args)
{
  const gchar *name;

  g_return_if_fail (GNETWORK_IS_PARAMS (params));
  g_return_if_fail (first_name != NULL && first_name[0] != '\0');

  name = first_name;

  while (name)
    {
      GValue value = { 0, };
      GParamSpec *pspec;
      gchar *error;

      pspec = g_param_spec_pool_lookup (pspec_pool, name, params->type, TRUE);

      if (pspec == NULL)
	{
	  g_warning ("%s: `%s' structures associated with `%s' have no parameters named `%s'.",
		     G_STRFUNC, g_type_name (params->g_class.g_type), g_type_name (params->type),
		     name);
	  break;
	}
      else if (!(pspec->flags & G_PARAM_READABLE))
	{
	  g_warning ("%s: the `%s' parameter for `%s' structures associated with `%s' is not "
		     "readable.", G_STRFUNC, pspec->name, g_type_name (params->g_class.g_type),
		     g_type_name (params->type));
	  break;
	}

      g_value_init (&value, G_PARAM_SPEC_VALUE_TYPE (pspec));

      real_get_value (params, pspec, &value);

      G_VALUE_LCOPY (&value, var_args, 0, &error);
      if (error)
	{
	  g_warning ("%s: %s", G_STRFUNC, error);
	  g_free (error);
	  g_value_unset (&value);
	  break;
	}

      g_value_unset (&value);

      name = va_arg (var_args, gchar *);
    }
}


/**
 * gnetwork_params_get:
 * @params: the socket parameters to examine.
 * @first_name: the name of the first parameter to retrieve.
 * @Varargs: pairs of parameter name and value return locations, terminated by %NULL.
 * 
 * Retrieves data parameters from @params. @first_name is the name of the first
 * property to retrieve, followed the location of a variable to store the
 * returned data:
 * 
 * <informalexample><programlisting>gint value = -1;
 * gnetwork_params_get (data, "my-parameter", &amp;value, NULL);</programlisting></informalexample>
 * 
 * The "name"/value pairs list should be terminated with a %NULL.
 * 
 * Since: 1.0
 **/
void
gnetwork_params_get (GNetworkParams * params, const gchar * first_name, ...)
{
  va_list var_args;

  g_return_if_fail (GNETWORK_IS_PARAMS (params));
  g_return_if_fail (first_name != NULL && first_name[0] != '\0');

  va_start (var_args, first_name);
  gnetwork_params_get_valist (params, first_name, var_args);
  va_end (var_args);
}


/**
 * gnetwork_params_install_param:
 * @pspec: the parameter specification to install.
 * @type: the type to attach this specification to.
 * 
 * Associates a parameter described by @pspec to @type. This allows
 * #GNetworkParams structures associated with @type to know about this
 * parameter.
 * 
 * Since: 1.0
 **/
void
gnetwork_params_install_param (GParamSpec *pspec, GType type)
{
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));
  g_return_if_fail (type != G_TYPE_INVALID);

  g_param_spec_pool_insert (pspec_pool, pspec, type);
}


/**
 * gnetwork_params_find:
 * @type: the type which the parameter is associated with.
 * @name: the name of the parameter to find.
 * 
 * Searches for a parameter specification called @name, associated with @type.
 * The returned value must not be modified or freed.
 * 
 * Returns: the parameter specification, or %NULL if none was found.
 * 
 * Since: 1.0
 **/
GParamSpec *
gnetwork_params_find (GType type, const gchar * name)
{
  g_return_val_if_fail (type != G_TYPE_INVALID, NULL);
  g_return_val_if_fail (name != NULL && name[0] != '\0', NULL);

  return g_param_spec_pool_lookup (pspec_pool, name, type, TRUE);
}


/**
 * gnetwork_params_list_all:
 * @type: the type to query.
 * @n_params: a location to store the number of items found.
 * 
 * Retrieves the parameter specifications associated with @type and it's
 * parents. The returned value must be freed with g_free() when no longer
 * needed.
 * 
 * Returns: an array of parameter specifications.
 * 
 * Since: 1.0
 **/
GParamSpec **
gnetwork_params_list_all (GType type, guint * n_params)
{
  g_return_val_if_fail (type != G_TYPE_INVALID, NULL);
  g_return_val_if_fail (n_params != NULL, NULL);

  return g_param_spec_pool_list (pspec_pool, type, n_params);
}


/**
 * gnetwork_params_list_all_for_type:
 * @type: the type to query.
 * 
 * Retrieves a list of all the parameter specifications associated with @type.
 * The returned value must be freed with g_list_free() when no longer needed.
 * 
 * Returns: an array of parameter specifications.
 * 
 * Since: 1.0
 **/
GList *
gnetwork_params_list_all_for_type (GType type)
{
  g_return_val_if_fail (type != G_TYPE_INVALID, NULL);

  return g_param_spec_pool_list_owned (pspec_pool, type);
}
