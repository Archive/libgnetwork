/* 
 * GNetwork Library: libgnetwork/gnetwork-utils.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_UTILS_H__
#define __GNETWORK_UTILS_H__

#include "gnetwork-interfaces.h"
#include "gnetwork-ip-address.h"

#include <sys/types.h>
#include <sys/socket.h>

G_BEGIN_DECLS


#if defined AF_INET6
# define SOCKET_TYPE AF_INET6
# define INET_ADDRESS_SIZE INET6_ADDRSTRLEN
#else
# define SOCKET_TYPE AF_INET
# define INET_ADDRESS_SIZE INET_ADDRSTRLEN
#endif /* AF_INET6 */


#define G_INT(ptr)	((gint *) (ptr))
#define G_UCHAR(ptr)	((guchar *) (ptr))


#define GNETWORK_WARN_METHOD_DATA_TYPE_MISMATCH(otype,arg_num,itype,method_name,vtype,value) \
  (g_warning ("The %s object requires the value for argument %u of the %s.%s() method to " \
	      "contain %s data. The  given data was %s.", g_type_name (otype), arg_num, \
	      g_type_name (itype), method_name, g_type_name (vtype), \
	      (value != NULL ? G_VALUE_TYPE_NAME (value) : NULL)))

typedef void (*GNetworkIOCallback) (GIOChannel * channel, const GError * error, gpointer data);


GValueArray *_gnetwork_slist_to_value_array (GSList * list, GType item_type);
void _gnetwork_hash_table_clear (GHashTable * table);
void _gnetwork_slist_from_hash_table (gpointer key, gpointer value, gpointer data);

gboolean _gnetwork_enum_value_is_valid (GType enum_type, gint value);
gboolean _gnetwork_flags_value_is_valid (GType flags_type, guint value);

GNetworkProtocols _gnetwork_get_socket_protocol (gpointer socket);
struct sockaddr *_gnetwork_ip_address_to_sockaddr (const GNetworkIpAddress * address, guint16 port,
						   gint * sa_size);
void _gnetwork_ip_address_set_from_sockaddr (GNetworkIpAddress * address,
					     const struct sockaddr *sa);
gchar *_gnetwork_sockaddr_get_address (const struct sockaddr *sa);
guint16 _gnetwork_sockaddr_get_port (const struct sockaddr *sa);

gboolean _gnetwork_boolean_accumulator (GSignalInvocationHint * ihint, GValue * return_accu,
					const GValue * handler_return, gpointer dummy);


G_END_DECLS
#endif /* !__GNETWORK_UTILS_H__ */
