/*
 * GNetwork Library: libgnetwork/gnetwork-unix-connection.c
 *
 * Copyright (C) 2001-2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-unix-connection.h"

#include "marshal.h"
#include "gnetwork-connection.h"
#include "gnetwork-threads.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/un.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <glib/gi18n.h>


enum
{
  PROP_0,

  /* Object Properties */
  UNIX_STATUS,
  FILENAME,

  /* Shortcut for Servers */
  SOCKET_FD,

  /* GNetworkConnection Properties */
  CXN_TYPE,
  CXN_STATUS,
  CXN_BYTES_RECEIVED,
  CXN_BYTES_SENT,
  CXN_BUFFER_SIZE
};


struct _GNetworkUnixConnectionPrivate
{
  /* Properties */
  /* GNetworkUnixConnection */
  gchar *filename;

  /* GNetworkConnectionIface */
  guint buffer_size;
  gulong bytes_received;
  gulong bytes_sent;

  /* Object Data */
  GSList *buffer;
  gint sockfd;
  GIOChannel *channel;
  guint source_id;
  GIOCondition source_cond:6;

  /* Property Bits */
  /* GNetworkUnixConnection */
  GNetworkUnixConnectionStatus unix_status:3;

  /* GNetworkConnectionIface */
  GNetworkConnectionType cxn_type:2;
  GNetworkConnectionStatus cxn_status:3;
};


typedef struct
{
  gpointer data;
  gulong length;
}
BufferItem;


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer parent_class = NULL;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static GError *
get_connection_error_from_errno (gint en, const gchar * filename)
{
  GError *error = NULL;

  switch (en)
    {
    case EINPROGRESS:
      g_assert_not_reached ();
      break;

    case ECONNREFUSED:
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_REFUSED,
			   _("The file \"%s\" could not be used as a connection because the "
			     "service refused to allow it, or it is not a service."), filename);
      break;

    default:
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
			   _("The file \"%s\" could not be used as a connection because an error "
			     "occurred inside the GNetwork library."), filename);
      break;
    }

  return error;
}

/* ********************** *
 *  Connection Functions  *
 * ********************** */

static void
gnetwork_unix_connection_close (GNetworkUnixConnection * connection)
{
  GNetworkUnixConnectionStatus status;

  g_return_if_fail (GNETWORK_IS_UNIX_CONNECTION (connection));

  if (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_CLOSING ||
      connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_CLOSED)
    return;

  /* Save the old status */
  status = connection->_priv->unix_status;

  connection->_priv->unix_status = GNETWORK_UNIX_CONNECTION_CLOSING;
  connection->_priv->cxn_status = GNETWORK_CONNECTION_CLOSING;

  g_object_freeze_notify (G_OBJECT (connection));
  g_object_notify (G_OBJECT (connection), "unix-status");
  g_object_notify (G_OBJECT (connection), "status");
  g_object_thaw_notify (G_OBJECT (connection));

  switch (status)
    {
      /* Fully Open & Ready. */
    case GNETWORK_UNIX_CONNECTION_OPENING:
    case GNETWORK_UNIX_CONNECTION_OPEN:
      if (connection->_priv->source_id != 0)
	{
	  gnetwork_thread_source_remove (connection->_priv->source_id);
	  connection->_priv->source_id = 0;
	  connection->_priv->source_cond = 0;
	}

      if (connection->_priv->channel != NULL)
	{
	  g_io_channel_shutdown (connection->_priv->channel, FALSE, NULL);
	  g_io_channel_unref (connection->_priv->channel);
	  connection->_priv->channel = NULL;
	}
      else if (connection->_priv->sockfd > 0)
	{
	  shutdown (connection->_priv->sockfd, SHUT_RDWR);
	  close (connection->_priv->sockfd);
	}
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  connection->_priv->cxn_status = GNETWORK_CONNECTION_CLOSED;
  connection->_priv->unix_status = GNETWORK_UNIX_CONNECTION_CLOSED;
  connection->_priv->sockfd = -1;

  g_object_freeze_notify (G_OBJECT (connection));
  g_object_notify (G_OBJECT (connection), "status");
  g_object_notify (G_OBJECT (connection), "unix-status");
  g_object_notify (G_OBJECT (connection), "socket");
  g_object_thaw_notify (G_OBJECT (connection));
}


static gboolean
io_channel_handler (GIOChannel * channel, GIOCondition cond, GNetworkUnixConnection * connection)
{
  gboolean retval;

  if (connection->_priv->unix_status <= GNETWORK_UNIX_CONNECTION_CLOSED)
    return FALSE;

  /* Error and EOF */
  if (cond & (G_IO_ERR | G_IO_HUP))
    {
      gnetwork_unix_connection_close (connection);
      return FALSE;
    }

  retval = FALSE;

  /* Read */
  if (cond & (G_IO_IN | G_IO_PRI))
    {
      GIOStatus status;
      guchar *buffer;
      gsize bytes_read;
      GError *error = NULL;

      buffer = g_new (guchar, connection->_priv->buffer_size + 1);
      
      status = g_io_channel_read_chars (channel, buffer, connection->_priv->buffer_size,
					&bytes_read, &error);

      switch (status)
	{
	case G_IO_STATUS_AGAIN:
	  retval = TRUE;
	  break;

	case G_IO_STATUS_NORMAL:
	  if (bytes_read > 0)
	    {
	      connection->_priv->bytes_received += bytes_read;
	      g_object_notify (G_OBJECT (connection), "bytes-received");

	      /* Make sure the read buffer is 0-terminated. */
	      buffer[bytes_read] = 0;
	      gnetwork_connection_received (GNETWORK_CONNECTION (connection), buffer, bytes_read);
	    }
	  retval = TRUE;
	  break;

	case G_IO_STATUS_ERROR:
	  gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
	  g_error_free (error);
	  /* Fall through to close the connection */

	case G_IO_STATUS_EOF:
	  if (connection->_priv->cxn_status == GNETWORK_CONNECTION_OPEN)
	    gnetwork_unix_connection_close (connection);
	  break;

	default:
	  g_assert_not_reached ();
	  break;
	}

      g_free (buffer);
    }

  /* Write */
  if (cond & G_IO_OUT)
    {
      if (connection->_priv->buffer != NULL)
	{
	  GIOStatus status;
	  gsize bytes_sent;
	  BufferItem *item;
	  GError *error = NULL;

	  item = connection->_priv->buffer->data;

	  status = g_io_channel_write_chars (channel, item->data, item->length, &bytes_sent,
					     &error);

	  switch (status)
	    {
	    /* Stop immediately & come back later. */
	    case G_IO_STATUS_AGAIN:
	      return TRUE;
	      break;

	    case G_IO_STATUS_NORMAL:
	      if (bytes_sent > 0)
		{
		  connection->_priv->bytes_sent += bytes_sent;
		  g_object_notify (G_OBJECT (connection), "bytes-sent");

		  gnetwork_connection_sent (GNETWORK_CONNECTION (connection), item->data,
					    bytes_sent);

		  /* Partial write, put the unsent data back on the top of the queue. */
		  if (bytes_sent < item->length)
		    {
		      BufferItem *new_item = g_new0 (BufferItem, 1);

		      new_item->data = g_new (guchar, item->length - bytes_sent + 1);
		      memcpy (new_item->data, G_UCHAR (item->data) + bytes_sent, new_item->length);
		      new_item->length = item->length - bytes_sent;
		      connection->_priv->buffer->data = new_item;
		    }
		  else
		    {
		      connection->_priv->buffer = g_slist_remove_link (connection->_priv->buffer,
								       connection->_priv->buffer);
		    }
		}
	      retval = TRUE;
	      break;

	    case G_IO_STATUS_ERROR:
	      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
	      g_error_free (error);
	      /* Fall through to close the connection. */

	    case G_IO_STATUS_EOF:
	      if (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_OPEN)
		gnetwork_unix_connection_close (connection);
	      break;

	    default:
	      g_assert_not_reached ();
	      break;
	    }

	  /* Free the data */
	  g_free (item->data);
	  g_free (item);
	}

      /* We've got nothing else to send, so stop listening for G_IO_OUT. */
      if (connection->_priv->buffer == NULL)
	{
	  gnetwork_thread_source_remove (connection->_priv->source_id);
	  connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);
	  connection->_priv->source_id =
	    gnetwork_thread_io_add_watch (connection->_priv->channel,
					  connection->_priv->source_cond,
					  (GIOFunc) io_channel_handler, connection);
	  retval = FALSE;
	}
    }

  return retval;
}


/* connect() Is Done */
static gboolean
connect_done_handler (GIOChannel * channel, GIOCondition cond, GNetworkUnixConnection * connection)
{
  GError *error;
  gint result, error_val, length;

  errno = 0;
  error_val = 0;
  length = 0;
  result = getsockopt (connection->_priv->sockfd, SOL_SOCKET, SO_ERROR, &error_val, &length);

  /* Couldn't get socket options */
  if (result != 0)
    {
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
			   _("The file \"%s\" could not be used as a connection because an error "
			     "occurred inside the GNetwork library."), connection->_priv->filename);
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_OPENING)
	gnetwork_unix_connection_close (connection);
      return FALSE;
    }

  if (error_val != 0)
    {
      error = get_connection_error_from_errno (errno, connection->_priv->filename);
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_OPENING)
	gnetwork_unix_connection_close (connection);
    }
  else
    {
      connection->_priv->channel = g_io_channel_unix_new (connection->_priv->sockfd);
      g_io_channel_set_encoding (connection->_priv->channel, NULL, NULL);
      g_io_channel_set_buffered (connection->_priv->channel, FALSE);

      connection->_priv->unix_status = GNETWORK_UNIX_CONNECTION_OPEN;
      connection->_priv->cxn_status = GNETWORK_CONNECTION_OPEN;

      g_object_freeze_notify (G_OBJECT (connection));
      g_object_notify (G_OBJECT (connection), "unix-status");
      g_object_notify (G_OBJECT (connection), "status");
      g_object_thaw_notify (G_OBJECT (connection));

      connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);
      connection->_priv->source_id = gnetwork_thread_io_add_watch (connection->_priv->channel,
								   connection->_priv->source_cond,
								   (GIOFunc) io_channel_handler,
								   connection);
    }

  return FALSE;
}


/* *********************************** *
 *  GNetworkConnectionIface Functions  *
 * *********************************** */

static void
gnetwork_unix_connection_open (GNetworkUnixConnection * connection)
{
  GError *error;
  GObject *object;
  gint flags;

  g_return_if_fail (GNETWORK_IS_UNIX_CONNECTION (connection));
  g_return_if_fail (connection->_priv->cxn_status == GNETWORK_CONNECTION_CLOSED);

  object = G_OBJECT (connection);

  /* If the socket property hasn't been set, we need to create one, but only for client
     connections. */
  if (connection->_priv->sockfd < 0)
    {
      if (connection->_priv->cxn_type == GNETWORK_CONNECTION_SERVER)
	{
	  g_warning ("You cannot open a UNIX server connection without first setting the "
		     "\"socket\" property on the object to the accepted socket.");
	  return;
	}
      else if (connection->_priv->cxn_type == GNETWORK_CONNECTION_CLIENT)
	{

	  errno = 0;
	  connection->_priv->sockfd = socket (AF_UNIX, SOCK_STREAM, 0);
	  g_object_notify (object, "socket");

	  if (connection->_priv->sockfd < 0)
	    {
	      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
				   _("The file \"%s\" could not be used as a connection because an "
				     "error occurred inside the GNetwork library."),
				   connection->_priv->filename);
	      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
	      g_error_free (error);

	      if (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_OPENING)
		gnetwork_unix_connection_close (connection);
	      return;
	    }
	}
    }

  /* Retrieve the current socket flags */
  flags = fcntl (connection->_priv->sockfd, F_GETFL, 0);
  if (flags == -1)
    {
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
			   _("The file \"%s\" could not be used as a connection because an error "
			     "occurred inside the GNetwork library."), connection->_priv->filename);
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_OPENING)
	gnetwork_unix_connection_close (connection);
      return;
    }

  /* Add the non-blocking flag */
  if (fcntl (connection->_priv->sockfd, F_SETFL, flags | O_NONBLOCK) == -1)
    {
      error = g_error_new (GNETWORK_CONNECTION_ERROR, GNETWORK_CONNECTION_ERROR_INTERNAL,
			   _("The file \"%s\" could not be used as a connection because an error "
			     "occurred inside the GNetwork library."), connection->_priv->filename);
      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
      g_error_free (error);

      if (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_OPENING)
	gnetwork_unix_connection_close (connection);
      return;
    }

  if (connection->_priv->cxn_type == GNETWORK_CONNECTION_CLIENT)
    {
      struct sockaddr_un sun;
      gint result;

      memset (&sun, 0, sizeof (sun));

      sun.sun_family = AF_UNIX;
      strncpy (sun.sun_path, connection->_priv->filename, G_N_ELEMENTS (sun.sun_path));

      errno = 0;
      result = connect (connection->_priv->sockfd, (struct sockaddr *) &(sun), sizeof (sun));

      if (result != 0)
	{
	  if (errno == EINPROGRESS)
	    {
	      connection->_priv->channel = g_io_channel_unix_new (connection->_priv->sockfd);
	      g_io_channel_set_encoding (connection->_priv->channel, NULL, NULL);
	      g_io_channel_set_buffered (connection->_priv->channel, FALSE);

	      connection->_priv->source_id =
		gnetwork_thread_io_add_watch (connection->_priv->channel, GNETWORK_IO_ANY,
					      (GIOFunc) connect_done_handler, connection);

	      return;
	    }
	  else if (errno != EISCONN)
	    {
	      error = get_connection_error_from_errno (errno, connection->_priv->filename);
	      gnetwork_connection_error (GNETWORK_CONNECTION (connection), error);
	      g_error_free (error);

	      if (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_OPENING)
		gnetwork_unix_connection_close (connection);
	     return;
	    }
	}
    }

  connection->_priv->channel = g_io_channel_unix_new (connection->_priv->sockfd);
  g_io_channel_set_encoding (connection->_priv->channel, NULL, NULL);
  g_io_channel_set_buffered (connection->_priv->channel, FALSE);

  connection->_priv->unix_status = GNETWORK_UNIX_CONNECTION_OPEN;
  connection->_priv->cxn_status = GNETWORK_CONNECTION_OPEN;

  g_object_freeze_notify (G_OBJECT (connection));
  g_object_notify (G_OBJECT (connection), "unix-status");
  g_object_notify (G_OBJECT (connection), "status");
  g_object_thaw_notify (G_OBJECT (connection));

  connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP); 
  connection->_priv->source_id = gnetwork_thread_io_add_watch (connection->_priv->channel,
							       connection->_priv->source_cond,
							       (GIOFunc) io_channel_handler,
							       connection);
}


static void
gnetwork_unix_connection_send (GNetworkUnixConnection * connection, gconstpointer data,
			       gulong length)
{
  BufferItem * item;

  g_return_if_fail (GNETWORK_IS_UNIX_CONNECTION (connection));
  g_return_if_fail (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_OPEN);

  item = g_new (BufferItem, 1);
  item->data = g_new (guchar, length + 1);
  G_UCHAR (item->data)[length] = 0;
  memcpy (item->data, data, length);
  item->length = length;

  connection->_priv->buffer = g_slist_append (connection->_priv->buffer, item);

  /* Watch for writability if we're not already */
  if (!(connection->_priv->source_cond & G_IO_OUT))
    {
      if (connection->_priv->source_id != 0)
	gnetwork_thread_source_remove (connection->_priv->source_id);

      connection->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_OUT | G_IO_ERR | G_IO_HUP);
      connection->_priv->source_id =
	gnetwork_thread_io_add_watch (connection->_priv->channel, connection->_priv->source_cond,
				      (GIOFunc) io_channel_handler, connection);
    }
}


static void
gnetwork_unix_connection_connection_iface_init (GNetworkConnectionIface * iface)
{
  iface->open = (GNetworkConnectionFunc) gnetwork_unix_connection_open;
  iface->close = (GNetworkConnectionFunc) gnetwork_unix_connection_close;
  iface->send = (GNetworkConnectionSendFunc) gnetwork_unix_connection_send;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_unix_connection_set_property (GObject * object, guint property, const GValue * value,
				       GParamSpec * param_spec)
{
  GNetworkUnixConnection *connection = GNETWORK_UNIX_CONNECTION (object);

  switch (property)
    {
    case FILENAME:
      {
	const gchar *filename = g_value_get_string (value);

	g_return_if_fail (filename == NULL || filename[0] != '\0');
	g_return_if_fail (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_CLOSED);

	g_free (connection->_priv->filename);

	if (filename != NULL)
	  {
	    connection->_priv->filename = g_strdup (filename);
	  }
	else
	  {
	    static guint pid = 0, index_ = 0;

	    if (pid == 0)
	      pid = getpid ();

	    connection->_priv->filename = g_strdup_printf ("%s" G_DIR_SEPARATOR_S "%s"
							   G_DIR_SEPARATOR_S "%x-%x-%x%x",
							   g_get_tmp_dir (), g_get_user_name (),
							   pid, index_, g_rand_int (NULL) ^ pid,
							   g_rand_int (NULL) ^ index_);

	    index_++;
	  }
      }
      break;

    case SOCKET_FD:
      g_return_if_fail (connection->_priv->unix_status == GNETWORK_UNIX_CONNECTION_CLOSED);

      connection->_priv->sockfd = GPOINTER_TO_INT (g_value_get_pointer (value));
      break;

    case CXN_TYPE:
      connection->_priv->cxn_type = g_value_get_enum (value);
      break;
    case CXN_BUFFER_SIZE:
      g_return_if_fail (connection->_priv->unix_status < GNETWORK_UNIX_CONNECTION_OPENING);
      connection->_priv->buffer_size = g_value_get_uint (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_unix_connection_get_property (GObject * object, guint property, GValue * value,
				       GParamSpec * param_spec)
{
  GNetworkUnixConnection *connection = GNETWORK_UNIX_CONNECTION (object);

  switch (property)
    {
      /* GNetworkUnixConnection */
    case UNIX_STATUS:
      g_value_set_enum (value, connection->_priv->unix_status);
      break;
    case FILENAME:
      g_value_set_string (value, connection->_priv->filename);
      break;
    case SOCKET_FD:
      g_value_set_pointer (value, GINT_TO_POINTER (connection->_priv->sockfd));
      break;

      /* GNetworkConnectionIface */
    case CXN_TYPE:
      g_value_set_enum (value, connection->_priv->cxn_type);
      break;
    case CXN_STATUS:
      g_value_set_enum (value, connection->_priv->cxn_status);
      break;
    case CXN_BUFFER_SIZE:
      g_value_set_uint (value, connection->_priv->buffer_size);
      break;
    case CXN_BYTES_RECEIVED:
      g_value_set_ulong (value, connection->_priv->bytes_received);
      break;
    case CXN_BYTES_SENT:
      g_value_set_ulong (value, connection->_priv->bytes_sent);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_unix_connection_dispose (GObject * object)
{
  GNetworkUnixConnection *connection = GNETWORK_UNIX_CONNECTION (object);

  if (connection->_priv->unix_status > GNETWORK_UNIX_CONNECTION_CLOSED)
    gnetwork_unix_connection_close (connection);

  if (G_OBJECT_CLASS (parent_class)->dispose)
    (*G_OBJECT_CLASS (parent_class)->dispose) (object);
}


static void
gnetwork_unix_connection_finalize (GObject * object)
{
  GNetworkUnixConnection *connection = GNETWORK_UNIX_CONNECTION (object);

  g_free (connection->_priv->filename);
  g_free (connection->_priv);

  if (G_OBJECT_CLASS (parent_class)->finalize)
    (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_unix_connection_class_init (GNetworkUnixConnectionClass * class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  parent_class = g_type_class_peek_parent (class);

  object_class->get_property = gnetwork_unix_connection_get_property;
  object_class->set_property = gnetwork_unix_connection_set_property;
  object_class->dispose = gnetwork_unix_connection_dispose;
  object_class->finalize = gnetwork_unix_connection_finalize;

  g_object_class_install_property (object_class, UNIX_STATUS,
				   g_param_spec_enum ("unix-status", _("UNIX/IP Connection Status"),
						      _("The current status of the UNIX "
							"connection."),
						      GNETWORK_TYPE_UNIX_CONNECTION_STATUS,
						      GNETWORK_UNIX_CONNECTION_CLOSED,
						      G_PARAM_READABLE));

  g_object_class_install_property (object_class, FILENAME,
				   g_param_spec_string ("filename", _("Filename"),
							_("The filename of the UNIX socket in "
							  "question."), NULL,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class, SOCKET_FD,
				   g_param_spec_pointer ("socket",
							 _("Socket File Descriptor"),
							 _("The socket file descriptor. For use by "
							   "GNetworkUnixServer."),
							 (G_PARAM_PRIVATE | G_PARAM_READWRITE)));

  /* GNetworkConnectionIface Properties */
  g_object_class_override_property (object_class, CXN_TYPE, "connection-type");
  g_object_class_override_property (object_class, CXN_STATUS, "status");
  g_object_class_override_property (object_class, CXN_BYTES_RECEIVED, "bytes-sent");
  g_object_class_override_property (object_class, CXN_BYTES_SENT, "bytes-received");
  g_object_class_override_property (object_class, CXN_BUFFER_SIZE, "buffer-size");
}


static void
gnetwork_unix_connection_instance_init (GNetworkUnixConnection * connection)
{
  connection->_priv = g_new (GNetworkUnixConnectionPrivate, 1);

  connection->_priv->cxn_type = GNETWORK_CONNECTION_CLIENT;
  connection->_priv->cxn_status = GNETWORK_CONNECTION_CLOSED;
  connection->_priv->unix_status = GNETWORK_UNIX_CONNECTION_CLOSED;

  connection->_priv->filename = NULL;
  connection->_priv->buffer = NULL;

  connection->_priv->channel = NULL;
  connection->_priv->source_id = 0;
  connection->_priv->sockfd = -1;
}


/* ************ *
 *  PUBLIC API  *
 * ************ */

GType
gnetwork_unix_connection_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      static const GTypeInfo info = {
	sizeof (GNetworkUnixConnectionClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_unix_connection_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkUnixConnection),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_unix_connection_instance_init,
	NULL			/* value table */
      };
      static const GInterfaceInfo cxn_info = {
	(GInterfaceInitFunc) gnetwork_unix_connection_connection_iface_init,
	NULL, NULL
      };

      type = g_type_register_static (G_TYPE_OBJECT, "GNetworkUnixConnection", &info, 0);

      g_type_add_interface_static (type, GNETWORK_TYPE_CONNECTION, &cxn_info);
    }

  return type;
}
