/* 
 * GNetwork Library: libgnetwork/gnetwork-interfaces.h
 *
 * Copyright (C) 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_INTERFACES_H__
#define __GNETWORK_INTERFACES_H__

#include "gnetwork-object.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_INTERFACE \
  (gnetwork_interface_get_type ())
#define GNETWORK_INTERFACE(object) \
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GNETWORK_TYPE_INTERFACE, GNetworkInterface))
#define GNETWORK_INTERFACE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_INTERFACE, GNetworkInterfaceClass))
#define GNETWORK_IS_INTERFACE(object) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GNETWORK_TYPE_INTERFACE))
#define GNETWORK_IS_INTERFACE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_INTERFACE))
#define GNETWORK_INTERFACE_GET_CLASS(object) \
  (G_TYPE_INSTANCE_GET_CLASS ((object), GNETWORK_TYPE_INTERFACE, GNetworkInterfaceClass))


typedef enum /* <prefix=GNETWORK_INTERFACE> */
{
  GNETWORK_INTERFACE_UNKNOWN,
  GNETWORK_INTERFACE_LOOPBACK,
  GNETWORK_INTERFACE_ETHERNET,
  GNETWORK_INTERFACE_DIALUP,
  GNETWORK_INTERFACE_SERIAL,
  GNETWORK_INTERFACE_PARALLEL,
  GNETWORK_INTERFACE_IRDA_LAN,
  GNETWORK_INTERFACE_WAVE_LAN,
  GNETWORK_INTERFACE_SHAPER,
  GNETWORK_INTERFACE_IPV6_IN_IPV4,
  GNETWORK_INTERFACE_IP_IN_IP_TUNNEL
}
GNetworkInterfaceType;

typedef enum /* <flags,prefix=GNETWORK_INTERFACE> */
{
  GNETWORK_INTERFACE_NONE		= 0,

  /* Status */
  GNETWORK_INTERFACE_IS_UP		= 1 << 0,
  GNETWORK_INTERFACE_IS_RUNNING		= 1 << 1,
  GNETWORK_INTERFACE_IS_DEBUGGING	= 1 << 2,

  /* Special Types */
  GNETWORK_INTERFACE_IS_LOOPBACK        = 1 << 3,
  GNETWORK_INTERFACE_IS_POINT_TO_POINT  = 1 << 4,
  GNETWORK_INTERFACE_IS_LOAD_MASTER	= 1 << 5,
  GNETWORK_INTERFACE_IS_LOAD_SLAVE	= 1 << 6,

  /* Capabilities */
  GNETWORK_INTERFACE_CAN_BROADCAST	= 1 << 7,
  GNETWORK_INTERFACE_CAN_MULTICAST	= 1 << 8,
  GNETWORK_INTERFACE_NO_TRAILERS	= 1 << 9,

  GNETWORK_INTERFACE_NO_ARP		= 1 << 10,
  GNETWORK_INTERFACE_CAN_SET_MEDIA	= 1 << 11,
  GNETWORK_INTERFACE_ALTERNATE_LINK	= 1 << 12,
  GNETWORK_INTERFACE_AUTOSELECTED_MEDIA = 1 << 13,

  /* Modes */
  GNETWORK_INTERFACE_RECV_ALL_PACKETS	= 1 << 14,
  GNETWORK_INTERFACE_RECV_ALL_MULTICAST	= 1 << 15
}
GNetworkInterfaceFlags;


typedef enum /* <prefix=GNETWORK_PROTOCOL> */
{
  GNETWORK_PROTOCOL_NONE    = 0,
  GNETWORK_PROTOCOL_IPv4    = 1 << 0,
  GNETWORK_PROTOCOL_IPv6    = 1 << 1,
  GNETWORK_PROTOCOL_PACKET  = 1 << 2
}
GNetworkProtocols;


typedef enum /* <prefix=GNETWORK_INTERFACE_UPDATE> */
{
  GNETWORK_INTERFACE_UPDATE_NEVER,
  GNETWORK_INTERFACE_UPDATE_IDLE,
  GNETWORK_INTERFACE_UPDATE_TIMEOUT
}
GNetworkInterfaceUpdateMode;


typedef struct _GNetworkInterface GNetworkInterface;
typedef struct _GNetworkInterfacePrivate GNetworkInterfacePrivate;
typedef struct _GNetworkInterfaceClass GNetworkInterfaceClass;

struct _GNetworkInterface
{
  GNetworkObject parent;

  GNetworkInterfacePrivate *_priv;
};

struct _GNetworkInterfaceClass
{
  /* <private> */
  GNetworkObjectClass parent_class;

  void (*__gnetwork_reserved1);
  void (*__gnetwork_reserved2);
  void (*__gnetwork_reserved3);
  void (*__gnetwork_reserved4);
};

GType gnetwork_interface_get_type (void) G_GNUC_CONST;

GNetworkInterface *gnetwork_interface_new (void);
GNetworkInterface *gnetwork_interface_get_for_name (const gchar * name);
GNetworkInterface *gnetwork_interface_get_for_address (const gchar * address);

void gnetwork_interface_load_from_name (GNetworkInterface *interface, const gchar * name);
void gnetwork_interface_load_from_address (GNetworkInterface *interface, const gchar * address);
GSList *gnetwork_interface_get_all_interfaces (void);

G_CONST_RETURN GSList *gnetwork_interface_get_ip4_multicasts (GNetworkInterface * interface);
G_CONST_RETURN GSList *gnetwork_interface_get_ip6_multicasts (GNetworkInterface * interface);

gint gnetwork_interface_collate (gconstpointer interface1, gconstpointer interface2);



#define GNETWORK_TYPE_INTERFACE_INFO	(gnetwork_interface_info_get_type ())
#define GNETWORK_IS_INTERFACE_INFO(ptr)	(G_TYPE_CHECK_CLASS_TYPE (ptr, GNETWORK_TYPE_INTERFACE_INFO))
#define GNETWORK_INTERFACE_INFO(ptr)	(G_TYPE_CHECK_CLASS_CAST (ptr, GNETWORK_TYPE_INTERFACE_INFO, GNetworkInterfaceInfo))



typedef struct _GNetworkInterfaceInfo GNetworkInterfaceInfo;


GType gnetwork_interface_info_get_type (void) G_GNUC_CONST;

GNetworkInterfaceInfo *gnetwork_interface_get_info (const gchar * name);
GNetworkInterfaceInfo *gnetwork_interface_get_info_by_address (const gchar * address);

gchar *gnetwork_interface_info_get_name (const GNetworkInterfaceInfo * info);
GNetworkProtocols gnetwork_interface_info_get_protocols (const GNetworkInterfaceInfo * info);
gconstpointer gnetwork_interface_info_get_address (const GNetworkInterfaceInfo * info,
						   GNetworkProtocols protocol);
gconstpointer gnetwork_interface_info_get_broadcast_address (const GNetworkInterfaceInfo * info,
							     GNetworkProtocols protocol);
gconstpointer gnetwork_interface_info_get_netmask (const GNetworkInterfaceInfo * info,
						   GNetworkProtocols protocol);
G_CONST_RETURN GSList *gnetwork_interface_info_get_multicasts (const GNetworkInterfaceInfo * info,
							       GNetworkProtocols protocol);
GNetworkInterfaceFlags gnetwork_interface_info_get_flags (const GNetworkInterfaceInfo * info);
guint gnetwork_interface_info_get_index (const GNetworkInterfaceInfo * info);

GNetworkInterfaceInfo *gnetwork_interface_info_ref (GNetworkInterfaceInfo * info);
void gnetwork_interface_info_unref (GNetworkInterfaceInfo * info);
gint gnetwork_interface_info_collate (const GNetworkInterfaceInfo * info1,
				      const GNetworkInterfaceInfo * info2);


G_END_DECLS

#endif /* !__GNETWORK_INTERFACES_H__ */
