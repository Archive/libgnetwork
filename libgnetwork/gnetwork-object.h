/* 
 * GNetwork Library: libgnetwork/gnetwork-object.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_OBJECT_H__
#define __GNETWORK_OBJECT_H__

#include <glib-object.h>

G_BEGIN_DECLS


#define GNETWORK_TYPE_OBJECT \
  (gnetwork_object_get_type ())
#define GNETWORK_OBJECT(object) \
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GNETWORK_TYPE_OBJECT, GNetworkObject))
#define GNETWORK_OBJECT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_OBJECT, GNetworkObjectClass))
#define GNETWORK_IS_OBJECT(object) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GNETWORK_TYPE_OBJECT))
#define GNETWORK_IS_OBJECT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_OBJECT))
#define GNETWORK_OBJECT_GET_CLASS(object) \
  (G_TYPE_INSTANCE_GET_CLASS ((object), GNETWORK_TYPE_OBJECT, GNetworkObjectClass))

#define GNETWORK_OBJECT_LOCK(object) \
  (gnetwork_object_lock (GNETWORK_OBJECT (object)))
#define GNETWORK_OBJECT_UNLOCK(object) \
  (gnetwork_object_unlock (GNETWORK_OBJECT (object)))
#define GNETWORK_OBJECT_TRYLOCK(object) \
  (gnetwork_object_trylock (GNETWORK_OBJECT (object)))

#ifndef G_TYPE_MAIN_CONTEXT
# define G_TYPE_MAIN_CONTEXT GNETWORK_TYPE_MAIN_CONTEXT
#endif /* G_TYPE_MAIN_CONTEXT */

#define GNETWORK_TYPE_MAIN_CONTEXT \
  (gnetwork_gmain_context_get_type ())


typedef struct _GNetworkObject GNetworkObject;
typedef struct _GNetworkObjectPrivate GNetworkObjectPrivate;
typedef struct _GNetworkObjectClass GNetworkObjectClass;
  
typedef gboolean (*GNetworkObjectSourceFunc) (GNetworkObject * object, gpointer user_data);
typedef gboolean (*GNetworkObjectIoFunc) (GNetworkObject * object, GIOChannel * channel,
					  GIOCondition condition, gpointer user_data);


struct _GNetworkObject
{
  GObject parent;

  GNetworkObjectPrivate *_priv;
};

struct _GNetworkObjectClass
{
  /* <private> */
  GObjectClass parent_class;

  void (*__gnetwork_padding1);
  void (*__gnetwork_padding2);
  void (*__gnetwork_padding3);
  void (*__gnetwork_padding4);
};


GType gnetwork_object_get_type (void) G_GNUC_CONST;
GType gnetwork_gmain_context_get_type (void) G_GNUC_CONST;

void gnetwork_object_lock (GNetworkObject * object);
void gnetwork_object_unlock (GNetworkObject * object);
gboolean gnetwork_object_trylock (GNetworkObject * object);

#define gnetwork_object_add_idle(object,func,user_data) \
  (gnetwork_object_add_idle_full ((object), G_PRIORITY_DEFAULT_IDLE, (func), (user_data), \
					   NULL))
guint gnetwork_object_add_idle_full (GNetworkObject * object, gint priority,
				     GNetworkObjectSourceFunc func, gpointer user_data,
				     GDestroyNotify notify);

#define gnetwork_object_add_timeout(object,interval,func,user_data) \
  (gnetwork_object_add_timeout_full ((object), interval, G_PRIORITY_DEFAULT, (func), (user_data), \
				     NULL))
guint gnetwork_object_add_timeout_full (GNetworkObject * object, gint interval, gint priority,
					GNetworkObjectSourceFunc func, gpointer user_data,
					GDestroyNotify notify);

#define gnetwork_object_add_io_watch(object,channel,condition,func,user_data) \
  (gnetwork_object_add_io_watch_full ((object), (channel), (condition), G_PRIORITY_DEFAULT, \
				      (func), (user_data), NULL))
guint gnetwork_object_add_io_watch_full (GNetworkObject * object, GIOChannel * channel,
					 GIOCondition condition, gint priority,
					 GNetworkObjectIoFunc func, gpointer user_data,
					 GDestroyNotify notify);

guint gnetwork_object_attach_source (GNetworkObject * object, GSource * source);
gboolean gnetwork_object_remove_source (GNetworkObject * object, guint tag);

void gnetwork_object_class_install_param (GNetworkObjectClass * klass, GParamSpec * pspec);
GParamSpec *gnetwork_object_class_find_param (GNetworkObjectClass * klass, const gchar * name);
GParamSpec **gnetwork_object_class_list_params (GNetworkObjectClass * klass, guint * n_params);
GList *gnetwork_object_class_list_params_for_type (GNetworkObjectClass * klass, GType type);



G_END_DECLS

#endif /* __GNETWORK_OBJECT_H__ */
