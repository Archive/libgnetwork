/* 
 * GNetwork Library: libgnetwork/gnetwork-ip-address.h
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_IP_ADDRESS_H__
#define __GNETWORK_IP_ADDRESS_H__

#include <glib-object.h>

#include <string.h>

G_BEGIN_DECLS


#define GNETWORK_TYPE_IP_ADDRESS	(gnetwork_ip_address_get_type ())
#define GNETWORK_IP_ADDRESS(addr)	((GNetworkIpAddress *) (addr))

#define GNETWORK_IP_ADDRESS8(addr,pos) \
  (GNETWORK_IP_ADDRESS (addr)->addr8[pos])
#define GNETWORK_IP_ADDRESS16(addr,pos) \
  (GNETWORK_IP_ADDRESS (addr)->addr16[pos])
#define GNETWORK_IP_ADDRESS32(addr,pos) \
  (GNETWORK_IP_ADDRESS (addr)->addr32[pos])
#define GNETWORK_IP_ADDRESS64(addr,pos) \
  (GNETWORK_IP_ADDRESS (addr)->addr64[pos])

typedef enum /* <flags,prefix=GNETWORK_IP_ADDRESS_MULTICAST> */
{
  GNETWORK_IP_ADDRESS_MULTICAST_IS_TRANSIENT = 0x1
}
GNetworkIpAddressMulticastFlags;


typedef enum /* <prefix=GNETWORK_IP_ADDRESS_MULTICAST_SCOPE> */
{
  GNETWORK_IP_ADDRESS_MULTICAST_SCOPE_RESERVED_0 = 0,
  GNETWORK_IP_ADDRESS_MULTICAST_SCOPE_NODE_LOCAL = 0x1,
  GNETWORK_IP_ADDRESS_MULTICAST_SCOPE_LINK_LOCAL = 0x2,
  GNETWORK_IP_ADDRESS_MULTICAST_SCOPE_SITE_LOCAL = 0x5,
  GNETWORK_IP_ADDRESS_MULTICAST_SCOPE_ORGANIZATION_LOCAL = 0x8,
  GNETWORK_IP_ADDRESS_MULTICAST_SCOPE_GLOBAL = 0xE,
  GNETWORK_IP_ADDRESS_MULTICAST_SCOPE_RESERVED_F = 0xF
}
GNetworkIpAddressMulticastScope;


typedef union _GNetworkIpAddress GNetworkIpAddress;

union _GNetworkIpAddress
{
  guint8 addr8[16];
  guint16 addr16[8];
  guint32 addr32[4];
  guint64 addr64[2];
};


GType gnetwork_ip_address_get_type (void) G_GNUC_CONST;

GNetworkIpAddress *gnetwork_ip_address_new (const gchar * str);
gchar *gnetwork_ip_address_to_string (const GNetworkIpAddress * address);
gboolean gnetwork_ip_address_set_from_string (GNetworkIpAddress * address, const gchar * str);
GNetworkIpAddress *gnetwork_ip_address_dup (const GNetworkIpAddress * address);

gint gnetwork_ip_address_collate (const GNetworkIpAddress * address1,
				  const GNetworkIpAddress * address2);
gboolean gnetwork_ip_address_equal (gconstpointer address1, gconstpointer address2);
guint gnetwork_ip_address_hash (gconstpointer address);
gboolean gnetwork_str_is_ip_address (const gchar * str);


#define gnetwork_ip_address_get8(addr,pos) \
  (addr != NULL ? GNETWORK_IP_ADDRESS8 (addr, pos) : 0)
#define gnetwork_ip_address_get16(addr,pos) \
  (addr != NULL ? GUINT16_FROM_BE (GNETWORK_IP_ADDRESS16 (addr,pos)) : 0)
#define gnetwork_ip_address_get32(addr,pos) \
  (addr != NULL ? GUINT32_FROM_BE (GNETWORK_IP_ADDRESS32 (addr, pos)) : 0)
#define gnetwork_ip_address_get64(addr,pos) \
  (addr != NULL ? GUINT64_FROM_BE (GNETWORK_IP_ADDRESS64 (addr, pos)) : 0)

#define gnetwork_ip_address_set8(addr,pos,val) \
  (addr != NULL ? GNETWORK_IP_ADDRESS8 (addr, pos) = val : \
   g_warning ("Invalid cast from `null' to %s.", g_type_name (GNETWORK_TYPE_IP_ADDRESS)))
#define gnetwork_ip_address_set16(addr,pos,val) \
  (addr != NULL ? GNETWORK_IP_ADDRESS16 (addr, pos) = GUINT16_TO_BE (val) : \
   g_warning ("Invalid cast from `null' to %s.", g_type_name (GNETWORK_TYPE_IP_ADDRESS)))
#define gnetwork_ip_address_set32(addr,pos,val) \
  (addr != NULL ? GNETWORK_IP_ADDRESS32 (addr, pos) = GUINT32_TO_BE (val) : \
   g_warning ("Invalid cast from `null' to %s.", g_type_name (GNETWORK_TYPE_IP_ADDRESS)))
#define gnetwork_ip_address_set64(addr,pos,val) \
  (addr != NULL ? GNETWORK_IP_ADDRESS64 (addr, pos) = GUINT64_TO_BE (val) : \
   g_warning ("Invalid cast from `null' to %s.", g_type_name (GNETWORK_TYPE_IP_ADDRESS)))

#define gnetwork_ip_address_is_address(addr) \
  (addr != NULL && gnetwork_ip_address_get8 (addr, 15) != 0)
#define gnetwork_ip_address_is_network(addr) \
  (addr != NULL && gnetwork_ip_address_get8 (addr, 15) == 0)
#define gnetwork_ip_address_is_valid(addr) \
  (GNETWORK_IP_ADDRESS64 (addr, 0) != 0 || GNETWORK_IP_ADDRESS64 (addr, 1) != 0)
#define gnetwork_ip_address_is_multicast(addr) \
  (addr != NULL && \
   ((gnetwork_ip_address_is_ipv4 (addr) && GNETWORK_IP_ADDRESS8 (addr, 12) > 224) || \
    gnetwork_ip_address_get8 (addr, 0) == 0xFF))
#define gnetwork_ip_address_get_multicast_flags(addr) \
  ((addr != NULL && GNETWORK_IP_ADDRESS16 (addr, 0) == 0xFFFF) ? \
   (gnetwork_ip_address_get8 (addr, 1) & 0xF0) : 0)
#define gnetwork_ip_address_get_multicast_scope(addr) \
  ((addr != NULL && GNETWORK_IP_ADDRESS16 (addr, 0) == 0xFFFF) ? \
   (gnetwork_ip_address_get8 (addr, 1) & 0x0F) : 0);

#define gnetwork_ip_address_is_ipv4(addr) \
  (addr != NULL && \
   gnetwork_ip_address_is_valid (addr) && \
   GNETWORK_IP_ADDRESS64 (addr, 0) == 0 && \
   GNETWORK_IP_ADDRESS8 (addr, 12) != 0 && \
   GNETWORK_IP_ADDRESS16 (addr, 4) == 0 && \
   (GNETWORK_IP_ADDRESS16 (addr, 5) == 0xFFFF || \
    GNETWORK_IP_ADDRESS16 (addr, 5) == 0))
#define gnetwork_ip_address_get_ipv4(addr) \
  (GUINT32_FROM_BE (GNETWORK_IP_ADDRESS32 (addr, 3)))

#define gnetwork_ip_address_init(addr) \
  (memset ((addr), 0, sizeof (GNetworkIpAddress)))

#define GNETWORK_IP_ADDRESS_INIT \
  {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}

#ifdef __GNUC__
#define gnetwork_ip_address_set_ipv4(addr,ip) \
  (G_GNUC_EXTENSION ({ \
    gnetwork_ip_address_set64 (addr, 0, 0); \
    gnetwork_ip_address_set16 (addr, 4, 0); \
    gnetwork_ip_address_set16 (addr, 5, 0xFFFF); \
    gnetwork_ip_address_set32 (addr, 3, ip); \
  }))
#else
void gnetwork_ip_address_set_ipv4 (GNetworkIpAddress *address, guint32 ip);
#endif /* __GNUC__ */


G_END_DECLS

#endif /* !__GNETWORK_IP_ADDRESS_H__ */
