/*
 * GNetwork Library: libgnetwork/gnetwork-socket.c
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-socket.h"

#include "gnetwork-errors.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"
#include "marshal.h"

#include <glib/gi18n.h>

#define GNETWORK_SOCKET_GET_PRIVATE(obj)	(GNETWORK_SOCKET (obj)->_priv)


/**
 * GNetworkSocket:
 * 
 * The structure for socket object instances. This structures contains no
 * public members.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkSocketClass:
 * @open: the vtable entry for the "open" method.
 * @close: the vtable entry for the "close" method.
 * @error: the C closure location for the "error" signal.
 * 
 * The structure for the socket object class. All signals and methods are
 * parent-relative, but may be %NULL.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkSocketState:
 * @GNETWORK_SOCKET_STATE_FIRST: the lowest integer used by GNetworkSocket.
 * @GNETWORK_SOCKET_STATE_CLOSING: the low-level socket connection is closing.
 * @GNETWORK_SOCKET_STATE_CLOSED: the low-level socket connection is closed.
 * @GNETWORK_SOCKET_STATE_OPENING: the low-level socket connection is currently being opened.
 * @GNETWORK_SOCKET_STATE_OPEN: the low-level socket connection is open.
 * @GNETWORK_SOCKET_STATE_LAST: the highest integer used by GNetworkSocket.
 * 
 * An enumeration of common values used by the "state" property. Subclasses
 * should not override any of the integers above, instead use the LAST and FIRST items
 * to stake out unique integers.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkSocketError:
 * @GNETWORK_SOCKET_ERROR_INTERNAL: a generic error.
 * @GNETWORK_SOCKET_ERROR_PERMISSIONS: the user does not have permissions to use this socket.
 * @GNETWORK_SOCKET_ERROR_ALREADY_EXISTS: the socket already exists.
 * 
 * An enumeration of common socket errors.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_SOCKET:
 * @object: the pointer to cast.
 * 
 * Casts the #GNetworkSocket derived pointer at @object into a (GNetworkSocket*)
 * pointer. Depending on the current debugging level, this function may invoke
 * certain runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_SOCKET:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GNetworkSocket (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_SOCKET_CLASS:
 * @klass: the pointer to cast.
 * 
 * Casts a the #GNetworkSocketClass derieved pointer at @klass into a
 * (GNetworkSocketClass*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_SOCKET_CLASS:
 * @klass: the pointer to check.
 * 
 * Checks whether @klass is a #GNetworkSocketClass (or derived) class.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_SOCKET_GET_CLASS:
 * @object: the pointer to check.
 * 
 * Retrieves a pointer to the #GNetworkSocketClass for the #GNetworkSocket (or
 * derived) instance @object.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_TYPE_SOCKET:
 * 
 * The registered #GType of #GNetworkSocket.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_SOCKET_ERROR:
 * 
 * The GQuark error domain used for #GNetworkSocketError.
 * 
 * Since: 1.0
 **/


/* ************** *
 *  Enumerations  *
 * ************** */

enum
{
  PROP_0,

  STATE,
  BYTES_RECEIVED,
  BYTES_SENT
};

enum
{
  ERROR,

  LAST_SIGNAL
};


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkSocketPrivate
{
  gulong bytes_received;
  gulong bytes_sent;

  gint state;
};


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer gnetwork_socket_parent_class = NULL;
static guint gnetwork_socket_signals[LAST_SIGNAL] = { 0 };


/* ********************** *
 *  GLogLevelFlags GType  *
 * ********************** */

#define GNETWORK_TYPE_GLOG_LEVEL_FLAGS	(gnetwork_glog_level_flags_get_type ())

static GType
gnetwork_glog_level_flags_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      static const GFlagsValue values[] = {
	{ G_LOG_FLAG_RECURSION, "G_LOG_FLAG_RECURSION", "recursion" },
	{ G_LOG_FLAG_FATAL, "G_LOG_FLAG_FATAL", "fatal" },
	{ G_LOG_LEVEL_ERROR, "G_LOG_LEVEL_ERROR", "error" },
	{ G_LOG_LEVEL_CRITICAL, "G_LOG_LEVEL_CRITICAL", "critical" },
	{ G_LOG_LEVEL_WARNING, "G_LOG_LEVEL_WARNING", "warning" },
	{ G_LOG_LEVEL_MESSAGE, "G_LOG_LEVEL_MESSAGE", "message" },
	{ G_LOG_LEVEL_INFO, "G_LOG_LEVEL_INFO", "info" },
	{ G_LOG_LEVEL_DEBUG, "G_LOG_LEVEL_DEBUG", "debug" },
	{ 0, NULL, NULL }
      };

      type = g_flags_register_static ("GLogLevelFlags", values);
    }

  return type;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_socket_set_property (GObject * object, guint property, const GValue * value,
			      GParamSpec * pspec)
{
  GNetworkSocketPrivate *priv = GNETWORK_SOCKET_GET_PRIVATE (object);

  switch (property)
    {
    case STATE:
      priv->state = g_value_get_int (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_socket_get_property (GObject * object, guint property, GValue * value, GParamSpec * pspec)
{
  GNetworkSocketPrivate *priv = GNETWORK_SOCKET_GET_PRIVATE (object);

  switch (property)
    {
      /* GNetworkSocket */
    case STATE:
      g_value_set_int (value, priv->state);
      break;
    case BYTES_RECEIVED:
      g_value_set_ulong (value, priv->bytes_received);
      break;
    case BYTES_SENT:
      g_value_set_ulong (value, priv->bytes_sent);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_socket_dispose (GObject * object)
{
  GNetworkSocket *socket = GNETWORK_SOCKET (object);

  if (socket->_priv->state > GNETWORK_SOCKET_STATE_CLOSED)
    gnetwork_socket_close (socket);

  if (G_OBJECT_CLASS (gnetwork_socket_parent_class)->dispose != NULL)
    (*G_OBJECT_CLASS (gnetwork_socket_parent_class)->dispose) (object);
}


static void
gnetwork_socket_finalize (GObject * object)
{
  GNetworkSocketPrivate *priv = GNETWORK_SOCKET_GET_PRIVATE (object);

  g_free (priv);

  if (G_OBJECT_CLASS (gnetwork_socket_parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (gnetwork_socket_parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_socket_class_init (GNetworkSocketClass * class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GNetworkObjectClass *netobject_class = GNETWORK_OBJECT_CLASS (class);

  gnetwork_socket_parent_class = g_type_class_peek_parent (class);

  gobject_class->set_property = gnetwork_socket_set_property;
  gobject_class->get_property = gnetwork_socket_get_property;
  gobject_class->dispose = gnetwork_socket_dispose;
  gobject_class->finalize = gnetwork_socket_finalize;

  class->error = NULL;

  class->open = NULL;
  class->close = NULL;

  /* Signals */
/**
 * GNetworkSocket::error:
 * @socket: the object which emitted this signal.
 * @params: parameters specific to this signal emission.
 * 
 * This signal is emitted by socket objects when they experience an error. The
 * return value indicates whether or not the socket should remain open or not.
 * This signal is a detailed signal, which means it can be filtered by
 * connecting to a particular error domain:
 * 
 * <informalexample><programlisting>g_signal_connect (socket, "error::gnetwork-socket-error",
 *                   G_CALLBACK (my_error_cb), NULL);</programlisting></informalexample>
 * 
 * Object callbacks which are connected to this signal should take care to
 * always pass %FALSE returns they receive when chaining to the parent
 * implementation.
 * 
 * Returns: %TRUE if the socket should remain open, %FALSE if it should be closed.
 * 
 * Since: 1.0
 **/
  gnetwork_socket_signals[ERROR] =
    g_signal_new ("error",
		  G_TYPE_FROM_CLASS (class),
		  (G_SIGNAL_DETAILED | G_SIGNAL_RUN_LAST),
		  G_STRUCT_OFFSET (GNetworkSocketClass, error),
		  NULL, NULL,
		  _gnetwork_marshal_BOOLEAN__BOXED,
		  G_TYPE_BOOLEAN, 1, GNETWORK_TYPE_PARAMS);

  /* Methods */

  /* GObject Properties */
  g_object_class_install_property (gobject_class, STATE,
				   g_param_spec_int ("socket-state", _("Socket State"),
						     _("The state of this socket (writable only "
						       "by subclasses)."),
						     G_MININT, G_MAXINT,
						     GNETWORK_SOCKET_STATE_CLOSED,
						     G_PARAM_READABLE));
  g_object_class_install_property (gobject_class, BYTES_RECEIVED,
				   g_param_spec_ulong ("bytes-received", _("Bytes Received"),
						       _("The number of bytes received through "
							 "this socket."), 0, G_MAXULONG, 0,
						       G_PARAM_READABLE));
  g_object_class_install_property (gobject_class, BYTES_SENT,
				   g_param_spec_ulong ("bytes-sent", _("Bytes Sent"),
						       _("The number of bytes sent through this "
							 "socket."), 0, G_MAXULONG, 0,
						       G_PARAM_READABLE));

  /* Signal/Method Parameters */
  gnetwork_object_class_install_param (netobject_class,
				       g_param_spec_boxed ("socket-error", _("Error"),
							   _("The error which occurred (used in "
							     "the \"error\" signal)."),
							  G_TYPE_ERROR, G_PARAM_READWRITE));
  gnetwork_object_class_install_param (netobject_class,
				       g_param_spec_flags ("socket-error-flags", _("Error Flags"),
							   _("The severity of an error (used in "
							     "the \"error\" signal)."),
							  GNETWORK_TYPE_GLOG_LEVEL_FLAGS,
							  G_LOG_LEVEL_CRITICAL, G_PARAM_READWRITE));

  g_type_class_add_private (class, sizeof (GNetworkSocketPrivate));
}


static void
gnetwork_socket_instance_init (GNetworkSocket * socket)
{
  socket->_priv = G_TYPE_INSTANCE_GET_PRIVATE (socket, GNETWORK_TYPE_SOCKET,
					       GNetworkSocketPrivate);

  socket->_priv->bytes_received = 0;
  socket->_priv->bytes_sent = 0;
  socket->_priv->state = GNETWORK_SOCKET_STATE_CLOSED;
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_socket_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      static const GTypeInfo info = {
	sizeof (GNetworkSocketClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_socket_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkSocket),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_socket_instance_init,
	NULL			/* value table */
      };

      type = g_type_register_static (GNETWORK_TYPE_OBJECT, "GNetworkSocket", &info,
				     G_TYPE_FLAG_ABSTRACT);
    }

  return type;
}


/**
 * gnetwork_socket_open:
 * @socket: the socket object to open.
 * 
 * This function calls the "open" method to start the connection process for
 * @socket.
 * 
 * Since: 1.0
 **/
void
gnetwork_socket_open (GNetworkSocket * socket)
{
  GNetworkSocketClass *class;

  g_return_if_fail (GNETWORK_IS_SOCKET (socket));

  if (socket->_priv->state != GNETWORK_SOCKET_STATE_CLOSED)
    return;

  class = GNETWORK_SOCKET_GET_CLASS (socket);

  if (class->open != NULL)
    {
      (*class->open) (socket);
    }
  else
    {
      g_warning ("%s: The socket type `%s' does not support the open() method.", G_STRLOC,
		 G_OBJECT_TYPE_NAME (socket));
    }
}


/**
 * gnetwork_socket_close:
 * @socket: the socket object to close.
 * 
 * This function calls the "close" method to close @socket.
 * 
 * Since: 1.0
 **/
void
gnetwork_socket_close (GNetworkSocket * socket)
{
  GNetworkSocketClass *class;

  g_return_if_fail (GNETWORK_IS_SOCKET (socket));

  if (socket->_priv->state <= GNETWORK_SOCKET_STATE_CLOSED)
    return;

  class = GNETWORK_SOCKET_GET_CLASS (socket);

  if (class->close != NULL)
    {
      (*class->close) (socket);
    }
  else
    {
      g_warning ("%s: The socket type `%s' does not support the close() method.", G_STRLOC,
		 G_OBJECT_TYPE_NAME (socket));
    }
}


/**
 * gnetwork_socket_add_to_bytes_sent:
 * @socket: the socket object to modify.
 * @length: the number of bytes which were sent.
 * 
 * Updates the "bytes-sent" property of @socket by adding @length to it.
 * Applications will typically not need to call this function, it exists
 * for subclasses to easily update the byte counters.
 * 
 * Since: 1.0
 **/
void
gnetwork_socket_add_to_bytes_sent (GNetworkSocket * socket, gulong length)
{
  g_return_if_fail (GNETWORK_IS_SOCKET (socket));
  g_return_if_fail (length > 0);

  socket->_priv->bytes_sent += length;
  g_object_notify (G_OBJECT (socket), "bytes-sent");
}


/**
 * gnetwork_socket_add_to_bytes_received:
 * @socket: the socket object to modify.
 * @length: the number of bytes which were received.
 * 
 * Updates the "bytes-received" property of @socket by adding @length to it.
 * Applications will typically not need to call this function, it exists for
 * subclasses to easily update the byte counters.
 * 
 * Since: 1.0
 **/
void
gnetwork_socket_add_to_bytes_received (GNetworkSocket * socket, gulong length)
{
  g_return_if_fail (GNETWORK_IS_SOCKET (socket));
  g_return_if_fail (length > 0);

  socket->_priv->bytes_received += length;
  g_object_notify (G_OBJECT (socket), "bytes-received");
}


/**
 * gnetwork_socket_set_state:
 * @socket: the socket object to modify.
 * @state: the new state.
 * 
 * Modifies the "state" property of @socket to @state. Applications will
 * typically not need to call this function, it exists for subclasses to easily
 * update the byte counters.
 * 
 * Since: 1.0
 **/
void
gnetwork_socket_set_state (GNetworkSocket * socket, gint state)
{
  g_return_if_fail (GNETWORK_IS_SOCKET (socket));

  socket->_priv->state = state;
  g_object_notify (G_OBJECT (socket), "state");
}


/**
 * gnetwork_socket_error:
 * @socket: the socket object.
 * @params: the parameters for this error.
 * 
 * Emits the "error" signal for @socket. Applications typically will not use
 * this function, it exists for subclasses of the socket object. The return
 * value determines whether the socket should remain open or not.
 * 
 * Returns: %TRUE if the socket should remain open, %FALSE if it should be closed.
 * 
 * Since: 1.0
 **/
gboolean
gnetwork_socket_error (GNetworkSocket * socket, GNetworkParams * params)
{
  gboolean retval;
  GError *error;
  GQuark domain;

  g_return_val_if_fail (GNETWORK_IS_SOCKET (socket), FALSE);
  g_return_val_if_fail (GNETWORK_IS_PARAMS (params), FALSE);

  gnetwork_params_get (params, "socket-error", &error, NULL);

  if (error != NULL)
    {
      domain = error->domain;
      g_error_free (error);
    }
  else
    {
      domain = 0;
    }

  g_signal_emit (socket, gnetwork_socket_signals[ERROR], domain, params, &retval);

  return retval;
}


GQuark
gnetwork_socket_error_get_quark (void)
{
  static GQuark quark = 0;

  if (G_UNLIKELY (quark == 0))
    quark = g_quark_from_static_string ("gnetwork-socket-error");
  
  return quark;
}
