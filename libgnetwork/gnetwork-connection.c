/*
 * GNetwork Library: libgnetwork/gnetwork-connection.c
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */


#include "gnetwork-connection.h"

#include "gnetwork-type-builtins.h"
#include "gnetwork-errors.h"

#include "marshal.h"

#include <glib/gi18n.h>

#define G_UCHAR(ptr)	((guchar *) (ptr))


enum
{
  RECEIVED,
  SENT,
  ERROR,
  LAST_SIGNAL
};


typedef struct _EnumString
{
  const guint value;
  const gchar *const str;
}
EnumString;


static gint signals[LAST_SIGNAL] = { 0 };


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_connection_base_init (gpointer g_iface)
{
  static gboolean initialized = FALSE;

  if (!initialized)
    {
      signals[RECEIVED] =
	g_signal_new ("received",
		      GNETWORK_TYPE_CONNECTION,
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (GNetworkConnectionIface, received),
		      NULL, NULL, _gnetwork_marshal_VOID__POINTER_ULONG,
		      G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_ULONG);
      signals[SENT] =
	g_signal_new ("sent",
		      GNETWORK_TYPE_CONNECTION,
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET (GNetworkConnectionIface, sent),
		      NULL, NULL, _gnetwork_marshal_VOID__POINTER_ULONG,
		      G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_ULONG);
      signals[ERROR] =
	g_signal_new ("error",
		      GNETWORK_TYPE_CONNECTION,
		      (G_SIGNAL_RUN_FIRST | G_SIGNAL_DETAILED),
		      G_STRUCT_OFFSET (GNetworkConnectionIface, error),
		      NULL, NULL, g_cclosure_marshal_VOID__BOXED, G_TYPE_NONE, 1, G_TYPE_ERROR);

      g_object_interface_install_property (g_iface,
					   g_param_spec_enum ("connection-type",
							      _("Connection Type"),
							      _("The type of connection "
								"represented by the implementing "
								"object."),
							      GNETWORK_TYPE_CONNECTION_TYPE,
							      GNETWORK_CONNECTION_CLIENT,
							      G_PARAM_READWRITE));

      g_object_interface_install_property (g_iface,
					   g_param_spec_enum ("status", _("Connection Status"),
							      _("The status of this connection."),
							      GNETWORK_TYPE_CONNECTION_STATUS,
							      GNETWORK_CONNECTION_CLOSED,
							      G_PARAM_READABLE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_ulong ("bytes-received",
							       _("Bytes Received"),
							       _("The number of bytes received "
								 "through this connection."),
							       0, G_MAXULONG, 0, G_PARAM_READABLE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_ulong ("bytes-sent", _("Bytes Sent"),
							       _("The number of bytes sent through "
								 "this connection."),
							       0, G_MAXULONG, 0, G_PARAM_READABLE));
      g_object_interface_install_property (g_iface,
					   g_param_spec_uint ("buffer-size", _("Buffer Size"),
							      _("The maximum size in bytes of "
								"outgoing and incoming data "
								"packets."), 0, G_MAXUINT, 2048,
							      G_PARAM_READWRITE));

      initialized = TRUE;
    }
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_connection_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      static const GTypeInfo info = {
	sizeof (GNetworkConnectionIface),	/* class_size */
	gnetwork_connection_base_init,	/* base_init */
	NULL,			/* base_finalize */
	NULL,
	NULL,			/* class_finalize */
	NULL,			/* class_data */
	0,
	0,			/* n_preallocs */
	NULL
      };

      type = g_type_register_static (G_TYPE_INTERFACE, "GNetworkConnection", &info, 0);

      g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
    }

  return type;
}


/**
 * gnetwork_connection_open:
 * @connection: the connection to open.
 *
 * Starts the connection process for @connection.
 *
 * Since: 1.0
 **/
void
gnetwork_connection_open (GNetworkConnection * connection)
{
  GNetworkConnectionIface *iface;

  g_return_if_fail (GNETWORK_IS_CONNECTION (connection));

  iface = GNETWORK_CONNECTION_GET_IFACE (connection);

  g_return_if_fail (iface->open != NULL);

  (*iface->open) (connection);
}


/**
 * gnetwork_connection_close:
 * @connection: the connection to close.
 *
 * Closes the @connection in question.
 *
 * Since: 1.0
 **/
void
gnetwork_connection_close (GNetworkConnection * connection)
{
  GNetworkConnectionIface *iface;

  g_return_if_fail (GNETWORK_IS_CONNECTION (connection));

  iface = GNETWORK_CONNECTION_GET_IFACE (connection);

  g_return_if_fail (iface->close != NULL);

  (*iface->close) (connection);
}


/**
 * gnetwork_connection_send:
 * @connection: the connection to send through.
 * @data: the data to send.
 * @length: the length in bytes of @data.
 *
 * Sends the data in @data through @connection. If @length is less than one,
 * @data is assumed to be terminated by %0. This function will perform the
 * necessary calculations for @length. After calling the implementation's
 * send function, the "send" signal will be emitted.
 *
 * Since: 1.0
 **/
void
gnetwork_connection_send (GNetworkConnection * connection, gconstpointer data, glong length)
{
  GNetworkConnectionIface *iface;

  g_return_if_fail (GNETWORK_IS_CONNECTION (connection));
  g_return_if_fail (data != NULL);
  g_return_if_fail (data != 0);

  iface = GNETWORK_CONNECTION_GET_IFACE (connection);

  g_return_if_fail (iface->send != NULL);

  if (length < 0)
    {
      for (length = 0; *(G_UCHAR (data) + length) != 0; length++);
    }

  (*iface->send) (connection, data, (gulong) length);
}


/**
 * gnetwork_connection_received:
 * @connection: the connection to use.
 * @data: the data being recieved.
 * @length: the length of @data in bytes.
 *
 * Emits the "received" signal for @connection, using the values in @data and @length.
 * Implementations of the #GNetworkConnectionIface interface should use this function
 * when data has been received.
 *
 * Since: 1.0
 **/
void
gnetwork_connection_received (GNetworkConnection * connection, gconstpointer data, gulong length)
{
  g_return_if_fail (GNETWORK_IS_CONNECTION (connection));
  g_return_if_fail (data != NULL);
  g_return_if_fail (length > 0);

  g_signal_emit (connection, signals[RECEIVED], 0, data, length);
}


/**
 * gnetwork_connection_sent:
 * @connection: the connection to use.
 * @data: the data which was sent.
 * @length: the length of @data in bytes.
 *
 * Emits the "sent" signal for @connection, using the values in @data and
 * @length. Implementations of the #GNetworkConnectionIface interface should
 * call this function when data has been sent.
 *
 * Since: 1.0
 **/
void
gnetwork_connection_sent (GNetworkConnection * connection, gconstpointer data, gulong length)
{
  g_return_if_fail (GNETWORK_IS_CONNECTION (connection));
  g_return_if_fail (data != NULL);
  g_return_if_fail (length > 0);

  g_signal_emit (connection, signals[SENT], 0, data, length);
}


/**
 * gnetwork_connection_error:
 * @connection: the connection to use.
 * @error: the error structure.
 *
 * Emits the "error" signal for @connection, using @error. Callers to this function
 * should use their own error domains.
 *
 * Since: 1.0
 **/
void
gnetwork_connection_error (GNetworkConnection * connection, const GError * error)
{
  g_return_if_fail (GNETWORK_IS_CONNECTION (connection));
  g_return_if_fail (error != NULL);

  g_signal_emit (connection, signals[ERROR], error->domain, error);
}


/**
 * gnetwork_connection_strerror:
 * @error: the connection error code to use.
 * 
 * Retrieves a string message describing @error. The returned data should not
 * be modified or freed.
 *
 * Returns: the string message describing @error.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_connection_strerror (GNetworkConnectionError error)
{
  const gchar *str;

  g_return_val_if_fail (_(""), NULL);

  switch (error)
    {
    case GNETWORK_CONNECTION_ERROR_INTERNAL:
      str = _("There was an error inside the networking library.");
      break;
    case GNETWORK_CONNECTION_ERROR_REFUSED:
      str = _("The service will not let you connect.");
      break;
    case GNETWORK_CONNECTION_ERROR_TIMEOUT:
      str = _("The service may be down, or you may have been disconnected from the network.");
      break;
    case GNETWORK_CONNECTION_ERROR_UNREACHABLE:
      str = _("The service could not be contacted.");
      break;
    case GNETWORK_CONNECTION_ERROR_PERMISSIONS:
      str = _("Your computer or firewall is configured to prevent access to the service.");
      break;
    default:
      g_assert_not_reached ();
      str = NULL;
  };

  return str;
}


G_LOCK_DEFINE_STATIC (quark);

GQuark
gnetwork_connection_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (quark);

  if (quark == 0)
    {
      quark = g_quark_from_static_string ("gnetwork-connection-error");
    }

  G_UNLOCK (quark);

  return quark;
}
