/*
 * GNetwork Library: libgnetwork/gnetwork-udp-datagram.c
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-udp-datagram.h"

#include "gnetwork-datagram.h"
#include "gnetwork-dns.h"
#include "gnetwork-errors.h"
#include "gnetwork-interfaces.h"
#include "gnetwork-threads.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"
#include "marshal.h"

#include <glib/gi18n.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>


enum
{
  PROP_0,

  INTERFACE,
  INTERFACE_INFO,
  PORT,
  TTL,
  BROADCAST,
  SOCKET_FD,

  DGRAM_STATUS,
  DGRAM_BYTES_RECEIVED,
  DGRAM_BYTES_SENT,
  DGRAM_BUFFER_SIZE
};


enum
{
  RECEIVED,
  SENT,
  ERROR,
  LAST_SIGNAL
};


struct _GNetworkUdpDatagramPrivate
{
  /* GNetworkUdpDatagram Properties */
  gchar *interface;
  GNetworkInterfaceInfo *interface_info;
  guint16 port;

  /* GNetworkDatagram Properties */
  guint buffer_size;
  gulong bytes_received;
  gulong bytes_sent;

  /* Object Data */
  GSList *buffer;
  GIOChannel *channel;
  gint sockfd;
  guint source_id;
  GIOCondition source_cond:6;

  /* GNetworkUdpDatagram Property Bits */
  GNetworkUdpDatagramTtl ttl:9;
  gboolean broadcast:1;

  /* GNetworkDatagramIface Property Bits */
  GNetworkDatagramStatus dgram_status:3;
};


struct _GNetworkUdpTarget
{
  GNetworkIpAddress ip_address;
  gchar *hostname;
  guint16 port;
};


typedef struct
{
  GNetworkUdpTarget *target;

  gpointer data;
  gulong length;
}
BufferItem;


typedef struct
{
  GNetworkUdpDatagram *udp;
  BufferItem *item;
}
DnsCbData;


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer parent_class = NULL;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static void
free_buffer_item (BufferItem * item)
{
  if (item == NULL)
    return;

  g_free (item->data);
  g_free (item);
}


static void
free_dns_cb_data (DnsCbData * data)
{
  if (data == NULL)
    return;

  g_object_unref (data->udp);
  free_buffer_item (data->item);
}


/* ********************* *
 *  GIOChannel Callback  *
 * ********************* */

static gboolean
io_channel_handler (GIOChannel * channel, GIOCondition cond, GNetworkUdpDatagram * udp)
{
  GNetworkUdpTarget *target;
  GError *error;
  gssize bytes_done;
  gboolean retval;
  struct sockaddr *sa;
  socklen_t sa_size;

  /* Error */
  if (cond & (G_IO_HUP | G_IO_ERR))
    {
      gnetwork_thread_source_remove (udp->_priv->source_id);
      g_io_channel_shutdown (channel, FALSE, NULL);
      g_io_channel_unref (channel);

      udp->_priv->source_cond = 0;
      udp->_priv->source_id = 0;
      udp->_priv->channel = NULL;
      udp->_priv->sockfd = -1;

      udp->_priv->dgram_status = GNETWORK_DATAGRAM_CLOSED;
      g_object_notify (G_OBJECT (udp), "status");
      return FALSE;
    }

  retval = TRUE;

  /* Read */
  if (cond & (G_IO_IN | G_IO_PRI))
    {
      gint en;
      gchar *buffer;
      GValue value = { 0 };

      buffer = g_new0 (gchar, udp->_priv->buffer_size);
      target = g_new0 (GNetworkUdpTarget, 1);

      sa_size = MAX (sizeof (struct sockaddr_in), sizeof (struct sockaddr_in6));
      sa = g_malloc0 (sa_size);

      errno = 0;
#ifdef MSG_NOSIGNAL
      bytes_done = recvfrom (udp->_priv->sockfd, buffer, udp->_priv->buffer_size, MSG_NOSIGNAL,
			     sa, &sa_size);
#else
      bytes_done = recvfrom (udp->_priv->sockfd, buffer, udp->_priv->buffer_size, 0,
	      		    sa, &sa_size);
#endif
      en = errno;

      target->hostname = NULL;
      _gnetwork_ip_address_set_from_sockaddr (&(target->ip_address), sa);
      target->port = _gnetwork_sockaddr_get_port (sa);

      g_value_init (&value, GNETWORK_TYPE_UDP_TARGET);
      g_value_take_boxed (&value, target);
      g_free (sa);

      /* Error */
      if (bytes_done < 0)
	{
	  switch (en)
	    {
	      /* Dead socket */
	    case EPIPE:
	      gnetwork_thread_source_remove (udp->_priv->source_id);
	      g_io_channel_shutdown (channel, FALSE, NULL);
	      g_io_channel_unref (channel);

	      udp->_priv->source_cond = 0;
	      udp->_priv->source_id = 0;
	      udp->_priv->channel = NULL;
	      udp->_priv->sockfd = -1;

	      udp->_priv->dgram_status = GNETWORK_DATAGRAM_CLOSED;
	      g_object_notify (G_OBJECT (udp), "status");
	      retval = FALSE;
	      break;

	      /* Non-blocking issue, foggettaboutit */
	    case EAGAIN:
	      break;

	    default:
	      error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
				   _("An error occurred inside the GNetwork library while reading "
				     "data from the socket."));
	      gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), &value, error);
	      g_error_free (error);
	      break;
	    }
	}
      else
	{
	  udp->_priv->bytes_received += bytes_done;
	  g_object_notify (G_OBJECT (udp), "bytes-received");
	  gnetwork_datagram_received (GNETWORK_DATAGRAM (udp), &value, buffer, bytes_done);
	}

      g_free (buffer);
      g_value_unset (&value);
    }

  /* Write */
  if (cond & G_IO_OUT)
    {
      if (udp->_priv->buffer != NULL)
	{
	  BufferItem *item;
	  gint en;
	  GValue value = { 0 };

	  item = udp->_priv->buffer->data;
	  udp->_priv->buffer = g_slist_remove_link (udp->_priv->buffer, udp->_priv->buffer);

	  sa = _gnetwork_ip_address_to_sockaddr (&(item->target->ip_address), item->target->port,
						 &sa_size);

	  errno = 0;
#ifdef MSG_NOSIGNAL
	  bytes_done = sendto (udp->_priv->sockfd, item->data, item->length, MSG_NOSIGNAL, sa,
			       sa_size);
#else
	  bytes_done = sendto (udp->_priv->sockfd, item->data, item->length, 0, sa,
		  	       sa_size);
#endif
	  en = errno;
	  g_free (sa);

	  g_value_init (&value, GNETWORK_TYPE_UDP_TARGET);
	  g_value_take_boxed (&value, item->target);
	  item->target = NULL;

	  error = NULL;
	  /* Error */
	  if (bytes_done < 0)
	    {
	      switch (en)
		{
		  /* Dead socket */
		case EPIPE:
		  gnetwork_thread_source_remove (udp->_priv->source_id);
		  g_io_channel_shutdown (channel, FALSE, NULL);
		  g_io_channel_unref (channel);

		  udp->_priv->source_cond = 0;
		  udp->_priv->source_id = 0;
		  udp->_priv->channel = NULL;
		  udp->_priv->sockfd = -1;

		  udp->_priv->dgram_status = GNETWORK_DATAGRAM_CLOSED;
		  g_object_notify (G_OBJECT (udp), "status");
		  retval = FALSE;
		  break;

		  /* Non-blocking issue, foggettaboutit */
		case EAGAIN:
		  retval = TRUE;
		  break;

		default:
		  if (error == NULL)
		    error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
					 _("An error occurred inside the GNetwork library while "
					   "sending data through the socket."));

		  gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), &value, error);
		  g_error_free (error);
		  retval = TRUE;
		  break;
		}

	      gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), &value, error);
	      g_error_free (error);
	    }
	  /* EOF */
	  else if (bytes_done == 0)
	    {
	      gnetwork_thread_source_remove (udp->_priv->source_id);
	      g_io_channel_shutdown (channel, FALSE, NULL);
	      g_io_channel_unref (channel);

	      udp->_priv->source_cond = 0;
	      udp->_priv->source_id = 0;
	      udp->_priv->channel = NULL;
	      udp->_priv->sockfd = -1;

	      udp->_priv->dgram_status = GNETWORK_DATAGRAM_CLOSED;
	      g_object_notify (G_OBJECT (udp), "status");
	      retval = FALSE;
	    }
	  /* OK */
	  else
	    {
	      udp->_priv->bytes_sent += bytes_done;
	      g_object_notify (G_OBJECT (udp), "bytes-sent");
	      gnetwork_datagram_sent (GNETWORK_DATAGRAM (udp), &value, item->data, bytes_done);
	      retval = TRUE;
	    }

	  free_buffer_item (item);
	  g_value_unset (&value);
	}

      /* The out buffer is empty, stop watching G_IO_OUT */
      if (udp->_priv->buffer == NULL)
	{
	  gnetwork_thread_source_remove (udp->_priv->source_id);
	  udp->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_HUP | G_IO_ERR);
	  udp->_priv->source_id = gnetwork_thread_io_add_watch (channel, udp->_priv->source_cond,
								(GIOFunc) io_channel_handler, udp);
	  retval = FALSE;
	}
    }

  return retval;
}


/* ********************************* *
 *  GNetworkDatagramIface Functions  *
 * ********************************* */

static void
gnetwork_udp_datagram_open (GNetworkUdpDatagram * udp)
{
  GError *error;
  gint value;
  struct sockaddr_storage sa = { 0 };
  GNetworkProtocols protocol;

  if (udp->_priv->dgram_status > GNETWORK_DATAGRAM_CLOSED)
    return;

  udp->_priv->dgram_status = GNETWORK_DATAGRAM_OPENING;
  g_object_notify (G_OBJECT (udp), "status");

  if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
    return;

  if (udp->_priv->sockfd < 0)
    {
      errno = 0;
      udp->_priv->sockfd = socket (AF_INET6, SOCK_DGRAM, 0);
      protocol = GNETWORK_PROTOCOL_IPv6;

      /* If IPv6 is not supported, fallback to IPv4 */
      if (udp->_priv->sockfd < 0 && errno == EAFNOSUPPORT)
	{
	  udp->_priv->sockfd = socket (AF_INET, SOCK_DGRAM, 0);
	  protocol = GNETWORK_PROTOCOL_IPv4;
	}

      if (udp->_priv->sockfd < 0)
	{
	  if (udp->_priv->port != 0)
	    {
	      error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
				   _("The socket on port %u could not be opened because an error "
				     "occurred inside the GNetwork library."), udp->_priv->port);
	    }
	  else
	    {
	      error =
		g_error_new_literal (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
				     _("The socket could not be opened because an error "
				       "occurred inside the GNetwork library."));
	    }

	  gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
	  g_error_free (error);

	  if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
	    return;
	}

      g_object_notify (G_OBJECT (udp), "socket");

      if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
	return;
    }
  else
    {
      protocol = _gnetwork_get_socket_protocol (GINT_TO_POINTER (udp->_priv->sockfd));
    }

  /* Retrieve the current socket flags */
  value = fcntl (udp->_priv->sockfd, F_GETFL, 0);
  if (value == -1)
    {
      if (udp->_priv->port != 0)
	{
	  error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
			       _("The socket on port %u could not be made asynchronous."),
			       udp->_priv->port);
	}
      else
	{
	  error = g_error_new_literal (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
				       _("The socket could not be made asynchronous."));
	}

      gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
      g_error_free (error);

      if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
	return;
    }

  /* Add the non-blocking flag */
  if ((value & ~O_NONBLOCK) && fcntl (udp->_priv->sockfd, F_SETFL, value | O_NONBLOCK) == -1)
    {
      if (udp->_priv->port != 0)
	{
	  error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
			       _("The socket on port %u could not be made asynchronous."),
			       udp->_priv->port);
	}
      else
	{
	  error = g_error_new_literal (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
				       _("The socket could not be made asynchronous."));
	}

      gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
      g_error_free (error);

      if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
	return;
    }

  /* Allow other sockets to bind to this port */
  value = TRUE;
  if (setsockopt (udp->_priv->sockfd, SOL_SOCKET, SO_REUSEADDR, (gpointer) & value,
		  sizeof (value)) == -1)
    {
      if (udp->_priv->port != 0)
	{
	  error = g_error_new (GNETWORK_UDP_DATAGRAM_ERROR,
			       GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_REUSE,
			       _("The socket on port %u could not be set to allow use by other "
				 "applications."), udp->_priv->port);
	}
      else
	{
	  error = g_error_new_literal (GNETWORK_UDP_DATAGRAM_ERROR,
				       GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_REUSE,
				       _("The socket could not be set to allow use by other "
					 "applications."));
	}

      gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
      g_error_free (error);

      if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
	return;
    }

  /* Set the broadcast property */
  value = udp->_priv->broadcast;
  if (setsockopt (udp->_priv->sockfd, SOL_SOCKET, SO_BROADCAST, (gpointer) & value,
		  sizeof (value)) == -1)
    {
      if (udp->_priv->port != 0)
	{
	  error = g_error_new (GNETWORK_UDP_DATAGRAM_ERROR,
			       GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_BROADCAST,
			       _("The socket on port %u could not be set to allow broadcasting."),
			       udp->_priv->port);
	}
      else
	{
	  error = g_error_new_literal (GNETWORK_UDP_DATAGRAM_ERROR,
				       GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_BROADCAST,
				       _("The socket could not be set to allow broadcasting."));
	}

      gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
      g_error_free (error);

      if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
	return;
    }

  /* Set the requested TTL. */
  value = udp->_priv->ttl;
  if (setsockopt (udp->_priv->sockfd, IPPROTO_IP, IP_TTL, (gpointer) & value, sizeof (value)) == -1)
    {
      if (udp->_priv->port != 0)
	{
	  error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
			       _("The time-to-live property for the socket on port %u could not be "
				 "changed."), udp->_priv->port);
	}
      else
	{
	  error = g_error_new_literal (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
				       _("The time-to-live property for the socket could not be "
					 "changed."));
	}

      gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
      g_error_free (error);

      if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
	return;
    }

  if (protocol == GNETWORK_PROTOCOL_IPv6 &&
      setsockopt (udp->_priv->sockfd, IPPROTO_IPV6, IPV6_UNICAST_HOPS, (gpointer) & value,
		  sizeof (value)) == -1)
    {
      if (udp->_priv->port != 0)
	{
	  error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
			       _("The time-to-live property for the socket on port %u could not be "
				 "changed."), udp->_priv->port);
	}
      else
	{
	  error = g_error_new_literal (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
				       _("The time-to-live property for the socket could not be "
					 "changed."));
	}

      gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
      g_error_free (error);

      if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
	return;
    }

  /* Bind to the port */
  switch (protocol)
    {
    case GNETWORK_PROTOCOL_IPv4:
      {
	struct sockaddr_in *sin = (struct sockaddr_in *) &sa;

	sin->sin_family = AF_INET;

	if (udp->_priv->interface_info != NULL)
	  {
	    sin->sin_addr.s_addr =
	      GNETWORK_IP_ADDRESS32 (gnetwork_interface_info_get_address
				     (udp->_priv->interface_info, protocol), 3);
	  }
	else
	  {
	    sin->sin_addr.s_addr = INADDR_ANY;
	  }

	sin->sin_port = g_htons (udp->_priv->port);
      }
      break;
    case GNETWORK_PROTOCOL_IPv6:
      {
	struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *) &sa;

	sin6->sin6_family = AF_INET6;

	if (udp->_priv->interface_info != NULL)
	  {
	    sin6->sin6_addr =
	      *((struct in6_addr *) gnetwork_interface_info_get_address (udp->_priv->interface_info,
									 protocol));
	  }
	else
	  {
	    sin6->sin6_addr = in6addr_any;
	  }

	sin6->sin6_port = g_htons (udp->_priv->port);
      }
      break;
    default:
      g_assert_not_reached ();
      break;
    }

  errno = 0;
  if (bind (udp->_priv->sockfd, (struct sockaddr *) &sa, sizeof (sa)) < 0)
    {
      switch (errno)
	{
	case EADDRINUSE:
	  /* If we're not trying to set the port and we're getting this error, it's because the
	     kernel is handing out ports already in use, which it should *never* do. */
	  g_assert (udp->_priv->port != 0);

	  error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_ALREADY_EXISTS,
			       _("The socket on port %u could not be opened because another socket "
				 "is using that port."), udp->_priv->port);
	  break;
	case EACCES:
	  /* If we're not trying to set the port and we're getting this error, it's because the
	     kernel is handing out ports < 1024, which it should *never* do. */
	  g_assert (udp->_priv->port != 0);

	  error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_PERMISSIONS,
			       _("The socket on port %u could not be opened because ports under "
				 "1024 can only be opened by the root user."), udp->_priv->port);
	  break;
	default:
	  if (udp->_priv->port != 0)
	    {
	      error = g_error_new (GNETWORK_DATAGRAM_ERROR, GNETWORK_DATAGRAM_ERROR_INTERNAL,
				   _("The socket on port %u could not be opened because an error "
				     "occurred inside the GNetwork library."), udp->_priv->port);
	    }
	  else
	    {
	      error = g_error_new_literal (GNETWORK_DATAGRAM_ERROR,
					   GNETWORK_DATAGRAM_ERROR_INTERNAL,
					   _("The socket could not be opened because an error "
					     "occurred inside the GNetwork library."));
	    }
	  break;
	}

      gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
      g_error_free (error);

      if (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING)
	return;
    }

  udp->_priv->channel = g_io_channel_unix_new (udp->_priv->sockfd);
  g_io_channel_set_encoding (udp->_priv->channel, NULL, NULL);
  g_io_channel_set_buffered (udp->_priv->channel, FALSE);

  udp->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);
  udp->_priv->source_id = gnetwork_thread_io_add_watch (udp->_priv->channel,
							udp->_priv->source_cond,
							(GIOFunc) io_channel_handler, udp);

  udp->_priv->dgram_status = GNETWORK_DATAGRAM_OPEN;
  g_object_notify (G_OBJECT (udp), "status");
}


static void
gnetwork_udp_datagram_close (GNetworkUdpDatagram * udp)
{
  if (udp->_priv->dgram_status <= GNETWORK_DATAGRAM_CLOSED)
    return;

  if (udp->_priv->channel != NULL)
    {
      g_io_channel_unref (udp->_priv->channel);
      udp->_priv->channel = NULL;
    }

  if (udp->_priv->sockfd > 0)
    {
      shutdown (udp->_priv->sockfd, SHUT_RDWR);
      close (udp->_priv->sockfd);
      udp->_priv->sockfd = -1;
    }

  udp->_priv->dgram_status = GNETWORK_DATAGRAM_CLOSED;
  g_object_notify (G_OBJECT (udp), "socket");
  g_object_notify (G_OBJECT (udp), "status");
}


static void
dns_callback (const GSList * entries, const GError * error, DnsCbData * data)
{
  if (data->udp->_priv->dgram_status == GNETWORK_DATAGRAM_OPEN)
    {
      if (entries != NULL && entries->data != NULL)
	{
	  memcpy (&(data->item->target->ip_address),
		  gnetwork_dns_entry_get_ip_address (entries->data), sizeof (GNetworkIpAddress));

	  /* Append this data to the buffer and watch for a G_IO_OUT, if not already */
	  data->udp->_priv->buffer = g_slist_append (data->udp->_priv->buffer, data->item);

	  if (!(data->udp->_priv->source_cond & G_IO_OUT))
	    {
	      gnetwork_thread_source_remove (data->udp->_priv->source_id);
	      data->udp->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_HUP | G_IO_OUT | G_IO_ERR);
	      data->udp->_priv->source_id =
		gnetwork_thread_io_add_watch (data->udp->_priv->channel,
					      data->udp->_priv->source_cond,
					      (GIOFunc) io_channel_handler, data->udp);
	    }
	}
      else if (error != NULL)
	{
	  GValue value = { 0 };

	  g_value_init (&value, GNETWORK_TYPE_UDP_TARGET);
	  g_value_take_boxed (&value, data->item);

	  gnetwork_datagram_error (GNETWORK_DATAGRAM (data->udp), &value, error);
	  g_value_unset (&value);
	}
      else
	{
	  g_assert_not_reached ();
	}

      /* In either case, the target is not ours to free anymore */
      data->item = NULL;
    }
}


static void
gnetwork_udp_datagram_dgram_send (GNetworkUdpDatagram * udp, const GValue * destination,
				  gconstpointer data, glong length)
{
  BufferItem *item;

  g_return_if_fail (GNETWORK_IS_UDP_DATAGRAM (udp));
  g_return_if_fail (destination != NULL);
  g_return_if_fail (data != NULL);
  g_return_if_fail (length != 0);
  g_return_if_fail (udp->_priv->dgram_status == GNETWORK_DATAGRAM_OPEN);

  if (destination == NULL || !G_VALUE_HOLDS (destination, GNETWORK_TYPE_UDP_TARGET))
    {
      GNETWORK_WARN_METHOD_DATA_TYPE_MISMATCH (GNETWORK_TYPE_UDP_DATAGRAM, 2,
					       GNETWORK_TYPE_DATAGRAM, "send",
					       GNETWORK_TYPE_UDP_TARGET, destination);
      return;
    }

  if (length < 0)
    for (length = 0; *(G_UCHAR (data) + length) != 0; length++);

  item = g_new0 (BufferItem, 1);

  item->target = g_value_dup_boxed (destination);

  item->data = g_malloc (length + 1);
  memcpy (item->data, data, length);
  *(G_UCHAR (item->data) + length) = 0;
  item->length = length;

  if (gnetwork_ip_address_is_valid (&(item->target->ip_address)))
    {
      /* Append this to the buffer and watch for a G_IO_OUT if not already */
      udp->_priv->buffer = g_slist_append (udp->_priv->buffer, item);

      if (!(udp->_priv->source_cond & G_IO_OUT))
	{
	  gnetwork_thread_source_remove (udp->_priv->source_id);
	  udp->_priv->source_cond = (G_IO_IN | G_IO_PRI | G_IO_OUT | G_IO_HUP | G_IO_ERR);
	  udp->_priv->source_id =
	    gnetwork_thread_io_add_watch (udp->_priv->channel, udp->_priv->source_cond,
					  (GIOFunc) io_channel_handler, udp);
	}
    }
  else
    {
      DnsCbData *dns_data = g_new0 (DnsCbData, 1);

      dns_data->udp = g_object_ref (udp);
      dns_data->item = item;

      gnetwork_dns_get (item->target->hostname, (GNetworkDnsCallbackFunc) dns_callback, dns_data,
			(GDestroyNotify) free_dns_cb_data);
    }
}


static void
gnetwork_udp_datagram_dgram_iface_init (GNetworkDatagramIface * iface)
{
  iface->open = (GNetworkDatagramFunc) gnetwork_udp_datagram_open;
  iface->close = (GNetworkDatagramFunc) gnetwork_udp_datagram_close;
  iface->send = (GNetworkDatagramSendFunc) gnetwork_udp_datagram_dgram_send;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_udp_datagram_set_property (GObject * object, guint property, const GValue * value,
				    GParamSpec * param_spec)
{
  GNetworkUdpDatagram *udp = GNETWORK_UDP_DATAGRAM (object);

  switch (property)
    {
    case INTERFACE:
      g_return_if_fail (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING);
      {
	GNetworkInterfaceInfo *info;
	const gchar *interface;

	interface = g_value_get_string (value);

	info = gnetwork_interface_get_info (interface);

	if (info != NULL && info != udp->_priv->interface_info)
	  {
	    g_free (udp->_priv->interface);
	    udp->_priv->interface = g_strdup (udp->_priv->interface);

	    gnetwork_interface_info_unref (udp->_priv->interface_info);
	    udp->_priv->interface_info = gnetwork_interface_info_ref (info);

	    g_object_notify (object, "interface-info");
	  }

	gnetwork_interface_info_unref (info);
      }
      break;
    case INTERFACE_INFO:
      g_return_if_fail (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPENING);

      {
	GNetworkInterfaceInfo *info = g_value_dup_boxed (value);

	if (GNETWORK_IS_INTERFACE_INFO (info))
	  {
	    g_free (udp->_priv->interface);
	    udp->_priv->interface = g_strdup (gnetwork_interface_info_get_name (info));

	    gnetwork_interface_info_unref (udp->_priv->interface_info);
	    udp->_priv->interface_info = gnetwork_interface_info_ref (info);

	    g_object_notify (object, "interface");
	  }

	gnetwork_interface_info_unref (info);
      }
      break;
    case PORT:
      g_return_if_fail (udp->_priv->dgram_status == GNETWORK_DATAGRAM_CLOSED);

      udp->_priv->port = g_value_get_uint (value);
      break;
    case TTL:
      {
	gint ttl = g_value_get_enum (value);

	if (udp->_priv->sockfd > 0)
	  {
	    gboolean success;

	    switch (_gnetwork_get_socket_protocol (GINT_TO_POINTER (udp->_priv->sockfd)))
	      {
	      case GNETWORK_PROTOCOL_IPv4:
		success = (setsockopt (udp->_priv->sockfd, IPPROTO_IP, IP_TTL, (gpointer) ttl,
				       sizeof (ttl)) >= 0);
		break;
	      case GNETWORK_PROTOCOL_IPv6:
		success = (setsockopt (udp->_priv->sockfd, IPPROTO_IPV6, IPV6_UNICAST_HOPS,
				       (gpointer) ttl, sizeof (ttl)) >= 0);
		break;
	      default:
		g_assert_not_reached ();
		success = FALSE;
		break;
	      }

	    if (!success)
	      {
		GError *error;

		if (udp->_priv->port != 0)
		  {
		    error = g_error_new (GNETWORK_UDP_DATAGRAM_ERROR,
					 GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_TTL,
					 _("The packet lifetime for the socket on port %u could "
					   "not be set."), udp->_priv->port);
		  }
		else
		  {
		    error = g_error_new_literal (GNETWORK_UDP_DATAGRAM_ERROR,
						 GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_TTL,
						 _("The packet lifetime could not be set."));
		  }

		gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
		g_error_free (error);
		return;
	      }
	  }

	udp->_priv->ttl = ttl;
      }
      break;
    case BROADCAST:
      {
	GError *error;
	gboolean broadcast = g_value_get_boolean (value);

	if (udp->_priv->broadcast != (broadcast == TRUE))
	  return;

	if (udp->_priv->sockfd > 0 &&
	    setsockopt (udp->_priv->sockfd, SOL_SOCKET, SO_BROADCAST, (gpointer) broadcast,
			sizeof (broadcast)) == -1)
	  {
	    if (udp->_priv->port != 0)
	      {
		error = g_error_new (GNETWORK_UDP_DATAGRAM_ERROR,
				     GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_BROADCAST,
				     _("The socket on port %u could not be set to allow "
				       "broadcasting."), udp->_priv->port);
	      }
	    else
	      {
		error = g_error_new_literal (GNETWORK_UDP_DATAGRAM_ERROR,
					     GNETWORK_UDP_DATAGRAM_ERROR_CANNOT_SET_BROADCAST,
					     _("The socket could not be set to allow "
					       "broadcasting."));
	      }

	    gnetwork_datagram_error (GNETWORK_DATAGRAM (udp), NULL, error);
	    g_error_free (error);
	    return;
	  }

	udp->_priv->broadcast = (broadcast != FALSE);
      }
      break;
    case SOCKET_FD:
      g_return_if_fail (udp->_priv->dgram_status <= GNETWORK_DATAGRAM_CLOSED);
      udp->_priv->sockfd = GPOINTER_TO_INT (g_value_get_pointer (value));
      break;

    case DGRAM_BUFFER_SIZE:
      g_return_if_fail (udp->_priv->dgram_status < GNETWORK_DATAGRAM_OPEN);
      udp->_priv->buffer_size = g_value_get_uint (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_udp_datagram_get_property (GObject * object, guint property, GValue * value,
				    GParamSpec * param_spec)
{
  GNetworkUdpDatagram *udp = GNETWORK_UDP_DATAGRAM (object);

  switch (property)
    {
      /* GNetworkUdpDatagram */
    case INTERFACE:
      g_value_set_string (value, udp->_priv->interface);
      break;
    case INTERFACE_INFO:
      g_value_set_boxed (value, udp->_priv->interface_info);
      break;
    case PORT:
      g_value_set_uint (value, udp->_priv->port);
      break;
    case TTL:
      g_value_set_enum (value, udp->_priv->ttl);
      break;
    case BROADCAST:
      g_value_set_boolean (value, udp->_priv->broadcast);
      break;
    case SOCKET_FD:
      g_value_set_pointer (value, GINT_TO_POINTER (udp->_priv->sockfd));
      break;

      /* GNetworkDatagramIface */
    case DGRAM_STATUS:
      g_value_set_enum (value, udp->_priv->dgram_status);
      break;
    case DGRAM_BUFFER_SIZE:
      g_value_set_uint (value, udp->_priv->buffer_size);
      break;
    case DGRAM_BYTES_RECEIVED:
      g_value_set_ulong (value, udp->_priv->bytes_received);
      break;
    case DGRAM_BYTES_SENT:
      g_value_set_ulong (value, udp->_priv->bytes_sent);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_udp_datagram_dispose (GObject * object)
{
  GNetworkUdpDatagram *udp = GNETWORK_UDP_DATAGRAM (object);

  if (udp->_priv->dgram_status > GNETWORK_DATAGRAM_CLOSED)
    gnetwork_udp_datagram_close (udp);

  gnetwork_interface_info_unref (udp->_priv->interface_info);

  if (G_OBJECT_CLASS (parent_class)->dispose)
    (*G_OBJECT_CLASS (parent_class)->dispose) (object);
}


static void
gnetwork_udp_datagram_finalize (GObject * object)
{
  GNetworkUdpDatagram *udp = GNETWORK_UDP_DATAGRAM (object);

  g_free (udp->_priv->interface);
  g_free (udp->_priv);

  if (G_OBJECT_CLASS (parent_class)->finalize)
    (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_udp_datagram_class_init (GNetworkUdpDatagramClass * class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  parent_class = g_type_class_peek_parent (class);

  object_class->get_property = gnetwork_udp_datagram_get_property;
  object_class->set_property = gnetwork_udp_datagram_set_property;
  object_class->dispose = gnetwork_udp_datagram_dispose;
  object_class->finalize = gnetwork_udp_datagram_finalize;

  g_object_class_install_property (object_class, BROADCAST,
				   g_param_spec_boolean ("broadcast", _("Allow Broadcasting"),
							 _("Whether or not to allow broadcasting "
							   "through this datagram."), FALSE,
							 (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
  g_object_class_install_property (object_class, INTERFACE,
				   g_param_spec_string ("interface", _("Local Interface"),
							_("The name of the interface to bind to "
							  "(e.g. \"eth0\")."), NULL,
							(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
  g_object_class_install_property (object_class, INTERFACE_INFO,
				   g_param_spec_boxed ("interface-info",
						       _("Local Interface Information"),
						       _("Information about the interface this "
							 "socket is bound to."),
						       GNETWORK_TYPE_INTERFACE_INFO,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class, PORT,
				   g_param_spec_uint ("port", _("Local Port"),
						      _("The local port to bind to."),
						      0, 65535, 0,
						      (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
  g_object_class_install_property (object_class, SOCKET_FD,
				   g_param_spec_pointer ("socket",
							 _("Socket File Descriptor"),
							 _("The socket file descriptor."),
							 (G_PARAM_PRIVATE | G_PARAM_READWRITE)));
  g_object_class_install_property (object_class, TTL,
				   g_param_spec_enum ("ttl", _("Time-To-Live"),
						      _("The distance messages sent through this "
							"datagram should travel."),
						      GNETWORK_TYPE_UDP_DATAGRAM_TTL,
						      GNETWORK_UDP_DATAGRAM_TTL_DEFAULT,
						      (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

  /* GNetworkConnectionIface Properties */
  g_object_class_override_property (object_class, DGRAM_STATUS, "status");
  g_object_class_override_property (object_class, DGRAM_BYTES_RECEIVED, "bytes-sent");
  g_object_class_override_property (object_class, DGRAM_BYTES_SENT, "bytes-received");
  g_object_class_override_property (object_class, DGRAM_BUFFER_SIZE, "buffer-size");
}


static void
gnetwork_udp_datagram_instance_init (GNetworkUdpDatagram * udp)
{
  udp->_priv = g_new (GNetworkUdpDatagramPrivate, 1);

  /* Properties */
  udp->_priv->dgram_status = GNETWORK_DATAGRAM_CLOSED;
  udp->_priv->interface = NULL;
  udp->_priv->interface_info = NULL;
  udp->_priv->sockfd = -1;

  /* Object Data */
  udp->_priv->buffer = NULL;
  udp->_priv->channel = NULL;
  udp->_priv->source_id = 0;
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */


/* ********************* *
 *  GNetworkUdpDatagram  *
 * ********************* */

GType
gnetwork_udp_datagram_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      static const GTypeInfo target = {
	sizeof (GNetworkUdpDatagramClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_udp_datagram_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkUdpDatagram),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_udp_datagram_instance_init,
	NULL			/* value table */
      };
      static const GInterfaceInfo dgram_target = {
	(GInterfaceInitFunc) gnetwork_udp_datagram_dgram_iface_init,
	NULL, NULL
      };

      type = g_type_register_static (G_TYPE_OBJECT, "GNetworkUdpDatagram", &target, 0);

      g_type_add_interface_static (type, GNETWORK_TYPE_DATAGRAM, &dgram_target);
    }

  return type;
}


/**
 * gnetwork_udp_datagram_send_to:
 * @udp: the datagram object.
 * @host: the destination hostname, IP address, or %NULL.
 * @port: the destination port.
 * @data: the data to send.
 * @length: the length of @data in bytes, or %-1.
 * 
 * Sends @data through @udp to @port on @host. If @length is %-1, @data is assumed to
 * be %0-terminated, and the proper length will be calculated.
 * 
 * Subclasses should call this function to properly send data.
 * 
 * Since: 1.0
 **/
void
gnetwork_udp_datagram_send_to (GNetworkUdpDatagram * udp, const gchar * host, guint16 port,
			       gconstpointer data, glong length)
{
  GNetworkUdpTarget *target;
  GValue value;

  g_return_if_fail (GNETWORK_IS_UDP_DATAGRAM (udp));
  g_return_if_fail (host == NULL || host[0] != '\0');
  g_return_if_fail (data != NULL);
  g_return_if_fail (length != 0);

  target = gnetwork_udp_target_new (host, port);

  memset (&value, 0, sizeof (value));
  g_value_init (&value, GNETWORK_TYPE_UDP_TARGET);
  g_value_take_boxed (&value, target);
  gnetwork_udp_datagram_dgram_send (udp, &value, data, length);
  g_value_unset (&value);
}


/* ******************* *
 *  GNetworkUdpTarget  *
 * ******************* */

GType
gnetwork_udp_target_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      type = g_boxed_type_register_static ("GNetworkUdpTarget",
					   (GBoxedCopyFunc) gnetwork_udp_target_dup,
					   (GBoxedFreeFunc) gnetwork_udp_target_free);
    }

  return type;
}


/**
 * gnetwork_udp_target_new:
 * @host: the IP address or hostname of the target.
 * @port: the port number.
 * 
 * Creates a new #GNetworkUdpTarget structure based on the information in @host
 * and @port. The returned data should be freed with gnetwork_udp_target_free()
 * when no longer needed.
 * 
 * Returns: a newly allocated target structure.
 * 
 * Since: 1.0
 **/
GNetworkUdpTarget *
gnetwork_udp_target_new (const gchar * host, guint16 port)
{
  GNetworkUdpTarget *target;

  g_return_val_if_fail (host == NULL || host[0] != '\0', NULL);

  target = g_new0 (GNetworkUdpTarget, 1);

  gnetwork_udp_target_set_host (target, host);
  gnetwork_udp_target_set_port (target, port);

  return target;
}


/**
 * gnetwork_udp_target_get_ip_address:
 * @target: the message target to examine.
 * 
 * Returns a copy of the IP address that @target points to. If @target does not
 * know it's IP address, %NULL will be returned. The returned data should be
 * freed with g_free() when no longer needed.
 * 
 * Returns: a newly allocated #GnetworkIpAddress for the IP address of @target.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN GNetworkIpAddress *
gnetwork_udp_target_get_ip_address (const GNetworkUdpTarget * target)
{
  g_return_val_if_fail (target != NULL, NULL);

  return &(target->ip_address);
}


/**
 * gnetwork_udp_target_set_ip_address:
 * @target: the message target to modify.
 * @address: the new IP address.
 * 
 * Sets @target to use @address for it's destination. The host in @target will
 * be set to %NULL as well.
 * 
 * Since: 1.0
 **/
void
gnetwork_udp_target_set_ip_address (GNetworkUdpTarget * target, const GNetworkIpAddress * address)
{
  g_return_if_fail (target != NULL);
  g_return_if_fail (address != NULL);

  memcpy (target, address, sizeof (GNetworkIpAddress));
  g_free (target->hostname);
  target->hostname = NULL;
}


/**
 * gnetwork_udp_target_get_host:
 * @target: the message target to examine.
 * 
 * Retrieves a copy of the hostname (in string form, e.g. "www.gnome.org") that
 * @target points to. If @target does not know it's hostname, then %NULL will
 * be returned. The returned string should not be modifed or freed.
 * 
 * See also: gnetwork_udp_target_get_ip_address().
 * 
 * Returns: a newly allocated string for the IP address of @target.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gnetwork_udp_target_get_host (const GNetworkUdpTarget * target)
{
  g_return_val_if_fail (target != NULL, NULL);

  return target->hostname;
}


/**
 * gnetwork_udp_target_set_host:
 * @target: the message target to modify.
 * @host: the new hostname, or %NULL.
 * 
 * Sets the destination hostname of @target to @host. The IP address of @target
 * will also be modified, to the address in @host if it is a string
 * representation of an IP address, and to all zeros if @host is not.
 * 
 * Since: 1.0
 **/
void
gnetwork_udp_target_set_host (GNetworkUdpTarget * target, const gchar * host)
{
  g_return_if_fail (target != NULL);

  g_free (target->hostname);
  gnetwork_ip_address_set_from_string (GNETWORK_IP_ADDRESS (target), host);
  target->hostname = g_strdup (host);
}


/**
 * gnetwork_udp_target_get_port:
 * @target: the message target to examine.
 * 
 * Retreives the port number that @target points at.
 * 
 * Returns: the target port.
 * 
 * Since: 1.0
 **/
guint16
gnetwork_udp_target_get_port (const GNetworkUdpTarget * target)
{
  g_return_val_if_fail (target != NULL, 0);

  return target->port;
}


/**
 * gnetwork_udp_target_set_port:
 * @target: the message target to modify.
 * @port: the new port number.
 * 
 * Sets the destination port of @target to @port. A port number of %0 allows the
 * system to automatically select a port to use (for UDP clients only).
 * 
 * Since: 1.0
 **/
void
gnetwork_udp_target_set_port (GNetworkUdpTarget * target, guint16 port)
{
  g_return_if_fail (target != NULL);

  target->port = port;
}


/**
 * gnetwork_udp_target_dup:
 * @src: the message target to duplicate.
 * 
 * Creates a copy of the data in @src. The returned data should be freed with
 * gnetwork_udp_target_free() when no longer needed.
 * 
 * Returns: a copy of @src.
 * 
 * Since: 1.0
 **/
GNetworkUdpTarget *
gnetwork_udp_target_dup (const GNetworkUdpTarget * src)
{
  GNetworkUdpTarget *dest;

  if (src == NULL)
    return NULL;

  dest = g_memdup (src, sizeof (GNetworkUdpTarget));
  dest->hostname = g_strdup (src->hostname);

  return dest;
}


/**
 * gnetwork_udp_target_free:
 * @target: the data to free.
 * 
 * Frees the memory used by @target.
 * 
 * Since: 1.0
 **/
void
gnetwork_udp_target_free (GNetworkUdpTarget * target)
{
  if (target == NULL)
    return;

  g_free (target->hostname);
  g_free (target);
}


G_LOCK_DEFINE_STATIC (quark);

GQuark
gnetwork_udp_datagram_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (quark);
  if (quark == 0)
    {
      quark = g_quark_from_static_string ("gnetwork-udp-datagram-error");
    }
  G_UNLOCK (quark);

  return quark;
}
