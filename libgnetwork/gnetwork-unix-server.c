/* 
 * GNetwork Library: libgnetwork/gnetwork-unix-server.c
 *
 * Copyright (C) 2001, 2003 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */


#include "gnetwork-unix-server.h"

#include "gnetwork-unix-connection.h"
#include "gnetwork-threads.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"

#include <glib/gi18n.h>

#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>


#ifndef DEFAULT_BUFFER_SIZE
# define DEFAULT_BUFFER_SIZE 2048
#endif /* DEFAULT_BUFFER_SIZE */


enum
{
  PROP_0,
  /* User-settable */
  FILENAME,

  /* GNetworkServer Properties */
  SVR_STATUS,
  SVR_BYTES_SENT,
  SVR_BYTES_RECEIVED,
  SVR_BUFFER_SIZE,
  SVR_CLOSE_CHILDREN,
  SVR_MAX_CONNECTIONS,
  SVR_CONNECTIONS
};


struct _GNetworkUnixServerPrivate
{
  /* Object Properties */
  gchar *filename;

  /* GNetworkServer Properties */
  GSList *connections;
  guint64 bytes_sent;
  guint64 bytes_received;
  guint max_connections;

  GNetworkServerCreateFunc create_func;
  gpointer data;
  GDestroyNotify notify;

  /* The Socket Itself */
  GIOChannel *channel;
  gint sockfd;
  gint incoming_id;

  /* Property Bits */
  /* GNetworkServer */
  GNetworkServerStatus status:2;
  gboolean close_children:1;
  /* GNetworkUnixServer */
  gboolean reverse_lookups:1;
};


struct _GNetworkUnixServerCreationData
{
  GTypeClass g_class;

  gchar *filename;
  gint fd;
};


/* ********************* *
 *  Classwide Variables  *
 * ********************* */

static gpointer parent_class = NULL;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static GNetworkConnection *
create_incoming (GNetworkUnixServer * server, const GValue * server_data, gpointer user_data,
		 GError ** error)
{
  const GNetworkUnixServerCreationData *data = g_value_get_boxed (server_data);

  return g_object_new (GNETWORK_TYPE_UNIX_CONNECTION,
		       "connection-type", GNETWORK_CONNECTION_SERVER,
		       "filename", data->filename, "socket", GINT_TO_POINTER (data->fd), NULL);
}


/* ************************* *
 *  New Connection Handling  *
 * ************************* */

/* GNetworkConnection Callbacks */
static void
cxn_received_cb (GNetworkConnection * connection, gconstpointer data, gulong length,
		 GNetworkUnixServer * server)
{
  server->_priv->bytes_received += length;
  g_object_notify (G_OBJECT (server), "bytes-received");
}


static void
cxn_sent_cb (GNetworkConnection * connection, gconstpointer data, gulong length,
	     GNetworkUnixServer * server)
{
  server->_priv->bytes_sent += length;
  g_object_notify (G_OBJECT (server), "bytes-sent");
}


static void
cxn_notify_status_cb (GNetworkConnection * connection, GParamSpec * pspec,
		      GNetworkUnixServer * server)
{
  GNetworkConnectionStatus status;

  g_object_get (connection, "status", &status, NULL);

  if (status == GNETWORK_CONNECTION_CLOSED)
    {
      g_signal_handlers_disconnect_by_func (connection, cxn_notify_status_cb, server);
      g_signal_handlers_disconnect_by_func (connection, cxn_received_cb, server);
      g_signal_handlers_disconnect_by_func (connection, cxn_sent_cb, server);

      server->_priv->connections = g_slist_remove (server->_priv->connections, connection);
      g_object_unref (G_OBJECT (connection));
      g_object_notify (G_OBJECT (server), "connections");
    }
}


/* GIOFunc */
static gboolean
incoming_handler (GIOChannel * channel, GIOCondition cond, GNetworkUnixServer * server)
{
  if (server->_priv->status != GNETWORK_SERVER_OPEN)
    return FALSE;

  /* If there is a connection limit && we're already there */
  if (server->_priv->max_connections != 0 &&
      g_slist_length (server->_priv->connections) >= server->_priv->max_connections)
    return TRUE;

  if (cond & G_IO_IN || cond & G_IO_PRI)
    {
      GNetworkConnection *cxn;
      GError *error;
      gint cxn_fd;
      struct sockaddr_un sun;
      socklen_t sun_size;
      GNetworkUnixServerCreationData *creation_data;
      GValue value = { 0 };

      sun_size = sizeof (struct sockaddr_un);
      memset (&sun, 0, sun_size);
      cxn_fd = accept (server->_priv->sockfd, (struct sockaddr *) &sun, &sun_size);

      /* For whatever reason the incoming connection failed */
      if (cxn_fd < 0)
	return TRUE;

      creation_data = g_new0 (GNetworkUnixServerCreationData, 1);
      creation_data->g_class.g_type = GNETWORK_TYPE_UNIX_SERVER_CREATION_DATA;
      creation_data->filename = g_strdup (server->_priv->filename);
      creation_data->fd = cxn_fd;

      g_value_init (&value, GNETWORK_TYPE_UNIX_SERVER_CREATION_DATA);
      g_value_take_boxed (&value, creation_data);

      error = NULL;
      cxn = (*server->_priv->create_func) (GNETWORK_SERVER (server), &value, server->_priv->data,
					   &error);
      g_value_unset (&value);

      if (cxn != NULL && GNETWORK_IS_UNIX_CONNECTION (cxn))
	{
	  /* Add it to the list of connections we've got */
	  server->_priv->connections = g_slist_prepend (server->_priv->connections,
							g_object_ref (cxn));

	  /* Connect signals & tell the world we've got a new incoming connection */
	  g_signal_connect_object (cxn, "received", (GCallback) cxn_received_cb, server, 0);
	  g_signal_connect_object (cxn, "sent", (GCallback) cxn_sent_cb, server, 0);
	  g_signal_connect_object (cxn, "notify::status", (GCallback) cxn_notify_status_cb,
				   server, 0);

	  gnetwork_server_new_connection (GNETWORK_SERVER (server), GNETWORK_CONNECTION (cxn));
	  g_object_notify (G_OBJECT (server), "connections");

	  /* Actually open the connection, we do it after we've connected the signals & told the
	     world about it, so no data is lost. */
	  gnetwork_connection_open (GNETWORK_CONNECTION (cxn));
	  g_object_unref (cxn);
	}
      else
	{
	  shutdown (cxn_fd, SHUT_RDWR);
	  close (cxn_fd);
	}

      return TRUE;
    }

  return FALSE;
}


/* ******************************* *
 *  GNetworkServerIface Functions  *
 * ******************************* */

static void
gnetwork_unix_server_open (GNetworkUnixServer * server)
{
  GError *error;
  struct sockaddr_un sun;
  gint bind_retval, listen_retval, yes = 1;

  g_return_if_fail (GNETWORK_IS_UNIX_SERVER (server));

  g_object_freeze_notify (G_OBJECT (server));
  server->_priv->status = GNETWORK_SERVER_OPENING;
  g_object_notify (G_OBJECT (server), "status");

  server->_priv->bytes_sent = 0;
  g_object_notify (G_OBJECT (server), "bytes-sent");

  server->_priv->bytes_received = 0;
  g_object_notify (G_OBJECT (server), "bytes-received");
  g_object_thaw_notify (G_OBJECT (server));

  /* Create the socket */
  errno = 0;
  server->_priv->sockfd = socket (AF_UNIX, SOCK_STREAM, 0);
  if (server->_priv->sockfd < 0)
    {
      switch (errno)
	{
	case EMFILE:
	  error =
	    g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_TOO_MANY_PROCESSES,
				 gnetwork_server_strerror
				 (GNETWORK_SERVER_ERROR_TOO_MANY_PROCESSES));
	  break;
	case EACCES:
	  error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_PERMISSIONS,
				       gnetwork_server_strerror
				       (GNETWORK_SERVER_ERROR_PERMISSIONS));
	  break;

	case ENFILE:
	case ENOBUFS:
	case ENOMEM:
	  error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_NO_MEMORY,
				       gnetwork_server_strerror (GNETWORK_SERVER_ERROR_NO_MEMORY));
	  break;

	default:
	  error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_INTERNAL,
				       gnetwork_server_strerror (GNETWORK_SERVER_ERROR_INTERNAL));
	  break;
	}

      gnetwork_server_error (GNETWORK_SERVER (server), error);
      g_error_free (error);

      server->_priv->status = GNETWORK_SERVER_CLOSED;
      g_object_notify (G_OBJECT (server), "status");
      return;
    }

  setsockopt (server->_priv->sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof (int));
  fcntl (server->_priv->sockfd, F_SETFL, O_NONBLOCK);

  memset (sun.sun_path, 0, sizeof (sun.sun_path));
  sun.sun_family = AF_UNIX;
  strncpy (sun.sun_path, server->_priv->filename, sizeof (sun.sun_path) - 1);

  /* Bind the socket */
  errno = 0;
  bind_retval = bind (server->_priv->sockfd, ((struct sockaddr *) (&sun)),
		      sizeof (struct sockaddr_un));

  if (bind_retval < 0)
    {
      switch (errno)
	{
	case EINVAL:
	  error = g_error_new (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_ALREADY_EXISTS,
			       _("The Unix service could not be started because the file \"%s\" "
				 "already exists."), server->_priv->filename);
	  break;
	case EACCES:
	  error = g_error_new (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_PERMISSIONS,
			       _("The Unix service could not be started because the file \"%s\" "
				 "is protected."), server->_priv->filename);
	  break;
	case EROFS:
	  error = g_error_new (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_PERMISSIONS,
			       _("The Unix service could not be started because the file \"%s\" "
				 "could not be created."), server->_priv->filename);
	  break;
	case EADDRINUSE:
	  error = g_error_new (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_ALREADY_EXISTS,
			       _("The Unix service could not be started because the file \"%s\" "
				 "already exists."), server->_priv->filename);
	  break;

	default:
	  error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_INTERNAL,
				       gnetwork_server_strerror (GNETWORK_SERVER_ERROR_INTERNAL));
	  break;
	}

      gnetwork_server_error (GNETWORK_SERVER (server), error);
      g_error_free (error);

      shutdown (server->_priv->sockfd, SHUT_RDWR);
      close (server->_priv->sockfd);
      unlink (server->_priv->filename);
      server->_priv->sockfd = -1;

      server->_priv->status = GNETWORK_SERVER_CLOSED;
      g_object_notify (G_OBJECT (server), "status");
      return;
    }

  errno = 0;
  listen_retval = listen (server->_priv->sockfd, server->_priv->max_connections);
  if (listen_retval < 0)
    {
      error = g_error_new_literal (GNETWORK_SERVER_ERROR, GNETWORK_SERVER_ERROR_INTERNAL,
				   gnetwork_server_strerror (GNETWORK_SERVER_ERROR_INTERNAL));

      gnetwork_server_error (GNETWORK_SERVER (server), error);
      g_error_free (error);

      shutdown (server->_priv->sockfd, SHUT_RDWR);
      close (server->_priv->sockfd);
      unlink (server->_priv->filename);
      server->_priv->sockfd = -1;

      server->_priv->status = GNETWORK_SERVER_CLOSED;
      g_object_notify (G_OBJECT (server), "status");
      return;
    }

  server->_priv->channel = g_io_channel_unix_new (server->_priv->sockfd);
  server->_priv->incoming_id = gnetwork_thread_io_add_watch_full (server->_priv->channel,
								  G_PRIORITY_DEFAULT,
								  GNETWORK_IO_ANY,
								  (GIOFunc) incoming_handler,
								  g_object_ref (server),
								  g_object_unref);

  server->_priv->status = GNETWORK_SERVER_OPEN;
  g_object_notify (G_OBJECT (server), "status");
}


static void
gnetwork_unix_server_close (GNetworkUnixServer * server)
{
  g_return_if_fail (GNETWORK_IS_UNIX_SERVER (server));

  if (server->_priv->status <= GNETWORK_SERVER_CLOSED)
    return;

  g_object_freeze_notify (G_OBJECT (server));
  server->_priv->status = GNETWORK_SERVER_CLOSING;
  g_object_notify (G_OBJECT (server), "status");

  for (; server->_priv->connections != NULL;
       g_slist_remove_link (server->_priv->connections, server->_priv->connections))
    {
      if (server->_priv->close_children)
	{
	  gnetwork_connection_close (GNETWORK_CONNECTION (server->_priv->connections->data));
	}
      else
	{
	  g_signal_handlers_disconnect_by_func (server->_priv->connections->data,
						cxn_notify_status_cb, server);
	  g_signal_handlers_disconnect_by_func (server->_priv->connections->data,
						cxn_received_cb, server);
	  g_signal_handlers_disconnect_by_func (server->_priv->connections->data,
						cxn_sent_cb, server);
	  g_object_unref (server->_priv->connections->data);
	}
    }
  g_object_notify (G_OBJECT (server), "connections");
  g_object_thaw_notify (G_OBJECT (server));

  g_io_channel_shutdown (server->_priv->channel, FALSE, NULL);
  g_io_channel_unref (server->_priv->channel);
  server->_priv->channel = NULL;
  server->_priv->sockfd = -1;
  unlink (server->_priv->filename);

  server->_priv->status = GNETWORK_SERVER_CLOSED;
  g_object_notify (G_OBJECT (server), "status");
}


static void
gnetwork_unix_server_set_create_func (GNetworkUnixServer * server, GNetworkServerCreateFunc func,
				      gpointer data, GDestroyNotify notify)
{
  g_return_if_fail (GNETWORK_IS_UNIX_SERVER (server));

  if (server->_priv->notify != NULL && server->_priv->data != NULL)
    (*server->_priv->notify) (server->_priv->data);

  if (func != NULL)
    {
      server->_priv->create_func = func;
      server->_priv->data = data;
      server->_priv->notify = notify;
    }
  else
    {
      server->_priv->create_func = (GNetworkServerCreateFunc) create_incoming;
    }

  server->_priv->data = data;
  server->_priv->notify = notify;
}


static void
gnetwork_unix_server_server_iface_init (GNetworkServerIface * iface)
{
  iface->open = (GNetworkServerFunc) gnetwork_unix_server_open;
  iface->close = (GNetworkServerFunc) gnetwork_unix_server_close;

  iface->set_create_func = (GNetworkServerSetCreateFunc) gnetwork_unix_server_set_create_func;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_unix_server_set_property (GObject * object, guint property, const GValue * value,
				   GParamSpec * param_spec)
{
  GNetworkUnixServer *server = GNETWORK_UNIX_SERVER (object);

  switch (property)
    {
    case FILENAME:
      g_return_if_fail (server->_priv->status < GNETWORK_SERVER_OPENING);

      g_free (server->_priv->filename);
      server->_priv->filename = g_value_dup_string (value);
      break;

    case SVR_MAX_CONNECTIONS:
      server->_priv->max_connections = g_value_get_uint (value);
      break;
    case SVR_CLOSE_CHILDREN:
      server->_priv->close_children = g_value_get_boolean (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_unix_server_get_property (GObject * object, guint property, GValue * value,
				   GParamSpec * param_spec)
{
  GNetworkUnixServer *server = GNETWORK_UNIX_SERVER (object);

  switch (property)
    {
    case FILENAME:
      g_value_set_string (value, server->_priv->filename);
      break;

    case SVR_STATUS:
      g_value_set_enum (value, server->_priv->status);
      break;
    case SVR_MAX_CONNECTIONS:
      g_value_set_uint (value, server->_priv->max_connections);
      break;
    case SVR_BYTES_SENT:
      g_value_set_ulong (value, server->_priv->bytes_sent);
      break;
    case SVR_BYTES_RECEIVED:
      g_value_set_ulong (value, server->_priv->bytes_received);
      break;
    case SVR_CONNECTIONS:
      g_value_take_boxed (value, _gnetwork_slist_to_value_array (server->_priv->connections,
								 GNETWORK_TYPE_CONNECTION));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_unix_server_dispose (GObject * object)
{
  GNetworkUnixServer *server = GNETWORK_UNIX_SERVER (object);

  if (server->_priv->status > GNETWORK_SERVER_CLOSED)
    gnetwork_unix_server_close (server);

  if (server->_priv->notify != NULL && server->_priv->data != NULL)
    (*server->_priv->notify) (server->_priv->data);

  if (G_OBJECT_CLASS (parent_class)->dispose != NULL)
    (*G_OBJECT_CLASS (parent_class)->dispose) (object);
}


static void
gnetwork_unix_server_finalize (GObject * object)
{
  GNetworkUnixServer *server = GNETWORK_UNIX_SERVER (object);

  g_free (server->_priv->filename);

  if (G_OBJECT_CLASS (parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_unix_server_class_init (GNetworkUnixServerClass * class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  parent_class = g_type_class_peek_parent (class);

  object_class->set_property = gnetwork_unix_server_set_property;
  object_class->get_property = gnetwork_unix_server_get_property;
  object_class->dispose = gnetwork_unix_server_dispose;
  object_class->finalize = gnetwork_unix_server_finalize;

  g_object_class_install_property (object_class, FILENAME,
				   g_param_spec_string ("filename", _("Filename"),
							_("The socket filename."), NULL,
							G_PARAM_READWRITE));

  g_object_class_override_property (object_class, SVR_STATUS, "status");
  g_object_class_override_property (object_class, SVR_BYTES_SENT, "bytes-sent");
  g_object_class_override_property (object_class, SVR_BYTES_RECEIVED, "bytes-received");
  g_object_class_override_property (object_class, SVR_MAX_CONNECTIONS, "max-connections");
  g_object_class_override_property (object_class, SVR_CLOSE_CHILDREN, "close-children");
  g_object_class_override_property (object_class, SVR_CONNECTIONS, "connections");
}


static void
gnetwork_unix_server_instance_init (GNetworkUnixServer * server)
{
  server->_priv = g_new (GNetworkUnixServerPrivate, 1);

  server->_priv->filename = NULL;
  server->_priv->connections = NULL;

  server->_priv->bytes_received = 0;
  server->_priv->bytes_sent = 0;
  server->_priv->create_func = (GNetworkServerCreateFunc) create_incoming;
}


/* ************************************************************************** *
 *  Public API                                                                *
 **************************************************************************** */

GType
gnetwork_unix_server_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      static const GTypeInfo info = {
	sizeof (GNetworkUnixServerClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_unix_server_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkUnixServer),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_unix_server_instance_init,
	NULL			/* value table */
      };
      static const GInterfaceInfo svr_info = {
	(GInterfaceInitFunc) gnetwork_unix_server_server_iface_init, NULL, NULL
      };

      type = g_type_register_static (G_TYPE_OBJECT, "GNetworkUnixServer", &info, 0);

      g_type_add_interface_static (type, GNETWORK_TYPE_SERVER, &svr_info);
    }

  return type;
}


/**
 * gnetwork_unix_server_new:
 * @filename: the filename to use, or %NULL.
 *
 * Creates a new, unopened Unix server using @filename. The @filename should
 * either be %NULL or the unique name of a file to create for use by the server.
 * If @filename is %NULL, a filename name will be automatically chosen in the
 * temporary directory.
 *
 * Returns: a new #GNetworkUnixServer object.
 *
 * Since: 1.0
 **/
GNetworkUnixServer *
gnetwork_unix_server_new (const gchar * filename)
{
  g_return_val_if_fail (filename == NULL || filename[0] != '\0', NULL);

  return g_object_new (GNETWORK_TYPE_UNIX_SERVER, "filename", filename, NULL);
}


GType
gnetwork_unix_server_creation_data_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      type = g_boxed_type_register_static ("GNetworkUnixServerCreationData",
					   (GBoxedCopyFunc) gnetwork_unix_server_creation_data_dup,
					   (GBoxedFreeFunc)
					   gnetwork_unix_server_creation_data_free);
    }

  return type;
}


/**
 * gnetwork_unix_server_creation_data_get_filename:
 * @data: the connection creation data to examine.
 * 
 * Retrieves the filename string of the incoming connection in @data.
 * 
 * Returns: the filename of @data.
 **/
G_CONST_RETURN gchar *
gnetwork_unix_server_creation_data_get_filename (const GNetworkUnixServerCreationData * data)
{
  g_return_val_if_fail (GNETWORK_IS_UNIX_SERVER_CREATION_DATA (data), NULL);

  return data->filename;
}


/**
 * gnetwork_unix_server_creation_data_get_socket:
 * @data: the connection creation data to examine.
 * 
 * Retrieves the socket data (a file descriptor on Unix) in @data.
 * 
 * Returns: the socket of @data.
 **/
gconstpointer
gnetwork_unix_server_creation_data_get_socket (const GNetworkUnixServerCreationData * data)
{
  g_return_val_if_fail (GNETWORK_IS_UNIX_SERVER_CREATION_DATA (data), NULL);

  return GINT_TO_POINTER (data->fd);
}


/**
 * gnetwork_unix_server_creation_data_dup:
 * @src: the creation data to copy.
 * 
 * Creates a copy of the creation data in @src. The returned data should be
 * freed with gnetwork_unix_server_creation_data_free() when no longer needed.
 * 
 * Returns: a newly allocated copy of @src.
 * 
 * Since: 1.0
 **/
GNetworkUnixServerCreationData *
gnetwork_unix_server_creation_data_dup (const GNetworkUnixServerCreationData * src)
{
  GNetworkUnixServerCreationData *dest;

  g_return_val_if_fail (src == NULL || GNETWORK_IS_UNIX_SERVER_CREATION_DATA (src), NULL);

  if (src == NULL)
    return NULL;

  dest = g_new0 (GNetworkUnixServerCreationData, 1);
  dest->g_class.g_type = GNETWORK_TYPE_UNIX_SERVER_CREATION_DATA;
  dest->filename = g_strdup (src->filename);
  dest->fd = src->fd;

  return dest;
}


/**
 * gnetwork_unix_server_creation_data_free:
 * @data: the creation data to delete.
 * 
 * Frees the memory used by @data.
 * 
 * Since: 1.0
 **/
void
gnetwork_unix_server_creation_data_free (GNetworkUnixServerCreationData * data)
{
  g_return_if_fail (data == NULL || GNETWORK_IS_UNIX_SERVER_CREATION_DATA (data));

  if (data != NULL)
    {
      g_free (data->filename);
      g_free (data);
    }
}
