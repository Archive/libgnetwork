/*
 * GNetwork Library: libgnetwork/gnetwork-ip-multicast.c
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-ip-multicast.h"

#include "gnetwork-datagram.h"
#include "gnetwork-interfaces.h"
#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"
#include "marshal.h"

#include <glib/gi18n.h>

#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>


enum
{
  PROP_0,

  GROUPS,
  TTL
};

enum
{
  RECEIVED,
  SENT,
  LAST_SIGNAL
};


struct _GNetworkIpMulticastPrivate
{
  GHashTable *groups;

  GNetworkUdpDatagramTtl ttl:9;
};


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer parent_class = NULL;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static void
join_group (const GNetworkIpAddress * group, gpointer value, GNetworkIpMulticast * multicast)
{
  gpointer sockfd;
  GNetworkDatagramStatus status;
  GNetworkProtocols protocol;
  GNetworkInterfaceInfo *info;
  gboolean success = FALSE;

  g_object_get (multicast, "status", &status, NULL);

  if (status <= GNETWORK_DATAGRAM_CLOSED)
    return;

  sockfd = GINT_TO_POINTER (-1);

  g_object_get (multicast, "socket", &sockfd, NULL);

  if (GPOINTER_TO_INT (sockfd) < 0)
    return;

  info = NULL;
  g_object_get (multicast, "interface-info", &info, NULL);

  protocol = _gnetwork_get_socket_protocol (sockfd);
  switch (protocol)
    {
    case GNETWORK_PROTOCOL_IPv4:
      if (gnetwork_ip_address_is_ipv4 (group))
	{
	  struct ip_mreq request = {
	    {GNETWORK_IP_ADDRESS32 (group, 3)},
	    {0}
	  };

	  if (info != NULL)
	    {
	      request.imr_interface.s_addr =
		GNETWORK_IP_ADDRESS32 (gnetwork_interface_info_get_address (info, protocol), 3);
	    }
	  else
	    {
	      request.imr_interface.s_addr = INADDR_ANY;
	    }

	  success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IP, IP_ADD_MEMBERSHIP, &request,
				 sizeof (request)) >= 0);
	}
      break;

    case GNETWORK_PROTOCOL_IPv6:
      {
	struct ipv6_mreq request;

	request.ipv6mr_multiaddr = *((struct in6_addr *) group);

	if (info != NULL)
	  request.ipv6mr_interface = gnetwork_interface_info_get_index (info);
	else
	  request.ipv6mr_interface = 0;

	success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IPV6, IPV6_JOIN_GROUP, &request,
			       sizeof (request)) >= 0);
      }
      break;
    default:
      g_assert_not_reached ();
      break;
    }

  gnetwork_interface_info_unref (info);

  if (!success)
    {
      GError *error;
      gchar *str;
      GValue value = { 0 };

      str = gnetwork_ip_address_to_string (group);
      error = g_error_new (GNETWORK_IP_MULTICAST_ERROR, GNETWORK_IP_MULTICAST_ERROR_JOIN_FAILED,
			   _("Could not join the multicast group at \"%s\"."), str);
      g_value_init (&value, GNETWORK_TYPE_UDP_TARGET);
      g_value_take_boxed (&value, gnetwork_udp_target_new (str, 0));
      g_free (str);

      gnetwork_datagram_error (GNETWORK_DATAGRAM (multicast), &value, error);
      g_error_free (error);
      g_value_unset (&value);
    }
}


static gboolean
leave_group (const GNetworkIpAddress * group, gpointer value, GNetworkIpMulticast * multicast)
{
  gpointer sockfd;
  GNetworkDatagramStatus status;
  GNetworkProtocols protocol;
  GNetworkInterfaceInfo *info;
  gboolean success = TRUE;

  g_object_get (multicast, "status", &status, NULL);

  if (status <= GNETWORK_DATAGRAM_CLOSED)
    return TRUE;

  sockfd = GINT_TO_POINTER (-1);

  g_object_get (multicast, "socket", &sockfd, NULL);

  if (GPOINTER_TO_INT (sockfd) < 0)
    return TRUE;

  info = NULL;
  g_object_get (multicast, "interface-info", &info, NULL);

  protocol = _gnetwork_get_socket_protocol (sockfd);
  switch (protocol)
    {
    case GNETWORK_PROTOCOL_IPv4:
      if (gnetwork_ip_address_is_ipv4 (group))
	{
	  struct ip_mreq request = {
	    {GNETWORK_IP_ADDRESS32 (group, 3)},
	    {0}
	  };

	  if (info != NULL)
	    {
	      request.imr_interface.s_addr =
		GNETWORK_IP_ADDRESS32 (gnetwork_interface_info_get_address (info, protocol), 3);
	    }
	  else
	    {
	      request.imr_interface.s_addr = INADDR_ANY;
	    }

	  success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IP, IP_DROP_MEMBERSHIP, &request,
				 sizeof (request)) >= 0);
	}
      break;

    case GNETWORK_PROTOCOL_IPv6:
      {
	struct ipv6_mreq request;

	request.ipv6mr_multiaddr = *((struct in6_addr *) group);
	if (info != NULL)
	  request.ipv6mr_interface = gnetwork_interface_info_get_index (info);
	else
	  request.ipv6mr_interface = 0;

	success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IPV6, IPV6_LEAVE_GROUP, &request,
			       sizeof (request)) >= 0);
      }
      break;
    default:
      g_assert_not_reached ();
      break;
    }

  gnetwork_interface_info_unref (info);

  if (!success)
    {
      GError *error;
      gchar *str;
      GValue value = { 0 };

      str = gnetwork_ip_address_to_string (group);
      error = g_error_new (GNETWORK_IP_MULTICAST_ERROR, GNETWORK_IP_MULTICAST_ERROR_LEAVE_FAILED,
			   _("Could not leave the multicast group at \"%s\"."), str);
      g_value_init (&value, GNETWORK_TYPE_UDP_TARGET);
      g_value_take_boxed (&value, gnetwork_udp_target_new (str, 0));
      g_free (str);

      gnetwork_datagram_error (GNETWORK_DATAGRAM (multicast), &value, error);
      g_error_free (error);
      g_value_unset (&value);
    }

  return TRUE;
}


/* *********** *
 *  Callbacks  *
 * *********** */

static void
notify_socket_cb (GNetworkIpMulticast * multicast, GParamSpec * pspec, gpointer data)
{
  gpointer sockfd;
  GNetworkDatagramStatus status;
  gint value;
  GNetworkProtocols protocol;
  gboolean success = TRUE;

  sockfd = GINT_TO_POINTER (-1);

  g_object_get (multicast, "socket", &sockfd, NULL);

  if (GPOINTER_TO_INT (sockfd) < 0)
    return;

  protocol = _gnetwork_get_socket_protocol (sockfd);

  /* Multicast Packet TTL */
  value = multicast->_priv->ttl;
  switch (protocol)
    {
    case GNETWORK_PROTOCOL_IPv4:
      success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IP, IP_MULTICAST_TTL,
			     (gpointer) value, sizeof (value)) >= 0);
      break;
    case GNETWORK_PROTOCOL_IPv6:
      success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
			     (gpointer) value, sizeof (value)) >= 0);
      break;
    default:
      g_assert_not_reached ();
      break;
    }

  if (!success)
    {
      GError *error;

      error = g_error_new_literal (GNETWORK_IP_MULTICAST_ERROR,
				   GNETWORK_IP_MULTICAST_ERROR_CANNOT_SET_TTL,
				   _("The multicast packet lifetime for the IP multicast socket "
				     "could not be set."));

      gnetwork_datagram_error (GNETWORK_DATAGRAM (multicast), NULL, error);
      g_error_free (error);

      g_object_get (multicast, "status", &status, NULL);

      if (status <= GNETWORK_DATAGRAM_CLOSED)
	return;
    }

  /* Loopback Speedups */
  value = TRUE;
  switch (protocol)
    {
    case GNETWORK_PROTOCOL_IPv4:
      success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IP, IP_MULTICAST_LOOP,
			     (gpointer) value, sizeof (value)) >= 0);
      break;
    case GNETWORK_PROTOCOL_IPv6:
      success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
			     (gpointer) value, sizeof (value)) >= 0);
      break;
    default:
      g_assert_not_reached ();
      break;
    }

  if (!success)
    {
      GError *error;

      error = g_error_new_literal (GNETWORK_IP_MULTICAST_ERROR,
				   GNETWORK_IP_MULTICAST_ERROR_CANNOT_SET_LOOPBACK,
				   _("Could not enable speed enhancements for the IP multicast "
				     "socket."));
      gnetwork_datagram_error (GNETWORK_DATAGRAM (multicast), NULL, error);
      g_error_free (error);

      g_object_get (multicast, "status", &status, NULL);

      if (status <= GNETWORK_DATAGRAM_CLOSED)
	return;
    }

  /* Join groups */
  g_hash_table_foreach (multicast->_priv->groups, (GHFunc) join_group, multicast);
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static GObject *
gnetwork_ip_multicast_constructor (GType type, guint n_params, GObjectConstructParam * params)
{
  GObject *object;

  object = (*G_OBJECT_CLASS (parent_class)->constructor) (type, n_params, params);

  /* Watch for changes to socket-fd, and set *our* socket options on the fd accordingly. */
  g_signal_connect (object, "notify::socket", G_CALLBACK (notify_socket_cb), NULL);
  // g_signal_connect (object, "notify::interface-info", G_CALLBACK (notify_interface_info_cb), NULL);

  return object;
}


static void
gnetwork_ip_multicast_set_property (GObject * object, guint property, const GValue * value,
				    GParamSpec * param_spec)
{
  GNetworkIpMulticast *multicast = GNETWORK_IP_MULTICAST (object);

  switch (property)
    {
    case TTL:
      {
	gpointer sockfd;
	gint ttl;
	GNetworkProtocols protocol;

	g_object_get (multicast, "socket", &sockfd, NULL);

	ttl = g_value_get_enum (value);

	if (GPOINTER_TO_INT (sockfd) >= 0)
	  {
	    gboolean success = TRUE;

	    protocol = _gnetwork_get_socket_protocol (sockfd);

	    switch (protocol)
	      {
	      case GNETWORK_PROTOCOL_IPv4:
		success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IP, IP_MULTICAST_TTL,
				       (gpointer) ttl, sizeof (ttl)) >= 0);
		break;
	      case GNETWORK_PROTOCOL_IPv6:
		success = (setsockopt (GPOINTER_TO_INT (sockfd), IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
				       (gpointer) ttl, sizeof (ttl)) >= 0);
		break;
	      default:
		g_assert_not_reached ();
		break;
	      }

	    if (!success)
	      {
		GError *error;

		error = g_error_new_literal (GNETWORK_IP_MULTICAST_ERROR,
					     GNETWORK_IP_MULTICAST_ERROR_CANNOT_SET_TTL,
					     _("The multicast packet lifetime for the IP multicast "
					       "socket could not be set."));

		gnetwork_datagram_error (GNETWORK_DATAGRAM (multicast), NULL, error);
		g_error_free (error);
	      }
	  }

	multicast->_priv->ttl = ttl;
      }
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
put_hash_keys_into_list (gpointer key, gpointer value, GSList ** list)
{
  *list = g_slist_append (*list, key);
}


static void
gnetwork_ip_multicast_get_property (GObject * object, guint property, GValue * value,
				    GParamSpec * param_spec)
{
  GNetworkIpMulticast *multicast = GNETWORK_IP_MULTICAST (object);

  switch (property)
    {
      /* GNetworkIpMulticast */
    case GROUPS:
      {
	GSList *list = NULL;

	g_hash_table_foreach (multicast->_priv->groups, (GHFunc) put_hash_keys_into_list, &list);
	g_value_take_boxed (value, _gnetwork_slist_to_value_array (list, GNETWORK_TYPE_IP_ADDRESS));
	g_slist_free (list);
      }
      break;
    case TTL:
      g_value_set_enum (value, multicast->_priv->ttl);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
      break;
    }
}


static void
gnetwork_ip_multicast_finalize (GObject * object)
{
  GNetworkIpMulticast *multicast = GNETWORK_IP_MULTICAST (object);
  GNetworkDatagramStatus status;

  g_object_get (object, "status", &status, NULL);

  if (status > GNETWORK_DATAGRAM_CLOSED)
    gnetwork_datagram_close (GNETWORK_DATAGRAM (multicast));

  g_hash_table_destroy (multicast->_priv->groups);
  g_free (multicast->_priv);

  if (G_OBJECT_CLASS (parent_class)->finalize)
    (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_ip_multicast_class_init (GNetworkIpMulticastClass * class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  parent_class = g_type_class_peek_parent (class);

  object_class->constructor = gnetwork_ip_multicast_constructor;
  object_class->set_property = gnetwork_ip_multicast_set_property;
  object_class->get_property = gnetwork_ip_multicast_get_property;
  object_class->finalize = gnetwork_ip_multicast_finalize;

  g_object_class_install_property (object_class, TTL,
				   g_param_spec_enum ("multicast-ttl", _("Time-To-Live"),
						      _("The distance multicast messages sent "
							"through this datagram should travel."),
						      GNETWORK_TYPE_UDP_DATAGRAM_TTL,
						      GNETWORK_UDP_DATAGRAM_TTL_DEFAULT,
						      G_PARAM_READWRITE));

  g_object_class_install_property (object_class, GROUPS,
				   g_param_spec_value_array
				   ("multicast-groups", _("Multicast Groups"),
				    _("A value array of the IP addresses of groups this "
				      "socket has joined to when open."),
				    g_param_spec_string ("multicast-group-ip-address",
							 _("Group IP Address"),
							 _("A group this socket has joined "
							   "when open."), NULL, G_PARAM_READABLE),
				    G_PARAM_READABLE));
}


static void
gnetwork_ip_multicast_instance_init (GNetworkIpMulticast * multicast)
{
  multicast->_priv = g_new (GNetworkIpMulticastPrivate, 1);

  multicast->_priv->groups = g_hash_table_new_full (gnetwork_ip_address_hash,
						    gnetwork_ip_address_equal, g_free, NULL);
  multicast->_priv->ttl = GNETWORK_UDP_DATAGRAM_TTL_DEFAULT;
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_ip_multicast_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      static const GTypeInfo info = {
	sizeof (GNetworkIpMulticastClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_ip_multicast_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkIpMulticast),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_ip_multicast_instance_init,
	NULL			/* value table */
      };

      type = g_type_register_static (GNETWORK_TYPE_UDP_DATAGRAM, "GNetworkIpMulticast", &info, 0);
    }

  return type;
}


/**
 * gnetwork_ip_multicast_new:
 * @interface: the IP address of the local interface to listen on, or %NULL.
 * @port: the local port to listen on, or %0.
 * 
 * Creates a new IP multicasting object which listens on @interface:@port. If
 * @interface is %NULL, then the object will listen on all local interfaces. If
 * @port is %0, then a port will be automatically chosen.
 * 
 * Unless you know you need a specific interface or port (for a server of some
 * kind), you should not request one.
 * 
 * Returns: a new #GNetworkIpMulticast object.
 * 
 * Since: 1.0
 **/
GNetworkIpMulticast *
gnetwork_ip_multicast_new (const gchar * interface, guint port)
{
  g_return_val_if_fail (interface == NULL || interface[0] != '\0', NULL);
  g_return_val_if_fail (port < 65535, NULL);

  return g_object_new (GNETWORK_TYPE_IP_MULTICAST, "interface", interface, "port", port, NULL);
}


/**
 * gnetwork_ip_multicast_join_group:
 * @multicast: the multicast object to modify.
 * @group: the address of the group to join.
 * 
 * Adds @group to the list of groups that @multicast is on. If @multicast is
 * already connected, @groupwill be joined immediately. Otherwise, it will be
 * joined after @multicast connects.
 * 
 * Since: 1.0
 **/
void
gnetwork_ip_multicast_join_group (GNetworkIpMulticast * multicast, const GNetworkIpAddress * group)
{
  g_return_if_fail (GNETWORK_IS_IP_MULTICAST (multicast));
  g_return_if_fail (gnetwork_ip_address_is_multicast (group));

  g_hash_table_insert (multicast->_priv->groups, gnetwork_ip_address_dup (group), NULL);

  join_group (group, NULL, multicast);
}


/**
 * gnetwork_ip_multicast_leave_group:
 * @multicast: the multicast object to modify.
 * @group: the address of the group to leave.
 * 
 * Removes @group from the list of groups @multicast is on. If @multicast is
 * already connected, the group will be left immediately. Otherwise, @group will
 * be removed from the list of groups to join after @multicast connects.
 * 
 * Since: 1.0
 **/
void
gnetwork_ip_multicast_leave_group (GNetworkIpMulticast * multicast, const GNetworkIpAddress * group)
{
  g_return_if_fail (GNETWORK_IS_IP_MULTICAST (multicast));
  g_return_if_fail (gnetwork_ip_address_is_multicast (group));
  g_return_if_fail (g_hash_table_lookup (multicast->_priv->groups, group) != NULL);

  leave_group (group, NULL, multicast);
  g_hash_table_remove (multicast->_priv->groups, group);
}


/**
 * gnetwork_ip_multicast_clear_groups:
 * @multicast: the multicast object to modify.
 * 
 * Clears the list of groups @multicast is on. If @multicast is connected, the
 * groups will be left immediately. Otherwise, the list of groups to join after
 * @multicast connects will be cleared.
 * 
 * Since: 1.0
 **/
void
gnetwork_ip_multicast_clear_groups (GNetworkIpMulticast * multicast)
{
  g_return_if_fail (GNETWORK_IS_IP_MULTICAST (multicast));

  g_hash_table_foreach_remove (multicast->_priv->groups, (GHRFunc) leave_group, multicast);
}


G_LOCK_DEFINE_STATIC (quark);

GQuark
gnetwork_ip_multicast_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (quark);
  if (quark == 0)
    {
      quark = g_quark_from_static_string ("gnetwork-ip-multicast-error");
    }
  G_UNLOCK (quark);

  return quark;
}
