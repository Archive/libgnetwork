/* 
 * GNetwork Library: libgnetwork/gnetwork-socket.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNETWORK_DATA_SOCKET_H__
#define __GNETWORK_DATA_SOCKET_H__

#include "gnetwork-socket.h"

G_BEGIN_DECLS


#define GNETWORK_TYPE_DATA_SOCKET \
  (gnetwork_data_socket_get_type ())
#define GNETWORK_DATA_SOCKET(object) \
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GNETWORK_TYPE_DATA_SOCKET, GNetworkDataSocket))
#define GNETWORK_DATA_SOCKET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GNETWORK_TYPE_DATA_SOCKET, GNetworkDataSocketClass))
#define GNETWORK_IS_DATA_SOCKET(object) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GNETWORK_TYPE_SOCKET))
#define GNETWORK_IS_DATA_SOCKET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_DATA_SOCKET))
#define GNETWORK_DATA_SOCKET_GET_CLASS(object) \
  (G_TYPE_INSTANCE_GET_CLASS ((object), GNETWORK_TYPE_DATA_SOCKET, GNetworkDataSocketClass))

#define GNETWORK_DATA_SOCKET_ERROR \
  (gnetwork_data_socket_error_get_quark ())

  
typedef enum /* <prefix=GNETWORK_DATA_SOCKET> */
{
  GNETWORK_DATA_SOCKET_UNKNOWN,

  GNETWORK_DATA_SOCKET_INCOMING,
  GNETWORK_DATA_SOCKET_OUTGOING
}
GNetworkDataSocketDirection;

typedef enum /* <prefix=GNETWORK_DATA_SOCKET_ERROR> */
{
  GNETWORK_DATA_SOCKET_ERROR_REFUSED,
  GNETWORK_DATA_SOCKET_ERROR_TIMED_OUT,
  GNETWORK_DATA_SOCKET_ERROR_UNREACHABLE,
  GNETWORK_DATA_SOCKET_ERROR_HOST_CLOSED,
  GNETWORK_DATA_SOCKET_ERROR_SEND_FAILED,
  GNETWORK_DATA_SOCKET_ERROR_RECEIVE_FAILED,
}
GNetworkDataSocketError;


typedef struct _GNetworkDataSocket GNetworkDataSocket;
typedef struct _GNetworkDataSocketPrivate GNetworkDataSocketPrivate;
typedef struct _GNetworkDataSocketClass GNetworkDataSocketClass;


struct _GNetworkDataSocket
{
  /* <private> */
  GNetworkSocket parent;

  GNetworkDataSocketPrivate *_priv;
};

struct _GNetworkDataSocketClass
{
  /* <private> */
  GNetworkSocketClass parent_class;

  /* <public> */
  /* Methods */
  void (*send)	    (GNetworkDataSocket * socket,
		     GNetworkParams * params);

  /* <private> */
  void (*__gnetwork_padding1);
  void (*__gnetwork_padding2);
  void (*__gnetwork_padding3);
  void (*__gnetwork_padding4);

  /* <public> */
  /* Signals */
  void (*sent)	    (GNetworkDataSocket * socket,
		     GNetworkParams * params);
  void (*received)  (GNetworkDataSocket * socket,
		     GNetworkParams * params);

  /* <private> */
  void (*__gnetwork_padding5);
  void (*__gnetwork_padding6);
  void (*__gnetwork_padding7);
  void (*__gnetwork_padding8);
};


GType gnetwork_data_socket_get_type (void) G_GNUC_CONST;

void gnetwork_data_socket_send (GNetworkDataSocket * socket, GNetworkParams * params);

void gnetwork_data_socket_sent (GNetworkDataSocket * socket, GNetworkParams * params);
void gnetwork_data_socket_received (GNetworkDataSocket * socket, GNetworkParams * params);


GQuark gnetwork_data_socket_error_get_quark (void) G_GNUC_CONST;


G_END_DECLS

#endif /* !__GNETWORK_DATA_SOCKET_H__ */
