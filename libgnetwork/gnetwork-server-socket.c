/*
 * GNetwork Library: libgnetwork/gnetwork-server-socket.c
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnetwork-server-socket.h"

#include "gnetwork-type-builtins.h"
#include "gnetwork-utils.h"

#include <glib/gi18n.h>


#define GNETWORK_SERVER_SOCKET_GET_PRIVATE(obj)	(GNETWORK_SERVER_SOCKET (obj)->_priv)


/**
 * GNetworkServerSocket:
 * 
 * The structure for server socket object instances. This structures contains no
 * public members.
 * 
 * Since: 1.0
 **/

/**
 * GNetworkServerSocketClass:
 * @create_socket: the vtable entry for the "create_socket" method.
 * @new_socket: the C closure location for the "new-socket" signal.
 * 
 * The structure for the server socket object class. All signals and methods are
 * parent-relative, but may be %NULL.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_SERVER_SOCKET:
 * @object: the pointer to cast.
 * 
 * Casts the #GNetworkServerSocket derived pointer at @object into a
 * (GNetworkServerSocket*) pointer. Depending on the current debugging level,
 * this function may invoke certain runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_SERVER_SOCKET:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GNetworkServerSocket (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_SERVER_SOCKET_CLASS:
 * @klass: the pointer to cast.
 * 
 * Casts a the #GNetworkServerSocketClass derieved pointer at @klass into a
 * (GNetworkServerSocketClass*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_IS_SERVER_SOCKET_CLASS:
 * @klass: the pointer to check.
 * 
 * Checks whether @klass is a #GNetworkServerSocketClass (or derived) class.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_SERVER_SOCKET_GET_CLASS:
 * @object: the pointer to check.
 * 
 * Retrieves a pointer to the #GNetworkServerSocketClass for the
 * #GNetworkDataSocket (or derived) instance @object.
 * 
 * Since: 1.0
 **/

/**
 * GNETWORK_TYPE_SERVER_SOCKET:
 * 
 * The registered #GType of #GNetworkServerSocket.
 * 
 * Since: 1.0
 **/


/* ************** *
 *  Enumerations  *
 * ************** */
 
enum
{
  PROP_0,

  SOCKETS,
  MAX_SOCKETS,
  CLOSE_SOCKETS
};

enum
{
  NEW_SOCKET,

  LAST_SIGNAL
};


/* *************** *
 *  Private Types  *
 * *************** */

struct _GNetworkServerSocketPrivate
{
  GSList *sockets;
  guint max_sockets;
  gboolean close_sockets:1;
};


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer gnetwork_server_socket_parent_class = NULL;
static guint gnetwork_server_socket_signals[LAST_SIGNAL] = { 0 };


/* ********************** *
 *  Connection Callbacks  *
 * ********************** */

static void
socket_sent_cb (GNetworkDataSocket * socket, GNetworkParams * data, GNetworkObject * server)
{
  guint length = 0;

  gnetwork_params_get (data, "socket-data-size", &length, NULL);

  if (length == 0)
    return;

  gnetwork_object_lock (server);
  gnetwork_socket_add_to_bytes_sent (GNETWORK_SOCKET (server), length);
  gnetwork_object_unlock (server);
}


static void
socket_received_cb (GNetworkDataSocket * socket, GNetworkParams * data, GNetworkObject * server)
{
  gulong length = 0;

  gnetwork_params_get (data, "socket-data-size", &length, NULL);

  if (length == 0)
    return;

  gnetwork_object_lock (server);
  gnetwork_socket_add_to_bytes_received (GNETWORK_SOCKET (server), length);
  gnetwork_object_unlock (server);
}


static void
socket_notify_socket_state_cb (GObject * socket, GParamSpec * pspec, GNetworkObject * server)
{
  gint state;

  g_object_get (socket, "socket-state", &state, NULL);

  if (state == GNETWORK_SOCKET_STATE_CLOSED)
    {
      GNetworkServerSocketPrivate *priv = GNETWORK_SERVER_SOCKET_GET_PRIVATE (server);

      gnetwork_object_lock (server);
      priv->sockets = g_slist_remove (priv->sockets, socket);
      gnetwork_object_unlock (server);
      g_object_unref (socket);
    }
}


/* ******************************** *
 *  GNetworkServerSocket Functions  *
 * ******************************** */

GNetworkDataSocket *
gnetwork_server_socket_real_create_socket (GNetworkServerSocket * server, GNetworkParams * params)
{
  GNetworkDataSocket *socket = NULL;

  return socket;
}


static void
gnetwork_server_socket_real_new_socket (GNetworkServerSocket * server, GNetworkDataSocket * socket)
{
  server->_priv->sockets = g_slist_prepend (server->_priv->sockets, socket);

  g_signal_connect_object (socket, "sent", G_CALLBACK (socket_sent_cb), server, 0);
  g_signal_connect_object (socket, "received", G_CALLBACK (socket_received_cb), server, 0);
  g_signal_connect_object (socket, "notify::socket-state",
			   G_CALLBACK (socket_notify_socket_state_cb), server, 0);
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnetwork_server_socket_set_property (GObject * object, guint property, const GValue * value,
				     GParamSpec * pspec)
{
  GNetworkServerSocketPrivate *priv = GNETWORK_SERVER_SOCKET_GET_PRIVATE (object);

  switch (property)
    {
    case MAX_SOCKETS:
      priv->max_sockets = g_value_get_uint (value);
      break;
    case CLOSE_SOCKETS:
      priv->close_sockets = (g_value_get_boolean (value) != FALSE);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_server_socket_get_property (GObject * object, guint property, GValue * value,
				     GParamSpec * pspec)
{
  GNetworkServerSocketPrivate *priv = GNETWORK_SERVER_SOCKET_GET_PRIVATE (object);

  switch (property)
    {
    case MAX_SOCKETS:
      g_value_set_uint (value, priv->max_sockets);
      break;
    case SOCKETS:
      g_value_take_boxed (value, _gnetwork_slist_to_value_array (priv->sockets,
								 GNETWORK_TYPE_DATA_SOCKET));
      break;
    case CLOSE_SOCKETS:
      g_value_set_boolean (value, priv->close_sockets);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}


static void
gnetwork_server_socket_dispose (GObject * object)
{
  GNetworkServerSocketPrivate *priv = GNETWORK_SERVER_SOCKET_GET_PRIVATE (object);

  while (priv->sockets != NULL)
    {
      if (priv->close_sockets)
	{
	  gnetwork_socket_close (priv->sockets->data);
	}
      else
	{
	  g_signal_handlers_disconnect_by_func (priv->sockets->data, socket_notify_socket_state_cb,
						object);
	  g_signal_handlers_disconnect_by_func (priv->sockets->data, socket_sent_cb, object);
	  g_signal_handlers_disconnect_by_func (priv->sockets->data, socket_received_cb, object);
	  g_object_unref (priv->sockets->data);
	}

      priv->sockets = g_slist_remove_link (priv->sockets, priv->sockets);
    }

  if (G_OBJECT_CLASS (gnetwork_server_socket_parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (gnetwork_server_socket_parent_class)->finalize) (object);
}


static void
gnetwork_server_socket_finalize (GObject * object)
{
  GNetworkServerSocketPrivate *priv = GNETWORK_SERVER_SOCKET_GET_PRIVATE (object);

  g_free (priv);

  if (G_OBJECT_CLASS (gnetwork_server_socket_parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (gnetwork_server_socket_parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnetwork_server_socket_class_init (GNetworkServerSocketClass * class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  // GNetworkObjectClass *netobject_class = GNETWORK_OBJECT_CLASS (class);

  gnetwork_server_socket_parent_class = g_type_class_peek_parent (class);

  gobject_class->set_property = gnetwork_server_socket_set_property;
  gobject_class->get_property = gnetwork_server_socket_get_property;
  gobject_class->dispose = gnetwork_server_socket_dispose;
  gobject_class->finalize = gnetwork_server_socket_finalize;

  class->new_socket = gnetwork_server_socket_real_new_socket;
  class->create_socket = gnetwork_server_socket_real_create_socket;

  /* Signals */
/**
 * GNetworkServerSocket::new-socket:
 * @server: the object which emitted this signal.
 * @socket: the newly created but un-opened socket object.
 * 
 * This signal is emitted by socket objects when a new socket has been created
 * in response to an incoming connection request.
 * 
 * Since: 1.0
 **/
  gnetwork_server_socket_signals[NEW_SOCKET] =
    g_signal_new ("new-socket",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GNetworkServerSocketClass, new_socket),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__OBJECT,
		  G_TYPE_NONE, 1, GNETWORK_TYPE_DATA_SOCKET);

  /* Properties */
  g_object_class_install_property (gobject_class, SOCKETS,
				   g_param_spec_value_array ("sockets", _("Sockets"),
							     _("A value array containing the "
							       "incoming sockets created by this "
							       "server."),
							     g_param_spec_object
							     ("socket", _("Socket."),
							      _("An incoming socket object used by "
								"this server."),
							      GNETWORK_TYPE_DATA_SOCKET,
							      G_PARAM_READABLE),
							     G_PARAM_READABLE));
  g_object_class_install_property (gobject_class, MAX_SOCKETS,
				   g_param_spec_uint ("max-sockets", _("Maximum Sockets"),
						      _("The maximum number of incoming sockets "
							"which can be created by this object."),
						      0, G_MAXUINT, 0, G_PARAM_READWRITE));
  g_object_class_install_property (gobject_class, CLOSE_SOCKETS,
				   g_param_spec_boolean ("close-sockets", _("Close Sockets"),
							 _("Whether or not to close incoming "
							   "sockets when the server is closed."),
						      FALSE, G_PARAM_READWRITE));


  g_type_class_add_private (class, sizeof (GNetworkServerSocketPrivate));  
}


static void
gnetwork_server_socket_instance_init (GNetworkServerSocket * server)
{
  server->_priv = G_TYPE_INSTANCE_GET_PRIVATE (server, GNETWORK_TYPE_SERVER_SOCKET,
					       GNetworkServerSocketPrivate);

  server->_priv->close_sockets = FALSE;
  server->_priv->max_sockets = 0;
  server->_priv->sockets = NULL;
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

GType
gnetwork_server_socket_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      static const GTypeInfo info = {
	sizeof (GNetworkServerSocketClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gnetwork_server_socket_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,			/* class data */
	sizeof (GNetworkServerSocket),
	0,			/* number of pre-allocs */
	(GInstanceInitFunc) gnetwork_server_socket_instance_init,
	NULL			/* value table */
      };

      type = g_type_register_static (GNETWORK_TYPE_SOCKET, "GNetworkServerSocket", &info,
				     G_TYPE_FLAG_ABSTRACT);
    }

  return type;
}


/**
 * gnetwork_server_socket_create_socket:
 * @server: the socket object to use.
 * @params: the parameters to use to create a new socket.
 * 
 * This function calls the "create_socket" method for @server to create a new
 * socket using the parameters in @params. The returned reference must be
 * deleted with g_object_unref() when no longer needed.
 * 
 * Returns: the newly created socket object, or %NULL.
 * 
 * Since: 1.0
 **/
GNetworkDataSocket *
gnetwork_server_socket_create_socket (GNetworkServerSocket * server, GNetworkParams * params)
{
  GNetworkServerSocketClass *class;

  g_return_val_if_fail (GNETWORK_IS_SERVER_SOCKET (server), NULL);
  g_return_val_if_fail (GNETWORK_IS_PARAMS (params), NULL);

  class = GNETWORK_SERVER_SOCKET_GET_CLASS (server);

  if (class->create_socket == NULL)
    {
      g_warning ("%s: The socket type `%s' does not support the send() method.", G_STRLOC,
		 G_OBJECT_TYPE_NAME (server));
      return NULL;
    }

  return (*class->create_socket) (server, params);
}


/**
 * gnetwork_server_socket_new_socket:
 * @server: the socket object.
 * @socket: the newly created socket object.
 * 
 * Emits the "new-socket" signal for @socket. Applications typically will not
 * use this function, it exists for subclasses of the server object.
 * 
 * Since: 1.0
 **/
void
gnetwork_server_socket_new_socket (GNetworkServerSocket * server, GNetworkDataSocket * socket)
{
  g_return_if_fail (GNETWORK_IS_SERVER_SOCKET (server));
  g_return_if_fail (GNETWORK_IS_DATA_SOCKET (socket));

  g_signal_emit (server, gnetwork_server_socket_signals[NEW_SOCKET], 0, socket);
}
